﻿using FastMember;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.GTApplicationDB;
using System.Data.Entity;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Dapper;

namespace GT.InboundApplication.Model.Common
{
    public class ApplicationUserRetriever
    {
        private static ApplicationUserRetriever _instance;
        public static ApplicationUserRetriever Current
        {
            get { if (_instance == null) { _instance = new ApplicationUserRetriever(); return _instance; } else { return _instance; } }
        }

        public ApplicationUser GetApplicationUserByName(string username)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                return db.Users.Single(item => item.UserName == username);
            }
        }
    }
    
    public static class SolutionHelper
    {
        public static DataTable ToTable(this IList source)
        {
            if (source == null) throw new ArgumentNullException();
            var table = new DataTable();
            if (source.Count == 0) return table;

            // blatently assume the list is homogeneous
            Type itemType = source[0].GetType();
            table.TableName = itemType.Name;
            List<string> names = new List<string>();
            foreach (var prop in itemType.GetProperties())
            {
                if (prop.CanRead && prop.GetIndexParameters().Length == 0)
                {
                    names.Add(prop.Name);
                    table.Columns.Add(prop.Name);
                }
            }
            names.TrimExcess();
            var accessor = TypeAccessor.Create(itemType);
            object[] values = new object[names.Count];
            foreach (var row in source)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = accessor[row, names[i]];
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static decimal divide(decimal? dividend, decimal? divisor)
        {
            if (divisor == null || divisor == 0) return 0;
            if (dividend == null) dividend = 0;
            return dividend.Value / divisor.Value;
        }
        public static double divide(double? dividend, double? divisor)
        {
            if (divisor == null || divisor == 0) return 0;
            if (dividend == null) dividend = 0;
            return dividend.Value / divisor.Value;
        }
        public static double divide(int? dividend, int? divisor)
        {
            if (divisor == null || divisor == 0) return 0;
            if (dividend == null) dividend = 0;
            return dividend.Value * 1.0 / divisor.Value;
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            //ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            return destImage;
        }
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }

    public static class HelperExtension{
        public static object GetValueIfExist(this DataRow dr, string colname)
        {
            if (dr.Table.Columns.Contains(colname))
            {
                return dr[colname];
            }
            return null;
        }

        public static bool isValidPhoneNumber(this string phonenumber)
        {
            //var phonepattern = new System.Text.RegularExpressions.Regex(@"^0[689]\d \d{3} \d{4}$");
            //if (phonepattern.IsMatch(phonenumber) == false)
            //{
            //    return false;
            //}
            //if (phonenumber.Replace(" ", "").Substring(3).Distinct().Count() == 1)
            //{
            //    return false;
            //}
            if (phonenumber.IsNullOrEmpty()) return false;
            if (phonenumber.Trim().Length == 10 || phonenumber.Trim().Length == 9)
            {
                return true;
            }
            return false;
        }

        public static bool isValidHomeNumber(this string phonenumber)
        {
            var phonepattern = new System.Text.RegularExpressions.Regex(@"^0[23457]\d{7}$");
            if (phonepattern.IsMatch(phonenumber) == false)
            {
                return false;
            }
            return true;
        }

        public static string FormatPhonenumber(this string phonenumber)
        {
            if (phonenumber != null && phonenumber.Trim().Length == 10 || phonenumber.Trim().Length == 9)
            {
                return phonenumber.Trim().Insert(6, " ").Insert(3, " ");
            }
            return phonenumber;
        }

        public static string FormatExtension(this string extension)
        {
            if (!extension.IsNullOrEmpty())
            {
                return "ต่อ " + extension;
            }
            return "";
        }

        public static bool isCashPayment(this string paymentmethod)
        {
            return paymentmethod.Contains("BankTransfer") || paymentmethod.Contains("CounterService");
        }

        public static string ResponseLabel(this string ResponseCategory)
        {
            switch (ResponseCategory)
            {
                case "1": return "<span class=\"label label-info\">Follow up</span>";
                case "2": return "<span class=\"label label-success\">Success</span>";
                case "3": return "<span class=\"label label-default\">Not Target</span>";
                case "4": return "<span class=\"label label-default\">Reject</span>";
                case "5": return "<span class=\"label label-white\">No Contact</span>";
                case "6": return "<span class=\"label label-default\">Not Update</span>";
                case "7": return "<span class=\"label label-success\">Success-Online</span>";
                case "8": return "<span class=\"label label-default\">Duplicate</span>";
                case "9": return "<span class=\"label label-default\">Refer</span>";
                case "10": return "<span class=\"label label-default\">Unsuccess</span>";
                case "11": return "<span class=\"label label-default\">Letter</span>";
                default: return ResponseCategory;
            }
        }

        public static string PriorityLabel(this int? Priority)
        {
            switch (Priority)
            {
                case 1: return "<span class=\"label label-damnhot\">ลีดร้อนโทรด่วน</span><span class=\"fire-icon\"></span>";
                case 2: return "<span class=\"label label-checkout\">อีกนิดปิดจ๊อบ</span>";
                case 3: return "<span class=\"label label-fresh\">ฮอตแค่ 24 ชม.</span>";
                case 5: return "<span class=\"label label-lastchance\">โทรก่อนหลุดมือ</span>";
                case 4: case 6: case 7 : return "<span class=\"label label-white\">Call Out</span>";
                default: return "<span class=\"label label-white\">"+Priority+"</span>";
            }
        }

        public static DataTable AddSummaryRow(this DataTable dt, string totalLabel = "")
        {
            int i;
            var columns = dt.Columns.Cast<DataColumn>();

            var rows = dt.AsEnumerable();
            var colsToSum = columns.Where(c => (c.DataType == typeof(int) || (c.DataType == typeof(string)
                    && rows.All(r => int.TryParse(r.Field<string>(c), out i)))));
            var columnsToSum = new HashSet<DataColumn>(colsToSum);

            DataRow row = dt.NewRow();
            row.SetField(0, totalLabel);
            foreach (var col in columns)
            {
                if (columnsToSum.Contains(col))
                {
                    int sum;
                    if (col.DataType == typeof(int))
                        sum = rows.Sum(r => r.Field<int>(col));
                    else
                        sum = rows.Sum(r => int.Parse(r.Field<string>(col)));
                    row.SetField(col.ColumnName, sum);
                }
            }
            dt.Rows.Add(row);
            return dt;
        }

        public static IEnumerable<T> QueryAsModel<T>(this DbContext context, string query, object parameters = null)
        {
            return context.Database.Connection.Query<T>(query, parameters);
        }

        public static bool IsWeekend(this DateTime d)
        {
            return d.DayOfWeek == DayOfWeek.Sunday || d.DayOfWeek == DayOfWeek.Saturday;
        }
    }

    public static class EnumExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum value)
            where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name).GetCustomAttribute<TAttribute>();
        }
    }
}
