﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.Common
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string DataUserName { get; set; }
        public string DataPassword { get; set; }
        public bool IsBlocked { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        [NotMapped]
        public string FullName { get{ return FirstName + " " + LastName;}}
        public string Department { get; set; }
        public string LicenseNumber { get; set; }
        public string TSRCode { get; set; }
        public string TSRCode_OL { get; set; }
        public string LicenseNumber_OL { get; set; }
        public bool IsAgency { get; set; }
        public bool IsVerified { get; set; }
        public string SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public string ExtensionCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? EnablePilotFeature { get; set; }
        public DateTime? AccessTimeStamp { get; set; }

        [NotMapped]
        public string Project { get; set; }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("IGTCRMContext")
        {
        }

        public IQueryable<ApplicationUser> ActiveUsers()
        {
            return this.Users.Where(c => c.IsBlocked == false && c.IsVerified == true && c.Roles.Any());
        }

        public IQueryable GetAdminSelectList()
        {
            var supRoles = this.Roles.Where(c => new[] { "PM" }.Contains(c.Name)).Select(s => s.Id).ToList();
            return from row in this.ActiveUsers().Where(c => c.Roles.Any(nc => supRoles.Contains(nc.RoleId)))
                select new { Id = row.Id, Name= row.FirstName + " " + row.LastName };
        }

        public IQueryable<ApplicationUser> ActiveTSRs()
        {
            var tsrRoles = this.Roles.Where(c=>new[]{"BOTH","NL","OL","TSR"}.Contains(c.Name)).Select(s=>s.Id).ToList();
            return this.ActiveUsers().Where(c => c.Roles.Any(r=>tsrRoles.Contains(r.RoleId)));
        }

        public IQueryable<ApplicationUser> AllTSRs()
        {
            var tsrRoles = this.Roles.Where(c => new[] { "BOTH", "NL", "OL", "TSR" }.Contains(c.Name)).Select(s => s.Id).ToList();
            return this.Users.Where(c => c.IsVerified == true && c.Roles.Any(r => tsrRoles.Contains(r.RoleId)));
        }

        public IQueryable<ApplicationUser> GetSupervisorList()
        {
            return from row in this.ActiveUsers().Where(c => c.Roles.Any(nc => nc.RoleId == "7"))
                   select row;
        }

        public IQueryable<ActiveUserModel.ActiveUserClass> GetSupervisorSelectList()
        {
            return from row in GetSupervisorList()
                   select new ActiveUserModel.ActiveUserClass() { CODE = row.Id, NAME = row.FirstName + " " + row.LastName };
        }

        public IQueryable<ApplicationUser> GetTeamMember(string supervisorId)
        {
            return this.ActiveTSRs().Where(c => c.SupervisorId == supervisorId);            
        }

        public ApplicationUser GetSupervisor(string userid)
        {
            return this.Users.Where(c=>c.Id == this.Users.Where(d => d.Id == userid).FirstOrDefault().SupervisorId).First();
        }

        public DateTime UpdateAccessTime(string userid)
        {
            if (!string.IsNullOrEmpty(userid))
            {
                var user = this.Users.Find(userid);
                user.AccessTimeStamp = DateTime.Now;
                this.SaveChanges();
                return user.AccessTimeStamp.Value;
            }
            return DateTime.Now;
        }

    }


}