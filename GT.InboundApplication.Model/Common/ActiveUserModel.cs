﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;


namespace GT.InboundApplication.Model.Common
{
    public class ActiveUserModel
    {
        public class ActiveUserClass
        {
            public string NAME { get; set; }
            public string CODE { get; set; }
            public string ROLENAME { get; set; }
            public string ROLEID { get; set; }
            public string TSRCODE { get; set; }
            public string SUPERVISERID { get; set; }
        }

        public List<ActiveUserClass> _ListActiveUsers(string connectionString)
        {
            // Note : Bind Active User.
            List<ActiveUserClass> users = new List<ActiveUserClass>();
            Dictionary<string, string> param = new Dictionary<string, string>();
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_LIST_ACTIVE_USER", param);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                ActiveUserClass user = new ActiveUserClass();
                user.FillByDataRow(dr);
                users.Add(user);
            }
            return users;
        }

        public List<ActiveUserClass> _ListActiveTSR(string connectionString, bool IsNonLifeProduct)
        {
            // Note : Bind Active User.
            List<ActiveUserClass> users = new List<ActiveUserClass>();
            Dictionary<string, string> param = new Dictionary<string, string>();
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_LIST_ACTIVE_TSR", param);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                ActiveUserClass user = new ActiveUserClass();
                user.FillByDataRow(dr);
                if(user.ROLENAME == "BOTH")
                {
                    users.Add(user); continue;
                }
                else if(user.ROLENAME == "NL" && IsNonLifeProduct)
                {
                    users.Add(user);
                }
                else if(user.ROLENAME == "OL" && !IsNonLifeProduct)
                {
                    users.Add(user);
                }
            }
            return users.OrderBy(o=>o.NAME).ToList();
        }
    }
}