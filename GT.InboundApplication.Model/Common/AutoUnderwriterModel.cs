﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.Common
{
    public class AutoUnderwriterModel
    {
        public enum ValidateResult { Pass, NotPass, Error, Refer }
        public class PayloadOutput
        {
            public int TotalApp { get; set; }
            public int TotalPass { get; set; }
            public int TotalFail { get; set; }
            public int TotalRefer { get; set; }
            public List<ApplicationOutput> AppResults { get; set; }
        }

        public class ApplicationOutput
        {
            public string ApplicationNo { get; set; }
            public bool IsPassed { get; set; }
            [JsonConverter(typeof(StringEnumConverter))]
            public ValidateResult ValidateResult { get; set; }
            public string ErrorMessage { get; set; }
            public string PreApprovalCode { get; set; }
            public DateTime ResponseDate { get; set; }
            public IEnumerable<ValidationResultItem> validationResultItems { get; set; }
        }

        public class PayloadItem
        {
            public string Sender { get; set; }
            public string TimeStamp { get; set; }
            public ApplicationFormItem App { get; set; }
        }

        public class ApplicationFormItem
        {
            public long Id { get; set; }
            public string ApplicationNo { get; set; }
            public DateTime AppDate { get; set; }
            public DateTime InceptionDate { get; set; }
            public ProductItem Product { get; set; }
            public InsuredItem Insured { get; set; }
            public PayerItem Payer { get; set; }
            public PaymentItem Payment { get; set; }
            public FATCAItem FATCA { get; set; }
            public SpouseItem Spouse { get; set; }
            public QuestionaireItem Questionaire { get; set; }
            public ConsentItem Consent { get; set; }
            public SellerItem Seller { get; set; }
            public List<BeneficiaryItem> Beneficiaries { get; set; }
            public List<AddressItem> Addresses { get; set; }
            public bool IsPassValidation { get; set; }
            public string PreApprovalCode { get; set; }
            public string ValidationResultMessage { get; set; }
            public List<ValidationResultItem> ValidationResults { get; set; }

            public override bool Equals(object obj)
            {
                var item = obj as ApplicationFormItem;
                return item != null &&
                       ApplicationNo == item.ApplicationNo &&
                       EqualityComparer<ProductItem>.Default.Equals(Product, item.Product) &&
                       EqualityComparer<InsuredItem>.Default.Equals(Insured, item.Insured) &&
                       EqualityComparer<PayerItem>.Default.Equals(Payer, item.Payer) &&
                       EqualityComparer<PaymentItem>.Default.Equals(Payment, item.Payment) &&
                       EqualityComparer<FATCAItem>.Default.Equals(FATCA, item.FATCA) &&
                       EqualityComparer<SpouseItem>.Default.Equals(Spouse, item.Spouse) &&
                       EqualityComparer<QuestionaireItem>.Default.Equals(Questionaire, item.Questionaire) &&
                       //EqualityComparer<ConsentItem>.Default.Equals(Consent, item.Consent) &&
                       EqualityComparer<SellerItem>.Default.Equals(Seller, item.Seller) &&
                       Beneficiaries.SequenceEqual(item.Beneficiaries) &&
                       Addresses.SequenceEqual(item.Addresses)
                       //EqualityComparer<List<BeneficiaryItem>>.Default.Equals(Beneficiaries, item.Beneficiaries) &&
                       //EqualityComparer<List<AddressItem>>.Default.Equals(Addresses, item.Addresses)
                       ;
            }

            public override int GetHashCode()
            {
                var hashCode = -1624300754;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ApplicationNo);
                hashCode = hashCode * -1521134295 + EqualityComparer<ProductItem>.Default.GetHashCode(Product);
                hashCode = hashCode * -1521134295 + EqualityComparer<InsuredItem>.Default.GetHashCode(Insured);
                hashCode = hashCode * -1521134295 + EqualityComparer<PayerItem>.Default.GetHashCode(Payer);
                hashCode = hashCode * -1521134295 + EqualityComparer<PaymentItem>.Default.GetHashCode(Payment);
                hashCode = hashCode * -1521134295 + EqualityComparer<FATCAItem>.Default.GetHashCode(FATCA);
                hashCode = hashCode * -1521134295 + EqualityComparer<SpouseItem>.Default.GetHashCode(Spouse);
                hashCode = hashCode * -1521134295 + EqualityComparer<QuestionaireItem>.Default.GetHashCode(Questionaire);
                hashCode = hashCode * -1521134295 + EqualityComparer<ConsentItem>.Default.GetHashCode(Consent);
                hashCode = hashCode * -1521134295 + EqualityComparer<SellerItem>.Default.GetHashCode(Seller);
                hashCode = hashCode * -1521134295 + EqualityComparer<List<BeneficiaryItem>>.Default.GetHashCode(Beneficiaries);
                hashCode = hashCode * -1521134295 + EqualityComparer<List<AddressItem>>.Default.GetHashCode(Addresses);
                return hashCode;
            }
        }
        public class ProductItem
        {
            public long Id { get; set; }
            public bool IsLifeProduct { get; set; }
            public string ProductName { get; set; }
            public string PlanCode { get; set; }
            public string SumInsured { get; set; }
            public string ProductCode { get; set; }

            public override bool Equals(object obj)
            {
                var item = obj as ProductItem;
                return item != null &&
                       Id == item.Id &&
                       IsLifeProduct == item.IsLifeProduct &&
                       ProductName == item.ProductName &&
                       PlanCode == item.PlanCode &&
                       SumInsured == item.SumInsured &&
                       ProductCode == item.ProductCode;
            }

            public override int GetHashCode()
            {
                var hashCode = -835685617;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + IsLifeProduct.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ProductName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PlanCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SumInsured);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ProductCode);
                return hashCode;
            }
        }
        public class PaymentItem : IEquatable<PaymentItem>
        {
            public long Id { get; set; }
            public string PaymentMethod { get; set; }
            public string PaymentMode { get; set; }
            public string PayPlan { get; set; }
            public DateTime TransactionDate { get; set; }
            public string ApprovalCode { get; set; }
            public string CashBankName { get; set; }
            public string CashBranchName { get; set; }
            public decimal Premium { get; set; }
            public decimal RenewalPremium { get; set; }
            public string CreditCardNo { get; set; }
            public string CreditCardClassType { get; set; }
            public string CreditCardType { get; set; }
            public string CreditHolderName { get; set; }
            public string CreditCardBankName { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as PaymentItem);
            }

            public bool Equals(PaymentItem item)
            {
                return item != null &&
                       Id == item.Id &&
                       PaymentMethod == item.PaymentMethod &&
                       PaymentMode == item.PaymentMode &&
                       PayPlan == item.PayPlan &&
                       ApprovalCode == item.ApprovalCode &&
                       CashBankName == item.CashBankName &&
                       CashBranchName == item.CashBranchName &&
                       Premium == item.Premium &&
                       RenewalPremium == item.RenewalPremium &&
                       CreditCardNo == item.CreditCardNo &&
                       CreditCardClassType == item.CreditCardClassType &&
                       CreditCardType == item.CreditCardType &&
                       CreditHolderName == item.CreditHolderName &&
                       CreditCardBankName == item.CreditCardBankName;
            }

            public override int GetHashCode()
            {
                var hashCode = 1561326074;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PaymentMethod);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PaymentMode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PayPlan);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ApprovalCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CashBankName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CashBranchName);
                hashCode = hashCode * -1521134295 + Premium.GetHashCode();
                hashCode = hashCode * -1521134295 + RenewalPremium.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CreditCardNo);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CreditCardClassType);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CreditCardType);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CreditHolderName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CreditCardBankName);
                return hashCode;
            }
        }
        public class ValidationResultItem
        {
            public long Id { get; set; }
            public string ConditionName { get; set; }
            public bool IsPassed { get; set; }
            public string ValidateResult { get; set; }
            public string Message { get; set; }
            public string InputValue { get; set; }

            public ValidationResultItem(bool IsPassed,
                   string conditionName,
                   string message,
                   string inputValue)
            {
                this.IsPassed = IsPassed;
                ConditionName = conditionName;
                Message = message;
                InputValue = inputValue;
            }
        }
        public class SellerItem
        {
            public long Id { get; set; }
            public string TSRCode { get; set; }
            public string TSRName { get; set; }
            public string PartnerName { get; set; }
            public string SupervisorName { get; set; }
            public string TSRLicenseNo { get; set; }

            public override bool Equals(object obj)
            {
                var item = obj as SellerItem;
                return item != null &&
                       Id == item.Id &&
                       TSRCode == item.TSRCode &&
                       TSRName == item.TSRName &&
                       PartnerName == item.PartnerName &&
                       SupervisorName == item.SupervisorName &&
                       TSRLicenseNo == item.TSRLicenseNo;
            }

            public override int GetHashCode()
            {
                var hashCode = 338724492;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TSRCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TSRName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PartnerName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SupervisorName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TSRLicenseNo);
                return hashCode;
            }
        }
        public class InsuredItem
        {
            public long Id { get; set; }

            public string Salutation { get; set; }
            public string SalutationCode { get; set; }

            public string FirstName { get; set; }
            public string LastName { get; set; }

            public int Age { get; set; }
            public DateTime BirthDate { get; set; }

            public string Gender { get; set; }
            public string GenderCode { get; set; }

            public string CitizenID { get; set; }
            public DateTime CitizenCardExpiryDate { get; set; }
            public string PassportNo { get; set; }

            public string Nationality { get; set; }
            public string NationalityCode { get; set; }

            public decimal Height { get; set; }
            public decimal Weight { get; set; }

            public int AnnualIncome { get; set; }

            public string ClientStatus { get; set; }
            public string ClientStatusCode { get; set; }

            public string OccupationClass { get; set; }
            public string OccupationClassCode { get; set; }

            public string Occupation { get; set; }
            public string OccupationCode { get; set; }

            public string OccupationDetail { get; set; }

            public string Position { get; set; }

            public string HomePhone { get; set; }
            public string OfficePhone { get; set; }
            public string MobilePhone { get; set; }
            public string EmailAddress { get; set; }

            public override bool Equals(object obj)
            {
                var item = obj as InsuredItem;
                return item != null &&
                       Id == item.Id &&
                       Salutation == item.Salutation &&
                       SalutationCode == item.SalutationCode &&
                       FirstName == item.FirstName &&
                       LastName == item.LastName &&
                       Age == item.Age &&
                       BirthDate == item.BirthDate &&
                       Gender == item.Gender &&
                       GenderCode == item.GenderCode &&
                       CitizenID == item.CitizenID &&
                       CitizenCardExpiryDate == item.CitizenCardExpiryDate &&
                       PassportNo == item.PassportNo &&
                       Nationality == item.Nationality &&
                       NationalityCode == item.NationalityCode &&
                       Height == item.Height &&
                       Weight == item.Weight &&
                       AnnualIncome == item.AnnualIncome &&
                       ClientStatus == item.ClientStatus &&
                       ClientStatusCode == item.ClientStatusCode &&
                       OccupationClass == item.OccupationClass &&
                       OccupationClassCode == item.OccupationClassCode &&
                       Occupation == item.Occupation &&
                       OccupationCode == item.OccupationCode &&
                       OccupationDetail == item.OccupationDetail &&
                       Position == item.Position &&
                       HomePhone == item.HomePhone &&
                       OfficePhone == item.OfficePhone &&
                       MobilePhone == item.MobilePhone &&
                       EmailAddress == item.EmailAddress;
            }

            public override int GetHashCode()
            {
                var hashCode = -874531656;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Salutation);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SalutationCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
                hashCode = hashCode * -1521134295 + Age.GetHashCode();
                hashCode = hashCode * -1521134295 + BirthDate.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Gender);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(GenderCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CitizenID);
                hashCode = hashCode * -1521134295 + CitizenCardExpiryDate.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PassportNo);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nationality);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NationalityCode);
                hashCode = hashCode * -1521134295 + Height.GetHashCode();
                hashCode = hashCode * -1521134295 + Weight.GetHashCode();
                hashCode = hashCode * -1521134295 + AnnualIncome.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ClientStatus);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ClientStatusCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationClass);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationClassCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Occupation);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationDetail);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Position);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(HomePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OfficePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MobilePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EmailAddress);
                return hashCode;
            }
        }
        public class SpouseItem : IEquatable<SpouseItem>
        {
            public long Id { get; set; }
            public string FullName { get; set; }
            public string Nationality { get; set; }
            public string NationalityCode { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as SpouseItem);
            }

            public bool Equals(SpouseItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       FullName == other.FullName &&
                       Nationality == other.Nationality &&
                       NationalityCode == other.NationalityCode;
            }

            public override int GetHashCode()
            {
                var hashCode = -557146920;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FullName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nationality);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NationalityCode);
                return hashCode;
            }
        }
        public class PayerItem : IEquatable<PayerItem>
        {
            public long Id { get; set; }
            public string Salutation { get; set; }
            public string SalutationCode { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }
            public string Gender { get; set; }
            public string GenderCode { get; set; }

            public DateTime BirthDate { get; set; }

            public DateTime CitizenCardExpiryDate { get; set; }
            public string CitizenID { get; set; }

            public string PassportNo { get; set; }
            public string Nationality { get; set; }
            public string NationalityCode { get; set; }

            public string ClientStatus { get; set; }
            public string ClientStatusCode { get; set; }

            public string OccupationClass { get; set; }
            public string OccupationClassCode { get; set; }

            public string Occupation { get; set; }
            public string OccupationCode { get; set; }

            public string Position { get; set; }
            public string InsuredRelationShip { get; set; }

            public string HomePhone { get; set; }
            public string OfficePhone { get; set; }
            public string MobilePhone { get; set; }
            public string EmailAddress { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as PayerItem);
            }

            public bool Equals(PayerItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       Salutation == other.Salutation &&
                       SalutationCode == other.SalutationCode &&
                       FirstName == other.FirstName &&
                       LastName == other.LastName &&
                       Age == other.Age &&
                       Gender == other.Gender &&
                       GenderCode == other.GenderCode &&
                       BirthDate == other.BirthDate &&
                       CitizenCardExpiryDate == other.CitizenCardExpiryDate &&
                       CitizenID == other.CitizenID &&
                       PassportNo == other.PassportNo &&
                       Nationality == other.Nationality &&
                       NationalityCode == other.NationalityCode &&
                       ClientStatus == other.ClientStatus &&
                       ClientStatusCode == other.ClientStatusCode &&
                       OccupationClass == other.OccupationClass &&
                       OccupationClassCode == other.OccupationClassCode &&
                       Occupation == other.Occupation &&
                       OccupationCode == other.OccupationCode &&
                       Position == other.Position &&
                       InsuredRelationShip == other.InsuredRelationShip &&
                       HomePhone == other.HomePhone &&
                       OfficePhone == other.OfficePhone &&
                       MobilePhone == other.MobilePhone &&
                       EmailAddress == other.EmailAddress;
            }

            public override int GetHashCode()
            {
                var hashCode = -1207697788;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Salutation);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SalutationCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
                hashCode = hashCode * -1521134295 + Age.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Gender);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(GenderCode);
                hashCode = hashCode * -1521134295 + BirthDate.GetHashCode();
                hashCode = hashCode * -1521134295 + CitizenCardExpiryDate.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CitizenID);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PassportNo);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nationality);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NationalityCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ClientStatus);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ClientStatusCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationClass);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationClassCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Occupation);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OccupationCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Position);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(InsuredRelationShip);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(HomePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OfficePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MobilePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EmailAddress);
                return hashCode;
            }
        }
        public class BeneficiaryItem : IEquatable<BeneficiaryItem>
        {
            public long Id { get; set; }
            public string FullName { get; set; }
            public int Age { get; set; }
            public string InsuredRelationShip { get; set; }
            public int BenefitPercent { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as BeneficiaryItem);
            }

            public bool Equals(BeneficiaryItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       FullName == other.FullName &&
                       Age == other.Age &&
                       InsuredRelationShip == other.InsuredRelationShip &&
                       BenefitPercent == other.BenefitPercent;
            }

            public override int GetHashCode()
            {
                var hashCode = -875508029;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FullName);
                hashCode = hashCode * -1521134295 + Age.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(InsuredRelationShip);
                hashCode = hashCode * -1521134295 + BenefitPercent.GetHashCode();
                return hashCode;
            }
        }
        public class AddressItem : IEquatable<AddressItem>
        {
            public long Id { get; set; }
            public string Category { get; set; }
            public string AddressType { get; set; }
            public bool IsContactAddress { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Province { get; set; }
            public string District { get; set; }
            public string SubDistrict { get; set; }
            public string PostCode { get; set; }
            public string HomePhone { get; set; }
            public string HomeExt { get; set; }
            public string Mobile1 { get; set; }
            public string Mobile2 { get; set; }
            public string OfficePhone { get; set; }
            public string OfficeExt { get; set; }
            public string Email { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as AddressItem);
            }

            public bool Equals(AddressItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       Category == other.Category &&
                       AddressType == other.AddressType &&
                       IsContactAddress == other.IsContactAddress &&
                       Address1 == other.Address1 &&
                       Address2 == other.Address2 &&
                       Province == other.Province &&
                       District == other.District &&
                       SubDistrict == other.SubDistrict &&
                       PostCode == other.PostCode &&
                       HomePhone == other.HomePhone &&
                       HomeExt == other.HomeExt &&
                       Mobile1 == other.Mobile1 &&
                       Mobile2 == other.Mobile2 &&
                       OfficePhone == other.OfficePhone &&
                       OfficeExt == other.OfficeExt &&
                       Email == other.Email;
            }

            public override int GetHashCode()
            {
                var hashCode = -149944555;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Category);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AddressType);
                hashCode = hashCode * -1521134295 + IsContactAddress.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address1);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address2);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Province);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(District);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SubDistrict);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PostCode);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(HomePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(HomeExt);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Mobile1);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Mobile2);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OfficePhone);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OfficeExt);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Email);
                return hashCode;
            }
        }
        public class FATCAItem : IEquatable<FATCAItem>
        {
            public long Id { get; set; }
            public string USIndicia { get; set; }
            public string AnswerFATCA01 { get; set; }
            public string AnswerFATCA01_1 { get; set; }
            public string AnswerFATCA02 { get; set; }
            public string AnswerFATCA03 { get; set; }
            public string AnswerFATCA04 { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as FATCAItem);
            }

            public bool Equals(FATCAItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       USIndicia == other.USIndicia &&
                       AnswerFATCA01 == other.AnswerFATCA01 &&
                       AnswerFATCA01_1 == other.AnswerFATCA01_1 &&
                       AnswerFATCA02 == other.AnswerFATCA02 &&
                       AnswerFATCA03 == other.AnswerFATCA03 &&
                       AnswerFATCA04 == other.AnswerFATCA04;
            }

            public override int GetHashCode()
            {
                var hashCode = 1664583919;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(USIndicia);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AnswerFATCA01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AnswerFATCA01_1);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AnswerFATCA02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AnswerFATCA03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AnswerFATCA04);
                return hashCode;
            }
        }
        public class ConsentItem
        {
            public long Id { get; set; }
            public string ConsentFlag { get; set; }
            public string ConsentType { get; set; }
            public string ConsentDataSource { get; set; }
            public string ConsentDate { get; set; }
            public string ConsentTime { get; set; }
            public string ConsentVersion { get; set; }
        }
        public class ConsentRevenueItem
        {
            public long Id { get; set; }
            public string ConsentRevenueFlag { get; set; }
            public string ConsentRevenueVersion { get; set; }
        }
        public class QuestionaireItem : IEquatable<QuestionaireItem>
        {
            public long Id { get; set; }

            public string Category { get; set; }

            public string Answer01 { get; set; }

            public string Answer01_Rem01 { get; set; }
            public string Answer01_Rem02 { get; set; }
            public string Answer01_Rem03 { get; set; }

            public string Answer01_01 { get; set; }
            public string Answer01_02 { get; set; }
            public string Answer01_03 { get; set; }
            public string Answer01_04 { get; set; }

            public string Answer02 { get; set; }

            public string Answer02_Rem01 { get; set; }
            public string Answer02_Rem02 { get; set; }
            public string Answer02_Rem03 { get; set; }

            public string Answer02_01 { get; set; }
            public string Answer02_02 { get; set; }
            public string Answer02_03 { get; set; }
            public string Answer02_04 { get; set; }

            public string Answer03 { get; set; }
            public string Answer03_ChangeDirection { get; set; }

            public string Answer03_Rem01 { get; set; }
            public string Answer03_Rem02 { get; set; }

            public string Answer03_01 { get; set; }
            public string Answer03_02 { get; set; }
            public string Answer03_03 { get; set; }
            public string Answer03_04 { get; set; }

            public string Answer04 { get; set; }

            public string Answer04_Rem01 { get; set; }
            public string Answer04_Rem02 { get; set; }
            public string Answer04_Rem03 { get; set; }
            public string Answer04_Rem04 { get; set; }

            public string Answer04_01 { get; set; }
            public string Answer04_02 { get; set; }

            public string Answer05 { get; set; }

            public string Answer05_01 { get; set; }
            public string Answer05_02 { get; set; }
            public string Answer05_03 { get; set; }

            public string Answer06 { get; set; }

            public string Answer06_01 { get; set; }

            public string Answer07 { get; set; }

            public string Answer07_Rem01 { get; set; }
            public string Answer07_Rem02 { get; set; }
            public string Answer07_Rem03 { get; set; }
            public string Answer07_Rem04 { get; set; }

            public string Answer07_01 { get; set; }
            public string Answer07_02 { get; set; }
            public string Answer07_03 { get; set; }
            public string Answer07_04 { get; set; }

            public override bool Equals(object obj)
            {
                return Equals(obj as QuestionaireItem);
            }

            public bool Equals(QuestionaireItem other)
            {
                return other != null &&
                       Id == other.Id &&
                       Category == other.Category &&
                       Answer01 == other.Answer01 &&
                       Answer01_Rem01 == other.Answer01_Rem01 &&
                       Answer01_Rem02 == other.Answer01_Rem02 &&
                       Answer01_Rem03 == other.Answer01_Rem03 &&
                       Answer02 == other.Answer02 &&
                       Answer02_Rem01 == other.Answer02_Rem01 &&
                       Answer02_Rem02 == other.Answer02_Rem02 &&
                       Answer02_Rem03 == other.Answer02_Rem03 &&
                       Answer02_01 == other.Answer02_01 &&
                       Answer02_02 == other.Answer02_02 &&
                       Answer02_03 == other.Answer02_03 &&
                       Answer03 == other.Answer03 &&
                       Answer03_ChangeDirection == other.Answer03_ChangeDirection &&
                       Answer03_Rem01 == other.Answer03_Rem01 &&
                       Answer03_Rem02 == other.Answer03_Rem02 &&
                       Answer03_01 == other.Answer03_01 &&
                       Answer03_02 == other.Answer03_02 &&
                       Answer03_03 == other.Answer03_03 &&
                       Answer03_04 == other.Answer03_04 &&
                       Answer04 == other.Answer04 &&
                       Answer04_Rem01 == other.Answer04_Rem01 &&
                       Answer04_Rem02 == other.Answer04_Rem02 &&
                       Answer04_Rem03 == other.Answer04_Rem03 &&
                       Answer04_Rem04 == other.Answer04_Rem04 &&
                       Answer04_01 == other.Answer04_01 &&
                       Answer05 == other.Answer05 &&
                       Answer05_01 == other.Answer05_01 &&
                       Answer05_02 == other.Answer05_02 &&
                       Answer05_03 == other.Answer05_03 &&
                       Answer06 == other.Answer06 &&
                       Answer06_01 == other.Answer06_01 &&
                       Answer07 == other.Answer07 &&
                       Answer07_Rem01 == other.Answer07_Rem01 &&
                       Answer07_Rem02 == other.Answer07_Rem02 &&
                       Answer07_Rem03 == other.Answer07_Rem03 &&
                       Answer07_Rem04 == other.Answer07_Rem04 &&
                       Answer07_01 == other.Answer07_01 &&
                       Answer07_02 == other.Answer07_02 &&
                       Answer07_03 == other.Answer07_03 &&
                       Answer07_04 == other.Answer07_04;
            }

            public override int GetHashCode()
            {
                var hashCode = 149265991;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Category);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer01_Rem01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer01_Rem02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer01_Rem03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_Rem01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_Rem02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_Rem03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer02_03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_ChangeDirection);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_Rem01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_Rem02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer03_04);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04_Rem01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04_Rem02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04_Rem03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04_Rem04);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer04_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer05);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer05_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer05_02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer05_03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer06);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer06_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_Rem01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_Rem02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_Rem03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_Rem04);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_01);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_02);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_03);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Answer07_04);
                return hashCode;
            }
        }

        public class PreAUWInputModel
        {
            public string Product { get; set; }
            public string Person { get; set; }

            public PreAUWInputModel(string product, string person)
            {
                Product = product;
                Person = person;
            }
        }
    }
}
