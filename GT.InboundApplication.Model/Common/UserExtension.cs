﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace GT.InboundApplication.Model.Common
{
    public static class UserExtension
    {
        public static bool IsSuperAdmin(this IPrincipal principal)
        {
            return principal.Identity.GetUserName().ToUpper() == "ADMINISTRATOR";
        }

        public static bool IsManager(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("PM","OWNER", "ADMIN", "CO-ADMIN", "DM", "SYSTEMADMIN", "INBOUNDHEAD");
        }

        public static bool IsAdmin(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("PM", "OWNER", "ADMIN", "CO-ADMIN", "DM", "SYSTEMADMIN", "INBOUNDHEAD", "SUPERVISOR", "PM");
        }

        public static bool IsDM(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("DM");
        }

        public static bool IsSaleAdmin(this IPrincipal principal)
        {
            return principal.IsInRole("ADMIN");
        }

        public static bool IsQC(this IPrincipal principal)
        {
            return principal.IsInRole("QC");
        }
        public static bool IsSupervisor(this IPrincipal principal)
        {
            return principal.IsInRole("SUPERVISOR");
        }
        public static bool IsSimpleSource(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("OL", "NL", "BOTH");
        }

        public static bool IsTSR(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("OL", "NL", "BOTH","TSR");
        }

        public static bool IsHybridTSR(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("BOTH", "SUPERVISOR", "PM",  "INBOUNDHEAD");
        }
        
        public static bool IsPM(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("PM");
        }

        public static bool IsInboundHead(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("INBOUNDHEAD");
        }

        public static bool IsSaleTeam(this IPrincipal principal)
        {
            return principal.IsInAnyRoles("OL", "NL", "BOTH", "TSR", "SUPERVISOR", "ADMIN", "OWNER", "PM", "SYSTEMADMIN", "INBOUNDHEAD");
        }

        public static bool IsUW(this IPrincipal principal)
        {
            return principal.IsInRole("UW");
        }

        public static bool IsInAllRoles(this IPrincipal principal, params string[] roles)
        {
            return roles.All(r => principal.IsInRole(r));
        }

        public static bool IsInAnyRoles(this IPrincipal principal, params string[] roles)
        {
            return roles.Any(r => principal.IsInRole(r));
        }

        public static string GetExtension(this IIdentity identity)
        {
            var userId = identity.GetUserId();
            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Id == userId);
                return user.ExtensionCode;
            }
        }

        public static bool isUnder(this IPrincipal principal,string role){
            if (principal.IsInboundHead())
            {
                if (role == "SYSTEMADMIN") return true;
            }
            if(principal.IsPM()){
                if (role == "INBOUNDHEAD" || role == "SYSTEMADMIN") return true;
            }
            return false;
        }
    }
}
