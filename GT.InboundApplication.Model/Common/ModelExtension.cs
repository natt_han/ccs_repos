﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.Common
{
    public static class ModelExtension
    {
        public static void FillInModelWithDefaultValue<T>(this T model)
        {
            if (model == null) return;
            var properties = model.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (property.GetValue(model) == null)
                {
                    if (property.PropertyType == typeof(Int32) || property.PropertyType == typeof(System.Nullable<Int32>))
                    {
                        property.SetValue(model, 0);
                    }
                    else if (property.PropertyType == typeof(string))
                    {
                        property.SetValue(model, string.Empty);
                    }
                    else if (property.PropertyType == typeof(decimal) || property.PropertyType == typeof(System.Nullable<decimal>))
                    {
                        property.SetValue(model, Decimal.Zero);
                    }
                    else if (property.PropertyType == typeof(Int16) || property.PropertyType == typeof(System.Nullable<Int16>))
                    {
                        Int16 temp = 0;
                        property.SetValue(model, temp);
                    }
                    else if (property.PropertyType == typeof(Int32) || property.PropertyType == typeof(System.Nullable<Int32>))
                    {
                        property.SetValue(model, 0);
                    }
                    else if (property.PropertyType == typeof(Int64) || property.PropertyType == typeof(System.Nullable<Int64>))
                    {
                        Int64 temp = 0;
                        property.SetValue(model, temp);
                    }
                    else if (property.PropertyType == typeof(Boolean) || property.PropertyType == typeof(System.Nullable<Boolean>))
                    {
                        property.SetValue(model, false);
                    }
                    else if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(System.Nullable<DateTime>))
                    {
                        property.SetValue(model, DateTime.Now);
                    }
                }
            }
        }

        public static string FormatPhonenumber(this string phonenumber)
        {
            if (phonenumber.Length >= 9)
            {
                return phonenumber.Insert(6, " ").Insert(3, " ");
            }
            return phonenumber;
        }
    }
}
