﻿using GT.InboundApplication.Model.GTApplicationDB;
using System.Collections;
using System.Data.Entity;
using System.Linq;


namespace GT.InboundApplication.Model.Common
{
    public static class OccupationExtenstion
    {
        public static IEnumerable GetOccupationRelated(this DbSet<Occupation> dbset, string occupationClassCode)
        {
            return from row in dbset.Where(row => row.Class == occupationClassCode).AsEnumerable()
                   select new
                   {
                       name = row.Name,
                       code = row.Code
                   };
        }
    }
}
