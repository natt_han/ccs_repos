﻿using DM.Utilities.NetFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.ViewModels
{
    public class BasePolicyViewModel
    {
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string Salutation { get; set; }
        public string SalutationCode { get; set; }
        public string ClientSex { get; set; }
        public string ClientSexCode { get; set; }
        public string ClientStatus { get; set; }
        public string ClientStatusCode { get; set; }
        public string ClientTelPhoneNumber1 { get; set; }
        public string ClientTelPhoneNumber2 { get; set; }
        public string ClientTelPhoneNumber3 { get; set; }
        public string ClientTelPhoneNumber4 { get; set; }
        public string ClientTelPhoneNumber1_Phone { get; set; }
        public string ClientTelPhoneNumber2_Phone { get; set; }
        public string ClientTelPhoneNumber3_Phone { get; set; }
        public string ClientTelPhoneNumber1_To { get; set; }
        public string ClientTelPhoneNumber2_To { get; set; }
        public string ClientTelPhoneNumber1_Extension { get; set; }
        public string ClientTelPhoneNumber2_Extension { get; set; }
        public string SpouseName { get; set; }
        public string SpouseNationalityCode { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<System.DateTime> ClientBirthDate { get; set; }
        public string Occupation { get; set; }
        public string OccupationClass { get; set; }
        public string OccupationClassCode { get; set; }
        public string OccupationPosition { get; set; }
        public string OccupationCode { get; set; }
        public string OccupationDescription { get; set; }
        public string OccupationWorkPlace { get; set; }
        public Nullable<short> Age { get; set; }
        public Nullable<decimal> Height { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public string BeneficiaryName1 { get; set; }
        public string BeneficiaryName2 { get; set; }
        public string BeneficiaryRelationShip1 { get; set; }
        public string BeneficiaryRelationShip2 { get; set; }
        public Nullable<int> BeneficiaryAge1 { get; set; }
        public Nullable<int> BeneficiaryAge2 { get; set; }
        public Nullable<int> BeneficiaryBenefitPercentage1 { get; set; }
        public Nullable<int> BeneficiaryBenefitPercentage2 { get; set; }
        public string Nationality { get; set; }
        public string NationalityCode { get; set; }
        public string OriginalityCode { get; set; }
        public string ReligionCode { get; set; }
        public string AnnualIncome { get; set; }
        public string CardId { get; set; }
        public string CardType { get; set; }
        public string CardIssueDistrict { get; set; }
        public string CardIssueProvince { get; set; }
        public string CardIssueCountry { get; set; }
        public Nullable<System.DateTime> CardExpiryDate { get; set; }
        public string AddressType { get; set; }
        public string AddressTypeCode { get; set; }
        public string ClientAddress1 { get; set; }
        public string ClientAddress2 { get; set; }
        public string ClientAddress3 { get; set; }
        public string ClientAddress4 { get; set; }
        public string ClientAddress5 { get; set; }
        public string PostCode { get; set; }
        public string ProvinceId { get; set; }        
        public string DistrictId { get; set; }
        public string SubDistrictId { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string RegistrationAddress1 { get; set; }
        public string RegistrationAddress2 { get; set; }
        public string RegistrationAddress3 { get; set; }
        public string RegistrationAddress4 { get; set; }
        public string RegistrationAddress5 { get; set; }
        public string Registration_PostCode { get; set; }
        public string Registration_ProvinceId { get; set; }        
        public string Registration_DistrictId { get; set; }
        public string Registration_SubDistrictId { get; set; }
        public string Registration_MobileNumber { get; set; }
        public string Registration_Email { get; set; }
        public string OfficeAddress1 { get; set; }
        public string OfficeAddress2 { get; set; }
        public string OfficeAddress3 { get; set; }
        public string OfficeAddress4 { get; set; }
        public string OfficeAddress5 { get; set; }
        public string Office_PostCode { get; set; }
        public string Office_ProvinceId { get; set; }        
        public string Office_DistrictId { get; set; }
        public string Office_SubDistrictId { get; set; }
        public string Office_MobileNumber { get; set; }
        public string Office_Email { get; set; }
        public string PresentAddress1 { get; set; }
        public string PresentAddress2 { get; set; }
        public string PresentAddress3 { get; set; }
        public string PresentAddress4 { get; set; }
        public string PresentAddress5 { get; set; }
        public string Present_PostCode { get; set; }
        public string Present_ProvinceId { get; set; }        
        public string Present_DistrictId { get; set; }
        public string Present_SubDistrictId { get; set; }
        public string Present_MobileNumber { get; set; }
        public string Present_Email { get; set; }
        public string BMI { get; set; }

        public string Contact_HomeAddress { get; set; }
        public string Contact_MooBan { get; set; }
        public string Contact_Moo { get; set; }
        public string Contact_Soi { get; set; }
        public string Contact_Road { get; set; }
        public string Registration_HomeAddress { get; set; }
        public string Registration_MooBan { get; set; }
        public string Registration_Moo { get; set; }
        public string Registration_Soi { get; set; }
        public string Registration_Road { get; set; }
        public string Office_HomeAddress { get; set; }
        public string Office_MooBan { get; set; }
        public string Office_Moo { get; set; }
        public string Office_Soi { get; set; }
        public string Office_Road { get; set; }
        public string Present_HomeAddress { get; set; }
        public string Present_MooBan { get; set; }
        public string Present_Moo { get; set; }
        public string Present_Soi { get; set; }
        public string Present_Road { get; set; }

        public string PayerName { get; set; }
        public string PayerSurname { get; set; }
        public Nullable<System.DateTime> PayerBirthDate { get; set; }
        public Nullable<short> PayerAge { get; set; }
        public string PayerCardId { get; set; }
        public string PayerCardType { get; set; }
        public string PayerOccupationClass { get; set; }
        public string PayerOccupationClassCode { get; set; }
        public string PayerOccupation { get; set; }
        public string PayerOccupationCode { get; set; }
        public string PayerOccupationPosition { get; set; }
        public string PayerSalutation { get; set; }
        public string PayerSalutationCode { get; set; }
        public string PayerRelationShip { get; set; }
        public string PayerPayerStatusCode { get; set; }
        public string PayerPayerSexCode { get; set; }
        public string PayerTelPhoneNumber1 { get; set; }
        public string PayerTelPhoneNumber1_To { get; set; }
        public string PayerTelPhoneNumber1_Extension { get; set; }
        public string PayerTelPhoneNumber2 { get; set; }
        public string PayerTelPhoneNumber3 { get; set; }
        public string PayerEmailAddress { get; set; }
        public string PayerHomeAddress { get; set; }
        public string PayerMooBan { get; set; }
        public string PayerMoo { get; set; }
        public string PayerSoi { get; set; }
        public string PayerRoad { get; set; }
        public string PayerPayerAddress1 { get; set; }
        public string PayerPayerAddress2 { get; set; }
        public string PayerProvinceId { get; set; }
        public string PayerDistrictId { get; set; }
        public string PayerSubDistrictId { get; set; }
        public string PayerAnnualIncome { get; set; }
        public Nullable<System.DateTime> PayerCardExpiryDate { get; set; }
        

        public string PaymentMethod { get; set; }
        public string GTBankKey { get; set; }
        public string GTBankName { get; set; }
        public string GTBankBranch { get; set; }
        public string GTBankAccountNo { get; set; }
        public string BankTransferCustomerName { get; set; }
        public string CreditCardType { get; set; }
        public string CreditCardBankName { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardHolderName { get; set; }
        public Nullable<System.DateTime> CreditCardExpiryDate { get; set; }
        public string GTBankAccountType { get; set; }
        public string CreditCardTypeCode { get; set; }
        public string CreditCardClassTypeCode { get; set; }
        public string CreditCardClassType { get; set; }

        public string CombineRegistrationAddress1
        {
            get
            {
                return Registration_HomeAddress + " " + (Registration_Moo.IsNullOrEmpty()?"":"ม."+Registration_Moo) +" " + (Registration_MooBan.IsNullOrEmpty() ? "" : Registration_MooBan);
            }
        }

        public string CombineRegistrationAddress2
        {
            get
            {
                return (Registration_Soi.IsNullOrEmpty() ? "" : "ซ." + Registration_Soi+" ") +(Registration_Road.IsNullOrEmpty() ? "" : "ถ." + Registration_Road);
            }
        }

        public string CombineOfficeAddress1
        {
            get
            {
                return Office_HomeAddress + " " + (Office_Moo.IsNullOrEmpty() ? "" : "ม." + Office_Moo) + " " + (Office_MooBan.IsNullOrEmpty() ? "" : Office_MooBan);
            }
        }

        public string CombineOfficeAddress2
        {
            get
            {
                return (Office_Soi.IsNullOrEmpty() ? "" : "ซ." + Office_Soi + " ") + (Office_Road.IsNullOrEmpty() ? "" : "ถ." + Office_Road);
            }
        }

        public string CombinePresentAddress1
        {
            get
            {
                return Present_HomeAddress + " " + (Present_Moo.IsNullOrEmpty() ? "" : "ม." + Present_Moo) + " " + (Present_MooBan.IsNullOrEmpty() ? "" : Present_MooBan);
            }
        }

        public string CombinePresentAddress2
        {
            get
            {
                return (Present_Soi.IsNullOrEmpty() ? "" : "ซ." + Present_Soi + " ") + (Present_Road.IsNullOrEmpty() ? "" : "ถ." + Present_Road);
            }
        }

        public string CombineContactAddress1
        {
            get
            {
                return Contact_HomeAddress + " " + (Contact_Moo.IsNullOrEmpty() ? "" : "ม." + Contact_Moo) + " " + (Contact_MooBan.IsNullOrEmpty() ? "" : Contact_MooBan);
            }
        }

        public string CombineContactAddress2
        {
            get
            {
                return (Contact_Soi.IsNullOrEmpty() ? "" : "ซ." + Contact_Soi + " ") + (Contact_Road.IsNullOrEmpty() ? "" : "ถ." + Contact_Road);
            }
        }

        public string CombinePayerAddress1
        {
            get
            {
                return PayerHomeAddress + " " + (PayerMoo.IsNullOrEmpty() ? "" : "ม." + PayerMoo) + " " + (PayerMooBan.IsNullOrEmpty() ? "" : PayerMooBan);
            }
        }

        public string CombinePayerAddress2
        {
            get
            {
                return (PayerSoi.IsNullOrEmpty() ? "" : "ซ." + PayerSoi + " ") + (PayerRoad.IsNullOrEmpty() ? "" : "ถ." + PayerRoad);
            }
        }

        public void UpdateAddressForCancer()
        {
            if (string.IsNullOrEmpty(this.Registration_ProvinceId))
            {
                this.RegistrationAddress1 = this.ClientAddress1;
                this.RegistrationAddress2 = this.ClientAddress2;
                this.Registration_ProvinceId = this.ProvinceId;
                this.Registration_DistrictId = this.DistrictId;
                this.Registration_SubDistrictId = this.SubDistrictId;
                this.RegistrationAddress3 = this.ClientAddress3;
                this.RegistrationAddress4 = this.ClientAddress4;
                this.RegistrationAddress5 = this.ClientAddress5;
                this.Registration_Email = this.EmailAddress;
                this.Registration_MobileNumber = this.ClientTelPhoneNumber3;
            }
        }

        public void ConvertAddress()
        {
            if (!this.Registration_HomeAddress.IsNullOrEmpty())
            {
                this.RegistrationAddress1 = CombineRegistrationAddress1;
                this.RegistrationAddress2 = CombineRegistrationAddress2;
            }
            else if (!this.RegistrationAddress1.IsNullOrEmpty())
            {
                this.Registration_HomeAddress = this.RegistrationAddress1;
                this.Registration_Road = this.RegistrationAddress2;
            }

            if (!this.Office_HomeAddress.IsNullOrEmpty())
            {
                this.OfficeAddress1 = CombineOfficeAddress1;
                this.OfficeAddress2 = CombineOfficeAddress2;
            }
            else if (!this.OfficeAddress1.IsNullOrEmpty())
            {
                this.Office_HomeAddress = this.OfficeAddress1;
                this.Office_Road = this.OfficeAddress2;
            }

            if (!this.Present_HomeAddress.IsNullOrEmpty())
            {
                this.PresentAddress1 = CombinePresentAddress1;
                this.PresentAddress2 = CombinePresentAddress2;
            }
            else if (!this.PresentAddress1.IsNullOrEmpty())
            {
                this.Present_HomeAddress = this.PresentAddress1;
                this.Present_Road = this.PresentAddress2;
            }
            if (!this.Present_SubDistrictId.IsNullOrEmpty()) {
                this.SubDistrictId = this.Present_SubDistrictId;
                this.DistrictId = this.Present_DistrictId;
                this.ProvinceId = this.Present_ProvinceId;
                this.ClientTelPhoneNumber3 = this.Present_MobileNumber;
            }

            if (!this.Contact_HomeAddress.IsNullOrEmpty())
            {
                this.ClientAddress1 = CombineContactAddress1;
                this.ClientAddress2 = CombineContactAddress2;
            }
            else if (!this.ClientAddress1.IsNullOrEmpty())
            {
                this.Contact_HomeAddress = this.ClientAddress1;
                this.Contact_Road = this.ClientAddress2;
            }

            if (!this.PayerHomeAddress.IsNullOrEmpty())
            {
                this.PayerPayerAddress1 = CombinePayerAddress1;
                this.PayerPayerAddress2 = CombinePayerAddress2;
            }
            else if (!this.PayerPayerAddress1.IsNullOrEmpty())
            {
                this.PayerHomeAddress = this.PayerPayerAddress1;
                this.PayerRoad = this.PayerPayerAddress2;
            }
        }
    }
}
