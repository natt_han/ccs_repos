﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.ViewModels
{
    public class HLSAdditionalQuestionViewModel
    {
        //ความดันโลหิตสูง
        public bool HypertensionFlag { get; set; }
        public string HypertensionAnswer01 { get; set; }
        public string HypertensionAnswer02 { get; set; }
        public string HypertensionAnswer03 { get; set; }
        public string HypertensionAnswer04 { get; set; }
        public string HypertensionAnswer05 { get; set; }
        public string HypertensionRemark { get; set; }
        //ไขมันในเลือดสูง
        public bool DyslipidemiaFlag { get; set; }
        public string DyslipidemiaAnswer01 { get; set; }
        public string DyslipidemiaAnswer02 { get; set; }
        public string DyslipidemiaAnswer03 { get; set; }
        public string DyslipidemiaAnswer04 { get; set; }
        public string DyslipidemiaRemark { get; set; }
        //ไซนัสอักเสบ
        public bool SinusitisFlag { get; set; }
        public string SinusitisAnswer01 { get; set; }
        public string SinusitisAnswer02 { get; set; }
        public string SinusitisAnswer03 { get; set; }
        public string SinusitisRemark { get; set; }
        //โรคภมิแพ้
        public bool AllergyFlag { get; set; }
        public string AllergyAnswer01 { get; set; }
        public string AllergyAnswer01_Rem01 { get; set; }//ชนิดภูมิแพ้
        public string AllergyAnswer01_Rem02 { get; set; }//อาการ
        public string AllergyAnswer02 { get; set; }
        public string AllergyRemark { get; set; }
        //โรคกรดไหลย้อน Gastroesophageal Reflux Disease: GERD
        public bool GERDFlag { get; set; }
        public string GERDAnswer01 { get; set; }
        public string GERDRemark { get; set; }
        //กระเพาะอาหารอักเสบ
        public bool GastritisFlag { get; set; }
        public string GastritisAnswer01 { get; set; }
        public string GastritisAnswer02 { get; set; }
        public string GastritisRemark { get; set; }
        //ริดสีดวง
        public bool HemorrhoidFlag { get; set; }
        public string HemorrhoidAnswer01 { get; set; }
        public string HemorrhoidAnswer02 { get; set; }
        public string HemorrhoidRemark { get; set; }
        //ต้อเนื้อ ต้อกระจก
        public bool CataractFlag { get; set; }
        public string CataractAnswer01 { get; set; }
        public string CataractAnswer02 { get; set; }
        public string CataractAnswer03 { get; set; }
        public string CataractAnswer01_Rem01 { get; set; }//ต้อชนิดใด ตาข้างไหน
        public string CataractRemark { get; set; }
        //นิ่วในถุงน้ำดี
        public bool GallstoneFlag { get; set; }
        public string GallstoneAnswer01 { get; set; }
        public string GallstoneAnswer02 { get; set; }
        public string GallstoneRemark { get; set; }
        //นิ่วในไต
        public bool KidneystoneFlag { get; set; }
        public string KidneystoneAnswer01 { get; set; }
        public string KidneystoneAnswer02 { get; set; }
        public string KidneystoneAnswer02_Rem01 { get; set; }
        public string KidneystoneRemark { get; set; }

        public bool ReadExclusionFlag { get; set; }
    }

    public class HLSSymptomViewModel
    {
        //เหนื่อย
        public bool TiredFlag { get; set; }
        public string TiredAnswer01 { get; set; }
        public string TiredRemark { get; set; }

        //หายใจไม่สะดวก
        public bool DyspneaFlag { get; set; }
        public string DyspneaAnswer01 { get; set; }
        public string DyspneaAnswer02 { get; set; }
        public string DyspneaRemark { get; set; }

        //ปวดกล้ามเนื้อ
        public bool MusclePainFlag { get; set; }
        public string MusclePainAnswer1_1 { get; set; }
        public string MusclePainAnswer1_2 { get; set; }
        public string MusclePainAnswer2_1 { get; set; }
        public string MusclePainAnswer2_2 { get; set; }
        public string MusclePainAnswer3_1 { get; set; }
        public string MusclePainAnswer4_1 { get; set; }
        public string MusclePainAnswer4_2 { get; set; }
        public string MusclePainAnswer4_3 { get; set; }
        public string MusclePainAnswer4_4 { get; set; }
        public string MusclePainAnswer5_1 { get; set; }
        public string MusclePainAnswer5_2 { get; set; }
        public string MusclePainAnswer5_3 { get; set; }
        public string MusclePainRemark { get; set; }

        //ปวดศีรษะ
        public bool HeadacheFlag { get; set; }
        public string HeadacheAnswer1_1 { get; set; }
        public string HeadacheAnswer2_1 { get; set; }
        public string HeadacheAnswer2_2 { get; set; }
        public string HeadacheAnswer2_3 { get; set; }
        public string HeadacheAnswer3_1 { get; set; }
        public string HeadacheAnswer3_2 { get; set; }
        public string HeadacheAnswer3_3 { get; set; }
        public string HeadacheAnswer3_4 { get; set; }
        public string HeadacheAnswer4_1 { get; set; }
        public string HeadacheAnswer4_2 { get; set; }
        public string HeadacheRemark { get; set; }

        public bool CovidFlag { get; set; }
        public string CovidAnswer1_1 { get; set; }
        public string CovidAnswer2_1 { get; set; }
        public string CovidAnswer2_2 { get; set; }
        public string CovidAnswer2_3 { get; set; }
        public string CovidAnswer2_4 { get; set; }
        public string CovidAnswer2_5 { get; set; }
        public string CovidAnswer3_1 { get; set; }
        public string CovidRemark { get; set; }

        //ไข้หวัด
        public bool FluFlag { get; set; }
        public string FluAnswer1_1 { get; set; }
        public string FluAnswer1_2 { get; set; }
        public string FluAnswer1_3 { get; set; }
        public string FluAnswer2_1 { get; set; }
        public string FluAnswer2_2 { get; set; }
        public string FluAnswer2_3 { get; set; }
        public string FluAnswer2_4 { get; set; }
        public string FluRemark { get; set; }

        //ท้องเสีย
        public bool DiarrheaFlag { get; set; }
        public string DiarrheaAnswer01 { get; set; }
        public string DiarrheaAnswer02 { get; set; }
        public string DiarrheaAnswer03 { get; set; }
        public string DiarrheaAnswer04 { get; set; }
        public string DiarrheaAnswer05 { get; set; }
        public string DiarrheaRemark { get; set; }

        //ไส้ติ่ง
        public bool AppendicitisFlag { get; set; }
        public string AppendicitisAnswer01 { get; set; }
        public string AppendicitisRemark { get; set; }

        //เนื้องอก ถุงน้ำ
        public bool CystFlag { get; set; }
        public string CystAnswer01 { get; set; }
        public string CystAnswer02 { get; set; }
        public string CystAnswer03 { get; set; }
        public string CystRemark { get; set; }
    }
}
