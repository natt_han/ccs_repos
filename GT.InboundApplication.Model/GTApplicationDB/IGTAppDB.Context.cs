﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GT.InboundApplication.Model.GTApplicationDB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class IGTAppDBContainer : DbContext
    {
        public IGTAppDBContainer()
            : base("name=IGTAppDBContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<GenHealthLumpSum_Payment> GenHealthLumpSum_PaymentSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_Assured> GenHealthLumpSum_AssuredSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_AuditTrail> GenHealthLumpSum_AuditTrailSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_Plan> GenHealthLumpSum_PlanSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_UnderWriteInfo> GenHealthLumpSum_UnderWriteInfoSet { get; set; }
        public virtual DbSet<Salutation> SalutationSet { get; set; }
        public virtual DbSet<Occupation> OccupationSet { get; set; }
        public virtual DbSet<Nationality> NationalitySet { get; set; }
        public virtual DbSet<ClientStatus> ClientStatusSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_MasterPlan> GenHealthLumpSum_MasterPlanSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_Policy> GenHealthLumpSum_PolicySet { get; set; }
        public virtual DbSet<Product> ProductSet { get; set; }
        public virtual DbSet<GTBankAccount> GTBankAccountSet { get; set; }
        public virtual DbSet<CreditCardType> CreditCardTypeSet { get; set; }
        public virtual DbSet<OccupationClass> OccupationClassSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_DCR> GenHealthLumpSum_DCRSet { get; set; }
        public virtual DbSet<Relation> RelationSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_MasterCoverage> GenHealthLumpSum_MasterCoverageSet { get; set; }
        public virtual DbSet<GenHealthProtect_Plan> GenHealthProtect_PlanSet { get; set; }
        public virtual DbSet<GenHealthProtect_AuditTrail> GenHealthProtect_AuditTrailSet { get; set; }
        public virtual DbSet<GenHealthProtect_UnderWriteInfo> GenHealthProtect_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenHealthProtect_Assured> GenHealthProtect_AssuredSet { get; set; }
        public virtual DbSet<GenHealthProtect_Payment> GenHealthProtect_PaymentSet { get; set; }
        public virtual DbSet<GenHealthProtect_MasterPlan> GenHealthProtect_MasterPlanSet { get; set; }
        public virtual DbSet<GenHealthProtect_MasterCoverage> GenHealthProtect_MasterCoverageSet { get; set; }
        public virtual DbSet<GenHealthProtect_Policy> GenHealthProtect_PolicySet { get; set; }
        public virtual DbSet<GenHealthProtect_DCR> GenHealthProtect_DCRSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_Payer> GenHealthLumpSum_PayerSet { get; set; }
        public virtual DbSet<GenHealthProtect_Payer> GenHealthProtect_PayerSet { get; set; }
        public virtual DbSet<LeadSource> LeadSourceSet { get; set; }
        public virtual DbSet<GenExclusivePA_Policy> GenExclusivePA_PolicySet { get; set; }
        public virtual DbSet<GenExclusivePA_Assured> GenExclusivePA_AssuredSet { get; set; }
        public virtual DbSet<GenExclusivePA_Plan> GenExclusivePA_PlanSet { get; set; }
        public virtual DbSet<GenExclusivePA_Payment> GenExclusivePA_PaymentSet { get; set; }
        public virtual DbSet<GenExclusivePA_UnderWriteInfo> GenExclusivePA_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenExclusivePA_DCR> GenExclusivePA_DCRSet { get; set; }
        public virtual DbSet<GenExclusivePA_AuditTrail> GenExclusivePA_AuditTrailSet { get; set; }
        public virtual DbSet<GenExclusivePA_MasterPlan> GenExclusivePA_MasterPlanSet { get; set; }
        public virtual DbSet<GenExclusivePA_MasterCoverage> GenExclusivePA_MasterCoverageSet { get; set; }
        public virtual DbSet<GenShieldPA_Policy> GenShieldPA_PolicySet { get; set; }
        public virtual DbSet<GenShieldPA_Assured> GenShieldPA_AssuredSet { get; set; }
        public virtual DbSet<GenShieldPA_UnderWriteInfo> GenShieldPA_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenShieldPA_Payment> GenShieldPA_PaymentSet { get; set; }
        public virtual DbSet<GenShieldPA_Plan> GenShieldPA_PlanSet { get; set; }
        public virtual DbSet<GenShieldPA_AuditTrail> GenShieldPA_AuditTrailSet { get; set; }
        public virtual DbSet<GenShieldPA_DCR> GenShieldPA_DCRSet { get; set; }
        public virtual DbSet<GenShieldPA_MasterPlan> GenShieldPA_MasterPlanSet { get; set; }
        public virtual DbSet<GenShieldPA_MasterCoverage> GenShieldPA_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMax7_Policy> GenMax7_PolicySet { get; set; }
        public virtual DbSet<GenMax7_Assured> GenMax7_AssuredSet { get; set; }
        public virtual DbSet<GenMax7_DCR> GenMax7_DCRSet { get; set; }
        public virtual DbSet<GenMax7_Plan> GenMax7_PlanSet { get; set; }
        public virtual DbSet<GenMax7_Payer> GenMax7_PayerSet { get; set; }
        public virtual DbSet<GenMax7_AuditTrail> GenMax7_AuditTrailSet { get; set; }
        public virtual DbSet<GenMax7_UnderWriteInfo> GenMax7_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenMax7_Payment> GenMax7_PaymentSet { get; set; }
        public virtual DbSet<GenMax7_MasterCoverage> GenMax7_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMax7_MasterPlan> GenMax7_MasterPlanSet { get; set; }
        public virtual DbSet<GenMax7_FATCA> GenMax7_FATCASet { get; set; }
        public virtual DbSet<LifeSalutation> LifeSalutationSet { get; set; }
        public virtual DbSet<LifePayLife> LifePayLifeSet { get; set; }
        public virtual DbSet<LifeGTBankAccount> LifeGTBankAccountSet { get; set; }
        public virtual DbSet<Religion> ReligionSet { get; set; }
        public virtual DbSet<GenSmart5_Policy> GenSmart5_PolicySet { get; set; }
        public virtual DbSet<GenSmart5_Assured> GenSmart5_AssuredSet { get; set; }
        public virtual DbSet<GenSmart5_FATCA> GenSmart5_FATCASet { get; set; }
        public virtual DbSet<GenSmart5_UnderWriteInfo> GenSmart5_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenSmart5_DCR> GenSmart5_DCRSet { get; set; }
        public virtual DbSet<GenSmart5_Plan> GenSmart5_PlanSet { get; set; }
        public virtual DbSet<GenSmart5_Payment> GenSmart5_PaymentSet { get; set; }
        public virtual DbSet<GenSmart5_MasterPlan> GenSmart5_MasterPlanSet { get; set; }
        public virtual DbSet<GenSmart5_MasterCoverage> GenSmart5_MasterCoverageSet { get; set; }
        public virtual DbSet<GenLifePlus10_Policy> GenLifePlus10_PolicySet { get; set; }
        public virtual DbSet<GenLifePlus10_Assured> GenLifePlus10_AssuredSet { get; set; }
        public virtual DbSet<GenLifePlus10_Plan> GenLifePlus10_PlanSet { get; set; }
        public virtual DbSet<GenLifePlus10_DCR> GenLifePlus10_DCRSet { get; set; }
        public virtual DbSet<GenLifePlus10_Payment> GenLifePlus10_PaymentSet { get; set; }
        public virtual DbSet<GenLifePlus10_MasterPlan> GenLifePlus10_MasterPlanSet { get; set; }
        public virtual DbSet<GenLifePlus10_MasterCoverage> GenLifePlus10_MasterCoverageSet { get; set; }
        public virtual DbSet<GenLifePlus10_UnderWriteInfo> GenLifePlus10_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenLifePlus10_FATCA> GenLifePlus10_FATCASet { get; set; }
        public virtual DbSet<GenLifePlus10_Payer> GenLifePlus10_PayerSet { get; set; }
        public virtual DbSet<GenLifePlus10_AuditTrail> GenLifePlus10_AuditTrailSet { get; set; }
        public virtual DbSet<GenSmart5_AuditTrail> GenSmart5_AuditTrailSet { get; set; }
        public virtual DbSet<GenSmart5_Payer> GenSmart5_PayerSet { get; set; }
        public virtual DbSet<GenSabye4_Assured> GenSabye4_AssuredSet { get; set; }
        public virtual DbSet<GenSabye4_AuditTrail> GenSabye4_AuditTrailSet { get; set; }
        public virtual DbSet<GenSabye4_DCR> GenSabye4_DCRSet { get; set; }
        public virtual DbSet<GenSabye4_FATCA> GenSabye4_FATCASet { get; set; }
        public virtual DbSet<GenSabye4_MasterCoverage> GenSabye4_MasterCoverageSet { get; set; }
        public virtual DbSet<GenSabye4_MasterPlan> GenSabye4_MasterPlanSet { get; set; }
        public virtual DbSet<GenSabye4_Payer> GenSabye4_PayerSet { get; set; }
        public virtual DbSet<GenSabye4_Payment> GenSabye4_PaymentSet { get; set; }
        public virtual DbSet<GenSabye4_Plan> GenSabye4_PlanSet { get; set; }
        public virtual DbSet<GenSabye4_Policy> GenSabye4_PolicySet { get; set; }
        public virtual DbSet<GenSabye4_UnderWriteInfo> GenSabye4_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenHighLife3_Assured> GenHighLife3_AssuredSet { get; set; }
        public virtual DbSet<GenHighLife3_AuditTrail> GenHighLife3_AuditTrailSet { get; set; }
        public virtual DbSet<GenHighLife3_DCR> GenHighLife3_DCRSet { get; set; }
        public virtual DbSet<GenHighLife3_FATCA> GenHighLife3_FATCASet { get; set; }
        public virtual DbSet<GenHighLife3_MasterCoverage> GenHighLife3_MasterCoverageSet { get; set; }
        public virtual DbSet<GenHighLife3_MasterPlan> GenHighLife3_MasterPlanSet { get; set; }
        public virtual DbSet<GenHighLife3_Payer> GenHighLife3_PayerSet { get; set; }
        public virtual DbSet<GenHighLife3_Payment> GenHighLife3_PaymentSet { get; set; }
        public virtual DbSet<GenHighLife3_Plan> GenHighLife3_PlanSet { get; set; }
        public virtual DbSet<GenHighLife3_Policy> GenHighLife3_PolicySet { get; set; }
        public virtual DbSet<GenHighLife3_UnderWriteInfo> GenHighLife3_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenHealthProtect_MasterPlanCoverage> GenHealthProtect_MasterPlanCoverageSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_MasterPricePlan> GenHealthLumpSum_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_LegalScriptTemplate> GenHealthLumpSum_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenHealthProtect_MasterPricePlan> GenHealthProtect_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenHealthProtect_LegalScriptTemplate> GenHealthProtect_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenExclusivePA_MasterPricePlan> GenExclusivePA_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenExclusivePA_LegalScriptTemplate> GenExclusivePA_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenShieldPA_MasterPricePlan> GenShieldPA_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenShieldPA_LegalScriptTemplate> GenShieldPA_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenMax7_MasterPricePlan> GenMax7_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMax7_LegalScriptTemplate> GenMax7_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenLifePlus10_MasterPricePlan> GenLifePlus10_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenLifePlus10_LegalScriptTemplate> GenLifePlus10_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenSmart5_MasterPricePlan> GenSmart5_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenSmart5_LegalScriptTemplate> GenSmart5_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenSabye4_MasterPricePlan> GenSabye4_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenSabye4_LegalScriptTemplate> GenSabye4_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenHighLife3_MasterPricePlan> GenHighLife3_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenHighLife3_LegalScriptTemplate> GenHighLife3_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenFamilyPA_Assured> GenFamilyPA_AssuredSet { get; set; }
        public virtual DbSet<GenFamilyPA_Policy> GenFamilyPA_PolicySet { get; set; }
        public virtual DbSet<GenFamilyPA_Plan> GenFamilyPA_PlanSet { get; set; }
        public virtual DbSet<GenFamilyPA_DCR> GenFamilyPA_DCRSet { get; set; }
        public virtual DbSet<GenFamilyPA_UnderWriteInfo> GenFamilyPA_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenFamilyPA_Payment> GenFamilyPA_PaymentSet { get; set; }
        public virtual DbSet<GenFamilyPA_AuditTrail> GenFamilyPA_AuditTrailSet { get; set; }
        public virtual DbSet<GenFamilyPA_MasterPricePlan> GenFamilyPA_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenFamilyPA_MasterCoverage> GenFamilyPA_MasterCoverageSet { get; set; }
        public virtual DbSet<GenFamilyPA_LegalScriptTemplate> GenFamilyPA_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenFamilyPA_Subsidiary1> GenFamilyPA_Subsidiary1Set { get; set; }
        public virtual DbSet<GenFamilyPA_Subsidiary2> GenFamilyPA_Subsidiary2Set { get; set; }
        public virtual DbSet<GenFamilyPA_Subsidiary3> GenFamilyPA_Subsidiary3Set { get; set; }
        public virtual DbSet<GenFamilyPA_Subsidiary4> GenFamilyPA_Subsidiary4Set { get; set; }
        public virtual DbSet<GenAomSub6_Policy> GenAomSub6_PolicySet { get; set; }
        public virtual DbSet<GenAomSub6_Assured> GenAomSub6_AssuredSet { get; set; }
        public virtual DbSet<GenAomSub6_Plan> GenAomSub6_PlanSet { get; set; }
        public virtual DbSet<GenAomSub6_DCR> GenAomSub6_DCRSet { get; set; }
        public virtual DbSet<GenAomSub6_FATCA> GenAomSub6_FATCASet { get; set; }
        public virtual DbSet<GenAomSub6_Payment> GenAomSub6_PaymentSet { get; set; }
        public virtual DbSet<GenAomSub6_AuditTrail> GenAomSub6_AuditTrailSet { get; set; }
        public virtual DbSet<GenAomSub6_UnderWriteInfo> GenAomSub6_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenAomSub6_Payer> GenAomSub6_PayerSet { get; set; }
        public virtual DbSet<GenAomSub6_MasterPricePlan> GenAomSub6_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenAomSub6_MasterCoverage> GenAomSub6_MasterCoverageSet { get; set; }
        public virtual DbSet<GenAomSub6_LegalScriptTemplate> GenAomSub6_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Policy> GenMotor_VOL1_PolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Assured> GenMotor_VOL1_AssuredSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_OldPolicy> GenMotor_VOL1_OldPolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Plan> GenMotor_VOL1_PlanSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_TaxInvoice> GenMotor_VOL1_TaxInvoiceSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_DCR> GenMotor_VOL1_DCRSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_AuditTrail> GenMotor_VOL1_AuditTrailSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Vehicle> GenMotor_VOL1_VehicleSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Driver> GenMotor_VOL1_DriverSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_MasterPricePlan> GenMotor_VOL1_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_MasterCoverage> GenMotor_VOL1_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_VehicleBrand> GenMotor_VOL1_VehicleBrandSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_VehicleModel> GenMotor_VOL1_VehicleModelSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_VehicleType> GenMotor_VOL1_VehicleTypeSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Payment> GenMotor_VOL1_PaymentSet { get; set; }
        public virtual DbSet<GenMotor_VOL1_Inspection> GenMotor_VOL1_InspectionSet { get; set; }
        public virtual DbSet<GenHealthLumpSum_Plan1> GenHealthLumpSum_Plan1Set { get; set; }
        public virtual DbSet<ValuePreference> ValuePreferenceSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Policy> GenMotor_VOL1Std_PolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Assured> GenMotor_VOL1Std_AssuredSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Plan> GenMotor_VOL1Std_PlanSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Vehicle> GenMotor_VOL1Std_VehicleSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_OldPolicy> GenMotor_VOL1Std_OldPolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_AuditTrail> GenMotor_VOL1Std_AuditTrailSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Driver> GenMotor_VOL1Std_DriverSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Inspection> GenMotor_VOL1Std_InspectionSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_Payment> GenMotor_VOL1Std_PaymentSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_DCR> GenMotor_VOL1Std_DCRSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_TaxInvoice> GenMotor_VOL1Std_TaxInvoiceSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_Policy> GenMotor_VOL2Plus_PolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_Assured> GenMotor_VOL2Plus_AssuredSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_Vehicle> GenMotor_VOL2Plus_VehicleSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_AuditTrail> GenMotor_VOL2Plus_AuditTrailSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_OldPolicy> GenMotor_VOL2Plus_OldPolicySet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_Payment> GenMotor_VOL2Plus_PaymentSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_DCR> GenMotor_VOL2Plus_DCRSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_Plan> GenMotor_VOL2Plus_PlanSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_MasterPricePlan> GenMotor_VOL2Plus_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_TaxInvoice> GenMotor_VOL2Plus_TaxInvoiceSet { get; set; }
        public virtual DbSet<GenMotor_VOL2Plus_MasterCoverage> GenMotor_VOL2Plus_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMotor_VOL1Std_MasterCoverage> GenMotor_VOL1Std_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMotor_CMI_Policy> GenMotor_CMI_PolicySet { get; set; }
        public virtual DbSet<GenMotor_CMI_Vehicle> GenMotor_CMI_VehicleSet { get; set; }
        public virtual DbSet<GenMotor_CMI_AuditTrail> GenMotor_CMI_AuditTrailSet { get; set; }
        public virtual DbSet<GenMotor_CMI_Payment> GenMotor_CMI_PaymentSet { get; set; }
        public virtual DbSet<GenMotor_CMI_DCR> GenMotor_CMI_DCRSet { get; set; }
        public virtual DbSet<GenMotor_CMI_Plan> GenMotor_CMI_PlanSet { get; set; }
        public virtual DbSet<GenMotor_CMI_Assured> GenMotor_CMI_AssuredSet { get; set; }
        public virtual DbSet<GenMotor_CMI_MasterPricePlan> GenMotor_CMI_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMotor_CMI_MasterCoverage> GenMotor_CMI_MasterCoverageSet { get; set; }
        public virtual DbSet<GenMotor_CMI_TaxInvoice> GenMotor_CMI_TaxInvoiceSet { get; set; }
        public virtual DbSet<VehicleBrand> VehicleBrandSet { get; set; }
        public virtual DbSet<MenuPreference> MenuPreferenceSet { get; set; }
        public virtual DbSet<GenCancer_Policy> GenCancer_PolicySet { get; set; }
        public virtual DbSet<GenCancer_Assured> GenCancer_AssuredSet { get; set; }
        public virtual DbSet<GenCancer_AuditTrail> GenCancer_AuditTrailSet { get; set; }
        public virtual DbSet<GenCancer_Plan> GenCancer_PlanSet { get; set; }
        public virtual DbSet<GenCancer_Payer> GenCancer_PayerSet { get; set; }
        public virtual DbSet<GenCancer_Payment> GenCancer_PaymentSet { get; set; }
        public virtual DbSet<GenCancer_DCR> GenCancer_DCRSet { get; set; }
        public virtual DbSet<GenCancer_UnderWriteInfo> GenCancer_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenCancer_MasterPricePlan> GenCancer_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenCancer_MasterCoverage> GenCancer_MasterCoverageSet { get; set; }
        public virtual DbSet<GenCancer_LegalScriptTemplate> GenCancer_LegalScriptTemplateSet { get; set; }
        public virtual DbSet<AddressType> AddressTypeSet { get; set; }
        public virtual DbSet<GenMotorType1Std_Policy> GenMotorType1Std_PolicySet { get; set; }
        public virtual DbSet<GenMotorType1Std_Assured> GenMotorType1Std_AssuredSet { get; set; }
        public virtual DbSet<GenMotorType1Std_Plan> GenMotorType1Std_PlanSet { get; set; }
        public virtual DbSet<GenMotorType1Std_CTPL> GenMotorType1Std_CTPLSet { get; set; }
        public virtual DbSet<GenMotorType1Std_AuditTrail> GenMotorType1Std_AuditTrailSet { get; set; }
        public virtual DbSet<GenMotorType1Std_Inspection> GenMotorType1Std_InspectionSet { get; set; }
        public virtual DbSet<GenMotorType1Std_Driver> GenMotorType1Std_DriverSet { get; set; }
        public virtual DbSet<GenMotorType1Std_OldPolicy> GenMotorType1Std_OldPolicySet { get; set; }
        public virtual DbSet<GenMotorType1Std_Payment> GenMotorType1Std_PaymentSet { get; set; }
        public virtual DbSet<GenMotorType1Std_Vehicle> GenMotorType1Std_VehicleSet { get; set; }
        public virtual DbSet<GenMotorType1Std_PlanDiscount> GenMotorType1Std_PlanDiscountSet { get; set; }
        public virtual DbSet<GenMotorType1Std_DCR> GenMotorType1Std_DCRSet { get; set; }
        public virtual DbSet<GenGroupSME_Policy> GenGroupSME_PolicySet { get; set; }
        public virtual DbSet<GenGroupSME_DCR> GenGroupSME_DCRSet { get; set; }
        public virtual DbSet<GenGroupSME_Payment> GenGroupSME_PaymentSet { get; set; }
        public virtual DbSet<GenGroupSME_Assured> GenGroupSME_AssuredSet { get; set; }
        public virtual DbSet<GenGroupSME_Employee> GenGroupSME_EmployeeSet { get; set; }
        public virtual DbSet<GenGroupSME_AuditTrail> GenGroupSME_AuditTrailSet { get; set; }
        public virtual DbSet<GenGroupSME_Plan> GenGroupSME_PlanSet { get; set; }
        public virtual DbSet<GenGroupSME_MasterPricePlan> GenGroupSME_MasterPricePlanSet { get; set; }
        public virtual DbSet<CreditCardClassType> CreditCardClassTypeSet { get; set; }
        public virtual DbSet<MotorConstant> MotorConstantSet { get; set; }
        public virtual DbSet<MotorPolicyNumber> MotorPolicyNumberSet { get; set; }
        public virtual DbSet<GroupCompany> GroupCompanySet { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethodSet { get; set; }
        public virtual DbSet<MasterPolicy> MasterPolicySet { get; set; }
        public virtual DbSet<GenMotorType1Std_ReferDocument> GenMotorType1Std_ReferDocumentSet { get; set; }
        public virtual DbSet<MotorModel> MotorModelSet { get; set; }
        public virtual DbSet<MasterPolicy_Delivery> MasterPolicy_DeliverySet { get; set; }
        public virtual DbSet<UWRejectReason> UWRejectReasonSet { get; set; }
        public virtual DbSet<GenMotorType1SGL_MasterPricePlan> GenMotorType1SGL_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMotorType2Plus_MasterPricePlan> GenMotorType2Plus_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenMotorType3_MasterPricePlan> GenMotorType3_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenProLife8_Policy> GenProLife8_PolicySet { get; set; }
        public virtual DbSet<GenProLife8_Assured> GenProLife8_AssuredSet { get; set; }
        public virtual DbSet<GenProLife8_DCR> GenProLife8_DCRSet { get; set; }
        public virtual DbSet<GenProLife8_Payment> GenProLife8_PaymentSet { get; set; }
        public virtual DbSet<GenProLife8_FATCA> GenProLife8_FATCASet { get; set; }
        public virtual DbSet<GenProLife8_UnderWriteInfo> GenProLife8_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenProLife8_Plan> GenProLife8_PlanSet { get; set; }
        public virtual DbSet<GenProLife8_Payer> GenProLife8_PayerSet { get; set; }
        public virtual DbSet<GenProLife8_AuditTrail> GenProLife8_AuditTrailSet { get; set; }
        public virtual DbSet<GenProLife8_MasterPricePlan> GenProLife8_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenProLife8_MasterCoverage> GenProLife8_MasterCoverageSet { get; set; }
        public virtual DbSet<GenCancerLife_Policy> GenCancerLife_PolicySet { get; set; }
        public virtual DbSet<GenCancerLife_Assured> GenCancerLife_AssuredSet { get; set; }
        public virtual DbSet<GenCancerLife_Payment> GenCancerLife_PaymentSet { get; set; }
        public virtual DbSet<GenCancerLife_Plan> GenCancerLife_PlanSet { get; set; }
        public virtual DbSet<GenCancerLife_DCR> GenCancerLife_DCRSet { get; set; }
        public virtual DbSet<GenCancerLife_FATCA> GenCancerLife_FATCASet { get; set; }
        public virtual DbSet<GenCancerLife_UnderWriteInfo> GenCancerLife_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenCancerLife_MasterCoverage> GenCancerLife_MasterCoverageSet { get; set; }
        public virtual DbSet<GenCancerLife_Payer> GenCancerLife_PayerSet { get; set; }
        public virtual DbSet<GenCancerLife_AuditTrail> GenCancerLife_AuditTrailSet { get; set; }
        public virtual DbSet<GenCancerLife_MasterPricePlan> GenCancerLife_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenExclusivePA_Payer> GenExclusivePA_PayerSet { get; set; }
        public virtual DbSet<GenShieldPA_Payer> GenShieldPA_PayerSet { get; set; }
        public virtual DbSet<SupervisorTargetType> SupervisorTargetTypeSet { get; set; }
        public virtual DbSet<SupervisorTarget> SupervisorTargetSet { get; set; }
        public virtual DbSet<GenSenior55_Policy> GenSenior55_PolicySet { get; set; }
        public virtual DbSet<GenSenior55_Assured> GenSenior55_AssuredSet { get; set; }
        public virtual DbSet<GenSenior55_Payment> GenSenior55_PaymentSet { get; set; }
        public virtual DbSet<GenSenior55_Plan> GenSenior55_PlanSet { get; set; }
        public virtual DbSet<GenSenior55_DCR> GenSenior55_DCRSet { get; set; }
        public virtual DbSet<GenSenior55_FATCA> GenSenior55_FATCASet { get; set; }
        public virtual DbSet<GenSenior55_UnderWriteInfo> GenSenior55_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenSenior55_MasterCoverage> GenSenior55_MasterCoverageSet { get; set; }
        public virtual DbSet<GenSenior55_Payer> GenSenior55_PayerSet { get; set; }
        public virtual DbSet<GenSenior55_AuditTrail> GenSenior55_AuditTrailSet { get; set; }
        public virtual DbSet<GenSenior55_MasterPricePlan> GenSenior55_MasterPricePlanSet { get; set; }
        public virtual DbSet<GenPABonus_Policy> GenPABonus_PolicySet { get; set; }
        public virtual DbSet<GenPABonus_Assured> GenPABonus_AssuredSet { get; set; }
        public virtual DbSet<GenPABonus_Payment> GenPABonus_PaymentSet { get; set; }
        public virtual DbSet<GenPABonus_Plan> GenPABonus_PlanSet { get; set; }
        public virtual DbSet<GenPABonus_DCR> GenPABonus_DCRSet { get; set; }
        public virtual DbSet<GenPABonus_FATCA> GenPABonus_FATCASet { get; set; }
        public virtual DbSet<GenPABonus_UnderWriteInfo> GenPABonus_UnderWriteInfoSet { get; set; }
        public virtual DbSet<GenPABonus_Payer> GenPABonus_PayerSet { get; set; }
        public virtual DbSet<GenPABonus_AuditTrail> GenPABonus_AuditTrailSet { get; set; }
        public virtual DbSet<GenPABonus_MasterPricePlan> GenPABonus_MasterPricePlanSet { get; set; }
        public virtual DbSet<TSRTarget> TSRTargetSet { get; set; }
    }
}
