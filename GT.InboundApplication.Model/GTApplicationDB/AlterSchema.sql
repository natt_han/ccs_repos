﻿--20201101 HLS Revamp + GEB
ALTER table [GenHealthLumpSum_MasterPricePlanSet] Add PersonalAccident Decimal(18,2) null default 0,Dental Decimal(18,2) null default 0, NursingService Decimal(18,2) null default 0,IsGEB NVARCHAR(1) null;
ALTER table [GenHealthLumpSum_PolicySet] Add GEBCard varbinary(max) null;
ALTER table MasterPolicySet Add GEBCard NVARCHAR(1) null;
ALTER table [GTCRM_DB].dbo.[LeadProfileSet] Add GEBCard nvarchar(max) null;
--ALTER PROCEDURE [dbo].[PROC_SYNC_MASTER_POLICY]
--ALTER TRIGGER [dbo].[MasterPolicy_U_UpdateLogs]
--ALTER PROCEDURE [dbo].[PROC_LEADINTEGRATION_GetLeadTransaction]

-- 20200929 Add Reject Sub Question
ALTER TABLE GenHealthLumpSum_UnderWriteInfoSet ADD Answer02_01 nvarchar(max) NULL,Answer02_02 nvarchar(max) NULL,Answer02_03 nvarchar(max) NULL ;
ALTER TABLE GenCancer_UnderWriteInfoSet ADD Answer05_01 nvarchar(max) NULL,Answer05_02 nvarchar(max) NULL,Answer05_03 nvarchar(max) NULL ;
ALTER TABLE GenProLife8_UnderWriteInfoSet ADD Answer02_03 nvarchar(max) NULL ;
ALTER TABLE GenCancerLife_UnderWriteInfoSet ADD Answer01_01 nvarchar(max) NULL,Answer01_02 nvarchar(max) NULL,Answer01_03 nvarchar(max) NULL ;
-- 20200624 Add ConsentFlag to MasterPolicy
ALTER TABLE MasterPolicySet ADD [ConsentFlag] nvarchar(max)  NULL;
GO;
-- 20200512 New Product
CREATE TRIGGER [dbo].[GenPABonus_IU_SyncToMaster] 
   ON  [dbo].[GenPABonus_PolicySet]
   AFTER INSERT,UPDATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @proposalId bigint;
	if (select count(0) from inserted)=1
		begin
			select @proposalId = ProposalId from inserted;
			EXEC PROC_SYNC_MASTER_POLICY 'GENPABONUS',@proposalId;
		end;
	else
	begin
		DECLARE cur CURSOR FORWARD_ONLY READ_ONLY LOCAL FOR
        SELECT ProposalId FROM INSERTED
		OPEN cur
		FETCH NEXT FROM cur INTO @proposalId

		WHILE @@FETCH_STATUS = 0 BEGIN
			EXEC PROC_SYNC_MASTER_POLICY 'GENPABONUS',@proposalId;
			FETCH NEXT FROM cur INTO @proposalId
		END

		CLOSE cur
		DEALLOCATE cur
	end;
END
GO

CREATE TRIGGER [dbo].[GenSenior55_IU_SyncToMaster]
   ON  [dbo].[GenSenior55_PolicySet]
   AFTER INSERT,UPDATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @proposalId bigint;
	if (select count(0) from inserted)=1
		begin
			select @proposalId = ProposalId from inserted;
			EXEC PROC_SYNC_MASTER_POLICY 'GENSENIOR55',@proposalId;
		end;
	else
	begin
		DECLARE cur CURSOR FORWARD_ONLY READ_ONLY LOCAL FOR
        SELECT ProposalId FROM INSERTED
		OPEN cur
		FETCH NEXT FROM cur INTO @proposalId

		WHILE @@FETCH_STATUS = 0 BEGIN
			EXEC PROC_SYNC_MASTER_POLICY 'GENSENIOR55',@proposalId;
			FETCH NEXT FROM cur INTO @proposalId
		END

		CLOSE cur
		DEALLOCATE cur
	end;
END
GO
---------------------------------------
﻿-- 20200505 add payer consent
alter table [dbo].[GenHealthLumpSum_PayerSet] add [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL;
alter table [dbo].[GenCancer_PayerSet] add [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL;
alter table [dbo].[GenProLife8_PayerSet] add [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL;
alter table [dbo].[GenCancerLife_PayerSet] add [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL;
alter table [dbo].[GenPABonus_PayerSet] add [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL;



alter table [dbo].[GenHealthLumpSum_PolicySet] add ProspectId bigint;
alter table [dbo].[GenCancer_PolicySet] add ProspectId bigint;
alter table [dbo].[GenCancerLife_PolicySet] add ProspectId bigint;
alter table [dbo].[GenProLife8_PolicySet] add ProspectId bigint;
GO
ALTER TRIGGER [dbo].[TRG_Update_MasterPolicy]
   ON  [dbo].[Prospect_SaleActivitySet]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	update m set SuccessDate=a.CreatedDate from MasterPolicySet m  with (nolock)
	join inserted s on m.APPLICATIONNO=s.ApplicationNo and s.ResponseReasonCode='4'
	join Prospect_ActivitySet a  with (nolock) on a.itemid=s.Prospect_Activity_ItemId
	where s.ApplicationNo <>'';

	---- case delete
	if(exists(SELECT 1 FROM DELETED) and not exists(SELECT 1 FROM INSERTED))
	begin
		--insert into DebugLog (LogText) values ('Delete:');
		update m set SuccessDate=null from MasterPolicySet m  with (nolock)
		join DELETED s on m.APPLICATIONNO=s.ApplicationNo and s.ResponseReasonCode='4'
		join Prospect_ActivitySet a  with (nolock) on a.itemid=s.Prospect_Activity_ItemId
		where s.ApplicationNo <>'';
	end
END
GO
