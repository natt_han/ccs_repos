﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using DM.Utilities.NetFramework.Models;

using Newtonsoft.Json;

namespace GT.InboundApplication.Model.GTApplicationDB
{

    public static class IGTAppExtension
    {
        

        public static System.Data.Entity.Infrastructure.DbSqlQuery<MasterPolicy> SearchLead(this IGTAppDBContainer db, string telphone, string telephone2, string telephone2_ex, string productcate)
        {
            
            return db.MasterPolicySet.SqlQuery(@"select m.* from MasterPolicySet m left join Prospect_Sale p on l.ProductCode=p.OfferProductId 
where l.status=1 and l.LeadFlag in ('N','O') and TelPhone=@p0 and ISNULL(p.Category,'Health')=@p1", telphone, productcate);
               
        }

        public static IQueryable<MasterPolicy> MotorSet(this DbSet<MasterPolicy> dbset)
        {
            var motorproducts = new String[]{"GENMOTORTYPE1STD","GENMOTORTYPE2PLUS","GENMOTORTYPE1SGL","GENMOTORTYPE3"};//"GENMOTORCMI"
            return dbset.Where(c => motorproducts.Contains(c.PRODUCTNAME));
        }

        public static IQueryable<MasterPolicy> ActiveSet(this DbSet<MasterPolicy> dbset)
        {            
            return dbset.Where(c => !(c.ApplicationStatus == "CA" && c.SuccessDate == null));
        }

        public static IQueryable<Relation> ActiveBeneficiarySet(this DbSet<Relation> dbset, bool isAUW = false)
        {

            return dbset.Where(c => (c.DisplayFor == 1 || c.DisplayFor == 2) && c.Active && (isAUW ? c.ForAUW == true : c.ForAUW == c.ForAUW));
        }

        public static IQueryable<Relation> ActivePayerSet(this DbSet<Relation> dbset, bool isAUW = false)
        {
            return dbset.Where(c => (c.DisplayFor == 1 || c.DisplayFor == 3) && c.Active && (isAUW ? c.ForAUW == true : c.ForAUW == c.ForAUW));
        }
    }

    public partial class IGTAppDBContainer
    {
        public virtual IQueryable<MasterPolicy> ActiveMasterPolicySet { get { return MasterPolicySet.ActiveSet(); } }
    }

    public partial class GenHealthLumpSum_UnderWriteInfo
    {        
        public GT.InboundApplication.Model.ViewModels.HLSAdditionalQuestionViewModel AdditionalQuestion { get; set; }
        public GT.InboundApplication.Model.ViewModels.HLSSymptomViewModel SymptomAnswerVM { get; set; }

        public GenHealthLumpSum_UnderWriteInfo()
        {
            AdditionalQuestion = new ViewModels.HLSAdditionalQuestionViewModel();
            SymptomAnswerVM = new ViewModels.HLSSymptomViewModel();
        }

        public void SerializeDiseaseAnswer()
        {
            this.DiseaseAnswer = JsonConvert.SerializeObject(this.AdditionalQuestion);
        }

        public void DeserializeDiseaseAnswer()
        {
            if (!this.DiseaseAnswer.IsNullOrEmpty())
            {
                this.AdditionalQuestion = JsonConvert.DeserializeObject<ViewModels.HLSAdditionalQuestionViewModel>(this.DiseaseAnswer);
            }
            else
            {
                this.AdditionalQuestion = new ViewModels.HLSAdditionalQuestionViewModel();
            }
            
        }

        public string DiseaseAnswerRemark
        {
            get
            {
                StringBuilder rem = new StringBuilder();
                if (this.DiseaseAnswer.IsNullOrEmpty()) return "";
                this.DeserializeDiseaseAnswer();
                if (this.AdditionalQuestion == null) return "";
                if (AdditionalQuestion.HypertensionFlag)
                {
                    rem.AppendLine("โรคความดันโลหิตสูง");
                    if (AdditionalQuestion.HypertensionAnswer01 == "Yes") rem.AppendLine("- เป็นโรคความดันโลหิตสูงไม่เกิน 10 ปี");
                    if (AdditionalQuestion.HypertensionAnswer02 == "Yes") rem.AppendLine("- กำลังรักษาอยู่ พบแพทย์รับยาตามนัด อย่างสม่ำเสมอ ไม่มีการหยุดยาเอง");
                    if (AdditionalQuestion.HypertensionAnswer03 == "Yes") rem.AppendLine(@"- ระดับความดันโลหิตภายใน 6 เดือนที่ผ่านมาอยู่ในเกณฑ์ 140 / 90 มิลลิเมตรปรอท ทุกครั้ง
และไม่เคยมีอาการผิดปกติเช่น  เจ็บหน้าอก/ เหนื่อยง่าย / ชาตามร่างกาย / หมดสติ /ตามัว ");
                    if (AdditionalQuestion.HypertensionAnswer04 == "Yes") rem.AppendLine("- เคยตรวจเอ็กซเรย์ปอด / ตรวจคลื่นไฟฟ้าหัวใจ ผลการตรวจนั้นปกติทุกครั้ง");
                    if (AdditionalQuestion.HypertensionAnswer05 == "Yes") rem.AppendLine("- ไม่เคยถูกแพทย์วินิจฉัยว่ามีภาวะแทรกซ้อนอื่นๆ");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.HypertensionRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.HypertensionRemark);
                }
                if (AdditionalQuestion.DyslipidemiaFlag)
                {
                    rem.AppendLine("ภาวะไขมันในเลือดสูง");
                    if (AdditionalQuestion.DyslipidemiaAnswer01 == "Yes") rem.AppendLine("- ระดับโคเลสเตอรอลภายใน 1 ปีที่ผ่านมาอยู่ในเกณฑ์ ไม่เกิน 300 มิลลิกรัม/เดซิลิตร ทุกครั้ง");
                    if (AdditionalQuestion.DyslipidemiaAnswer02 == "Yes") rem.AppendLine("- ระดับไขมันไม่ดี (LDL) ภายใน 1 ปีที่ผ่านมาอยู่ในเกณฑ์ ไม่เกิน 180 มิลลิกรัม/เดซิลิตร ทุกครั้ง");
                    if (AdditionalQuestion.DyslipidemiaAnswer03 == "Yes") rem.AppendLine("- ระดับไตรกลีเซอร์ไรด์ (Triglyceride) ภายใน 1 ปีที่ผ่านมาอยู่ในเกณฑ์ ไม่เกิน  มิลลิกรัม/เดซิลิตร ทุกครั้ง");
                    if (AdditionalQuestion.DyslipidemiaAnswer04 == "Yes") rem.AppendLine("- ไม่สูบบุหรี่  หรือสูบไม่เกินวันละ 10 มวน / วัน");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.DyslipidemiaRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.DyslipidemiaRemark);
                }
                if (AdditionalQuestion.SinusitisFlag)
                {
                    rem.AppendLine("โรคไซนัสอักเสบ / โพรงจมูกอักเสบ");
                    if (AdditionalQuestion.SinusitisAnswer01 == "Yes") rem.AppendLine("- รักษาด้วยวิธีการทานยาเป็นครั้งคราว หรือทานยาสม่ำเสมอ");
                    if (AdditionalQuestion.SinusitisAnswer02 == "Yes") rem.AppendLine("- ผ่าตัดแล้วและผลตรวจชิ้นเนื้อปกติไม่ใช่เนื้อร้าย แต่ปัจจุบันยังมีอาการของโรคไซนัสอักเสบ");
                    if (AdditionalQuestion.SinusitisAnswer03 == "Yes") rem.AppendLine("- ผ่าตัดแล้วและผลตรวจชิ้นเนื้อปกติไม่ใช่เนื้อร้ายและปัจจุบันไม่มีอาการที่เกี่ยวข้องกับไซนัสอักเสบแล้ว");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.SinusitisRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.SinusitisRemark);
                }
                if (AdditionalQuestion.AllergyFlag)
                {
                    rem.AppendLine("โรคภูมิแพ้");
                    if (AdditionalQuestion.AllergyAnswer01 == "Yes") rem.AppendFormat("- เคยเข้ารับการปรึกษาแพทย์และได้รับการตรวจวินิจฉัยว่าเป็น{0} ({1})", AdditionalQuestion.AllergyAnswer01_Rem01, AdditionalQuestion.AllergyAnswer01_Rem02).AppendLine();
                    if (AdditionalQuestion.AllergyAnswer02 == "Yes") rem.AppendLine("- ไม่เคยได้รับการตรวจวินิจฉัยว่าเป็น โรคภูมิแพ้ตัวเอง (โรคเอสแอลอี)");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.AllergyRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.AllergyRemark);
                }
                if (AdditionalQuestion.GERDFlag)
                {
                    rem.AppendLine("โรคกรดไหลย้อน");
                    if (AdditionalQuestion.GERDAnswer01 == "Yes") rem.AppendLine("- มีอาการ / ได้รับการวินิจฉัยว่ามีภาวะโรคกรดไหลย้อนภายในไม่เกิน 5 ปีที่ผ่านมา");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.GERDRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.GERDRemark);
                }
                if (AdditionalQuestion.GastritisFlag)
                {
                    rem.AppendLine("โรคกระเพาะอาหารอักเสบ");
                    if (AdditionalQuestion.GastritisAnswer01 == "Yes") rem.AppendLine("- เป็นกระเพาะอาหารอักเสบและรักษาด้วยวิธีการรับประทานยาเท่านั้น โดยที่แพทย์ไม่ได้แจ้งว่าจะต้องทำการตรวจวินิฉัยเพิ่มเติมด้วยวิธีการส่องกล้อง หรือเอ็กซเรย์คอมพิวเตอร์ หรือ MRI หรือต้องผ่าตัด");
                    if (AdditionalQuestion.GastritisAnswer02 == "Yes") rem.AppendLine("- เคยตรวจวินิจฉัยเพิ่มเติมด้วยวิธีการส่องกล้อง หรือเอ็กซเรย์คอมพิวเตอร์ หรือ MRI แพทย์ได้แจ้งผลการตรวจให้ทราบว่าไม่มีแผลในกระเพาะอาหาร  / ไม่มีเนื้องอก / ไม่มีซีสหรือถุงน้ำ หรือไม่พบความผิดปกติใดๆ");
                    if (AdditionalQuestion.GastritisAnswer02 == "Unknown") rem.AppendLine("- ไม่เคยตรวจ");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.GastritisRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.GastritisRemark);
                }
                if (AdditionalQuestion.HemorrhoidFlag)
                {
                    rem.AppendLine("โรคริดสีดวงทวาร");
                    if (AdditionalQuestion.HemorrhoidAnswer01 == "Yes") rem.AppendLine("- มีอาการของริดสีดวงทวารและยังไม่ได้รักษาด้วยวิธีการผ่าตัด");
                    if (AdditionalQuestion.HemorrhoidAnswer02 == "Yes") rem.AppendLine("- ไม่เคยมีอาการเลือดออกนานติดต่อกันกว่า 1 สัปดาห์หรือเลือดออกเป็นๆหายๆบ่อยๆ โดยที่ไม่ทราบสาเหตุ");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.HemorrhoidRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.HemorrhoidRemark);                    
                }
                if (AdditionalQuestion.CataractFlag)
                {
                    rem.AppendLine("โรคต้อเนื้อ / ต้อกระจก");
                    if (AdditionalQuestion.CataractAnswer01 == "Yes") rem.Append("- ยังไม่ได้ทำการผ่าตัด ").AppendLine(AdditionalQuestion.CataractAnswer01_Rem01);
                    if (AdditionalQuestion.CataractAnswer02 == "Yes") rem.AppendLine("- ไม่เคยได้รับการวินิจฉัยว่าเป็น ต้อหิน");
                    if (AdditionalQuestion.CataractAnswer03 == "Yes") rem.AppendLine("- มีอาการมองเห็นได้ไม่ค่อยชัดแต่ยังไม่สูญเสียการมองเห็น");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.CataractRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.CataractRemark);
                }
                if (AdditionalQuestion.GallstoneFlag)
                {
                    rem.AppendLine("โรคนิ่วในถุงน้ำดี");
                    if (AdditionalQuestion.GallstoneAnswer01 == "Yes") rem.AppendLine("- ได้รับการวินิจฉัยโรคนิ่วในถุงน้ำดี แต่ยังไม่ได้รับการรักษาด้วยวิธีการผ่าตัด");
                    if (AdditionalQuestion.GallstoneAnswer02 == "Yes") rem.AppendLine("- ผ่าตัดแล้วผลตรวจชิ้นเนื้อทางด้านพยาธิวิทยา  เป็นชิ้นเนื้อปกติ ไม่ใช่เนื้อร้าย ไม่มีภาวะแทรกซ้อนจากการผ่าตัดและปัจจุบันจบการรักษาแล้ว");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.GallstoneRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.GallstoneRemark);
                }
                if (AdditionalQuestion.KidneystoneFlag)
                {
                    rem.AppendLine("โรคนิ่วในไตหรือนิ่วในระบบทางเดินปัสสาวะ");
                    if (AdditionalQuestion.KidneystoneAnswer01 == "Yes") rem.AppendLine("- เคยเข้ารับการปรึกษาแพทย์ หรือได้รับการตรวจวินิจฉัย อาทิ เช่น เอ็กซเรย์ /เอ็กซเรย์คอมพิวเตอร์ / การตรวจอัลตร้าซาวน์ หรือการตรวจอื่นๆที่วินิจฉัยว่าเป็นโรคนิ่วในไตหรือนิ่วในระบบทางเดินปัสสาวะ");
                    if (AdditionalQuestion.KidneystoneAnswer02 == "Yes") rem.AppendLine("- เป็นการรักษาด้วยวิธีการส่องกล้อง  หรือการสลายนิ่วด้วยคลื่นกระแทก (shock wave) หรือการผ่าตัดภายใน 1 ปีที่ผ่านมา");
                    if (!string.IsNullOrEmpty(AdditionalQuestion.KidneystoneRemark)) rem.Append("- ").AppendLine(AdditionalQuestion.KidneystoneRemark);
                }
                var exclusion = this.GetExclusion();
                if (AdditionalQuestion.ReadExclusionFlag && exclusion != "")
                {
                    rem.AppendFormat("(TSR ได้ดำเนินการแจ้งข้อเสนอใหม่ดังนี้ ไม่คุ้มครองการรักษา {0})", exclusion);
                }
                return rem.ToString();
            }
        }

        public string GetExclusion()
        {            
            var o = this.AdditionalQuestion;
            var exclusion = "";
            if (o.HypertensionFlag && o.HypertensionAnswer01 == "Yes" && o.HypertensionAnswer02 == "Yes" && o.HypertensionAnswer03 == "Yes" && (o.HypertensionAnswer04 == "Yes" || o.HypertensionAnswer04 == "Unknown") && o.HypertensionAnswer05 == "Yes")
            {
                exclusion += ", โรคความดันโลหิตสูง";
            }
            if (o.DyslipidemiaFlag && o.DyslipidemiaAnswer01 == "Yes" && (o.DyslipidemiaAnswer02 == "Yes" || o.DyslipidemiaAnswer02 == "Unknown") && (o.DyslipidemiaAnswer03 == "Yes" || o.DyslipidemiaAnswer03 == "Unknown") && o.DyslipidemiaAnswer04 == "Yes")
            {
                exclusion += ", ภาวะไขมันในเลือดสูง";
            }
            if (o.SinusitisFlag && o.SinusitisAnswer01 == "Yes")
            {
                exclusion += ", โรคไซนัสอักเสบ / โพรงจมูกอักเสบ";
            }
            if (o.AllergyFlag && o.AllergyAnswer01 == "Yes" && o.AllergyAnswer02=="Yes")
            {
                exclusion += ", "+o.AllergyAnswer01_Rem01;
            }
            if (o.GERDFlag && o.GERDAnswer01 == "Yes")
            {
                exclusion += ", โรคกรดไหลย้อน";
            }
            if (o.GastritisFlag && o.GastritisAnswer01 == "Yes" && (o.GastritisAnswer02 == "Yes" || o.GastritisAnswer02 == "Unknown"))
            {
                exclusion += ", โรคกระเพาะอาหารอักเสบ";
            }
            if (o.HemorrhoidFlag && o.HemorrhoidAnswer01 == "Yes" && o.HemorrhoidAnswer02 == "Yes")
            {
                exclusion += ", โรคริดสีดวงทวาร";
            }
            if (o.CataractFlag && o.CataractAnswer01 == "Yes" && o.CataractAnswer02 == "Yes" && o.CataractAnswer03 == "Yes")
            {
                exclusion += ", " + o.CataractAnswer01_Rem01;
            }
            if (o.GallstoneFlag && o.GallstoneAnswer01 == "Yes")
            {
                exclusion += ", โรคนิ่วในถุงน้ำดี";
            }
            if (o.KidneystoneFlag && (o.KidneystoneAnswer01 == "Yes" || o.KidneystoneAnswer01 == "Yes"))
            {
                exclusion += ", โรคนิ่วในระบบทางเดินปัสสาวะ";
            }
            return string.IsNullOrEmpty(exclusion)?"":exclusion.Substring(2);
        }

        public void SerializeSymtomAnswer()
        {
            this.SymptomAnswer = JsonConvert.SerializeObject(this.SymptomAnswerVM);
        }

        public void DeserializeSymtomAnswer()
        {
            if (!this.SymptomAnswer.IsNullOrEmpty())
            {
                this.SymptomAnswerVM = JsonConvert.DeserializeObject<ViewModels.HLSSymptomViewModel>(this.SymptomAnswer);
            }
            else
            {
                this.SymptomAnswerVM = new ViewModels.HLSSymptomViewModel();
            }
        }

        private string TreatDesc(string val)
        {
            switch (val)
            {
                case "Treated": return "จบการรักษา";
                case "InProgress": return "อยู่ระหว่างการรักษา";
                case "NotTreated": return "ยังไม่ได้รักษา";
                default: return "";
            }
        }
        public string SymptomAnswerRemark
        {
            get
            {
                StringBuilder rem = new StringBuilder();
                if (this.SymptomAnswer.IsNullOrEmpty()) return "";
                this.DeserializeSymtomAnswer();
                if (this.SymptomAnswerVM == null) return "";
                var vm = this.SymptomAnswerVM;
                if (vm.TiredFlag)
                {
                    rem.AppendLine("เหนื่อย");
                    if (vm.TiredAnswer01 == "Yes") rem.AppendLine("- จบการรักษาและไม่มีอาการเกิน 6 เดือน");
                    if (!string.IsNullOrEmpty(vm.TiredRemark)) rem.AppendLine(string.Format("- {0}", vm.TiredRemark));
                }
                if (vm.DyspneaFlag)
                {
                    rem.AppendLine("หายใจไม่สะดวก มีการตรวจคลื่นหัวใจหรือวิ่งสายพาน");
                    if (vm.DyspneaAnswer01 == "Yes") rem.AppendLine("- มีการตรวจแบบวิ่งสายพาน ตรวจคลื่นหัวใจ มีเอกสารยืนยันผลตรวจ");
                    if (vm.DyspneaAnswer01 == "No") rem.AppendLine("- มีการตรวจสุขภาพประจำปีหลังจากที่มีการตรวจแบบวิ่งสายพาน ตรวจคลื่นหัวใจ และผลการตรวจสุขภาพประจำปี ผลปกติ");
                    if (!string.IsNullOrEmpty(vm.DyspneaRemark)) rem.AppendLine(string.Format("- {0}", vm.DyspneaRemark));
                }
                if (vm.MusclePainFlag)
                {
                    rem.AppendLine("ปวดกล้ามเนื้อ");
                    rem.AppendLine(string.Format("- เริ่มมีอาการเมื่อ : {0} , และมีอาการปวดบริเวณ : {1}",vm.MusclePainAnswer1_1,vm.MusclePainAnswer1_2));
                    rem.AppendLine(string.Format("- เป็นตั้งแต่ : {0} , อาการปัจจุบัน : {1}", vm.MusclePainAnswer2_1, vm.MusclePainAnswer2_2));
                    rem.AppendLine(string.Format("- วิธีการรักษา : {0}", vm.MusclePainAnswer3_1));
                    rem.AppendLine(string.Format(@"- สถานที่รักษา วิธีการตรวจ ผลการตรวจ : {0} 
  วันเดือนปีที่ตรวจ และจำนวนวันที่ให้นอนโรงพยาบาล : {1}
  ผลการวินิจฉัยจากแพทย์และสาเหตุที่ทำให้มีอาการปวดกล้ามเนื้อ : {2}
  วิธีการรักษาและ คำแนะนำจากแพทย์ : {3}", vm.MusclePainAnswer4_1, vm.MusclePainAnswer4_2, vm.MusclePainAnswer4_3, vm.MusclePainAnswer4_4));
                    rem.AppendLine(string.Format(@"- {0}
  การติดตามการรักษา : {1}
  วันที่จบการรักษา : {2}", vm.MusclePainAnswer5_1=="Yes"? "ปัจจุบันยังมีอาการ" : (vm.MusclePainAnswer5_1 == "No" ? "ปัจจุบันไม่มีอาการแล้ว" : ""), vm.MusclePainAnswer5_2, vm.MusclePainAnswer5_3));
                    if (!string.IsNullOrEmpty(vm.MusclePainRemark)) rem.AppendLine(string.Format("- {0}", vm.MusclePainRemark));
                }
                if (vm.HeadacheFlag)
                {
                    rem.AppendLine("ปวดศีรษะ");
                    rem.AppendLine(string.Format("- วันที่เริ่มมีอาการ และรายละเอียดอาการปวด : {0}", vm.HeadacheAnswer1_1));
                    rem.AppendLine(string.Format(@"- ช่วงเวลาที่มีอาการปวด : {0}
  จำนวนครั้งที่มีอาการปวด : {1}
  วันที่มีอาการหลังสุด : {2}", vm.HeadacheAnswer2_1, vm.HeadacheAnswer2_2, vm.HeadacheAnswer2_3));
                    rem.AppendLine(string.Format(@"- สถานที่รักษา วิธีการตรวจ ผลการตรวจ : {0} 
  วันเดือนปีที่ตรวจ และจำนวนวันที่ให้นอนโรงพยาบาล : {1}
  ผลการวินิจฉัยจากแพทย์และสาเหตุที่ทำให้มีอาการปวดศีรษะ : {2}
  วิธีการรักษาและ คำแนะนำจากแพทย์ : {3}", vm.HeadacheAnswer3_1, vm.HeadacheAnswer3_2, vm.HeadacheAnswer3_3, vm.HeadacheAnswer3_4));
                    if (vm.HeadacheAnswer4_1 == "Yes") rem.AppendLine(string.Format(" - มีการตรวจความดัน , ค่าความดัน : {0}",vm.HeadacheAnswer4_2));
                    if (vm.HeadacheAnswer4_1 == "No") rem.AppendLine("- ไม่มีการตรวจความดัน");
                    if (!string.IsNullOrEmpty(vm.HeadacheRemark)) rem.AppendLine(string.Format("- {0}", vm.HeadacheRemark));
                }
                if (vm.CovidFlag)
                {
                    rem.AppendLine("โควิด19");
                    if (!string.IsNullOrEmpty(vm.CovidAnswer1_1)) rem.AppendLine(string.Format("- {0}",this.TreatDesc(vm.CovidAnswer1_1)));
                    var swab = "";
                    if (vm.CovidAnswer2_3 == "Do") swab = string.Format("ได้รับการทำ Swab test หลังจากจบการรักษาจากโรงพยาบาล , จำนวนครั้งที่ทำ วันที่ทำ และผล : {0}",vm.CovidAnswer2_4);
                    if (vm.CovidAnswer2_3 == "Dont") swab = "ไม่มีการทำ Swab test หลังจากจบการรักษาจากโรงพยาบาล";
                    rem.AppendLine(string.Format(@"- 1. วันเดือนปีที่เป็น COVID-19 : {0}  
  2. สถานที่รักษา วันเดือนปีที่ตรวจรักษา ผลการรักษา : {1}
  3. Swab test : {2}
  4. การติดตามการรักษา หรืออาการปัจจุบัน : {3}",vm.CovidAnswer2_1,vm.CovidAnswer2_2,swab,vm.CovidAnswer2_5));
                    if (!string.IsNullOrEmpty(vm.CovidRemark)) rem.AppendLine(string.Format("- {0}", vm.CovidRemark));
                }
                if (vm.FluFlag)
                {
                    rem.AppendLine("ไข้หวัดธรรมดา หรือไข้หวัดใหญ่");
                    if (!string.IsNullOrEmpty(vm.FluAnswer1_1)) rem.AppendLine(string.Format("- {0} {1}", this.TreatDesc(vm.FluAnswer1_1),(vm.FluAnswer1_1=="Treated"?string.Format(" , วันที่จบการรักษา : {0}", vm.FluAnswer1_2):"")));
                    if (vm.FluAnswer1_3 == "Yes") rem.AppendLine("- ได้รับคำวินิจฉัยให้กักตัวเนื่องจากมีปัจจัยเสี่ยงต่อการติดเชื้อ COVID-19");
                    if (vm.FluAnswer1_3 == "No") rem.AppendLine("- ไม่ได้รับคำวินิจฉัยให้กักตัวเนื่องจากมีปัจจัยเสี่ยงต่อการติดเชื้อ COVID-19");
                    rem.AppendLine(string.Format(@"- 1. วันเดือนปีที่เริ่มมีอาการ : {0}  
  2. สถานที่รักษา วันที่ตรวจรักษา จำนวนวันที่นอนโรงพยาบาล : {1}
     วิธีการรักษาและคำแนะนำจากแพทย์ : {2}
  3. อาการปัจจุบันและการติดตามการรักษา : {3}",vm.FluAnswer2_1,vm.FluAnswer2_2,vm.FluAnswer2_3,vm.FluAnswer2_4));
                    if (!string.IsNullOrEmpty(vm.FluRemark)) rem.AppendLine(string.Format("- {0}", vm.FluRemark));
                }
                if (vm.DiarrheaFlag)
                {
                    rem.AppendLine("ท้องเสีย");
                    if (vm.DiarrheaAnswer01 == "Yes") rem.AppendLine("- ท้องเสียจากอาหารเป็นพิษ จบการรักษาและไม่มีอาการมากกว่า 1 เดือน");
                    if (vm.DiarrheaAnswer02 == "Yes") rem.AppendLine("- ท้องเสียจากไข้หวัดลงกระเพาะ จบการรักษาและไม่มีอาการมากกว่า 1 เดือน");
                    if (vm.DiarrheaAnswer03 == "Yes") rem.AppendLine("- มีภาวะแทรกซ้อน การถ่ายเป็นเลือด หรือไม่รู้สาเหตุ");
                    if (vm.DiarrheaAnswer04 == "Yes") rem.AppendLine("- มีอาการท้องเสียบ่อย เช่น ทุก 3 เดือน โดยไม่ได้พบแพทย์");
                    if (vm.DiarrheaAnswer05 == "Yes") rem.AppendLine("- มีอาการท้องเสียยังรอผลการตรวจ และ/หรือ ยังรักษาอยู่");
                    if (!string.IsNullOrEmpty(vm.DiarrheaRemark)) rem.AppendLine(string.Format("- {0}", vm.DiarrheaRemark));
                }
                if (vm.AppendicitisFlag)
                {
                    rem.AppendLine("ไส้ติ่ง");
                    if (vm.AppendicitisAnswer01 == "Yes") rem.AppendLine("- ผ่าตัดและจบการรักษามามากกว่า 6 เดือน");
                    if (vm.AppendicitisAnswer01 == "No") rem.AppendLine("- จบการรักษามาน้อยกว่า 6 เดือน");
                    if (!string.IsNullOrEmpty(vm.AppendicitisRemark)) rem.AppendLine(string.Format("- {0}", vm.AppendicitisRemark));
                }
                if (vm.CystFlag)
                {
                    rem.AppendLine("เนื้องอก ถุงน้ำ");
                    if (vm.CystAnswer01 == "Yes") rem.AppendLine("- ผ่าตัด ยังมีการรักษาหรือจบการรักษา มาน้อยกว่า 6 เดือน");
                    if (vm.CystAnswer02 == "Yes") rem.AppendLine("- ผ่าตัด ผลชิ้นเนื้อปกติ และจบการรักษา มามากกว่า 6 เดือน");
                    if (vm.CystAnswer03 == "Yes") rem.AppendLine("- ผ่าตัด ผลชิ้นเนื้อปกติ และจบการรักษา มามากกว่า 2 ปี");
                    if (!string.IsNullOrEmpty(vm.CystRemark)) rem.AppendLine(string.Format("- {0}", vm.CystRemark));
                }
                return rem.ToString();
            }            
        }
    }

    public partial class TSRTarget
    {
        [NotMapped]
        public string TSRName { get; set; }
    }
}
