
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/09/2020 15:45:35
-- Generated from EDMX file: C:\Users\witsaruth\Documents\Visual Studio 2013\Projects\sst_repos\GT.InboundApplication.Model\GTApplicationDB\IGTAppDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GTApplication_TDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_OccupationOccupationClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OccupationSet] DROP CONSTRAINT [FK_OccupationOccupationClass];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_DCRSet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_DCRSet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_LeadSourceProduct]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LeadSourceSet] DROP CONSTRAINT [FK_LeadSourceProduct];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet] DROP CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHealthProtect_PolicyGenHealthProtect_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHealthProtect_PolicySet] DROP CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_DCRSet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_DCRSet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_DCRSet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMax7_PolicyGenMax7_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMax7_PolicySet] DROP CONSTRAINT [FK_GenMax7_PolicyGenMax7_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_DCRSet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenLifePlus10_PolicyGenLifePlus10_UnderwriterInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenLifePlus10_PolicySet] DROP CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_UnderwriterInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_UnderwriterInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_UnderwriterInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_PolicySet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSmart5_PolicyGenSmart5_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSmart5_DCRSet] DROP CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_DCRSet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSabye4_PolicyGenSabye4_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSabye4_PolicySet] DROP CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_DCRSet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_UnderWriterInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_UnderWriterInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_Payment1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Payment1];
GO
IF OBJECT_ID(N'[dbo].[FK_GenHighLife3_PolicyGenHighLife3_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenHighLife3_PolicySet] DROP CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_DCRSet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary1];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary2]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary2];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary3];
GO
IF OBJECT_ID(N'[dbo].[FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenFamilyPA_PolicySet] DROP CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary4];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_DCRSet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenAomSub6_PolicyGenAomSub6_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenAomSub6_PolicySet] DROP CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_DCRSet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Driver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Driver];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_OldPolicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_OldPolicy];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_TaxInvoice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_TaxInvoice];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleBrandVehicleModel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_VehicleModelSet] DROP CONSTRAINT [FK_VehicleBrandVehicleModel];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleTypeVehicleModel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_VehicleModelSet] DROP CONSTRAINT [FK_VehicleTypeVehicleModel];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Inspection]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Inspection];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_OldPolicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_OldPolicy];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Driver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Driver];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Inspection]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Inspection];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_DCRSet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_TaxInvoice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_TaxInvoice];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_DCRSet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_OldPolicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_OldPolicy];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_TaxInvoice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] DROP CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_TaxInvoice];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_DCRSet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotor_CMI_PolicyGenMotor_CMI_TaxInvoice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotor_CMI_PolicySet] DROP CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_TaxInvoice];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancel_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancel_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_PolicySet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancer_PolicyGenCancer_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancer_DCRSet] DROP CONSTRAINT [FK_GenCancer_PolicyGenCancer_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_CTPL]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_CTPL];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Driver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Driver];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Inspection]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Inspection];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_OldPolicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_OldPolicy];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_DCRSet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_PlanDiscount]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_PlanDiscount];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_PolicySet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_PolicySet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_Employee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_EmployeeSet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Employee];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_PolicySet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_DCRSet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenGroupSME_PolicyGenGroupSME_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenGroupSME_PolicySet] DROP CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenMotorType1Std_PolicyGenMotorType1Std_ReferDocument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenMotorType1Std_PolicySet] DROP CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_ReferDocument];
GO
IF OBJECT_ID(N'[dbo].[FK_MasterPolicy_DeliveryMasterPolicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MasterPolicy_DeliverySet] DROP CONSTRAINT [FK_MasterPolicy_DeliveryMasterPolicy];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_DCRSet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenProLife8_PolicyGenProLife8_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenProLife8_PolicySet] DROP CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_DCRSet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenCancerLife_PolicyGenCancerLife_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenCancerLife_PolicySet] DROP CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenExclusivePA_PolicyGenExclusivePA_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenExclusivePA_PolicySet] DROP CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenShieldPA_PolicyGenShieldPA_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenShieldPA_PolicySet] DROP CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_SupervisorTargetSet_SupervisorTargetTypeSet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SupervisorTargetSet] DROP CONSTRAINT [FK_SupervisorTargetSet_SupervisorTargetTypeSet];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGen_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGen_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_DCRSet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenSenior55_PolicyGenSenior55_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenSenior55_PolicySet] DROP CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Assured];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_UnderWriteInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_UnderWriteInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_Plan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Plan];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_Payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Payment];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGen_Payer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGen_Payer];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_FATCA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_FATCA];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_DCR]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_DCRSet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_DCR];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_AuditTrail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_AuditTrail];
GO
IF OBJECT_ID(N'[dbo].[FK_GenPABonus_PolicyGenPABonus_Assured]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GenPABonus_PolicySet] DROP CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Assured];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[SalutationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SalutationSet];
GO
IF OBJECT_ID(N'[dbo].[OccupationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OccupationSet];
GO
IF OBJECT_ID(N'[dbo].[NationalitySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NationalitySet];
GO
IF OBJECT_ID(N'[dbo].[ClientStatusSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientStatusSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[ProductSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSet];
GO
IF OBJECT_ID(N'[dbo].[GTBankAccountSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GTBankAccountSet];
GO
IF OBJECT_ID(N'[dbo].[CreditCardTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CreditCardTypeSet];
GO
IF OBJECT_ID(N'[dbo].[OccupationClassSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OccupationClassSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[RelationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelationSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[LeadSourceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LeadSourceSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[LifeSalutationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LifeSalutationSet];
GO
IF OBJECT_ID(N'[dbo].[LifePayLifeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LifePayLifeSet];
GO
IF OBJECT_ID(N'[dbo].[LifeGTBankAccountSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LifeGTBankAccountSet];
GO
IF OBJECT_ID(N'[dbo].[ReligionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ReligionSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_MasterPlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_MasterPlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_MasterPlanCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_MasterPlanCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthProtect_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthProtect_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMax7_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMax7_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenLifePlus10_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenLifePlus10_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSmart5_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSmart5_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSabye4_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSabye4_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenHighLife3_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHighLife3_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_Subsidiary1Set]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_Subsidiary1Set];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_Subsidiary2Set]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_Subsidiary2Set];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_Subsidiary3Set]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_Subsidiary3Set];
GO
IF OBJECT_ID(N'[dbo].[GenFamilyPA_Subsidiary4Set]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenFamilyPA_Subsidiary4Set];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenAomSub6_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenAomSub6_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_OldPolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_OldPolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_TaxInvoiceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_TaxInvoiceSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_VehicleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_VehicleSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_DriverSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_DriverSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_VehicleBrandSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_VehicleBrandSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_VehicleModelSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_VehicleModelSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_VehicleTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_VehicleTypeSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1_InspectionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1_InspectionSet];
GO
IF OBJECT_ID(N'[dbo].[GenHealthLumpSum_Plan1Set]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenHealthLumpSum_Plan1Set];
GO
IF OBJECT_ID(N'[dbo].[ValuePreferenceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ValuePreferenceSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_VehicleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_VehicleSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_OldPolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_OldPolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_DriverSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_DriverSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_InspectionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_InspectionSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_TaxInvoiceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_TaxInvoiceSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_VehicleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_VehicleSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_OldPolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_OldPolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_TaxInvoiceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_TaxInvoiceSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL2Plus_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL2Plus_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_VOL1Std_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_VOL1Std_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_VehicleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_VehicleSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotor_CMI_TaxInvoiceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotor_CMI_TaxInvoiceSet];
GO
IF OBJECT_ID(N'[dbo].[VehicleBrandSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleBrandSet];
GO
IF OBJECT_ID(N'[dbo].[MenuPreferenceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenuPreferenceSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancer_LegalScriptTemplateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancer_LegalScriptTemplateSet];
GO
IF OBJECT_ID(N'[dbo].[AddressTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AddressTypeSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_CTPLSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_CTPLSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_InspectionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_InspectionSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_DriverSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_DriverSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_OldPolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_OldPolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_VehicleSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_VehicleSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_PlanDiscountSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_PlanDiscountSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_EmployeeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_EmployeeSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenGroupSME_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenGroupSME_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[CreditCardClassTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CreditCardClassTypeSet];
GO
IF OBJECT_ID(N'[dbo].[MotorConstantSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MotorConstantSet];
GO
IF OBJECT_ID(N'[dbo].[MotorPolicyNumberSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MotorPolicyNumberSet];
GO
IF OBJECT_ID(N'[dbo].[GroupCompanySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupCompanySet];
GO
IF OBJECT_ID(N'[dbo].[PaymentMethodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaymentMethodSet];
GO
IF OBJECT_ID(N'[dbo].[MasterPolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MasterPolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1Std_ReferDocumentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1Std_ReferDocumentSet];
GO
IF OBJECT_ID(N'[dbo].[MotorModelSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MotorModelSet];
GO
IF OBJECT_ID(N'[dbo].[MasterPolicy_DeliverySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MasterPolicy_DeliverySet];
GO
IF OBJECT_ID(N'[dbo].[UWRejectReasonSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UWRejectReasonSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType1SGL_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType1SGL_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType2Plus_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType2Plus_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenMotorType3_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenMotorType3_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenProLife8_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenProLife8_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenCancerLife_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenCancerLife_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenExclusivePA_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenExclusivePA_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenShieldPA_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenShieldPA_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[SupervisorTargetTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SupervisorTargetTypeSet];
GO
IF OBJECT_ID(N'[dbo].[SupervisorTargetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SupervisorTargetSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_MasterCoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_MasterCoverageSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenSenior55_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenSenior55_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_PolicySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_PolicySet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_AssuredSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_AssuredSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_PaymentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_PaymentSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_PlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_PlanSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_DCRSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_DCRSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_FATCASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_FATCASet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_UnderWriteInfoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_UnderWriteInfoSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_PayerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_PayerSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_AuditTrailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_AuditTrailSet];
GO
IF OBJECT_ID(N'[dbo].[GenPABonus_MasterPricePlanSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenPABonus_MasterPricePlanSet];
GO
IF OBJECT_ID(N'[dbo].[TSRTargetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TSRTargetSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'GenHealthLumpSum_PaymentSet'
CREATE TABLE [dbo].[GenHealthLumpSum_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenHealthLumpSum_AssuredSet'
CREATE TABLE [dbo].[GenHealthLumpSum_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,1)  NULL,
    [Weight] decimal(18,1)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthLumpSum_AuditTrailSet'
CREATE TABLE [dbo].[GenHealthLumpSum_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenHealthLumpSum_PlanSet'
CREATE TABLE [dbo].[GenHealthLumpSum_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL,
    [OPDCoverage] decimal(18,2)  NULL,
    [OPDCoverageFlag] bit  NULL
);
GO

-- Creating table 'GenHealthLumpSum_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenHealthLumpSum_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [DiseaseAnswer] nvarchar(max)  NULL,
    [SymptomAnswer] nvarchar(max)  NULL,
    [Answer02_01] nvarchar(max)  NULL,
    [Answer02_02] nvarchar(max)  NULL,
    [Answer02_03] nvarchar(max)  NULL
);
GO

-- Creating table 'SalutationSet'
CREATE TABLE [dbo].[SalutationSet] (
    [SalutationId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'OccupationSet'
CREATE TABLE [dbo].[OccupationSet] (
    [OccupationId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Class] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [OccupationClassOccupationClassId] int  NOT NULL
);
GO

-- Creating table 'NationalitySet'
CREATE TABLE [dbo].[NationalitySet] (
    [NationalityId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'ClientStatusSet'
CREATE TABLE [dbo].[ClientStatusSet] (
    [ClientStatusId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenHealthLumpSum_MasterPlanSet'
CREATE TABLE [dbo].[GenHealthLumpSum_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanType] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [YearPremium] decimal(18,2)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [FirstMonthPremium] decimal(18,2)  NOT NULL,
    [MonthlyPremium] decimal(18,2)  NOT NULL,
    [OPDCoverage] decimal(18,2)  NULL
);
GO

-- Creating table 'GenHealthLumpSum_PolicySet'
CREATE TABLE [dbo].[GenHealthLumpSum_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [IsHealthCheck] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GEBCard] varbinary(max)  NULL,
    [GenHealthLumpSum_Payment_ItemId] bigint  NOT NULL,
    [GenHealthLumpSum_AuditTrail_ItemId] bigint  NOT NULL,
    [GenHealthLumpSum_Assured_ItemId] bigint  NOT NULL,
    [GenHealthLumpSum_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenHealthLumpSum_Plan_ItemId] bigint  NOT NULL,
    [GenHealthLumpSum_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'ProductSet'
CREATE TABLE [dbo].[ProductSet] (
    [ProductId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ProductType] nvarchar(max)  NOT NULL,
    [CoverImage] nvarchar(max)  NOT NULL,
    [ShortDescription] nvarchar(max)  NULL,
    [LongDescription] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Category] nvarchar(max)  NULL,
    [ProductController] nvarchar(max)  NULL,
    [Seqn] int  NULL,
    [PolicyType] nvarchar(max)  NULL,
    [IsAUW] bit  NULL
);
GO

-- Creating table 'GTBankAccountSet'
CREATE TABLE [dbo].[GTBankAccountSet] (
    [GTBankAccountId] int IDENTITY(1,1) NOT NULL,
    [BankName] nvarchar(max)  NOT NULL,
    [BranchName] nvarchar(max)  NOT NULL,
    [AccountNumber] nvarchar(max)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'CreditCardTypeSet'
CREATE TABLE [dbo].[CreditCardTypeSet] (
    [CreditCardTypeId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'OccupationClassSet'
CREATE TABLE [dbo].[OccupationClassSet] (
    [OccupationClassId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenHealthLumpSum_DCRSet'
CREATE TABLE [dbo].[GenHealthLumpSum_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [GenHealthLumpSum_PolicyProposalId] bigint  NOT NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL
);
GO

-- Creating table 'RelationSet'
CREATE TABLE [dbo].[RelationSet] (
    [RelationId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [ForAUW] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [DisplayFor] int  NOT NULL
);
GO

-- Creating table 'GenHealthLumpSum_MasterCoverageSet'
CREATE TABLE [dbo].[GenHealthLumpSum_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_PlanSet'
CREATE TABLE [dbo].[GenHealthProtect_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL,
    [OPDCoverage] decimal(18,2)  NULL,
    [OPDCoverageFlag] bit  NULL
);
GO

-- Creating table 'GenHealthProtect_AuditTrailSet'
CREATE TABLE [dbo].[GenHealthProtect_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [GenHealthProtect_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenHealthProtect_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenHealthProtect_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_AssuredSet'
CREATE TABLE [dbo].[GenHealthProtect_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [GenHealthProtect_PolicyProposalId] bigint  NOT NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_PaymentSet'
CREATE TABLE [dbo].[GenHealthProtect_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_MasterPlanSet'
CREATE TABLE [dbo].[GenHealthProtect_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [PlanType] nvarchar(max)  NOT NULL,
    [PlanNo] nvarchar(max)  NOT NULL,
    [YearPremium] decimal(18,2)  NOT NULL,
    [FromAge] nvarchar(max)  NOT NULL,
    [ToAge] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] int  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] int  NULL,
    [UpdatedDate] datetime  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [FirstMonthPremium] decimal(18,2)  NULL,
    [MonthlyPremium] decimal(18,2)  NULL,
    [OPDCoverage] decimal(18,0)  NULL
);
GO

-- Creating table 'GenHealthProtect_MasterCoverageSet'
CREATE TABLE [dbo].[GenHealthProtect_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_PolicySet'
CREATE TABLE [dbo].[GenHealthProtect_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [GenHealthProtect_Assured_ItemId] bigint  NOT NULL,
    [GenHealthProtect_AuditTrail_ItemId] bigint  NOT NULL,
    [GenHealthProtect_Payment_ItemId] bigint  NOT NULL,
    [GenHealthProtect_Plan_ItemId] bigint  NOT NULL,
    [GenHealthProtect_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenHealthProtect_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenHealthProtect_DCRSet'
CREATE TABLE [dbo].[GenHealthProtect_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenHealthProtect_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenHealthLumpSum_PayerSet'
CREATE TABLE [dbo].[GenHealthLumpSum_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [PayerAddress1] nvarchar(max)  NULL,
    [PayerAddress2] nvarchar(max)  NULL,
    [PayerAddress3] nvarchar(max)  NULL,
    [PayerAddress4] nvarchar(max)  NULL,
    [PayerAddress5] nvarchar(max)  NULL,
    [PayerEmailAddress] nvarchar(max)  NULL,
    [PayerTaxId] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_PayerSet'
CREATE TABLE [dbo].[GenHealthProtect_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL
);
GO

-- Creating table 'LeadSourceSet'
CREATE TABLE [dbo].[LeadSourceSet] (
    [LeadSourceId] int IDENTITY(1,1) NOT NULL,
    [LeadSourceName] nvarchar(max)  NULL,
    [ApplicationPrefix] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [ApplicationNumberFrom] nvarchar(max)  NULL,
    [ApplicationNumberTo] nvarchar(max)  NULL,
    [ApplicationNumberCurrent] nvarchar(max)  NULL,
    [ApplicationNumberLength] nvarchar(max)  NULL,
    [ProductProductId] int  NOT NULL,
    [IsDefaultValue] bit  NULL,
    [IsPartialApplicationNo] bit  NOT NULL
);
GO

-- Creating table 'GenExclusivePA_PolicySet'
CREATE TABLE [dbo].[GenExclusivePA_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [IsHealthCheck] nvarchar(max)  NULL,
    [GenExclusivePA_Assured_ItemId] bigint  NOT NULL,
    [GenExclusivePA_AuditTrail_ItemId] bigint  NOT NULL,
    [GenExclusivePA_Payment_ItemId] bigint  NOT NULL,
    [GenExclusivePA_Plan_ItemId] bigint  NOT NULL,
    [GenExclusivePA_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenExclusivePA_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenExclusivePA_AssuredSet'
CREATE TABLE [dbo].[GenExclusivePA_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenExclusivePA_PlanSet'
CREATE TABLE [dbo].[GenExclusivePA_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenExclusivePA_PaymentSet'
CREATE TABLE [dbo].[GenExclusivePA_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenExclusivePA_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenExclusivePA_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL
);
GO

-- Creating table 'GenExclusivePA_DCRSet'
CREATE TABLE [dbo].[GenExclusivePA_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenExclusivePA_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenExclusivePA_AuditTrailSet'
CREATE TABLE [dbo].[GenExclusivePA_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenExclusivePA_MasterPlanSet'
CREATE TABLE [dbo].[GenExclusivePA_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Premium] decimal(18,2)  NULL,
    [YearPremium] decimal(18,2)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] int  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] int  NULL,
    [UpdatedDate] datetime  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL
);
GO

-- Creating table 'GenExclusivePA_MasterCoverageSet'
CREATE TABLE [dbo].[GenExclusivePA_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_PolicySet'
CREATE TABLE [dbo].[GenShieldPA_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [IsHealthCheck] nvarchar(max)  NULL,
    [GenShieldPA_Assured_ItemId] bigint  NOT NULL,
    [GenShieldPA_AuditTrail_ItemId] bigint  NOT NULL,
    [GenShieldPA_Payment_ItemId] bigint  NOT NULL,
    [GenShieldPA_Plan_ItemId] bigint  NOT NULL,
    [GenShieldPA_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenShieldPA_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenShieldPA_AssuredSet'
CREATE TABLE [dbo].[GenShieldPA_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenShieldPA_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_PaymentSet'
CREATE TABLE [dbo].[GenShieldPA_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_PlanSet'
CREATE TABLE [dbo].[GenShieldPA_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenShieldPA_AuditTrailSet'
CREATE TABLE [dbo].[GenShieldPA_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenShieldPA_DCRSet'
CREATE TABLE [dbo].[GenShieldPA_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenShieldPA_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenShieldPA_MasterPlanSet'
CREATE TABLE [dbo].[GenShieldPA_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [FirstInstallPremium] decimal(18,2)  NULL,
    [NextInstallPremium] decimal(18,2)  NULL,
    [YearPremium] decimal(18,2)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_MasterCoverageSet'
CREATE TABLE [dbo].[GenShieldPA_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_PolicySet'
CREATE TABLE [dbo].[GenMax7_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [GenMax7_Assured_ItemId] bigint  NOT NULL,
    [GenMax7_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMax7_Payer_ItemId] bigint  NOT NULL,
    [GenMax7_Payment_ItemId] bigint  NOT NULL,
    [GenMax7_Plan_ItemId] bigint  NOT NULL,
    [GenMax7_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenMax7_FATCA_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMax7_AssuredSet'
CREATE TABLE [dbo].[GenMax7_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_DCRSet'
CREATE TABLE [dbo].[GenMax7_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMax7_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenMax7_PlanSet'
CREATE TABLE [dbo].[GenMax7_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_PayerSet'
CREATE TABLE [dbo].[GenMax7_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_AuditTrailSet'
CREATE TABLE [dbo].[GenMax7_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMax7_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenMax7_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenMax7_PaymentSet'
CREATE TABLE [dbo].[GenMax7_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenMax7_MasterCoverageSet'
CREATE TABLE [dbo].[GenMax7_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_MasterPlanSet'
CREATE TABLE [dbo].[GenMax7_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [FirstYearRate] decimal(18,2)  NULL,
    [NextYearRate] decimal(18,2)  NULL,
    [FirstYearPremium] decimal(18,2)  NULL,
    [NextYearPremium] decimal(18,2)  NULL,
    [Annual_FirstYearPremium] decimal(18,2)  NULL,
    [Annual_NextYearPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMax7_FATCASet'
CREATE TABLE [dbo].[GenMax7_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'LifeSalutationSet'
CREATE TABLE [dbo].[LifeSalutationSet] (
    [SalutationId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Seq] int  NULL
);
GO

-- Creating table 'LifePayLifeSet'
CREATE TABLE [dbo].[LifePayLifeSet] (
    [LifePayLifeId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Seq] int  NULL
);
GO

-- Creating table 'LifeGTBankAccountSet'
CREATE TABLE [dbo].[LifeGTBankAccountSet] (
    [GTBankAccountId] int IDENTITY(1,1) NOT NULL,
    [BankName] nvarchar(max)  NOT NULL,
    [BranchName] nvarchar(max)  NOT NULL,
    [AccountNumber] nvarchar(max)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'ReligionSet'
CREATE TABLE [dbo].[ReligionSet] (
    [ReligionId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Seq] int  NULL
);
GO

-- Creating table 'GenSmart5_PolicySet'
CREATE TABLE [dbo].[GenSmart5_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [GenSmart5_AuditTrail_ItemId] bigint  NOT NULL,
    [GenSmart5_FATCA_ItemId] bigint  NOT NULL,
    [GenSmart5_Payer_ItemId] bigint  NOT NULL,
    [GenSmart5_Plan_ItemId] bigint  NOT NULL,
    [GenSmart5_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenSmart5_Assured_ItemId] bigint  NOT NULL,
    [GenSmart5_Payment_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenSmart5_AssuredSet'
CREATE TABLE [dbo].[GenSmart5_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSmart5_FATCASet'
CREATE TABLE [dbo].[GenSmart5_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSmart5_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenSmart5_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenSmart5_DCRSet'
CREATE TABLE [dbo].[GenSmart5_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenSmart5_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenSmart5_PlanSet'
CREATE TABLE [dbo].[GenSmart5_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSmart5_PaymentSet'
CREATE TABLE [dbo].[GenSmart5_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSmart5_MasterPlanSet'
CREATE TABLE [dbo].[GenSmart5_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [FirstYearRate] decimal(18,2)  NULL,
    [NextYearRate] decimal(18,2)  NULL,
    [FirstYearPremium] decimal(18,2)  NULL,
    [Annual_FirstYearPremium] decimal(18,2)  NULL,
    [NextYearPremium] decimal(18,2)  NULL,
    [Annual_NextYearPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenSmart5_MasterCoverageSet'
CREATE TABLE [dbo].[GenSmart5_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_PolicySet'
CREATE TABLE [dbo].[GenLifePlus10_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [GenLifePlus10_AuditTrail_ItemId] bigint  NOT NULL,
    [GenLifePlus10_Plan_ItemId] bigint  NOT NULL,
    [GenLifePlus10_Assured_ItemId] bigint  NOT NULL,
    [GenLifePlus10_FATCA_ItemId] bigint  NOT NULL,
    [GenLifePlus10_Payer_ItemId] bigint  NOT NULL,
    [GenLifePlus10_Payment_ItemId] bigint  NOT NULL,
    [GenLifePlus10_UnderWriteInfo_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenLifePlus10_AssuredSet'
CREATE TABLE [dbo].[GenLifePlus10_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_PlanSet'
CREATE TABLE [dbo].[GenLifePlus10_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_DCRSet'
CREATE TABLE [dbo].[GenLifePlus10_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenLifePlus10_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenLifePlus10_PaymentSet'
CREATE TABLE [dbo].[GenLifePlus10_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_MasterPlanSet'
CREATE TABLE [dbo].[GenLifePlus10_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [FirstYearRate] decimal(18,2)  NULL,
    [NextYearRate] decimal(18,2)  NULL,
    [FirstYearPremium] decimal(18,2)  NULL,
    [NextYearPremium] decimal(18,2)  NULL,
    [Annual_FirstYearPremium] decimal(18,2)  NULL,
    [Annual_NextYearPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenLifePlus10_MasterCoverageSet'
CREATE TABLE [dbo].[GenLifePlus10_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenLifePlus10_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenLifePlus10_FATCASet'
CREATE TABLE [dbo].[GenLifePlus10_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_PayerSet'
CREATE TABLE [dbo].[GenLifePlus10_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_AuditTrailSet'
CREATE TABLE [dbo].[GenLifePlus10_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSmart5_AuditTrailSet'
CREATE TABLE [dbo].[GenSmart5_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSmart5_PayerSet'
CREATE TABLE [dbo].[GenSmart5_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_AssuredSet'
CREATE TABLE [dbo].[GenSabye4_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_AuditTrailSet'
CREATE TABLE [dbo].[GenSabye4_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSabye4_DCRSet'
CREATE TABLE [dbo].[GenSabye4_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenSabye4_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenSabye4_FATCASet'
CREATE TABLE [dbo].[GenSabye4_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_MasterCoverageSet'
CREATE TABLE [dbo].[GenSabye4_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_MasterPlanSet'
CREATE TABLE [dbo].[GenSabye4_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Instalment_Premium] decimal(18,2)  NULL,
    [Annual_Premium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenSabye4_PayerSet'
CREATE TABLE [dbo].[GenSabye4_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_PaymentSet'
CREATE TABLE [dbo].[GenSabye4_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_PlanSet'
CREATE TABLE [dbo].[GenSabye4_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_PolicySet'
CREATE TABLE [dbo].[GenSabye4_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [GenSabye4_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenSabye4_Plan_ItemId] bigint  NOT NULL,
    [GenSabye4_Payment_ItemId] bigint  NOT NULL,
    [GenSabye4_Assured_ItemId] bigint  NOT NULL,
    [GenSabye4_FATCA_ItemId] bigint  NOT NULL,
    [GenSabye4_AuditTrail_ItemId] bigint  NOT NULL,
    [GenSabye4_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenSabye4_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenSabye4_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenHighLife3_AssuredSet'
CREATE TABLE [dbo].[GenHighLife3_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_AuditTrailSet'
CREATE TABLE [dbo].[GenHighLife3_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenHighLife3_DCRSet'
CREATE TABLE [dbo].[GenHighLife3_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenHighLife3_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenHighLife3_FATCASet'
CREATE TABLE [dbo].[GenHighLife3_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_MasterCoverageSet'
CREATE TABLE [dbo].[GenHighLife3_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_MasterPlanSet'
CREATE TABLE [dbo].[GenHighLife3_MasterPlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Instalment_Premium] decimal(18,2)  NULL,
    [Annual_Premium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenHighLife3_PayerSet'
CREATE TABLE [dbo].[GenHighLife3_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_PaymentSet'
CREATE TABLE [dbo].[GenHighLife3_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_PlanSet'
CREATE TABLE [dbo].[GenHighLife3_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_PolicySet'
CREATE TABLE [dbo].[GenHighLife3_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [GenHighLife3_Assured_ItemId] bigint  NOT NULL,
    [GenHighLife3_AuditTrail_ItemId] bigint  NOT NULL,
    [GenHighLife3_FATCA_ItemId] bigint  NOT NULL,
    [GenHighLife3_Plan_ItemId] bigint  NOT NULL,
    [GenHighLife3_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenHighLife3_Payment_ItemId] bigint  NOT NULL,
    [GenHighLife3_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenHighLife3_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenHighLife3_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenHealthProtect_MasterPlanCoverageSet'
CREATE TABLE [dbo].[GenHealthProtect_MasterPlanCoverageSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [CoverageItem1_Text] nvarchar(max)  NULL,
    [CoverageItem1_Value] nvarchar(max)  NULL,
    [CoverageItem2_Text] nvarchar(max)  NULL,
    [CoverageItem2_Value] nvarchar(max)  NULL,
    [CoverageItem3_Text] nvarchar(max)  NULL,
    [CoverageItem3_Value] nvarchar(max)  NULL,
    [CoverageItem4_Text] nvarchar(max)  NULL,
    [CoverageItem4_Value] nvarchar(max)  NULL,
    [CoverageItem5_Text] nvarchar(max)  NULL,
    [CoverageItem5_Value] nvarchar(max)  NULL,
    [CoverageItem6_Text] nvarchar(max)  NULL,
    [CoverageItem6_Value] nvarchar(max)  NULL,
    [CoverageItem7_Text] nvarchar(max)  NULL,
    [CoverageItem7_Value] nvarchar(max)  NULL,
    [CoverageItem8_Text] nvarchar(max)  NULL,
    [CoverageItem8_Value] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthLumpSum_MasterPricePlanSet'
CREATE TABLE [dbo].[GenHealthLumpSum_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanType] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [FirstTimeInstalmentPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [MaximumInpatientBenefit] decimal(18,2)  NULL,
    [RoomAndNursingService] decimal(18,2)  NULL,
    [ICURoomAndBoard] decimal(18,2)  NULL,
    [DrugAndXRay] decimal(18,2)  NULL,
    [EmergencyTreatment] decimal(18,2)  NULL,
    [AmbulanceServiceFee] decimal(18,2)  NULL,
    [SurgeryFee] decimal(18,2)  NULL,
    [PhysicianFee] decimal(18,2)  NULL,
    [ReplacementOrgans] decimal(18,2)  NULL,
    [Dialysis] decimal(18,2)  NULL,
    [DeathOrDismemberment] decimal(18,2)  NULL,
    [OPDPerVisit] decimal(18,2)  NULL,
    [PersonalAccident] decimal(18,2)  NULL,
    [Dental] decimal(18,2)  NULL,
    [NursingService] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [IsGEB] nvarchar(1)  NULL
);
GO

-- Creating table 'GenHealthLumpSum_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenHealthLumpSum_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_MasterPricePlanSet'
CREATE TABLE [dbo].[GenHealthProtect_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanType] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [FirstTimeInstalmentPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [MaximumInpatientBenefit] decimal(18,2)  NULL,
    [RoomAndNursingService] decimal(18,2)  NULL,
    [ICURoomAndBoard] decimal(18,2)  NULL,
    [DrugAndXRay] decimal(18,2)  NULL,
    [EmergencyTreatment] decimal(18,2)  NULL,
    [AmbulanceServiceFee] decimal(18,2)  NULL,
    [SurgeryFee] decimal(18,2)  NULL,
    [PhysicianFee] decimal(18,2)  NULL,
    [ReplacementOrgans] decimal(18,2)  NULL,
    [Dialysis] decimal(18,2)  NULL,
    [DeathOrDismemberment] decimal(18,2)  NULL,
    [OPDPerVisit] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [PayPlan] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthProtect_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenHealthProtect_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenExclusivePA_MasterPricePlanSet'
CREATE TABLE [dbo].[GenExclusivePA_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [FirstTimeInstalmentPremium] decimal(18,2)  NULL,
    [Death_NormalAccident] decimal(18,2)  NULL,
    [Death_PublicAccident] decimal(18,2)  NULL,
    [Death_Homicide] decimal(18,2)  NULL,
    [Death_MotorcycleAccident] decimal(18,2)  NULL,
    [MedicalFee] decimal(18,2)  NULL,
    [CompensationIncome] decimal(18,2)  NULL,
    [CompensationIncome_ICU] decimal(18,2)  NULL,
    [Cremation] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenExclusivePA_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenExclusivePA_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_MasterPricePlanSet'
CREATE TABLE [dbo].[GenShieldPA_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [FirstTimeInstalmentPremium] decimal(18,2)  NULL,
    [Death_NormalAccident] decimal(18,2)  NULL,
    [Death_PublicAccident] decimal(18,2)  NULL,
    [Death_Homicide] decimal(18,2)  NULL,
    [Death_MotorcycleAccident] decimal(18,2)  NULL,
    [MedicalFee] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenShieldPA_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenShieldPA_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMax7_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMax7_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1] decimal(18,2)  NULL,
    [DeathBenefit_2] decimal(18,2)  NULL,
    [DeathBenefit_3] decimal(18,2)  NULL,
    [DeathBenefit_4] decimal(18,2)  NULL,
    [DeathBenefit_5] decimal(18,2)  NULL,
    [DeathBenefit_6] decimal(18,2)  NULL,
    [DeathBenefit_7_15] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMax7_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenMax7_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenLifePlus10_MasterPricePlanSet'
CREATE TABLE [dbo].[GenLifePlus10_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1_3] decimal(18,2)  NULL,
    [DeathBenefit_4_7] decimal(18,2)  NULL,
    [DeathBenefit_8_20] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenLifePlus10_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenLifePlus10_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSmart5_MasterPricePlanSet'
CREATE TABLE [dbo].[GenSmart5_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1] decimal(18,2)  NULL,
    [DeathBenefit_2] decimal(18,2)  NULL,
    [DeathBenefit_3] decimal(18,2)  NULL,
    [DeathBenefit_4] decimal(18,2)  NULL,
    [DeathBenefit_5_10] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSmart5_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenSmart5_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSabye4_MasterPricePlanSet'
CREATE TABLE [dbo].[GenSabye4_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn_1_5] decimal(18,2)  NULL,
    [CashReturn_6_9] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1] decimal(18,2)  NULL,
    [DeathBenefit_2] decimal(18,2)  NULL,
    [DeathBenefit_3] decimal(18,2)  NULL,
    [DeathBenefit_4_10] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSabye4_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenSabye4_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHighLife3_MasterPricePlanSet'
CREATE TABLE [dbo].[GenHighLife3_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn_1_3] decimal(18,2)  NULL,
    [CashReturn_4_6] decimal(18,2)  NULL,
    [CashReturn_7_9] decimal(18,2)  NULL,
    [CashReturn_10_11] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1] decimal(18,2)  NULL,
    [DeathBenefit_2] decimal(18,2)  NULL,
    [DeathBenefit_3_12] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenHighLife3_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenHighLife3_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_AssuredSet'
CREATE TABLE [dbo].[GenFamilyPA_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_PolicySet'
CREATE TABLE [dbo].[GenFamilyPA_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [GenFamilyPA_Plan_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Assured_ItemId] bigint  NOT NULL,
    [GenFamilyPA_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Payment_ItemId] bigint  NOT NULL,
    [GenFamilyPA_AuditTrail_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Subsidiary1_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Subsidiary2_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Subsidiary3_ItemId] bigint  NOT NULL,
    [GenFamilyPA_Subsidiary4_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenFamilyPA_PlanSet'
CREATE TABLE [dbo].[GenFamilyPA_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PlanName] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [API] decimal(18,2)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [TotalAPI] decimal(18,2)  NULL,
    [TotalInstalmentPremium_First] decimal(18,2)  NULL,
    [TotalInstalmentPremium_Renewal] decimal(18,2)  NULL
);
GO

-- Creating table 'GenFamilyPA_DCRSet'
CREATE TABLE [dbo].[GenFamilyPA_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenFamilyPA_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenFamilyPA_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenFamilyPA_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_PaymentSet'
CREATE TABLE [dbo].[GenFamilyPA_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_AuditTrailSet'
CREATE TABLE [dbo].[GenFamilyPA_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [GenFamilyPA_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenFamilyPA_MasterPricePlanSet'
CREATE TABLE [dbo].[GenFamilyPA_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Relationship] nvarchar(max)  NULL,
    [RelationshipCode] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [SA] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [Death_NormalAccident] decimal(18,2)  NULL,
    [Death_PublicAccident] decimal(18,2)  NULL,
    [Death_MotorcycleAccident] decimal(18,2)  NULL,
    [InsurgencyBenefit] decimal(18,2)  NULL,
    [MedicalFee] decimal(18,2)  NULL,
    [CompensationIncomeFamily] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenFamilyPA_MasterCoverageSet'
CREATE TABLE [dbo].[GenFamilyPA_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenFamilyPA_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenFamilyPA_Subsidiary1Set'
CREATE TABLE [dbo].[GenFamilyPA_Subsidiary1Set] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SocialSecurityNumber] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [RelationShipCode] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [Age] int  NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] nvarchar(max)  NULL,
    [BeneficiaryAge2] nvarchar(max)  NULL,
    [BeneficiaryAge3] nvarchar(max)  NULL,
    [BeneficiaryAge4] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage1] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage2] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage3] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage4] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardId] nvarchar(max)  NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [SA] decimal(18,2)  NULL
);
GO

-- Creating table 'GenFamilyPA_Subsidiary2Set'
CREATE TABLE [dbo].[GenFamilyPA_Subsidiary2Set] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SocialSecurityNumber] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [RelationShipCode] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [Age] int  NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] nvarchar(max)  NULL,
    [BeneficiaryAge2] nvarchar(max)  NULL,
    [BeneficiaryAge3] nvarchar(max)  NULL,
    [BeneficiaryAge4] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage1] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage2] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage3] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage4] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardId] nvarchar(max)  NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [SA] decimal(18,2)  NULL
);
GO

-- Creating table 'GenFamilyPA_Subsidiary3Set'
CREATE TABLE [dbo].[GenFamilyPA_Subsidiary3Set] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SocialSecurityNumber] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [RelationShipCode] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [Age] int  NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] nvarchar(max)  NULL,
    [BeneficiaryAge2] nvarchar(max)  NULL,
    [BeneficiaryAge3] nvarchar(max)  NULL,
    [BeneficiaryAge4] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage1] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage2] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage3] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage4] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardId] nvarchar(max)  NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [SA] decimal(18,2)  NULL
);
GO

-- Creating table 'GenFamilyPA_Subsidiary4Set'
CREATE TABLE [dbo].[GenFamilyPA_Subsidiary4Set] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SocialSecurityNumber] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [RelationShipCode] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [Age] int  NULL,
    [Height] int  NULL,
    [Weight] int  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] nvarchar(max)  NULL,
    [BeneficiaryAge2] nvarchar(max)  NULL,
    [BeneficiaryAge3] nvarchar(max)  NULL,
    [BeneficiaryAge4] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage1] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage2] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage3] nvarchar(max)  NULL,
    [BeneficiaryBenefitPercentage4] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardId] nvarchar(max)  NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [InstalmentPremium_First] decimal(18,2)  NULL,
    [InstalmentPremium_Renewal] decimal(18,2)  NULL,
    [SA] decimal(18,2)  NULL
);
GO

-- Creating table 'GenAomSub6_PolicySet'
CREATE TABLE [dbo].[GenAomSub6_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [GenAomSub6_AuditTrail_ItemId] bigint  NOT NULL,
    [GenAomSub6_Payment_ItemId] bigint  NOT NULL,
    [GenAomSub6_Plan_ItemId] bigint  NOT NULL,
    [GenAomSub6_FATCA_ItemId] bigint  NOT NULL,
    [GenAomSub6_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenAomSub6_Assured_ItemId] bigint  NOT NULL,
    [GenAomSub6_Payer_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenAomSub6_AssuredSet'
CREATE TABLE [dbo].[GenAomSub6_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_PlanSet'
CREATE TABLE [dbo].[GenAomSub6_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_DCRSet'
CREATE TABLE [dbo].[GenAomSub6_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenAomSub6_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenAomSub6_FATCASet'
CREATE TABLE [dbo].[GenAomSub6_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_PaymentSet'
CREATE TABLE [dbo].[GenAomSub6_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_AuditTrailSet'
CREATE TABLE [dbo].[GenAomSub6_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenAomSub6_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenAomSub6_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL
);
GO

-- Creating table 'GenAomSub6_PayerSet'
CREATE TABLE [dbo].[GenAomSub6_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_MasterPricePlanSet'
CREATE TABLE [dbo].[GenAomSub6_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [DiscountAnnualRate] decimal(18,2)  NULL,
    [NormalAnnualRate] decimal(18,2)  NULL,
    [DiscountAnnualPremium] decimal(18,2)  NULL,
    [NormalAnnualPremium] decimal(18,2)  NULL,
    [DiscountInstalmentPremium] decimal(18,2)  NULL,
    [NormalInstalmentPremium] decimal(18,2)  NULL,
    [CashReturn_1_14] decimal(18,2)  NULL,
    [MaturityBenefit] decimal(18,2)  NULL,
    [DeathBenefit_1_3] decimal(18,2)  NULL,
    [DeathBenefit_4_5] decimal(18,2)  NULL,
    [DeathBenefit_6_15] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenAomSub6_MasterCoverageSet'
CREATE TABLE [dbo].[GenAomSub6_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenAomSub6_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenAomSub6_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_PolicySet'
CREATE TABLE [dbo].[GenMotor_VOL1_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [OldPolicyNo] nvarchar(max)  NOT NULL,
    [PolicyType] nvarchar(max)  NOT NULL,
    [PolicyTypeId] nvarchar(max)  NOT NULL,
    [InceptionEndDate] datetime  NULL,
    [PolicyLanguage] nvarchar(max)  NOT NULL,
    [PolicyLanguageId] nvarchar(max)  NOT NULL,
    [ImageCapture] nvarchar(max)  NOT NULL,
    [Remark] nvarchar(max)  NOT NULL,
    [Channel] nvarchar(max)  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [PolicyCustomization] nvarchar(max)  NOT NULL,
    [PolicyCustomizationCode] nvarchar(max)  NOT NULL,
    [GenMotor_VOL1_Assured_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_Driver_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_OldPolicy_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_Plan_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_TaxInvoice_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_Vehicle_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_Payment_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1_Inspection_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL1_AssuredSet'
CREATE TABLE [dbo].[GenMotor_VOL1_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsuredType] nvarchar(max)  NULL,
    [InsuredTypeCode] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [CardIssueDate] datetime  NULL,
    [CardExpiryDate] datetime  NULL,
    [MotorOccupation] nvarchar(max)  NULL,
    [CardIssueBy] nvarchar(max)  NULL,
    [MotorOccupationId] nvarchar(max)  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [BeneficiaryFullName] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [CorporateBranchCode] nvarchar(max)  NULL,
    [CorporateName] nvarchar(max)  NULL,
    [CorporateTaxId] nvarchar(max)  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_OldPolicySet'
CREATE TABLE [dbo].[GenMotor_VOL1_OldPolicySet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsurerName] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [CancellationDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_PlanSet'
CREATE TABLE [dbo].[GenMotor_VOL1_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PricePlanId] bigint  NOT NULL,
    [PolicyVolume] nvarchar(max)  NULL,
    [PolicyVolumeId] nvarchar(max)  NULL,
    [RepairingType] nvarchar(max)  NULL,
    [RepairingTypeId] nvarchar(max)  NULL,
    [PackageName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [SA_Damage_Thelf] decimal(18,2)  NULL,
    [SA_Damage_Fire] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerPerson] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerCase] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Assetment_PerCase] decimal(18,2)  NULL,
    [SA_Damage_Own_Assetment_PerCase] decimal(18,2)  NULL,
    [Deductible_for_Assetment] decimal(18,2)  NULL,
    [Number_of_Driver] decimal(18,2)  NULL,
    [Number_of_Passenger] decimal(18,2)  NULL,
    [SA_PA_Driver] decimal(18,2)  NULL,
    [SA_PA_Passenger] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [SA_Medical] decimal(18,2)  NULL,
    [SA_Bail_for_Driver] decimal(18,2)  NULL,
    [PM_Base] decimal(18,2)  NULL,
    [PM_Additional] decimal(18,2)  NULL,
    [PM_PA_Driver] decimal(18,2)  NULL,
    [PM_PA_Passenger] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [PM_Medical] decimal(18,2)  NULL,
    [PM_Bail_for_Driver] decimal(18,2)  NULL,
    [DC_Deductible] decimal(18,2)  NULL,
    [DC_DamageAssetment] decimal(18,2)  NULL,
    [DC_FleetPercentage] decimal(18,2)  NULL,
    [DC_FleetAmount] decimal(18,2)  NULL,
    [DC_NCBPercentage] decimal(18,2)  NULL,
    [DC_NCBAmount] decimal(18,2)  NULL,
    [DC_OtherPercentage] decimal(18,2)  NULL,
    [DC_OtherAmount] decimal(18,2)  NULL,
    [SC_SurchargePercentage] decimal(18,2)  NULL,
    [SC_SurchargeAmount] decimal(18,2)  NULL,
    [PM_Net] decimal(18,2)  NULL,
    [TaxAmount] decimal(18,2)  NULL,
    [StampAmount] decimal(18,2)  NULL,
    [PM_Total] decimal(18,2)  NULL,
    [DC_Total] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_TaxInvoiceSet'
CREATE TABLE [dbo].[GenMotor_VOL1_TaxInvoiceSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [Building] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ReceiptNo] nvarchar(max)  NULL,
    [ReceiptDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_DCRSet'
CREATE TABLE [dbo].[GenMotor_VOL1_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMotor_VOL1_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL1_AuditTrailSet'
CREATE TABLE [dbo].[GenMotor_VOL1_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_VehicleSet'
CREATE TABLE [dbo].[GenMotor_VOL1_VehicleSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleBrandId] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleModelId] nvarchar(max)  NULL,
    [VehicleType] nvarchar(max)  NULL,
    [VehicleTypeId] nvarchar(max)  NULL,
    [VehicleColor] nvarchar(max)  NULL,
    [VehicleColorId] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ChassisNo] nvarchar(max)  NULL,
    [EngineNo] nvarchar(max)  NULL,
    [VehicleBuyingYear] nvarchar(max)  NULL,
    [VehicleModelYear] nvarchar(max)  NULL,
    [PassengerSeat] nvarchar(max)  NULL,
    [EngineCapacity] nvarchar(max)  NULL,
    [VehicleWeight] nvarchar(max)  NULL,
    [GearType] nvarchar(max)  NULL,
    [GearTypeId] nvarchar(max)  NULL,
    [CarType] nvarchar(max)  NULL,
    [CarTypeId] nvarchar(max)  NULL,
    [VehicleAge] int  NULL,
    [HasSpecialEquipment] bit  NULL,
    [SpecialEquipmentDescription] nvarchar(max)  NULL,
    [SpecialEquipmentPrice] decimal(18,2)  NULL,
    [HasAccessories] bit  NULL,
    [AccessoryName1] nvarchar(max)  NULL,
    [AccessoryName2] nvarchar(max)  NULL,
    [AccessoryName3] nvarchar(max)  NULL,
    [AccessoryNamePrice1] decimal(18,2)  NULL,
    [AccessoryNamePrice2] decimal(18,2)  NULL,
    [AccessoryNamePrice3] decimal(18,2)  NULL,
    [VehicleBodyType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_DriverSet'
CREATE TABLE [dbo].[GenMotor_VOL1_DriverSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SpecificDriverId] nvarchar(max)  NULL,
    [SpecificDriver] nvarchar(max)  NULL,
    [Driver1_Salutation] nvarchar(max)  NULL,
    [Driver1_GivenName] nvarchar(max)  NULL,
    [Driver1_Surname] nvarchar(max)  NULL,
    [Driver1_CardId] nvarchar(max)  NULL,
    [Driver1_ClientSex] nvarchar(max)  NULL,
    [Driver1_ClientSexId] nvarchar(max)  NULL,
    [Driver1_Occupation] nvarchar(max)  NULL,
    [Driver1_OccupationId] nvarchar(max)  NULL,
    [Driver1_BirthDate] datetime  NULL,
    [Driver1_Age] smallint  NULL,
    [Driver1_LicenseNo] nvarchar(max)  NULL,
    [Driver1_LicenseType] nvarchar(max)  NULL,
    [Driver1_LicenseTypeId] nvarchar(max)  NULL,
    [Driver1_ReleaseDate] datetime  NULL,
    [Driver1_ReleaseBy] nvarchar(max)  NULL,
    [Driver1_ExpiryDate] datetime  NULL,
    [Driver2_Salutation] nvarchar(max)  NULL,
    [Driver2_GivenName] nvarchar(max)  NULL,
    [Driver2_Surname] nvarchar(max)  NULL,
    [Driver2_CardId] nvarchar(max)  NULL,
    [Driver2_ClientSex] nvarchar(max)  NULL,
    [Driver2_ClientSexId] nvarchar(max)  NULL,
    [Driver2_Occupation] nvarchar(max)  NULL,
    [Driver2_OccupationId] nvarchar(max)  NULL,
    [Driver2_BirthDate] datetime  NULL,
    [Driver2_Age] smallint  NULL,
    [Driver2_LicenseNo] nvarchar(max)  NULL,
    [Driver2_LicenseType] nvarchar(max)  NULL,
    [Driver2_LicenseTypeId] nvarchar(max)  NULL,
    [Driver2_ReleaseDate] datetime  NULL,
    [Driver2_ReleaseBy] nvarchar(max)  NULL,
    [Driver2_ExpiryDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotor_VOL1_MasterPricePlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PolicyVolume] nvarchar(max)  NULL,
    [PolicyVolumeId] nvarchar(max)  NULL,
    [RepairingType] nvarchar(max)  NULL,
    [RepairingTypeId] nvarchar(max)  NULL,
    [PackageName] nvarchar(max)  NULL,
    [VehicleType] nvarchar(max)  NULL,
    [VehicleAge] nvarchar(max)  NULL,
    [VehicleCapacity] nvarchar(max)  NULL,
    [VehicleTypeId] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [SA_Damage_Thelf] decimal(18,2)  NULL,
    [SA_Damage_Fire] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerPerson] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerCase] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Assetment_PerCase] decimal(18,2)  NULL,
    [SA_Damage_Own_Assetment_PerCase] decimal(18,2)  NULL,
    [Deductible_for_Assetment] decimal(18,2)  NULL,
    [Number_of_Driver] decimal(18,2)  NULL,
    [Number_of_Passenger] decimal(18,2)  NULL,
    [SA_PA_Driver] decimal(18,2)  NULL,
    [SA_PA_Passenger] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [SA_Medical] decimal(18,2)  NULL,
    [SA_Bail_for_Driver] decimal(18,2)  NULL,
    [PM_Base] decimal(18,2)  NULL,
    [PM_Additional] decimal(18,2)  NULL,
    [PM_PA_Driver] decimal(18,2)  NULL,
    [PM_PA_Passenger] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [PM_Medical] decimal(18,2)  NULL,
    [PM_Bail_for_Driver] decimal(18,2)  NULL,
    [DC_Deductible] decimal(18,2)  NULL,
    [DC_DamageAssetment] decimal(18,2)  NULL,
    [DC_FleetPercentage] decimal(18,2)  NULL,
    [DC_FleetAmount] decimal(18,2)  NULL,
    [DC_NCBPercentage] decimal(18,2)  NULL,
    [DC_NCBAmount] decimal(18,2)  NULL,
    [DC_OtherPercentage] decimal(18,2)  NULL,
    [DC_OtherAmount] decimal(18,2)  NULL,
    [SC_SurchargePercentage] decimal(18,2)  NULL,
    [SC_SurchargeAmount] decimal(18,2)  NULL,
    [PM_Net] decimal(18,2)  NULL,
    [TaxAmount] decimal(18,2)  NULL,
    [StampAmount] decimal(18,2)  NULL,
    [PM_Total] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_MasterCoverageSet'
CREATE TABLE [dbo].[GenMotor_VOL1_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_VehicleBrandSet'
CREATE TABLE [dbo].[GenMotor_VOL1_VehicleBrandSet] (
    [VehicleBrandId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_VehicleModelSet'
CREATE TABLE [dbo].[GenMotor_VOL1_VehicleModelSet] (
    [VehicleModelId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [EngineCapacity] nvarchar(max)  NOT NULL,
    [VehicleBrandVehicleBrandId] int  NOT NULL,
    [VehicleTypeVehicleTypeId] int  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL1_VehicleTypeSet'
CREATE TABLE [dbo].[GenMotor_VOL1_VehicleTypeSet] (
    [VehicleTypeId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1_PaymentSet'
CREATE TABLE [dbo].[GenMotor_VOL1_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1_InspectionSet'
CREATE TABLE [dbo].[GenMotor_VOL1_InspectionSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [PhoneNo_Mobile] nvarchar(max)  NULL,
    [PhoneNo_Home] nvarchar(max)  NULL,
    [PhoneNo_Office] nvarchar(max)  NULL,
    [InspectionDate] datetime  NULL,
    [InspectionTime] nvarchar(max)  NULL,
    [InspectionPlace] nvarchar(max)  NULL
);
GO

-- Creating table 'GenHealthLumpSum_Plan1Set'
CREATE TABLE [dbo].[GenHealthLumpSum_Plan1Set] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL,
    [OPDCoverage] decimal(18,2)  NULL,
    [OPDCoverageFlag] bit  NULL
);
GO

-- Creating table 'ValuePreferenceSet'
CREATE TABLE [dbo].[ValuePreferenceSet] (
    [ValuePreferenceId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_PolicySet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [OldPolicyNo] nvarchar(max)  NOT NULL,
    [PolicyType] nvarchar(max)  NOT NULL,
    [PolicyTypeId] nvarchar(max)  NOT NULL,
    [InceptionEndDate] datetime  NULL,
    [PolicyLanguage] nvarchar(max)  NOT NULL,
    [PolicyLanguageId] nvarchar(max)  NOT NULL,
    [ImageCapture] nvarchar(max)  NOT NULL,
    [Remark] nvarchar(max)  NOT NULL,
    [Channel] nvarchar(max)  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [PolicyCustomization] nvarchar(max)  NOT NULL,
    [PolicyCustomizationCode] nvarchar(max)  NOT NULL,
    [GenMotor_VOL1Std_Assured_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_OldPolicy_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_Plan_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_Vehicle_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_Driver_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_Payment_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_Inspection_ItemId] bigint  NOT NULL,
    [GenMotor_VOL1Std_TaxInvoice_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_AssuredSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsuredType] nvarchar(max)  NULL,
    [InsuredTypeCode] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [CardIssueDate] datetime  NULL,
    [CardExpiryDate] datetime  NULL,
    [MotorOccupation] nvarchar(max)  NULL,
    [CardIssueBy] nvarchar(max)  NULL,
    [MotorOccupationId] nvarchar(max)  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [BeneficiaryFullName] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [CorporateBranchCode] nvarchar(max)  NULL,
    [CorporateName] nvarchar(max)  NULL,
    [CorporateTaxId] nvarchar(max)  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_PlanSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PolicyVolume] nvarchar(max)  NULL,
    [PolicyVolumeId] nvarchar(max)  NULL,
    [RepairingType] nvarchar(max)  NULL,
    [RepairingTypeId] nvarchar(max)  NULL,
    [PackageName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [SA_Damage_Thelf] decimal(18,2)  NULL,
    [SA_Damage_Fire] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerPerson] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Death_PerCase] decimal(18,2)  NULL,
    [SA_Damage_ThirdParty_Assetment_PerCase] decimal(18,2)  NULL,
    [SA_Damage_Own_Assetment_PerCase] decimal(18,2)  NULL,
    [Deductible_for_Assetment] decimal(18,2)  NULL,
    [Number_of_Driver] decimal(18,2)  NULL,
    [Number_of_Passenger] decimal(18,2)  NULL,
    [SA_PA_Driver] decimal(18,2)  NULL,
    [SA_PA_Passenger] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [SA_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [SA_Medical] decimal(18,2)  NULL,
    [SA_Bail_for_Driver] decimal(18,2)  NULL,
    [PM_Base] decimal(18,2)  NULL,
    [PM_Additional] decimal(18,2)  NULL,
    [PM_PA_Driver] decimal(18,2)  NULL,
    [PM_PA_Passenger] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Driver] decimal(18,2)  NULL,
    [PM_Weekly_Compensation_for_Passenger] decimal(18,2)  NULL,
    [PM_Medical] decimal(18,2)  NULL,
    [PM_Bail_for_Driver] decimal(18,2)  NULL,
    [DC_Deductible] decimal(18,2)  NULL,
    [DC_DamageAssetment] decimal(18,2)  NULL,
    [DC_FleetPercentage] decimal(18,2)  NULL,
    [DC_FleetAmount] decimal(18,2)  NULL,
    [DC_NCBPercentage] decimal(18,2)  NULL,
    [DC_NCBAmount] decimal(18,2)  NULL,
    [DC_OtherPercentage] decimal(18,2)  NULL,
    [DC_OtherAmount] decimal(18,2)  NULL,
    [SC_SurchargePercentage] decimal(18,2)  NULL,
    [SC_SurchargeAmount] decimal(18,2)  NULL,
    [PM_Net] decimal(18,2)  NULL,
    [TaxAmount] decimal(18,2)  NULL,
    [StampAmount] decimal(18,2)  NULL,
    [PM_Total] decimal(18,2)  NULL,
    [DC_Total] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_VehicleSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_VehicleSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleBrandId] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleModelId] nvarchar(max)  NULL,
    [VehicleType] nvarchar(max)  NULL,
    [VehicleTypeId] nvarchar(max)  NULL,
    [VehicleColor] nvarchar(max)  NULL,
    [VehicleColorId] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ChassisNo] nvarchar(max)  NULL,
    [EngineNo] nvarchar(max)  NULL,
    [VehicleBuyingYear] nvarchar(max)  NULL,
    [VehicleModelYear] nvarchar(max)  NULL,
    [PassengerSeat] nvarchar(max)  NULL,
    [EngineCapacity] nvarchar(max)  NULL,
    [VehicleWeight] nvarchar(max)  NULL,
    [GearType] nvarchar(max)  NULL,
    [GearTypeId] nvarchar(max)  NULL,
    [CarType] nvarchar(max)  NULL,
    [CarTypeId] nvarchar(max)  NULL,
    [VehicleAge] int  NULL,
    [HasSpecialEquipment] bit  NULL,
    [SpecialEquipmentDescription] nvarchar(max)  NULL,
    [SpecialEquipmentPrice] decimal(18,2)  NULL,
    [HasAccessories] bit  NULL,
    [AccessoryName1] nvarchar(max)  NULL,
    [AccessoryName2] nvarchar(max)  NULL,
    [AccessoryName3] nvarchar(max)  NULL,
    [AccessoryNamePrice1] decimal(18,2)  NULL,
    [AccessoryNamePrice2] decimal(18,2)  NULL,
    [AccessoryNamePrice3] decimal(18,2)  NULL,
    [VehicleBodyType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_OldPolicySet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_OldPolicySet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsurerName] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [CancellationDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_AuditTrailSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_DriverSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_DriverSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SpecificDriverId] nvarchar(max)  NULL,
    [SpecificDriver] nvarchar(max)  NULL,
    [Driver1_Salutation] nvarchar(max)  NULL,
    [Driver1_GivenName] nvarchar(max)  NULL,
    [Driver1_Surname] nvarchar(max)  NULL,
    [Driver1_CardId] nvarchar(max)  NULL,
    [Driver1_ClientSex] nvarchar(max)  NULL,
    [Driver1_ClientSexId] nvarchar(max)  NULL,
    [Driver1_Occupation] nvarchar(max)  NULL,
    [Driver1_OccupationId] nvarchar(max)  NULL,
    [Driver1_BirthDate] datetime  NULL,
    [Driver1_Age] smallint  NULL,
    [Driver1_LicenseNo] nvarchar(max)  NULL,
    [Driver1_LicenseType] nvarchar(max)  NULL,
    [Driver1_LicenseTypeId] nvarchar(max)  NULL,
    [Driver1_ReleaseDate] datetime  NULL,
    [Driver1_ReleaseBy] nvarchar(max)  NULL,
    [Driver1_ExpiryDate] datetime  NULL,
    [Driver2_Salutation] nvarchar(max)  NULL,
    [Driver2_GivenName] nvarchar(max)  NULL,
    [Driver2_Surname] nvarchar(max)  NULL,
    [Driver2_CardId] nvarchar(max)  NULL,
    [Driver2_ClientSex] nvarchar(max)  NULL,
    [Driver2_ClientSexId] nvarchar(max)  NULL,
    [Driver2_Occupation] nvarchar(max)  NULL,
    [Driver2_OccupationId] nvarchar(max)  NULL,
    [Driver2_BirthDate] datetime  NULL,
    [Driver2_Age] smallint  NULL,
    [Driver2_LicenseNo] nvarchar(max)  NULL,
    [Driver2_LicenseType] nvarchar(max)  NULL,
    [Driver2_LicenseTypeId] nvarchar(max)  NULL,
    [Driver2_ReleaseDate] datetime  NULL,
    [Driver2_ReleaseBy] nvarchar(max)  NULL,
    [Driver2_ExpiryDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_InspectionSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_InspectionSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [PhoneNo_Mobile] nvarchar(max)  NULL,
    [PhoneNo_Home] nvarchar(max)  NULL,
    [PhoneNo_Office] nvarchar(max)  NULL,
    [InspectionDate] datetime  NULL,
    [InspectionTime] nvarchar(max)  NULL,
    [InspectionPlace] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_PaymentSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_DCRSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMotor_VOL1Std_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_TaxInvoiceSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_TaxInvoiceSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [Building] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ReceiptNo] nvarchar(max)  NULL,
    [ReceiptDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_PolicySet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [OldPolicyNo] nvarchar(max)  NOT NULL,
    [PolicyType] nvarchar(max)  NOT NULL,
    [PolicyTypeId] nvarchar(max)  NOT NULL,
    [InceptionEndDate] datetime  NULL,
    [PolicyLanguage] nvarchar(max)  NOT NULL,
    [PolicyLanguageId] nvarchar(max)  NOT NULL,
    [ImageCapture] nvarchar(max)  NOT NULL,
    [Remark] nvarchar(max)  NOT NULL,
    [Channel] nvarchar(max)  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [PolicyCustomization] nvarchar(max)  NOT NULL,
    [PolicyCustomizationCode] nvarchar(max)  NOT NULL,
    [GenMotor_VOL2Plus_Assured_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_Vehicle_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_Payment_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_OldPolicy_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_Plan_ItemId] bigint  NOT NULL,
    [GenMotor_VOL2Plus_TaxInvoice_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_AssuredSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsuredType] nvarchar(max)  NULL,
    [InsuredTypeCode] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [CardIssueDate] datetime  NULL,
    [CardExpiryDate] datetime  NULL,
    [MotorOccupation] nvarchar(max)  NULL,
    [CardIssueBy] nvarchar(max)  NULL,
    [MotorOccupationId] nvarchar(max)  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [BeneficiaryFullName] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [CorporateBranchCode] nvarchar(max)  NULL,
    [CorporateName] nvarchar(max)  NULL,
    [CorporateTaxId] nvarchar(max)  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_VehicleSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_VehicleSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleBrandId] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleModelId] nvarchar(max)  NULL,
    [VehicleType] nvarchar(max)  NULL,
    [VehicleTypeId] nvarchar(max)  NULL,
    [VehicleColor] nvarchar(max)  NULL,
    [VehicleColorId] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ChassisNo] nvarchar(max)  NULL,
    [EngineNo] nvarchar(max)  NULL,
    [VehicleBuyingYear] nvarchar(max)  NULL,
    [VehicleModelYear] nvarchar(max)  NULL,
    [PassengerSeat] nvarchar(max)  NULL,
    [EngineCapacity] nvarchar(max)  NULL,
    [VehicleWeight] nvarchar(max)  NULL,
    [GearType] nvarchar(max)  NULL,
    [GearTypeId] nvarchar(max)  NULL,
    [CarType] nvarchar(max)  NULL,
    [CarTypeId] nvarchar(max)  NULL,
    [VehicleAge] int  NULL,
    [HasSpecialEquipment] bit  NULL,
    [SpecialEquipmentDescription] nvarchar(max)  NULL,
    [SpecialEquipmentPrice] decimal(18,2)  NULL,
    [HasAccessories] bit  NULL,
    [AccessoryName1] nvarchar(max)  NULL,
    [AccessoryName2] nvarchar(max)  NULL,
    [AccessoryName3] nvarchar(max)  NULL,
    [AccessoryNamePrice1] decimal(18,2)  NULL,
    [AccessoryNamePrice2] decimal(18,2)  NULL,
    [AccessoryNamePrice3] decimal(18,2)  NULL,
    [VehicleBodyType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_AuditTrailSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_OldPolicySet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_OldPolicySet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsurerName] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [CancellationDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_PaymentSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_DCRSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMotor_VOL2Plus_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_PlanSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PricePlanId] bigint  NOT NULL,
    [PolicyVolumeId] nvarchar(max)  NULL,
    [PolicyVolume] nvarchar(max)  NULL,
    [RepairingTypeId] nvarchar(max)  NULL,
    [RepairingType] nvarchar(max)  NULL,
    [PackageName] nvarchar(max)  NULL,
    [EngineFrom] decimal(18,0)  NULL,
    [EngineTo] decimal(18,0)  NULL,
    [WeightFrom] decimal(18,0)  NULL,
    [WeightTo] decimal(18,2)  NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [BasicPremium] decimal(18,2)  NULL,
    [TPBI_EachPerson] decimal(18,2)  NULL,
    [TPBI_EachAccident] decimal(18,2)  NULL,
    [TPPD_EachAccident] decimal(18,2)  NULL,
    [PA_EachPerson] decimal(18,2)  NULL,
    [Driver] decimal(18,2)  NULL,
    [Passenger] decimal(18,2)  NULL,
    [BB_EachAccident] decimal(18,2)  NULL,
    [OwnDamage] decimal(18,2)  NULL,
    [ThirdParty] decimal(18,2)  NULL,
    [Fleet_Rate] decimal(18,2)  NULL,
    [Fleet_Amount] decimal(18,2)  NULL,
    [NCB_Rate] decimal(18,2)  NULL,
    [NCB_Amount] decimal(18,2)  NULL,
    [Other_Rate] decimal(18,2)  NULL,
    [Other_Amount] decimal(18,2)  NULL,
    [Fire] decimal(18,2)  NULL,
    [Thelf] decimal(18,2)  NULL,
    [Collision] decimal(18,2)  NULL,
    [Base] decimal(18,2)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [Basis_Premium] nvarchar(max)  NULL,
    [Package] nvarchar(max)  NULL,
    [Net_Premium] decimal(18,2)  NULL,
    [Total_Premium] decimal(18,2)  NULL,
    [Medical_Expense] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_MasterPricePlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PolicyVolumeId] nvarchar(max)  NULL,
    [RepairingTypeId] nvarchar(max)  NULL,
    [PackageName] nvarchar(max)  NULL,
    [EngineFrom] decimal(18,0)  NULL,
    [EngineTo] decimal(18,0)  NULL,
    [WeightFrom] decimal(18,0)  NULL,
    [WeightTo] decimal(18,2)  NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [BasicPremium] decimal(18,2)  NULL,
    [TPBI_EachPerson] decimal(18,2)  NULL,
    [TPBI_EachAccident] decimal(18,2)  NULL,
    [TPPD_EachAccident] decimal(18,2)  NULL,
    [PA_EachPerson] decimal(18,2)  NULL,
    [Driver] decimal(18,2)  NULL,
    [Passenger] decimal(18,2)  NULL,
    [BB_EachAccident] decimal(18,2)  NULL,
    [OwnDamage] decimal(18,2)  NULL,
    [ThirdParty] decimal(18,2)  NULL,
    [Fleet_Rate] decimal(18,2)  NULL,
    [Fleet_Amount] decimal(18,2)  NULL,
    [NCB_Rate] decimal(18,2)  NULL,
    [NCB_Amount] decimal(18,2)  NULL,
    [Other_Rate] decimal(18,2)  NULL,
    [Other_Amount] decimal(18,2)  NULL,
    [Fire] decimal(18,2)  NULL,
    [Thelf] decimal(18,2)  NULL,
    [Collision] decimal(18,2)  NULL,
    [Base] decimal(18,2)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [Basis_Premium] nvarchar(max)  NULL,
    [Package] nvarchar(max)  NULL,
    [Net_Premium] decimal(18,2)  NULL,
    [Total_Premium] decimal(18,2)  NULL,
    [Medical_Expense] decimal(18,2)  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_TaxInvoiceSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_TaxInvoiceSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [Building] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ReceiptNo] nvarchar(max)  NULL,
    [ReceiptDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_VOL2Plus_MasterCoverageSet'
CREATE TABLE [dbo].[GenMotor_VOL2Plus_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_VOL1Std_MasterCoverageSet'
CREATE TABLE [dbo].[GenMotor_VOL1Std_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_PolicySet'
CREATE TABLE [dbo].[GenMotor_CMI_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [OldPolicyNo] nvarchar(max)  NOT NULL,
    [PolicyType] nvarchar(max)  NOT NULL,
    [PolicyTypeId] nvarchar(max)  NOT NULL,
    [InceptionEndDate] datetime  NULL,
    [PolicyLanguage] nvarchar(max)  NOT NULL,
    [PolicyLanguageId] nvarchar(max)  NOT NULL,
    [ImageCapture] nvarchar(max)  NOT NULL,
    [Remark] nvarchar(max)  NOT NULL,
    [Channel] nvarchar(max)  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [PolicyCustomization] nvarchar(max)  NOT NULL,
    [PolicyCustomizationCode] nvarchar(max)  NOT NULL,
    [VMI_ProposalNo] nvarchar(max)  NULL,
    [StickerNo] nvarchar(max)  NOT NULL,
    [StickerYear] nvarchar(max)  NOT NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [GenMotor_CMI_Vehicle_ItemId] bigint  NOT NULL,
    [GenMotor_CMI_Assured_ItemId] bigint  NOT NULL,
    [GenMotor_CMI_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMotor_CMI_Payment_ItemId] bigint  NOT NULL,
    [GenMotor_CMI_Plan_ItemId] bigint  NOT NULL,
    [GenMotor_CMI_TaxInvoice_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_CMI_VehicleSet'
CREATE TABLE [dbo].[GenMotor_CMI_VehicleSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleBrandId] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleModelId] nvarchar(max)  NULL,
    [VehicleColor] nvarchar(max)  NULL,
    [VehicleColorId] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceAbbreviation] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ChassisNo] nvarchar(max)  NULL,
    [EngineNo] nvarchar(max)  NULL,
    [VehicleBuyingYear] nvarchar(max)  NULL,
    [VehicleModelYear] nvarchar(max)  NULL,
    [PassengerSeat] nvarchar(max)  NULL,
    [EngineCapacity] nvarchar(max)  NULL,
    [VehicleWeight] nvarchar(max)  NULL,
    [GearType] nvarchar(max)  NULL,
    [GearTypeId] nvarchar(max)  NULL,
    [CarType] nvarchar(max)  NULL,
    [CarTypeId] nvarchar(max)  NULL,
    [VehicleAge] int  NULL,
    [HasSpecialEquipment] bit  NULL,
    [SpecialEquipmentDescription] nvarchar(max)  NULL,
    [SpecialEquipmentPrice] decimal(18,2)  NULL,
    [HasAccessories] bit  NULL,
    [AccessoryName1] nvarchar(max)  NULL,
    [AccessoryName2] nvarchar(max)  NULL,
    [AccessoryName3] nvarchar(max)  NULL,
    [AccessoryNamePrice1] decimal(18,2)  NULL,
    [AccessoryNamePrice2] decimal(18,2)  NULL,
    [AccessoryNamePrice3] decimal(18,2)  NULL,
    [VehicleBodyType] nvarchar(max)  NULL,
    [VehicleSubModel] nvarchar(max)  NULL,
    [VehicleSubModelId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_AuditTrailSet'
CREATE TABLE [dbo].[GenMotor_CMI_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotor_CMI_PaymentSet'
CREATE TABLE [dbo].[GenMotor_CMI_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [CreditCardBankNameId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_DCRSet'
CREATE TABLE [dbo].[GenMotor_CMI_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMotor_CMI_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotor_CMI_PlanSet'
CREATE TABLE [dbo].[GenMotor_CMI_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PricePlanId] bigint  NOT NULL,
    [MotorCodeVMI] nvarchar(max)  NULL,
    [MotorCodeCMI] nvarchar(max)  NULL,
    [NetPremium] decimal(18,2)  NOT NULL,
    [Stamp] decimal(18,2)  NOT NULL,
    [Vat] decimal(18,2)  NOT NULL,
    [TotalPremium] decimal(18,2)  NOT NULL
);
GO

-- Creating table 'GenMotor_CMI_AssuredSet'
CREATE TABLE [dbo].[GenMotor_CMI_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [InsuredType] nvarchar(max)  NULL,
    [InsuredTypeCode] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [CardIssueDate] datetime  NULL,
    [CardExpiryDate] datetime  NULL,
    [MotorOccupation] nvarchar(max)  NULL,
    [CardIssueBy] nvarchar(max)  NULL,
    [MotorOccupationId] nvarchar(max)  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [BeneficiaryFullName] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [CorporateBranchCode] nvarchar(max)  NULL,
    [CorporateName] nvarchar(max)  NULL,
    [CorporateTaxId] nvarchar(max)  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [Address2] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotor_CMI_MasterPricePlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [MotorCodeVMI] nvarchar(max)  NULL,
    [VehicleWeightFrom] int  NOT NULL,
    [VehicleWeightTo] int  NOT NULL,
    [NetPremium] decimal(18,2)  NOT NULL,
    [Stamp] decimal(18,2)  NOT NULL,
    [Vat] decimal(18,2)  NOT NULL,
    [TotalPremium] decimal(18,2)  NOT NULL,
    [MotorCodeCMI] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_MasterCoverageSet'
CREATE TABLE [dbo].[GenMotor_CMI_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotor_CMI_TaxInvoiceSet'
CREATE TABLE [dbo].[GenMotor_CMI_TaxInvoiceSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [HomeNo] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Village] nvarchar(max)  NULL,
    [Building] nvarchar(max)  NULL,
    [SubAlley] nvarchar(max)  NULL,
    [Alley] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [BranchDistrict] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [BranchDistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ReceiptNo] nvarchar(max)  NULL,
    [ReceiptDate] datetime  NULL
);
GO

-- Creating table 'VehicleBrandSet'
CREATE TABLE [dbo].[VehicleBrandSet] (
    [VehicleBrandId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'MenuPreferenceSet'
CREATE TABLE [dbo].[MenuPreferenceSet] (
    [MenuPreferenceId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [CoverImage] nvarchar(max)  NULL,
    [PreferencesControllerName] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenCancer_PolicySet'
CREATE TABLE [dbo].[GenCancer_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [IsHealthCheck] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GenCancer_Assured_ItemId] bigint  NOT NULL,
    [GenCancer_AuditTrail_ItemId] bigint  NOT NULL,
    [GenCancer_Payer_ItemId] bigint  NOT NULL,
    [GenCancer_Payment_ItemId] bigint  NOT NULL,
    [GenCancer_Plan_ItemId] bigint  NOT NULL,
    [GenCancer_UnderWriteInfo_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenCancer_AssuredSet'
CREATE TABLE [dbo].[GenCancer_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3] nvarchar(max)  NULL,
    [ClientTelPhoneNumber4] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber3_Phone] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_To] nvarchar(max)  NULL,
    [ClientTelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [ClientTelPhoneNumber2_Extension] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,2)  NULL,
    [Weight] decimal(18,2)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [AddressType] nvarchar(max)  NOT NULL,
    [AddressTypeCode] nvarchar(max)  NOT NULL,
    [ClientAddress1] nvarchar(max)  NULL,
    [ClientAddress2] nvarchar(max)  NULL,
    [ClientAddress3] nvarchar(max)  NULL,
    [ClientAddress4] nvarchar(max)  NULL,
    [ClientAddress5] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [MobileNumber] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [RegistrationAddress1] nvarchar(max)  NULL,
    [RegistrationAddress2] nvarchar(max)  NULL,
    [RegistrationAddress3] nvarchar(max)  NULL,
    [RegistrationAddress4] nvarchar(max)  NULL,
    [RegistrationAddress5] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_ProvinceId] nvarchar(max)  NULL,
    [Registration_SubDistrictId] nvarchar(max)  NULL,
    [Registration_DistrictId] nvarchar(max)  NULL,
    [Registration_MobileNumber] nvarchar(max)  NOT NULL,
    [Registration_Email] nvarchar(max)  NOT NULL,
    [OfficeAddress1] nvarchar(max)  NULL,
    [OfficeAddress2] nvarchar(max)  NULL,
    [OfficeAddress3] nvarchar(max)  NULL,
    [OfficeAddress4] nvarchar(max)  NULL,
    [OfficeAddress5] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_ProvinceId] nvarchar(max)  NULL,
    [Office_SubDistrictId] nvarchar(max)  NULL,
    [Office_DistrictId] nvarchar(max)  NULL,
    [Office_MobileNumber] nvarchar(max)  NOT NULL,
    [Office_Email] nvarchar(max)  NOT NULL,
    [PresentAddress1] nvarchar(max)  NULL,
    [PresentAddress2] nvarchar(max)  NULL,
    [PresentAddress3] nvarchar(max)  NULL,
    [PresentAddress4] nvarchar(max)  NULL,
    [PresentAddress5] nvarchar(max)  NULL,
    [Present_PostCode] nvarchar(max)  NULL,
    [Present_ProvinceId] nvarchar(max)  NULL,
    [Present_SubDistrictId] nvarchar(max)  NULL,
    [Present_DistrictId] nvarchar(max)  NULL,
    [Present_MobileNumber] nvarchar(max)  NOT NULL,
    [Present_Email] nvarchar(max)  NOT NULL,
    [BMI] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancer_AuditTrailSet'
CREATE TABLE [dbo].[GenCancer_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenCancer_PlanSet'
CREATE TABLE [dbo].[GenCancer_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [PlanAnnaulizedPremium] decimal(18,2)  NULL
);
GO

-- Creating table 'GenCancer_PayerSet'
CREATE TABLE [dbo].[GenCancer_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [PayerAddress1] nvarchar(max)  NULL,
    [PayerAddress2] nvarchar(max)  NULL,
    [PayerAddress3] nvarchar(max)  NULL,
    [PayerAddress4] nvarchar(max)  NULL,
    [PayerAddress5] nvarchar(max)  NULL,
    [PayerEmailAddress] nvarchar(max)  NULL,
    [PayerTaxId] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancer_PaymentSet'
CREATE TABLE [dbo].[GenCancer_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenCancer_DCRSet'
CREATE TABLE [dbo].[GenCancer_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [GenHealthLumpSum_PolicyProposalId] bigint  NOT NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenCancer_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenCancer_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenCancer_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer05_01] nvarchar(max)  NULL,
    [Answer05_02] nvarchar(max)  NULL,
    [Answer05_03] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem04] nvarchar(max)  NULL,
    [Answer06_Rem05] nvarchar(max)  NULL,
    [Answer06_Rem06] nvarchar(max)  NULL,
    [Answer06_Rem07] nvarchar(max)  NULL,
    [Answer06_Rem08] nvarchar(max)  NULL,
    [Answer06_Rem09] nvarchar(max)  NULL,
    [Answer06_Rem10] nvarchar(max)  NULL,
    [Answer06_Rem11] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer11] nvarchar(max)  NULL,
    [Answer12] nvarchar(max)  NULL,
    [Answer11_Rem01] nvarchar(max)  NULL,
    [Answer11_Rem02] nvarchar(max)  NULL,
    [Answer11_Rem03] nvarchar(max)  NULL,
    [Answer12_Rem01] nvarchar(max)  NULL,
    [Answer12_Rem02] nvarchar(max)  NULL,
    [Answer12_Rem03] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancer_MasterPricePlanSet'
CREATE TABLE [dbo].[GenCancer_MasterPricePlanSet] (
    [PlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanType] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [API] decimal(18,2)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SA] decimal(18,2)  NULL,
    [FirstTimeInstalmentPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [First_Diagnose] decimal(18,2)  NULL,
    [Second_Opinion] decimal(18,2)  NULL,
    [Monthly_Benefit_Amount] decimal(18,2)  NULL,
    [Monthly_Benefit_Times] decimal(18,2)  NULL,
    [Male_Specific_Benefit] decimal(18,2)  NULL,
    [Funeral_Expenses] decimal(18,2)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenCancer_MasterCoverageSet'
CREATE TABLE [dbo].[GenCancer_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancer_LegalScriptTemplateSet'
CREATE TABLE [dbo].[GenCancer_LegalScriptTemplateSet] (
    [ScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PayerPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [PayerPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CreditCardParagraph] nvarchar(max)  NULL,
    [AssuredPayment_CashTransferParagraph] nvarchar(max)  NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'AddressTypeSet'
CREATE TABLE [dbo].[AddressTypeSet] (
    [AddressTypeId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1Std_PolicySet'
CREATE TABLE [dbo].[GenMotorType1Std_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionEndDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [IsGuaranteeClaimService] nvarchar(max)  NULL,
    [StickerNo] nvarchar(max)  NULL,
    [StickerYear] nvarchar(max)  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [PolicyType] nvarchar(max)  NULL,
    [IsCompleteDocument] bit  NULL,
    [CompleteDocumentRemark] nvarchar(max)  NULL,
    [CompleteDocumentDate] datetime  NULL,
    [MarketingDiscountCode] nvarchar(max)  NULL,
    [MarketingDiscount] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [GenMotorType1Std_Assured_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_AuditTrail_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_CTPL_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_Driver_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_Inspection_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_OldPolicy_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_Payment_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_Plan_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_Vehicle_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_PlanDiscount_ItemId] bigint  NOT NULL,
    [GenMotorType1Std_ReferDocument_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenMotorType1Std_AssuredSet'
CREATE TABLE [dbo].[GenMotorType1Std_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationId] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexId] nvarchar(max)  NULL,
    [MobileNumber] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [CardIssueBy] nvarchar(max)  NULL,
    [CardIssueDate] datetime  NULL,
    [CardExpiryDate] datetime  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationId] nvarchar(max)  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [Address2] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [CorporateBranchName] nvarchar(max)  NULL,
    [CorporateName] nvarchar(max)  NULL,
    [CorporateTax] nvarchar(max)  NULL,
    [BeneficiaryFullName] nvarchar(max)  NULL,
    [AssuredType] nvarchar(max)  NOT NULL,
    [MobileNumber2] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [Policy_Address] nvarchar(max)  NULL,
    [Policy_Address2] nvarchar(max)  NULL,
    [Policy_SubDistrict] nvarchar(max)  NULL,
    [Policy_District] nvarchar(max)  NULL,
    [Policy_Province] nvarchar(max)  NULL,
    [Policy_PostCode] nvarchar(max)  NULL,
    [Policy_SubDistrictId] nvarchar(max)  NULL,
    [Policy_DistrictId] nvarchar(max)  NULL,
    [Policy_ProvinceId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotorType1Std_PlanSet'
CREATE TABLE [dbo].[GenMotorType1Std_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Brand] nvarchar(max)  NULL,
    [BrandId] nvarchar(max)  NOT NULL,
    [ModelYear] nvarchar(max)  NULL,
    [ModelAge] nvarchar(max)  NOT NULL,
    [Model] nvarchar(max)  NULL,
    [MotorCodeVMI] nvarchar(max)  NULL,
    [MotorCodeVMIId] nvarchar(max)  NULL,
    [MotorCodeCMI] nvarchar(max)  NOT NULL,
    [SubModel] nvarchar(max)  NULL,
    [Package] nvarchar(max)  NULL,
    [VehicleGroup] nvarchar(max)  NULL,
    [Capacity] nvarchar(max)  NULL,
    [CapacityId] nvarchar(max)  NULL,
    [PolicyType] nvarchar(max)  NULL,
    [PolicyTypeId] nvarchar(max)  NULL,
    [SumInsure] decimal(18,2)  NULL,
    [OwnDamage] nvarchar(max)  NULL,
    [FireAndTheft] nvarchar(max)  NULL,
    [InjuryOrDeathPerPerson] decimal(18,2)  NULL,
    [InjuryOrDeathPerAccident] decimal(18,2)  NULL,
    [ThirdPartyPropertyDamage] decimal(18,2)  NULL,
    [PersonalAccidentPassengerNo] int  NULL,
    [PersonalAccidentDriver1] decimal(18,2)  NULL,
    [MedicalExpensePassengerNo] int  NULL,
    [MedicalExpenseDriver1] decimal(18,2)  NULL,
    [BailBond] decimal(18,2)  NULL,
    [RepairingTypeId] nvarchar(max)  NOT NULL,
    [RepairingType] nvarchar(max)  NOT NULL,
    [NetPremium] decimal(18,2)  NULL,
    [TotalPremium] decimal(18,2)  NULL,
    [CTPLPremium] decimal(18,2)  NULL,
    [GrandTotalPremium] decimal(18,2)  NULL,
    [GenAssistanceId] nvarchar(max)  NULL,
    [GenAssistance] nvarchar(max)  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanVersion] nvarchar(max)  NULL,
    [PlanExpiryDate] datetime  NULL,
    [PricePlanId] int  NULL
);
GO

-- Creating table 'GenMotorType1Std_CTPLSet'
CREATE TABLE [dbo].[GenMotorType1Std_CTPLSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [IsCTPL] bit  NULL,
    [CTPLPremium] decimal(18,2)  NULL,
    [CTPLMotorCode] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionEndDate] datetime  NULL,
    [PolicyNo] nvarchar(20)  NULL
);
GO

-- Creating table 'GenMotorType1Std_AuditTrailSet'
CREATE TABLE [dbo].[GenMotorType1Std_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1Std_InspectionSet'
CREATE TABLE [dbo].[GenMotorType1Std_InspectionSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationId] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [PhoneNo_Mobile] nvarchar(max)  NULL,
    [PhoneNo_Home] nvarchar(max)  NULL,
    [PhoneNo_Office] nvarchar(max)  NULL,
    [InspectionDate] datetime  NULL,
    [InspectionTime] nvarchar(max)  NULL,
    [Inspection_Address] nvarchar(max)  NULL,
    [Inspection_Address2] nvarchar(max)  NULL,
    [Inspection_SubDistrictId] nvarchar(max)  NULL,
    [Inspection_SubDistrict] nvarchar(max)  NULL,
    [Inspection_District] nvarchar(max)  NULL,
    [Inspection_DistrictId] nvarchar(max)  NULL,
    [Inspection_Province] nvarchar(max)  NULL,
    [Inspection_ProvinceId] nvarchar(max)  NULL,
    [Inspection_PostCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [ReceiveImageDate] datetime  NULL,
    [ClaimDIDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1Std_DriverSet'
CREATE TABLE [dbo].[GenMotorType1Std_DriverSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SpecificDriverId] nvarchar(max)  NULL,
    [SpecificDriver] nvarchar(max)  NULL,
    [Driver1_Salutation] nvarchar(max)  NULL,
    [Driver1_SalutationId] nvarchar(max)  NULL,
    [Driver1_GivenName] nvarchar(max)  NULL,
    [Driver1_Surname] nvarchar(max)  NULL,
    [Driver1_CardId] nvarchar(max)  NULL,
    [Driver1_ClientSex] nvarchar(max)  NULL,
    [Driver1_ClientSexId] nvarchar(max)  NULL,
    [Driver1_Occupation] nvarchar(max)  NULL,
    [Driver1_OccupationId] nvarchar(max)  NULL,
    [Driver1_BirthDate] datetime  NULL,
    [Driver1_Age] smallint  NULL,
    [Driver1_LicenseNo] nvarchar(max)  NULL,
    [Driver1_LicenseType] nvarchar(max)  NULL,
    [Driver1_LicenseTypeId] nvarchar(max)  NULL,
    [Driver1_ReleaseDate] datetime  NULL,
    [Driver1_ReleaseBy] nvarchar(max)  NULL,
    [Driver1_ExpiryDate] datetime  NULL,
    [Driver2_Salutation] nvarchar(max)  NULL,
    [Driver2_SalutationId] nvarchar(max)  NULL,
    [Driver2_GivenName] nvarchar(max)  NULL,
    [Driver2_Surname] nvarchar(max)  NULL,
    [Driver2_CardId] nvarchar(max)  NULL,
    [Driver2_ClientSex] nvarchar(max)  NULL,
    [Driver2_ClientSexId] nvarchar(max)  NULL,
    [Driver2_Occupation] nvarchar(max)  NULL,
    [Driver2_OccupationId] nvarchar(max)  NULL,
    [Driver2_BirthDate] datetime  NULL,
    [Driver2_Age] smallint  NULL,
    [Driver2_LicenseNo] nvarchar(max)  NULL,
    [Driver2_LicenseType] nvarchar(max)  NULL,
    [Driver2_LicenseTypeId] nvarchar(max)  NULL,
    [Driver2_ReleaseDate] datetime  NULL,
    [Driver2_ReleaseBy] nvarchar(max)  NULL,
    [Driver2_ExpiryDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1Std_OldPolicySet'
CREATE TABLE [dbo].[GenMotorType1Std_OldPolicySet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [IsReNewPolicy] bit  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InsurerName] nvarchar(max)  NULL,
    [PolicyExpiryDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1Std_PaymentSet'
CREATE TABLE [dbo].[GenMotorType1Std_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardBankNameId] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotorType1Std_VehicleSet'
CREATE TABLE [dbo].[GenMotorType1Std_VehicleSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleBrandId] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleModelId] nvarchar(max)  NULL,
    [VehicleColor] nvarchar(max)  NULL,
    [VehicleColorId] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceAbbreviation] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [ChassisNo] nvarchar(max)  NULL,
    [EngineNo] nvarchar(max)  NULL,
    [VehicleBuyingYear] nvarchar(max)  NULL,
    [VehicleModelYear] nvarchar(max)  NULL,
    [PassengerSeat] nvarchar(max)  NULL,
    [EngineCapacity] nvarchar(max)  NULL,
    [VehicleWeight] nvarchar(max)  NULL,
    [GearType] nvarchar(max)  NULL,
    [GearTypeId] nvarchar(max)  NULL,
    [CarType] nvarchar(max)  NULL,
    [CarTypeId] nvarchar(max)  NULL,
    [VehicleAge] int  NULL,
    [HasSpecialEquipment] bit  NULL,
    [SpecialEquipmentDescription] nvarchar(max)  NULL,
    [SpecialEquipmentPrice] decimal(18,2)  NULL,
    [HasAccessories] bit  NULL,
    [AccessoryName1] nvarchar(max)  NULL,
    [AccessoryName2] nvarchar(max)  NULL,
    [AccessoryName3] nvarchar(max)  NULL,
    [AccessoryNamePrice1] decimal(18,2)  NULL,
    [AccessoryNamePrice2] decimal(18,2)  NULL,
    [AccessoryNamePrice3] decimal(18,2)  NULL,
    [VehicleBodyType] nvarchar(max)  NULL,
    [CarService_Province] nvarchar(max)  NULL,
    [CarService_ProvinceId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotorType1Std_PlanDiscountSet'
CREATE TABLE [dbo].[GenMotorType1Std_PlanDiscountSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [IsEvidence] nvarchar(max)  NULL,
    [EvidenceDocument] nvarchar(max)  NULL,
    [EvidenceDocumentId] nvarchar(max)  NULL,
    [HasClaimHistory] nvarchar(max)  NULL,
    [NCBonPolicySchedule] nvarchar(max)  NULL,
    [NameDriver] nvarchar(max)  NULL,
    [HasSpecialEquipment] nvarchar(max)  NULL,
    [SpecialEquipment] nvarchar(max)  NULL,
    [SpecialEquipmentId] nvarchar(max)  NULL,
    [Deductible_OwnDamage] decimal(18,2)  NULL,
    [Deductible_ThirdPartyPropertyDamage] decimal(18,2)  NULL,
    [Deductible_TotalDeductibleDiscount] decimal(18,2)  NULL,
    [NameDriverId] nvarchar(max)  NULL,
    [NCBPercentageId] nvarchar(max)  NULL,
    [NCBPercentage] decimal(18,2)  NULL,
    [CCTV] nvarchar(max)  NULL,
    [CCTVId] nvarchar(max)  NULL,
    [SpecialDiscount] nvarchar(max)  NULL,
    [SpecialDiscountId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenMotorType1Std_DCRSet'
CREATE TABLE [dbo].[GenMotorType1Std_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenMotorType1Std_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenGroupSME_PolicySet'
CREATE TABLE [dbo].[GenGroupSME_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [GenGroupSME_Payment_ItemId] int  NOT NULL,
    [GenGroupSME_Assured_ItemId] bigint  NOT NULL,
    [GenGroupSME_AuditTrail_ItemId] bigint  NOT NULL,
    [GenGroupSME_Plan_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenGroupSME_DCRSet'
CREATE TABLE [dbo].[GenGroupSME_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenGroupSME_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenGroupSME_PaymentSet'
CREATE TABLE [dbo].[GenGroupSME_PaymentSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PaymentFormat] nvarchar(20)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL
);
GO

-- Creating table 'GenGroupSME_AssuredSet'
CREATE TABLE [dbo].[GenGroupSME_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [CorporateName] nvarchar(max)  NOT NULL,
    [BusinessType] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenGroupSME_EmployeeSet'
CREATE TABLE [dbo].[GenGroupSME_EmployeeSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [MemberId] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Surname] nvarchar(max)  NOT NULL,
    [Gender] nvarchar(max)  NULL,
    [DateOfBirth] datetime  NULL,
    [Age] nvarchar(max)  NULL,
    [Income] nvarchar(max)  NULL,
    [Telephone] nvarchar(max)  NULL,
    [Plan] nvarchar(max)  NULL,
    [Account] nvarchar(max)  NULL,
    [Bank] nvarchar(max)  NULL,
    [Company] nvarchar(max)  NULL,
    [EffectiveDate] datetime  NULL,
    [GenGroupSME_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenGroupSME_AuditTrailSet'
CREATE TABLE [dbo].[GenGroupSME_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenGroupSME_PlanSet'
CREATE TABLE [dbo].[GenGroupSME_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PlanCoverage1] nvarchar(10)  NULL,
    [PlanCode1] nvarchar(10)  NULL,
    [PlanName1] nvarchar(10)  NULL,
    [Employees1] int  NOT NULL,
    [Premium1] decimal(18,2)  NOT NULL,
    [PlanPremium1] decimal(18,2)  NOT NULL,
    [PlanCoverage2] nvarchar(10)  NULL,
    [PlanCode2] nvarchar(10)  NULL,
    [PlanName2] nvarchar(10)  NULL,
    [Employees2] int  NOT NULL,
    [Premium2] decimal(18,2)  NOT NULL,
    [PlanPremium2] decimal(18,2)  NOT NULL,
    [PlanCoverage3] nvarchar(10)  NULL,
    [PlanCode3] nvarchar(10)  NULL,
    [PlanName3] nvarchar(10)  NULL,
    [Employees3] int  NOT NULL,
    [Premium3] decimal(18,2)  NOT NULL,
    [PlanPremium3] decimal(18,2)  NOT NULL,
    [AnnualizedPremium] decimal(18,2)  NOT NULL
);
GO

-- Creating table 'GenGroupSME_MasterPricePlanSet'
CREATE TABLE [dbo].[GenGroupSME_MasterPricePlanSet] (
    [Coverage] nvarchar(10)  NOT NULL,
    [Plan] nvarchar(2)  NOT NULL,
    [SMESize] nvarchar(1)  NOT NULL,
    [Premium] decimal(18,2)  NOT NULL
);
GO

-- Creating table 'CreditCardClassTypeSet'
CREATE TABLE [dbo].[CreditCardClassTypeSet] (
    [CreditCardClassTypeId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'MotorConstantSet'
CREATE TABLE [dbo].[MotorConstantSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CategoryName] nvarchar(max)  NOT NULL,
    [Seqn] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'MotorPolicyNumberSet'
CREATE TABLE [dbo].[MotorPolicyNumberSet] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [StickerNo] nvarchar(max)  NULL,
    [StickerYear] nvarchar(max)  NULL,
    [LotName] nvarchar(max)  NULL,
    [IsUsed] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UsedByProductId] nvarchar(max)  NULL,
    [ProposalNo] nvarchar(max)  NULL
);
GO

-- Creating table 'GroupCompanySet'
CREATE TABLE [dbo].[GroupCompanySet] (
    [GroupCompanyId] int IDENTITY(1,1) NOT NULL,
    [PolicyNo] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'PaymentMethodSet'
CREATE TABLE [dbo].[PaymentMethodSet] (
    [Code] nvarchar(20)  NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [FileName] nvarchar(20)  NULL,
    [Note] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [DownloadActive] bit  NOT NULL,
    [DownloadCode] nvarchar(50)  NULL
);
GO

-- Creating table 'MasterPolicySet'
CREATE TABLE [dbo].[MasterPolicySet] (
    [PRODUCTNAME] varchar(16)  NOT NULL,
    [CRMACTIVITYID] bigint  NULL,
    [INCEPTIONDATE] datetime  NULL,
    [APPLICATIONNO] nvarchar(20)  NOT NULL,
    [POLICYNO] nvarchar(20)  NULL,
    [PLAN_API] decimal(18,2)  NOT NULL,
    [API] decimal(30,2)  NULL,
    [INSTALMENTPREMIUM] decimal(18,2)  NOT NULL,
    [PLANCODE] nvarchar(max)  NULL,
    [CREATE_DATE] datetime  NULL,
    [ASSURED_FIRSTNAME] nvarchar(max)  NULL,
    [ASSURED_LASTNAME] nvarchar(max)  NULL,
    [ASSURED_FULLNAME] nvarchar(max)  NULL,
    [ASSURED_AGE] int  NULL,
    [ASSURED_GENDER] nvarchar(max)  NULL,
    [ASSURED_PHONE] nvarchar(max)  NULL,
    [ASSURED_OCCUPATIONCLASS] nvarchar(max)  NOT NULL,
    [ASSURED_OCCUPATION] nvarchar(max)  NULL,
    [ASSURED_ANNUALINCOME] decimal(18,2)  NULL,
    [PID] nvarchar(max)  NULL,
    [ASSURED_POSITION] nvarchar(max)  NOT NULL,
    [ASSURED_PROVINCE] nvarchar(max)  NULL,
    [ASSURED_POSTCODE] nvarchar(max)  NULL,
    [CREATED_BY_CODE] nvarchar(128)  NULL,
    [CREATED_BY] nvarchar(max)  NULL,
    [PROPOSALID] bigint  NOT NULL,
    [AUDITITEMID] bigint  NOT NULL,
    [PAYPLAN] nvarchar(max)  NULL,
    [PAYMENTMETHOD] nvarchar(max)  NULL,
    [CARDTYPE] varchar(max)  NOT NULL,
    [CARDNUMBER] nvarchar(max)  NOT NULL,
    [CREDITCARDTYPE] nvarchar(max)  NOT NULL,
    [CREDITCARDCLASSTYPE] nvarchar(max)  NOT NULL,
    [PAYMENT_REMARK] nvarchar(max)  NOT NULL,
    [YES_DCR] int  NOT NULL,
    [REMARK] varchar(12)  NOT NULL,
    [SUMINSURED] decimal(18,2)  NULL,
    [TSRNAME] nvarchar(200)  NULL,
    [CREDITCARD_EXPIRYDATE] datetime  NULL,
    [CREDITCARD_BANKNAME] nvarchar(max)  NULL,
    [APPROVECODE] nvarchar(20)  NULL,
    [DCR_DATE] datetime  NULL,
    [POLICY_TYPE] nvarchar(30)  NULL,
    [MOTOR_NETPREMIUM] decimal(18,2)  NULL,
    [MOTOR_CTPLPREMIUM] decimal(18,2)  NULL,
    [ApplicationStatus] nvarchar(2)  NULL,
    [DCRStatus] int  NULL,
    [DCRRemark] nvarchar(max)  NULL,
    [QCStatus] int  NULL,
    [QCDate] datetime  NULL,
    [QCRemark] nvarchar(max)  NULL,
    [PolicyStatus] nvarchar(2)  NULL,
    [UnderwriteRemark] nvarchar(max)  NULL,
    [SubmitDate] datetime  NULL,
    [SuccessDate] datetime  NULL,
    [ProspectId] bigint  NULL,
    [CompletedDate] datetime  NULL,
    [PLANNAME] nvarchar(max)  NULL,
    [UPDATE_DATE] datetime  NULL,
    [DCRPayment] nvarchar(max)  NULL,
    [PolicyNo2] nvarchar(20)  NULL,
    [PolicyResultDate] datetime  NULL,
    [UWProcessDate] datetime  NULL,
    [UWUpdatedDate] datetime  NULL,
    [UWUpdatedBy] nvarchar(max)  NULL,
    [UWProcessStatus] varchar(2)  NULL,
    [InspectionStatus] nvarchar(max)  NULL,
    [InspectionBy] nvarchar(max)  NULL,
    [InspectionNo] nvarchar(max)  NULL,
    [InspectionSubmitDate] datetime  NULL,
    [InspectionAppointDate] datetime  NULL,
    [InspectionApproveDate] datetime  NULL,
    [InspectionImageRecieveDate] datetime  NULL,
    [InspectionRemark] nvarchar(max)  NULL,
    [AccountingStatus] nvarchar(max)  NULL,
    [AccountingSubmitDate] datetime  NULL,
    [AccountingResultDate] datetime  NULL,
    [AccountingRemark] nvarchar(max)  NULL,
    [DeliveryStatus] nvarchar(2)  NULL,
    [DeliveryStatusDesc] nvarchar(max)  NULL,
    [DeliveryRemark] nvarchar(max)  NULL,
    [UWRejectReasonCode] int  NULL,
    [UWRejectReasonDesc] nvarchar(max)  NULL,
    [RequestCorrectionDate] datetime  NULL,
    [RequestCompletedDate] datetime  NULL,
    [FirstDeliveryDate] datetime  NULL,
    [UWRecieveDate] datetime  NULL,
    [ContactName] nvarchar(max)  NULL,
    [ContactPhoneNo] nvarchar(max)  NULL,
    [SaleInspectionNote] nvarchar(max)  NULL,
    [SaleInspectionAppointment] nvarchar(max)  NULL,
    [FullDeliveryAddress] nvarchar(max)  NULL,
    [FullInspectionAddress] nvarchar(max)  NULL,
    [VehicleBrand] nvarchar(max)  NULL,
    [VehicleModel] nvarchar(max)  NULL,
    [VehicleSubModel] nvarchar(max)  NULL,
    [LicensePlateNo] nvarchar(max)  NULL,
    [LicensePlateProvince] nvarchar(max)  NULL,
    [CarServiceProvince] nvarchar(max)  NULL,
    [VoucherValue] nvarchar(max)  NULL,
    [VoucherTrackingNo] nvarchar(max)  NULL,
    [VoucherDeliveryDate] datetime  NULL,
    [QCResponse] nvarchar(max)  NULL,
    [QCResponseDate] datetime  NULL,
    [QCRemarkDate] datetime  NULL,
    [AUWProcessDate] datetime  NULL,
    [AUWApproveCode] nvarchar(max)  NULL,
    [AUWStatusCode] nvarchar(max)  NULL,
    [AUWErrorMessage] nvarchar(max)  NULL,
    [QCTime] nvarchar(5)  NULL,
    [DCR_Datetime] datetime  NULL,
    [UPDATED_BY_CODE] nvarchar(128)  NULL,
    [RequestCollectDate] datetime  NULL,
    [AppStatus] int  NULL,
    [IsAUW] bit  NULL,
    [GEBCard] nvarchar(1)  NULL
);
GO

-- Creating table 'GenMotorType1Std_ReferDocumentSet'
CREATE TABLE [dbo].[GenMotorType1Std_ReferDocumentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [NationalIdCard] bit  NOT NULL,
    [CarRegistration] bit  NOT NULL,
    [DrivingLicense] bit  NOT NULL,
    [SpecialEquiptmentDocument] bit  NOT NULL,
    [CarInsuranceExpirationNotice] bit  NOT NULL,
    [CompanyCertificate] bit  NOT NULL,
    [CarSalesContract] bit  NOT NULL,
    [Remark] nvarchar(max)  NULL,
    [ClaimDI] nvarchar(max)  NULL
);
GO

-- Creating table 'MotorModelSet'
CREATE TABLE [dbo].[MotorModelSet] (
    [MotorModelId] int IDENTITY(1,1) NOT NULL,
    [Make] nvarchar(max)  NULL,
    [Model] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Body] nvarchar(max)  NOT NULL,
    [ModelCode] nvarchar(max)  NOT NULL,
    [MakeCode] nvarchar(max)  NOT NULL,
    [CC] nvarchar(max)  NOT NULL,
    [GasType] nvarchar(max)  NOT NULL,
    [Seat] nvarchar(max)  NOT NULL,
    [CarryCapacity] nvarchar(max)  NOT NULL,
    [HighRiskGroup] nvarchar(max)  NOT NULL,
    [SubModel] nvarchar(max)  NOT NULL,
    [Active] bit  NULL,
    [Seqn] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [IsAvailableForType1SGL] bit  NULL,
    [IsAvailableForType2P] bit  NULL,
    [IsAvailableForType3P] bit  NULL,
    [MotorCode] nvarchar(max)  NULL
);
GO

-- Creating table 'MasterPolicy_DeliverySet'
CREATE TABLE [dbo].[MasterPolicy_DeliverySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DeliveryType] nvarchar(max)  NOT NULL,
    [DeliveryDate] datetime  NULL,
    [DeliveryTrackingNo] nvarchar(max)  NULL,
    [PRODUCTNAME] varchar(16)  NOT NULL,
    [PROPOSALID] bigint  NOT NULL,
    [Remark] nvarchar(max)  NULL,
    [CreatedDate] datetime  NOT NULL,
    [CreatedBy] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UWRejectReasonSet'
CREATE TABLE [dbo].[UWRejectReasonSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Desc] nvarchar(max)  NOT NULL,
    [Note] nvarchar(250)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenMotorType1SGL_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotorType1SGL_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PricePlanCode] nvarchar(max)  NULL,
    [ProductCode] nvarchar(max)  NOT NULL,
    [MotorCode] nvarchar(max)  NULL,
    [FromInsured] decimal(18,2)  NULL,
    [ToInsured] decimal(18,2)  NULL,
    [FromAge] int  NULL,
    [ToAge] int  NULL,
    [FromWeight] int  NULL,
    [ToWeight] int  NULL,
    [Premium] decimal(18,2)  NULL,
    [OwnDamage] decimal(18,2)  NULL,
    [FireAndTheft] decimal(18,2)  NULL,
    [InjuryOrDeathPerPerson] decimal(18,2)  NULL,
    [InjuryOrDeathPerAccident] decimal(18,2)  NULL,
    [ThirdPartyPropertyDamage] decimal(18,2)  NULL,
    [PersonalAccidentPassengerNo] int  NULL,
    [PersonalAccidentDriver1] decimal(18,2)  NULL,
    [MedicalExpensePassengerNo] int  NULL,
    [MedicalExpenseDriver1] decimal(18,2)  NULL,
    [BailBond] decimal(18,2)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Version] int  NULL,
    [ExpiryDate] datetime  NULL,
    [ActiveDate] datetime  NULL,
    [GarageType] nvarchar(max)  NULL,
    [GarageTypeId] int  NULL
);
GO

-- Creating table 'GenMotorType2Plus_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotorType2Plus_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PricePlanCode] nvarchar(max)  NULL,
    [ProductCode] nvarchar(max)  NULL,
    [MotorCode] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [FromAge] int  NULL,
    [ToAge] int  NULL,
    [FromWeight] int  NULL,
    [ToWeight] int  NULL,
    [Premium] decimal(18,2)  NULL,
    [NetPremium] decimal(18,2)  NULL,
    [OwnDamage] decimal(18,2)  NULL,
    [FireAndTheft] decimal(18,2)  NULL,
    [InjuryOrDeathPerPerson] decimal(18,2)  NULL,
    [InjuryOrDeathPerAccident] decimal(18,2)  NULL,
    [ThirdPartyPropertyDamage] decimal(18,2)  NULL,
    [PersonalAccidentPassengerNo] int  NULL,
    [PersonalAccidentDriver1] decimal(18,2)  NULL,
    [MedicalExpensePassengerNo] int  NULL,
    [MedicalExpenseDriver1] decimal(18,2)  NULL,
    [BailBond] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Version] int  NULL,
    [ExpiryDate] datetime  NULL,
    [ActiveDate] datetime  NULL,
    [GarageTypeId] int  NULL
);
GO

-- Creating table 'GenMotorType3_MasterPricePlanSet'
CREATE TABLE [dbo].[GenMotorType3_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PricePlanCode] nvarchar(max)  NULL,
    [ProductCode] nvarchar(max)  NULL,
    [MotorCode] nvarchar(max)  NULL,
    [SumInsured] decimal(18,2)  NULL,
    [FromAge] int  NULL,
    [ToAge] int  NULL,
    [FromWeight] int  NULL,
    [ToWeight] int  NULL,
    [Premium] decimal(18,2)  NULL,
    [NetPremium] decimal(18,2)  NULL,
    [OwnDamage] decimal(18,2)  NULL,
    [FireAndTheft] decimal(18,2)  NULL,
    [InjuryOrDeathPerPerson] decimal(18,2)  NULL,
    [InjuryOrDeathPerAccident] decimal(18,2)  NULL,
    [ThirdPartyPropertyDamage] decimal(18,2)  NULL,
    [PersonalAccidentPassengerNo] int  NULL,
    [PersonalAccidentDriver1] decimal(18,2)  NULL,
    [MedicalExpensePassengerNo] int  NULL,
    [MedicalExpenseDriver1] decimal(18,2)  NULL,
    [BailBond] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Version] int  NULL,
    [ExpiryDate] datetime  NULL,
    [ActiveDate] datetime  NULL,
    [GenAssistanceId] int  NULL,
    [IsCTPL] bit  NOT NULL
);
GO

-- Creating table 'GenProLife8_PolicySet'
CREATE TABLE [dbo].[GenProLife8_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GenProLife8_Assured_ItemId] bigint  NOT NULL,
    [GenProLife8_AuditTrail_ItemId] bigint  NOT NULL,
    [GenProLife8_FATCA_ItemId] bigint  NOT NULL,
    [GenProLife8_Payer_ItemId] bigint  NOT NULL,
    [GenProLife8_Payment_ItemId] bigint  NOT NULL,
    [GenProLife8_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenProLife8_Plan_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenProLife8_AssuredSet'
CREATE TABLE [dbo].[GenProLife8_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,1)  NULL,
    [Weight] decimal(18,1)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenProLife8_DCRSet'
CREATE TABLE [dbo].[GenProLife8_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenLifePlus10_PolicyProposalId] bigint  NOT NULL,
    [GenProLife8_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenProLife8_PaymentSet'
CREATE TABLE [dbo].[GenProLife8_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenProLife8_FATCASet'
CREATE TABLE [dbo].[GenProLife8_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenProLife8_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenProLife8_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer01_ChangingType] nvarchar(max)  NULL,
    [Answer02_Rem01] nvarchar(max)  NULL,
    [Answer03_Rem01] nvarchar(max)  NULL,
    [Answer04_Rem01] nvarchar(max)  NULL,
    [Answer05_Rem01] nvarchar(max)  NULL,
    [Answer06_Rem01] nvarchar(max)  NULL,
    [Answer07_Rem01] nvarchar(max)  NULL,
    [Answer01_Rem02] nvarchar(max)  NULL,
    [Answer02_Rem02] nvarchar(max)  NULL,
    [Answer03_Rem02] nvarchar(max)  NULL,
    [Answer04_Rem02] nvarchar(max)  NULL,
    [Answer05_Rem02] nvarchar(max)  NULL,
    [Answer06_Rem02] nvarchar(max)  NULL,
    [Answer07_Rem02] nvarchar(max)  NULL,
    [Answer01_Rem03] nvarchar(max)  NULL,
    [Answer02_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem03] nvarchar(max)  NULL,
    [Answer04_Rem03] nvarchar(max)  NULL,
    [Answer05_Rem03] nvarchar(max)  NULL,
    [Answer06_Rem03] nvarchar(max)  NULL,
    [Answer07_Rem03] nvarchar(max)  NULL,
    [Answer03_Rem04] nvarchar(max)  NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_ChangingAmount] int  NULL,
    [Answer01_01] nvarchar(max)  NOT NULL,
    [Answer01_02] nvarchar(max)  NOT NULL,
    [Answer01_03] nvarchar(max)  NOT NULL,
    [Answer01_04] nvarchar(max)  NOT NULL,
    [Answer02_01] nvarchar(max)  NOT NULL,
    [Answer02_02] nvarchar(max)  NOT NULL,
    [Answer02_03] nvarchar(max)  NULL,
    [Answer03_01] nvarchar(max)  NOT NULL,
    [Answer03_02] nvarchar(max)  NOT NULL,
    [Answer04_01] nvarchar(max)  NOT NULL,
    [Answer04_02] nvarchar(max)  NOT NULL,
    [Answer05_01] nvarchar(max)  NOT NULL,
    [Answer05_02] nvarchar(max)  NOT NULL,
    [Answer05_03] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenProLife8_PlanSet'
CREATE TABLE [dbo].[GenProLife8_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenProLife8_PayerSet'
CREATE TABLE [dbo].[GenProLife8_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL
);
GO

-- Creating table 'GenProLife8_AuditTrailSet'
CREATE TABLE [dbo].[GenProLife8_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenProLife8_MasterPricePlanSet'
CREATE TABLE [dbo].[GenProLife8_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [ContractType] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [AnnualRate] decimal(18,2)  NULL,
    [InstallmentRate] decimal(18,2)  NULL,
    [AnnualPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [DeathBenefit] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenProLife8_MasterCoverageSet'
CREATE TABLE [dbo].[GenProLife8_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_PolicySet'
CREATE TABLE [dbo].[GenCancerLife_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GenCancerLife_Assured_ItemId] bigint  NOT NULL,
    [GenCancerLife_AuditTrail_ItemId] bigint  NOT NULL,
    [GenCancerLife_FATCA_ItemId] bigint  NOT NULL,
    [GenCancerLife_Payer_ItemId] bigint  NOT NULL,
    [GenCancerLife_Payment_ItemId] bigint  NOT NULL,
    [GenCancerLife_Plan_ItemId] bigint  NOT NULL,
    [GenCancerLife_UnderWriteInfo_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenCancerLife_AssuredSet'
CREATE TABLE [dbo].[GenCancerLife_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,1)  NULL,
    [Weight] decimal(18,1)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_PaymentSet'
CREATE TABLE [dbo].[GenCancerLife_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenCancerLife_PlanSet'
CREATE TABLE [dbo].[GenCancerLife_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_DCRSet'
CREATE TABLE [dbo].[GenCancerLife_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenCancerLife_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenCancerLife_FATCASet'
CREATE TABLE [dbo].[GenCancerLife_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenCancerLife_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_01] nvarchar(max)  NULL,
    [Answer01_02] nvarchar(max)  NULL,
    [Answer01_03] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer08] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NOT NULL,
    [Answer01_Rem02] nvarchar(max)  NOT NULL,
    [Answer01_Rem03] nvarchar(max)  NOT NULL,
    [Answer02_Rem01] nvarchar(max)  NOT NULL,
    [Answer02_Rem02] nvarchar(max)  NOT NULL,
    [Answer02_Rem03] nvarchar(max)  NOT NULL,
    [Answer03_ChangeType] nvarchar(max)  NOT NULL,
    [Answer03_ChangeAmount] int  NOT NULL,
    [Answer05_Rem01] nvarchar(max)  NOT NULL,
    [Answer05_Rem02] nvarchar(max)  NOT NULL,
    [Answer05_Rem03] nvarchar(max)  NOT NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer07_Rem01] nvarchar(max)  NOT NULL,
    [Answer07_Rem02] nvarchar(max)  NOT NULL,
    [Answer07_Rem03] nvarchar(max)  NOT NULL,
    [Answer07_Rem04] nvarchar(max)  NOT NULL,
    [Answer02_01] nvarchar(max)  NOT NULL,
    [Answer02_02] nvarchar(max)  NOT NULL,
    [Answer02_03] nvarchar(max)  NOT NULL,
    [Answer03_01] nvarchar(max)  NOT NULL,
    [Answer03_02] nvarchar(max)  NOT NULL,
    [Answer03_03] nvarchar(max)  NOT NULL,
    [Answer03_04] nvarchar(max)  NOT NULL,
    [Answer04_01] nvarchar(max)  NOT NULL,
    [Answer05_01] nvarchar(max)  NOT NULL,
    [Answer05_02] nvarchar(max)  NOT NULL,
    [Answer05_03] nvarchar(max)  NOT NULL,
    [Answer06_01] nvarchar(max)  NOT NULL,
    [Answer07_01] nvarchar(max)  NOT NULL,
    [Answer07_02] nvarchar(max)  NOT NULL,
    [Answer07_03] nvarchar(max)  NOT NULL,
    [Answer07_04] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenCancerLife_MasterCoverageSet'
CREATE TABLE [dbo].[GenCancerLife_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_PayerSet'
CREATE TABLE [dbo].[GenCancerLife_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL
);
GO

-- Creating table 'GenCancerLife_AuditTrailSet'
CREATE TABLE [dbo].[GenCancerLife_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenCancerLife_MasterPricePlanSet'
CREATE TABLE [dbo].[GenCancerLife_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [AnnualRate] decimal(18,2)  NULL,
    [AnnualPremium] decimal(18,2)  NULL,
    [InstallmentPremium] decimal(18,2)  NULL,
    [CancerRiderPremium] decimal(18,2)  NULL,
    [WPInvasiveRiderPremium] decimal(18,2)  NULL,
    [AssurancePremium] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenExclusivePA_PayerSet'
CREATE TABLE [dbo].[GenExclusivePA_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [PayerAddress1] nvarchar(max)  NULL,
    [PayerAddress2] nvarchar(max)  NULL,
    [PayerAddress3] nvarchar(max)  NULL,
    [PayerAddress4] nvarchar(max)  NULL,
    [PayerAddress5] nvarchar(max)  NULL,
    [PayerEmailAddress] nvarchar(max)  NULL,
    [PayerTaxId] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'GenShieldPA_PayerSet'
CREATE TABLE [dbo].[GenShieldPA_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [PayerAddress1] nvarchar(max)  NULL,
    [PayerAddress2] nvarchar(max)  NULL,
    [PayerAddress3] nvarchar(max)  NULL,
    [PayerAddress4] nvarchar(max)  NULL,
    [PayerAddress5] nvarchar(max)  NULL,
    [PayerEmailAddress] nvarchar(max)  NULL,
    [PayerTaxId] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ProvinceId] nvarchar(max)  NULL,
    [SubDistrictId] nvarchar(max)  NULL,
    [DistrictId] nvarchar(max)  NULL
);
GO

-- Creating table 'SupervisorTargetTypeSet'
CREATE TABLE [dbo].[SupervisorTargetTypeSet] (
    [TargetType] nvarchar(50)  NOT NULL,
    [Seq] int  NOT NULL,
    [Active] bit  NOT NULL,
    [TrackingReport] bit  NOT NULL
);
GO

-- Creating table 'SupervisorTargetSet'
CREATE TABLE [dbo].[SupervisorTargetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [TargetType] nvarchar(50)  NOT NULL,
    [MonthlyTarget] decimal(18,2)  NULL,
    [DailyTarget] decimal(18,2)  NULL
);
GO

-- Creating table 'GenSenior55_PolicySet'
CREATE TABLE [dbo].[GenSenior55_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GenSenior55_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenSenior55_Plan_ItemId] bigint  NOT NULL,
    [GenSenior55_Payment_ItemId] bigint  NOT NULL,
    [GenSenior55_Payer_ItemId] bigint  NOT NULL,
    [GenSenior55_FATCA_ItemId] bigint  NOT NULL,
    [GenSenior55_AuditTrail_ItemId] bigint  NOT NULL,
    [GenSenior55_Assured_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenSenior55_AssuredSet'
CREATE TABLE [dbo].[GenSenior55_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,1)  NULL,
    [Weight] decimal(18,1)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [AddressType] nvarchar(max)  NOT NULL,
    [AddressTypeCode] nvarchar(max)  NOT NULL,
    [ContactAddress1] nvarchar(max)  NULL,
    [ContactAddress2] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [RegistrationAddress1] nvarchar(max)  NULL,
    [RegistrationAddress2] nvarchar(max)  NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [OfficeAddress1] nvarchar(max)  NULL,
    [OfficeAddress2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [PresentAddress1] nvarchar(max)  NULL,
    [PresentAddress2] nvarchar(max)  NULL,
    [Present_HomeAddress] nvarchar(max)  NULL,
    [Present_MooBan] nvarchar(max)  NULL,
    [Present_Moo] nvarchar(max)  NULL,
    [Present_Soi] nvarchar(max)  NULL,
    [Present_Road] nvarchar(max)  NULL,
    [Present_District] nvarchar(max)  NULL,
    [Present_DistrictCode] nvarchar(max)  NULL,
    [Present_SubDistrict] nvarchar(max)  NULL,
    [Present_SubDistrictCode] nvarchar(max)  NULL,
    [Present_Province] nvarchar(max)  NULL,
    [Present_ProvinceCode] nvarchar(max)  NULL,
    [Present_PostCode] nvarchar(max)  NULL,
    [Present_TelPhone1] nvarchar(max)  NULL,
    [Present_TelPhone1_Extension] nvarchar(max)  NULL,
    [Present_TelPhone1_To] nvarchar(max)  NULL,
    [Present_TelPhone2] nvarchar(max)  NOT NULL,
    [Present_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSenior55_PaymentSet'
CREATE TABLE [dbo].[GenSenior55_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenSenior55_PlanSet'
CREATE TABLE [dbo].[GenSenior55_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSenior55_DCRSet'
CREATE TABLE [dbo].[GenSenior55_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenSenior55_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenSenior55_FATCASet'
CREATE TABLE [dbo].[GenSenior55_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSenior55_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenSenior55_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer08] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NOT NULL,
    [Answer01_Rem02] nvarchar(max)  NOT NULL,
    [Answer01_Rem03] nvarchar(max)  NOT NULL,
    [Answer02_Rem01] nvarchar(max)  NOT NULL,
    [Answer02_Rem02] nvarchar(max)  NOT NULL,
    [Answer02_Rem03] nvarchar(max)  NOT NULL,
    [Answer03_ChangeType] nvarchar(max)  NOT NULL,
    [Answer03_ChangeAmount] int  NOT NULL,
    [Answer05_Rem01] nvarchar(max)  NOT NULL,
    [Answer05_Rem02] nvarchar(max)  NOT NULL,
    [Answer05_Rem03] nvarchar(max)  NOT NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer07_Rem01] nvarchar(max)  NOT NULL,
    [Answer07_Rem02] nvarchar(max)  NOT NULL,
    [Answer07_Rem03] nvarchar(max)  NOT NULL,
    [Answer07_Rem04] nvarchar(max)  NOT NULL,
    [Answer02_01] nvarchar(max)  NOT NULL,
    [Answer02_02] nvarchar(max)  NOT NULL,
    [Answer02_03] nvarchar(max)  NOT NULL,
    [Answer03_01] nvarchar(max)  NOT NULL,
    [Answer03_02] nvarchar(max)  NOT NULL,
    [Answer03_03] nvarchar(max)  NOT NULL,
    [Answer03_04] nvarchar(max)  NOT NULL,
    [Answer04_01] nvarchar(max)  NOT NULL,
    [Answer05_01] nvarchar(max)  NOT NULL,
    [Answer05_02] nvarchar(max)  NOT NULL,
    [Answer05_03] nvarchar(max)  NOT NULL,
    [Answer06_01] nvarchar(max)  NOT NULL,
    [Answer07_01] nvarchar(max)  NOT NULL,
    [Answer07_02] nvarchar(max)  NOT NULL,
    [Answer07_03] nvarchar(max)  NOT NULL,
    [Answer07_04] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenSenior55_MasterCoverageSet'
CREATE TABLE [dbo].[GenSenior55_MasterCoverageSet] (
    [CoverageId] int IDENTITY(1,1) NOT NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [CoverageTitle] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSenior55_PayerSet'
CREATE TABLE [dbo].[GenSenior55_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [PayerAddress1] nvarchar(max)  NULL,
    [PayerAddress2] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL
);
GO

-- Creating table 'GenSenior55_AuditTrailSet'
CREATE TABLE [dbo].[GenSenior55_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenSenior55_MasterPricePlanSet'
CREATE TABLE [dbo].[GenSenior55_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [AnnualRate] decimal(18,2)  NULL,
    [AnnualPremium] decimal(18,2)  NULL,
    [InstallmentPremium] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenPABonus_PolicySet'
CREATE TABLE [dbo].[GenPABonus_PolicySet] (
    [ProposalId] bigint IDENTITY(1,1) NOT NULL,
    [ProposalNo] nvarchar(max)  NULL,
    [PolicyNo] nvarchar(max)  NULL,
    [InceptionDate] datetime  NULL,
    [InceptionTime] nvarchar(max)  NULL,
    [ProposalDate] datetime  NULL,
    [PolicyExpiryDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [Success] bit  NULL,
    [ProductId] int  NULL,
    [LeadSourceId] int  NULL,
    [Ref_PolicyHolder] nvarchar(max)  NULL,
    [Ref_InsuredMember] nvarchar(max)  NULL,
    [CRMActivityId] bigint  NULL,
    [GroupPolicyNo] nvarchar(max)  NULL,
    [GroupCompanyName] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL,
    [ePolicyFlag] nvarchar(max)  NULL,
    [ePolicyEmailAddress] nvarchar(max)  NULL,
    [ProspectId] bigint  NULL,
    [GenPABonus_UnderWriteInfo_ItemId] bigint  NOT NULL,
    [GenPABonus_Plan_ItemId] bigint  NOT NULL,
    [GenPABonus_Payment_ItemId] bigint  NOT NULL,
    [GenPABonus_Payer_ItemId] bigint  NOT NULL,
    [GenPABonus_FATCA_ItemId] bigint  NOT NULL,
    [GenPABonus_AuditTrail_ItemId] bigint  NOT NULL,
    [GenPABonus_Assured_ItemId] bigint  NOT NULL
);
GO

-- Creating table 'GenPABonus_AssuredSet'
CREATE TABLE [dbo].[GenPABonus_AssuredSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Surname] nvarchar(max)  NULL,
    [GivenName] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [ClientSex] nvarchar(max)  NULL,
    [ClientSexCode] nvarchar(max)  NULL,
    [ClientStatus] nvarchar(max)  NULL,
    [ClientStatusCode] nvarchar(max)  NULL,
    [ClientBirthDate] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NOT NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NOT NULL,
    [OccupationCode] nvarchar(max)  NULL,
    [OccupationDescription] nvarchar(max)  NULL,
    [OccupationWorkPlace] nvarchar(max)  NOT NULL,
    [Age] smallint  NULL,
    [Height] decimal(18,1)  NULL,
    [Weight] decimal(18,1)  NULL,
    [BeneficiaryName1] nvarchar(max)  NULL,
    [BeneficiaryName2] nvarchar(max)  NULL,
    [BeneficiaryName3] nvarchar(max)  NULL,
    [BeneficiaryName4] nvarchar(max)  NULL,
    [BeneficiaryRelationShip1] nvarchar(max)  NULL,
    [BeneficiaryRelationShip2] nvarchar(max)  NULL,
    [BeneficiaryRelationShip3] nvarchar(max)  NULL,
    [BeneficiaryRelationShip4] nvarchar(max)  NULL,
    [BeneficiaryAge1] int  NULL,
    [BeneficiaryAge2] int  NULL,
    [BeneficiaryAge3] int  NULL,
    [BeneficiaryAge4] int  NULL,
    [BeneficiaryBenefitPercentage1] int  NULL,
    [BeneficiaryBenefitPercentage2] int  NULL,
    [BeneficiaryBenefitPercentage3] int  NULL,
    [BeneficiaryBenefitPercentage4] int  NULL,
    [Nationality] nvarchar(max)  NULL,
    [NationalityCode] nvarchar(max)  NOT NULL,
    [AnnualIncome] decimal(18,2)  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [CardIssueDistrict] nvarchar(max)  NULL,
    [CardIssueProvince] nvarchar(max)  NULL,
    [CardIssueCountry] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [Originality] nvarchar(max)  NULL,
    [OriginalityCode] nvarchar(max)  NULL,
    [AddressType] nvarchar(max)  NOT NULL,
    [AddressTypeCode] nvarchar(max)  NOT NULL,
    [Contact_Address1] nvarchar(max)  NULL,
    [Contact_Address2] nvarchar(max)  NULL,
    [Contact_HomeAddress] nvarchar(max)  NULL,
    [Contact_MooBan] nvarchar(max)  NULL,
    [Contact_Moo] nvarchar(max)  NULL,
    [Contact_Soi] nvarchar(max)  NULL,
    [Contact_Road] nvarchar(max)  NULL,
    [Contact_District] nvarchar(max)  NULL,
    [Contact_DistrictCode] nvarchar(max)  NULL,
    [Contact_SubDistrict] nvarchar(max)  NULL,
    [Contact_SubDistrictCode] nvarchar(max)  NULL,
    [Contact_Province] nvarchar(max)  NULL,
    [Contact_ProvinceCode] nvarchar(max)  NULL,
    [Contact_PostCode] nvarchar(max)  NULL,
    [Contact_TelPhone1] nvarchar(max)  NULL,
    [Contact_TelPhone1_Extension] nvarchar(max)  NULL,
    [Contact_TelPhone1_To] nvarchar(max)  NULL,
    [Contact_TelPhone2] nvarchar(max)  NOT NULL,
    [Contact_Email] nvarchar(max)  NULL,
    [Registration_Address1] nvarchar(max)  NULL,
    [Registration_Address2] nvarchar(max)  NULL,
    [Registration_HomeAddress] nvarchar(max)  NULL,
    [Registration_MooBan] nvarchar(max)  NULL,
    [Registration_Moo] nvarchar(max)  NULL,
    [Registration_Soi] nvarchar(max)  NULL,
    [Registration_Road] nvarchar(max)  NULL,
    [Registration_District] nvarchar(max)  NULL,
    [Registration_DistrictCode] nvarchar(max)  NULL,
    [Registration_SubDistrict] nvarchar(max)  NULL,
    [Registration_SubDistrictCode] nvarchar(max)  NULL,
    [Registration_Province] nvarchar(max)  NULL,
    [Registration_ProvinceCode] nvarchar(max)  NULL,
    [Registration_PostCode] nvarchar(max)  NULL,
    [Registration_TelPhone1] nvarchar(max)  NULL,
    [Registration_TelPhone1_Extension] nvarchar(max)  NULL,
    [Registration_TelPhone1_To] nvarchar(max)  NULL,
    [Registration_TelPhone2] nvarchar(max)  NULL,
    [Registration_Email] nvarchar(max)  NULL,
    [Office_Address1] nvarchar(max)  NULL,
    [Office_Address2] nvarchar(max)  NULL,
    [Office_HomeAddress] nvarchar(max)  NULL,
    [Office_MooBan] nvarchar(max)  NULL,
    [Office_Moo] nvarchar(max)  NULL,
    [Office_Soi] nvarchar(max)  NULL,
    [Office_Road] nvarchar(max)  NULL,
    [Office_SubDistrict] nvarchar(max)  NULL,
    [Office_SubDistrictCode] nvarchar(max)  NULL,
    [Office_District] nvarchar(max)  NULL,
    [Office_DistrictCode] nvarchar(max)  NULL,
    [Office_Province] nvarchar(max)  NULL,
    [Office_ProvinceCode] nvarchar(max)  NULL,
    [Office_PostCode] nvarchar(max)  NULL,
    [Office_TelPhone1] nvarchar(max)  NULL,
    [Office_TelPhone1_Extension] nvarchar(max)  NULL,
    [Office_TelPhone1_To] nvarchar(max)  NULL,
    [Office_TelPhone2] nvarchar(max)  NULL,
    [Office_Email] nvarchar(max)  NULL,
    [Present_Address1] nvarchar(max)  NULL,
    [Present_Address2] nvarchar(max)  NULL,
    [Present_HomeAddress] nvarchar(max)  NULL,
    [Present_MooBan] nvarchar(max)  NULL,
    [Present_Moo] nvarchar(max)  NULL,
    [Present_Soi] nvarchar(max)  NULL,
    [Present_Road] nvarchar(max)  NULL,
    [Present_District] nvarchar(max)  NULL,
    [Present_DistrictCode] nvarchar(max)  NULL,
    [Present_SubDistrict] nvarchar(max)  NULL,
    [Present_SubDistrictCode] nvarchar(max)  NULL,
    [Present_Province] nvarchar(max)  NULL,
    [Present_ProvinceCode] nvarchar(max)  NULL,
    [Present_PostCode] nvarchar(max)  NULL,
    [Present_TelPhone1] nvarchar(max)  NULL,
    [Present_TelPhone1_Extension] nvarchar(max)  NULL,
    [Present_TelPhone1_To] nvarchar(max)  NULL,
    [Present_TelPhone2] nvarchar(max)  NOT NULL,
    [Present_Email] nvarchar(max)  NULL,
    [SpouseName] nvarchar(max)  NULL,
    [SpouseNationality] nvarchar(max)  NULL,
    [SpouseNationalityCode] nvarchar(max)  NULL,
    [Religion] nvarchar(max)  NULL,
    [ReligionCode] nvarchar(max)  NULL
);
GO

-- Creating table 'GenPABonus_PaymentSet'
CREATE TABLE [dbo].[GenPABonus_PaymentSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [PayPlan] nvarchar(max)  NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [GTBankKey] nvarchar(max)  NULL,
    [GTBankName] nvarchar(max)  NULL,
    [GTBankBranch] nvarchar(max)  NULL,
    [GTBankAccountNo] nvarchar(max)  NULL,
    [BankTransferCustomerName] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CreditCardBankName] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [CreditCardHolderName] nvarchar(max)  NULL,
    [CreditCardExpiryDate] datetime  NULL,
    [GTBankAccountType] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [PayLiteCode] nvarchar(max)  NULL,
    [PayLite] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NULL,
    [CreditCardClassType] nvarchar(max)  NULL,
    [RequestCollectDate] datetime  NULL
);
GO

-- Creating table 'GenPABonus_PlanSet'
CREATE TABLE [dbo].[GenPABonus_PlanSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [ContractType] int  NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Deductible] decimal(18,2)  NULL,
    [AnnualizedPremium] decimal(18,2)  NULL,
    [InstalmentPremium] decimal(18,2)  NULL,
    [NextYearInstalmentPremium] decimal(18,2)  NULL,
    [CoverageType] nvarchar(max)  NULL,
    [YearPayment] nvarchar(max)  NULL,
    [CoverageYear] nvarchar(max)  NULL
);
GO

-- Creating table 'GenPABonus_DCRSet'
CREATE TABLE [dbo].[GenPABonus_DCRSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Bank] nvarchar(max)  NULL,
    [CreditCardNumber] nvarchar(max)  NULL,
    [Amount] decimal(18,2)  NULL,
    [ApprovalCode] nvarchar(max)  NULL,
    [IsApprove] nvarchar(max)  NULL,
    [Remark] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreditCardType] nvarchar(max)  NULL,
    [CanCutCreditCard] nvarchar(max)  NOT NULL,
    [LicenseNumber] nvarchar(max)  NOT NULL,
    [PaymentMethod] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreditCardTypeCode] nvarchar(max)  NULL,
    [GenPABonus_PolicyProposalId] bigint  NOT NULL
);
GO

-- Creating table 'GenPABonus_FATCASet'
CREATE TABLE [dbo].[GenPABonus_FATCASet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [USIndiciaCode] nvarchar(max)  NULL,
    [USIndicia] nvarchar(max)  NULL
);
GO

-- Creating table 'GenPABonus_UnderWriteInfoSet'
CREATE TABLE [dbo].[GenPABonus_UnderWriteInfoSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Answer01] nvarchar(max)  NULL,
    [Answer02] nvarchar(max)  NULL,
    [Answer03] nvarchar(max)  NULL,
    [Answer04] nvarchar(max)  NULL,
    [Answer05] nvarchar(max)  NULL,
    [Answer06] nvarchar(max)  NULL,
    [Answer07] nvarchar(max)  NULL,
    [Answer08] nvarchar(max)  NULL,
    [Answer01_Rem01] nvarchar(max)  NOT NULL,
    [Answer01_Rem02] nvarchar(max)  NOT NULL,
    [Answer01_Rem03] nvarchar(max)  NOT NULL,
    [Answer01_Rem04] nvarchar(max)  NOT NULL,
    [Answer01_Rem05] nvarchar(max)  NOT NULL,
    [Answer01_Rem06] nvarchar(max)  NOT NULL,
    [Answer02_Rem01] nvarchar(max)  NOT NULL,
    [Answer02_Rem02] nvarchar(max)  NOT NULL,
    [Answer02_Rem03] nvarchar(max)  NOT NULL,
    [Answer03_Rem01] nvarchar(max)  NOT NULL,
    [Answer03_ChangeType] nvarchar(max)  NOT NULL,
    [Answer03_ChangeAmount] int  NOT NULL,
    [Answer05_Rem01] nvarchar(max)  NOT NULL,
    [Answer05_Rem02] nvarchar(max)  NOT NULL,
    [Answer05_Rem03] nvarchar(max)  NOT NULL,
    [Answer05_Rem04] nvarchar(max)  NOT NULL,
    [Answer07_Rem01] nvarchar(max)  NOT NULL,
    [Answer07_Rem02] nvarchar(max)  NOT NULL,
    [Answer07_Rem03] nvarchar(max)  NOT NULL,
    [Answer07_Rem04] nvarchar(max)  NOT NULL,
    [Answer02_01] nvarchar(max)  NOT NULL,
    [Answer02_02] nvarchar(max)  NOT NULL,
    [Answer02_03] nvarchar(max)  NOT NULL,
    [Answer03_01] nvarchar(max)  NOT NULL,
    [Answer03_02] nvarchar(max)  NOT NULL,
    [Answer03_03] nvarchar(max)  NOT NULL,
    [Answer03_04] nvarchar(max)  NOT NULL,
    [Answer04_01] nvarchar(max)  NOT NULL,
    [Answer05_01] nvarchar(max)  NOT NULL,
    [Answer05_02] nvarchar(max)  NOT NULL,
    [Answer05_03] nvarchar(max)  NOT NULL,
    [Answer06_01] nvarchar(max)  NOT NULL,
    [Answer07_01] nvarchar(max)  NOT NULL,
    [Answer07_02] nvarchar(max)  NOT NULL,
    [Answer07_03] nvarchar(max)  NOT NULL,
    [Answer07_04] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GenPABonus_PayerSet'
CREATE TABLE [dbo].[GenPABonus_PayerSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL,
    [Age] smallint  NULL,
    [CardId] nvarchar(max)  NULL,
    [CardType] nvarchar(max)  NULL,
    [PayerSexCode] nvarchar(max)  NULL,
    [PayerSex] nvarchar(max)  NULL,
    [PayerStatus] nvarchar(max)  NULL,
    [PayerStatusCode] nvarchar(max)  NULL,
    [OccupationClass] nvarchar(max)  NULL,
    [OccupationClassCode] nvarchar(max)  NULL,
    [Occupation] nvarchar(max)  NULL,
    [OccupationPosition] nvarchar(max)  NULL,
    [Salutation] nvarchar(max)  NULL,
    [SalutationCode] nvarchar(max)  NULL,
    [RelationShip] nvarchar(max)  NULL,
    [TelPhoneNumber1] nvarchar(max)  NULL,
    [TelPhoneNumber1_To] nvarchar(max)  NULL,
    [TelPhoneNumber1_Extension] nvarchar(max)  NULL,
    [TelPhoneNumber2] nvarchar(max)  NULL,
    [TelPhoneNumber3] nvarchar(max)  NULL,
    [RequireTaxRefund] nvarchar(max)  NULL,
    [AnnualIncome] nvarchar(max)  NULL,
    [CardExpiryDate] datetime  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [Address1] nvarchar(max)  NULL,
    [Address2] nvarchar(max)  NULL,
    [HomeAddress] nvarchar(max)  NULL,
    [MooBan] nvarchar(max)  NULL,
    [Moo] nvarchar(max)  NULL,
    [Soi] nvarchar(max)  NULL,
    [Road] nvarchar(max)  NULL,
    [District] nvarchar(max)  NULL,
    [DistrictCode] nvarchar(max)  NULL,
    [SubDistrict] nvarchar(max)  NULL,
    [SubDistrictCode] nvarchar(max)  NULL,
    [Province] nvarchar(max)  NULL,
    [ProvinceCode] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [ConsentFlag] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [ConsentType] nvarchar(max)  NULL,
    [ConsentDataSource] nvarchar(max)  NULL,
    [ConsentDate] datetime  NULL,
    [ConsentTime] nvarchar(max)  NULL,
    [ConsentId] int  NULL,
    [ConsentRevenueId] int  NULL,
    [ConsentRevenueFlag] nvarchar(max)  NULL,
    [ConsentRevenueVersion] nvarchar(max)  NULL
);
GO

-- Creating table 'GenPABonus_AuditTrailSet'
CREATE TABLE [dbo].[GenPABonus_AuditTrailSet] (
    [ItemId] bigint IDENTITY(1,1) NOT NULL,
    [SourceNameList] int  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [TSRNumber] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedTime] nvarchar(max)  NULL,
    [TSRName] nvarchar(max)  NULL,
    [LicenseNumber] nvarchar(max)  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'GenPABonus_MasterPricePlanSet'
CREATE TABLE [dbo].[GenPABonus_MasterPricePlanSet] (
    [PricePlanId] int IDENTITY(1,1) NOT NULL,
    [PlanCode] nvarchar(max)  NULL,
    [PlanNo] nvarchar(max)  NULL,
    [FromAge] nvarchar(max)  NULL,
    [ToAge] nvarchar(max)  NULL,
    [PlanName] nvarchar(max)  NULL,
    [SumAssured] decimal(18,2)  NULL,
    [Gender] nvarchar(max)  NULL,
    [PayPlan] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [AnnualRate] decimal(18,2)  NULL,
    [AnnualPremium] decimal(18,2)  NULL,
    [InstallmentPremium] decimal(18,2)  NULL,
    [Death_NormalAccident] decimal(18,2)  NULL,
    [Death_PublicAccident] decimal(18,2)  NULL,
    [Death_Homicide] decimal(18,2)  NULL,
    [Death_MotorcycleAccident] decimal(18,2)  NULL,
    [MedicalFee] decimal(18,2)  NULL,
    [HospitalBenefit] decimal(18,2)  NULL,
    [HospitalBenefitICU] decimal(18,2)  NULL,
    [FuneralExpense] decimal(18,2)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'TSRTargetSet'
CREATE TABLE [dbo].[TSRTargetSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TSR] nvarchar(128)  NOT NULL,
    [Type] nvarchar(10)  NOT NULL,
    [Month] nvarchar(6)  NOT NULL,
    [Target] decimal(18,2)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_PaymentSet'
ALTER TABLE [dbo].[GenHealthLumpSum_PaymentSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_AssuredSet'
ALTER TABLE [dbo].[GenHealthLumpSum_AssuredSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_AuditTrailSet'
ALTER TABLE [dbo].[GenHealthLumpSum_AuditTrailSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_PlanSet'
ALTER TABLE [dbo].[GenHealthLumpSum_PlanSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenHealthLumpSum_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [SalutationId] in table 'SalutationSet'
ALTER TABLE [dbo].[SalutationSet]
ADD CONSTRAINT [PK_SalutationSet]
    PRIMARY KEY CLUSTERED ([SalutationId] ASC);
GO

-- Creating primary key on [OccupationId] in table 'OccupationSet'
ALTER TABLE [dbo].[OccupationSet]
ADD CONSTRAINT [PK_OccupationSet]
    PRIMARY KEY CLUSTERED ([OccupationId] ASC);
GO

-- Creating primary key on [NationalityId] in table 'NationalitySet'
ALTER TABLE [dbo].[NationalitySet]
ADD CONSTRAINT [PK_NationalitySet]
    PRIMARY KEY CLUSTERED ([NationalityId] ASC);
GO

-- Creating primary key on [ClientStatusId] in table 'ClientStatusSet'
ALTER TABLE [dbo].[ClientStatusSet]
ADD CONSTRAINT [PK_ClientStatusSet]
    PRIMARY KEY CLUSTERED ([ClientStatusId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHealthLumpSum_MasterPlanSet'
ALTER TABLE [dbo].[GenHealthLumpSum_MasterPlanSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [PK_GenHealthLumpSum_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ProductId] in table 'ProductSet'
ALTER TABLE [dbo].[ProductSet]
ADD CONSTRAINT [PK_ProductSet]
    PRIMARY KEY CLUSTERED ([ProductId] ASC);
GO

-- Creating primary key on [GTBankAccountId] in table 'GTBankAccountSet'
ALTER TABLE [dbo].[GTBankAccountSet]
ADD CONSTRAINT [PK_GTBankAccountSet]
    PRIMARY KEY CLUSTERED ([GTBankAccountId] ASC);
GO

-- Creating primary key on [CreditCardTypeId] in table 'CreditCardTypeSet'
ALTER TABLE [dbo].[CreditCardTypeSet]
ADD CONSTRAINT [PK_CreditCardTypeSet]
    PRIMARY KEY CLUSTERED ([CreditCardTypeId] ASC);
GO

-- Creating primary key on [OccupationClassId] in table 'OccupationClassSet'
ALTER TABLE [dbo].[OccupationClassSet]
ADD CONSTRAINT [PK_OccupationClassSet]
    PRIMARY KEY CLUSTERED ([OccupationClassId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_DCRSet'
ALTER TABLE [dbo].[GenHealthLumpSum_DCRSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [RelationId] in table 'RelationSet'
ALTER TABLE [dbo].[RelationSet]
ADD CONSTRAINT [PK_RelationSet]
    PRIMARY KEY CLUSTERED ([RelationId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenHealthLumpSum_MasterCoverageSet'
ALTER TABLE [dbo].[GenHealthLumpSum_MasterCoverageSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_PlanSet'
ALTER TABLE [dbo].[GenHealthProtect_PlanSet]
ADD CONSTRAINT [PK_GenHealthProtect_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_AuditTrailSet'
ALTER TABLE [dbo].[GenHealthProtect_AuditTrailSet]
ADD CONSTRAINT [PK_GenHealthProtect_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenHealthProtect_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenHealthProtect_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_AssuredSet'
ALTER TABLE [dbo].[GenHealthProtect_AssuredSet]
ADD CONSTRAINT [PK_GenHealthProtect_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_PaymentSet'
ALTER TABLE [dbo].[GenHealthProtect_PaymentSet]
ADD CONSTRAINT [PK_GenHealthProtect_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHealthProtect_MasterPlanSet'
ALTER TABLE [dbo].[GenHealthProtect_MasterPlanSet]
ADD CONSTRAINT [PK_GenHealthProtect_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenHealthProtect_MasterCoverageSet'
ALTER TABLE [dbo].[GenHealthProtect_MasterCoverageSet]
ADD CONSTRAINT [PK_GenHealthProtect_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [PK_GenHealthProtect_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_DCRSet'
ALTER TABLE [dbo].[GenHealthProtect_DCRSet]
ADD CONSTRAINT [PK_GenHealthProtect_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_PayerSet'
ALTER TABLE [dbo].[GenHealthLumpSum_PayerSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthProtect_PayerSet'
ALTER TABLE [dbo].[GenHealthProtect_PayerSet]
ADD CONSTRAINT [PK_GenHealthProtect_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [LeadSourceId] in table 'LeadSourceSet'
ALTER TABLE [dbo].[LeadSourceSet]
ADD CONSTRAINT [PK_LeadSourceSet]
    PRIMARY KEY CLUSTERED ([LeadSourceId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [PK_GenExclusivePA_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_AssuredSet'
ALTER TABLE [dbo].[GenExclusivePA_AssuredSet]
ADD CONSTRAINT [PK_GenExclusivePA_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_PlanSet'
ALTER TABLE [dbo].[GenExclusivePA_PlanSet]
ADD CONSTRAINT [PK_GenExclusivePA_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_PaymentSet'
ALTER TABLE [dbo].[GenExclusivePA_PaymentSet]
ADD CONSTRAINT [PK_GenExclusivePA_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenExclusivePA_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenExclusivePA_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_DCRSet'
ALTER TABLE [dbo].[GenExclusivePA_DCRSet]
ADD CONSTRAINT [PK_GenExclusivePA_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_AuditTrailSet'
ALTER TABLE [dbo].[GenExclusivePA_AuditTrailSet]
ADD CONSTRAINT [PK_GenExclusivePA_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenExclusivePA_MasterPlanSet'
ALTER TABLE [dbo].[GenExclusivePA_MasterPlanSet]
ADD CONSTRAINT [PK_GenExclusivePA_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenExclusivePA_MasterCoverageSet'
ALTER TABLE [dbo].[GenExclusivePA_MasterCoverageSet]
ADD CONSTRAINT [PK_GenExclusivePA_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [PK_GenShieldPA_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_AssuredSet'
ALTER TABLE [dbo].[GenShieldPA_AssuredSet]
ADD CONSTRAINT [PK_GenShieldPA_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenShieldPA_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenShieldPA_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_PaymentSet'
ALTER TABLE [dbo].[GenShieldPA_PaymentSet]
ADD CONSTRAINT [PK_GenShieldPA_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_PlanSet'
ALTER TABLE [dbo].[GenShieldPA_PlanSet]
ADD CONSTRAINT [PK_GenShieldPA_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_AuditTrailSet'
ALTER TABLE [dbo].[GenShieldPA_AuditTrailSet]
ADD CONSTRAINT [PK_GenShieldPA_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_DCRSet'
ALTER TABLE [dbo].[GenShieldPA_DCRSet]
ADD CONSTRAINT [PK_GenShieldPA_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenShieldPA_MasterPlanSet'
ALTER TABLE [dbo].[GenShieldPA_MasterPlanSet]
ADD CONSTRAINT [PK_GenShieldPA_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenShieldPA_MasterCoverageSet'
ALTER TABLE [dbo].[GenShieldPA_MasterCoverageSet]
ADD CONSTRAINT [PK_GenShieldPA_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [PK_GenMax7_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_AssuredSet'
ALTER TABLE [dbo].[GenMax7_AssuredSet]
ADD CONSTRAINT [PK_GenMax7_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_DCRSet'
ALTER TABLE [dbo].[GenMax7_DCRSet]
ADD CONSTRAINT [PK_GenMax7_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_PlanSet'
ALTER TABLE [dbo].[GenMax7_PlanSet]
ADD CONSTRAINT [PK_GenMax7_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_PayerSet'
ALTER TABLE [dbo].[GenMax7_PayerSet]
ADD CONSTRAINT [PK_GenMax7_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_AuditTrailSet'
ALTER TABLE [dbo].[GenMax7_AuditTrailSet]
ADD CONSTRAINT [PK_GenMax7_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenMax7_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenMax7_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_PaymentSet'
ALTER TABLE [dbo].[GenMax7_PaymentSet]
ADD CONSTRAINT [PK_GenMax7_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenMax7_MasterCoverageSet'
ALTER TABLE [dbo].[GenMax7_MasterCoverageSet]
ADD CONSTRAINT [PK_GenMax7_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenMax7_MasterPlanSet'
ALTER TABLE [dbo].[GenMax7_MasterPlanSet]
ADD CONSTRAINT [PK_GenMax7_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMax7_FATCASet'
ALTER TABLE [dbo].[GenMax7_FATCASet]
ADD CONSTRAINT [PK_GenMax7_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [SalutationId] in table 'LifeSalutationSet'
ALTER TABLE [dbo].[LifeSalutationSet]
ADD CONSTRAINT [PK_LifeSalutationSet]
    PRIMARY KEY CLUSTERED ([SalutationId] ASC);
GO

-- Creating primary key on [LifePayLifeId] in table 'LifePayLifeSet'
ALTER TABLE [dbo].[LifePayLifeSet]
ADD CONSTRAINT [PK_LifePayLifeSet]
    PRIMARY KEY CLUSTERED ([LifePayLifeId] ASC);
GO

-- Creating primary key on [GTBankAccountId] in table 'LifeGTBankAccountSet'
ALTER TABLE [dbo].[LifeGTBankAccountSet]
ADD CONSTRAINT [PK_LifeGTBankAccountSet]
    PRIMARY KEY CLUSTERED ([GTBankAccountId] ASC);
GO

-- Creating primary key on [ReligionId] in table 'ReligionSet'
ALTER TABLE [dbo].[ReligionSet]
ADD CONSTRAINT [PK_ReligionSet]
    PRIMARY KEY CLUSTERED ([ReligionId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [PK_GenSmart5_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_AssuredSet'
ALTER TABLE [dbo].[GenSmart5_AssuredSet]
ADD CONSTRAINT [PK_GenSmart5_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_FATCASet'
ALTER TABLE [dbo].[GenSmart5_FATCASet]
ADD CONSTRAINT [PK_GenSmart5_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenSmart5_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenSmart5_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_DCRSet'
ALTER TABLE [dbo].[GenSmart5_DCRSet]
ADD CONSTRAINT [PK_GenSmart5_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_PlanSet'
ALTER TABLE [dbo].[GenSmart5_PlanSet]
ADD CONSTRAINT [PK_GenSmart5_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_PaymentSet'
ALTER TABLE [dbo].[GenSmart5_PaymentSet]
ADD CONSTRAINT [PK_GenSmart5_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenSmart5_MasterPlanSet'
ALTER TABLE [dbo].[GenSmart5_MasterPlanSet]
ADD CONSTRAINT [PK_GenSmart5_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenSmart5_MasterCoverageSet'
ALTER TABLE [dbo].[GenSmart5_MasterCoverageSet]
ADD CONSTRAINT [PK_GenSmart5_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [PK_GenLifePlus10_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_AssuredSet'
ALTER TABLE [dbo].[GenLifePlus10_AssuredSet]
ADD CONSTRAINT [PK_GenLifePlus10_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_PlanSet'
ALTER TABLE [dbo].[GenLifePlus10_PlanSet]
ADD CONSTRAINT [PK_GenLifePlus10_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_DCRSet'
ALTER TABLE [dbo].[GenLifePlus10_DCRSet]
ADD CONSTRAINT [PK_GenLifePlus10_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_PaymentSet'
ALTER TABLE [dbo].[GenLifePlus10_PaymentSet]
ADD CONSTRAINT [PK_GenLifePlus10_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenLifePlus10_MasterPlanSet'
ALTER TABLE [dbo].[GenLifePlus10_MasterPlanSet]
ADD CONSTRAINT [PK_GenLifePlus10_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenLifePlus10_MasterCoverageSet'
ALTER TABLE [dbo].[GenLifePlus10_MasterCoverageSet]
ADD CONSTRAINT [PK_GenLifePlus10_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenLifePlus10_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenLifePlus10_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_FATCASet'
ALTER TABLE [dbo].[GenLifePlus10_FATCASet]
ADD CONSTRAINT [PK_GenLifePlus10_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_PayerSet'
ALTER TABLE [dbo].[GenLifePlus10_PayerSet]
ADD CONSTRAINT [PK_GenLifePlus10_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenLifePlus10_AuditTrailSet'
ALTER TABLE [dbo].[GenLifePlus10_AuditTrailSet]
ADD CONSTRAINT [PK_GenLifePlus10_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_AuditTrailSet'
ALTER TABLE [dbo].[GenSmart5_AuditTrailSet]
ADD CONSTRAINT [PK_GenSmart5_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSmart5_PayerSet'
ALTER TABLE [dbo].[GenSmart5_PayerSet]
ADD CONSTRAINT [PK_GenSmart5_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_AssuredSet'
ALTER TABLE [dbo].[GenSabye4_AssuredSet]
ADD CONSTRAINT [PK_GenSabye4_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_AuditTrailSet'
ALTER TABLE [dbo].[GenSabye4_AuditTrailSet]
ADD CONSTRAINT [PK_GenSabye4_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_DCRSet'
ALTER TABLE [dbo].[GenSabye4_DCRSet]
ADD CONSTRAINT [PK_GenSabye4_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_FATCASet'
ALTER TABLE [dbo].[GenSabye4_FATCASet]
ADD CONSTRAINT [PK_GenSabye4_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenSabye4_MasterCoverageSet'
ALTER TABLE [dbo].[GenSabye4_MasterCoverageSet]
ADD CONSTRAINT [PK_GenSabye4_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenSabye4_MasterPlanSet'
ALTER TABLE [dbo].[GenSabye4_MasterPlanSet]
ADD CONSTRAINT [PK_GenSabye4_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_PayerSet'
ALTER TABLE [dbo].[GenSabye4_PayerSet]
ADD CONSTRAINT [PK_GenSabye4_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_PaymentSet'
ALTER TABLE [dbo].[GenSabye4_PaymentSet]
ADD CONSTRAINT [PK_GenSabye4_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_PlanSet'
ALTER TABLE [dbo].[GenSabye4_PlanSet]
ADD CONSTRAINT [PK_GenSabye4_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [PK_GenSabye4_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSabye4_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenSabye4_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenSabye4_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_AssuredSet'
ALTER TABLE [dbo].[GenHighLife3_AssuredSet]
ADD CONSTRAINT [PK_GenHighLife3_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_AuditTrailSet'
ALTER TABLE [dbo].[GenHighLife3_AuditTrailSet]
ADD CONSTRAINT [PK_GenHighLife3_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_DCRSet'
ALTER TABLE [dbo].[GenHighLife3_DCRSet]
ADD CONSTRAINT [PK_GenHighLife3_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_FATCASet'
ALTER TABLE [dbo].[GenHighLife3_FATCASet]
ADD CONSTRAINT [PK_GenHighLife3_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenHighLife3_MasterCoverageSet'
ALTER TABLE [dbo].[GenHighLife3_MasterCoverageSet]
ADD CONSTRAINT [PK_GenHighLife3_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHighLife3_MasterPlanSet'
ALTER TABLE [dbo].[GenHighLife3_MasterPlanSet]
ADD CONSTRAINT [PK_GenHighLife3_MasterPlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_PayerSet'
ALTER TABLE [dbo].[GenHighLife3_PayerSet]
ADD CONSTRAINT [PK_GenHighLife3_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_PaymentSet'
ALTER TABLE [dbo].[GenHighLife3_PaymentSet]
ADD CONSTRAINT [PK_GenHighLife3_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_PlanSet'
ALTER TABLE [dbo].[GenHighLife3_PlanSet]
ADD CONSTRAINT [PK_GenHighLife3_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [PK_GenHighLife3_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHighLife3_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenHighLife3_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenHighLife3_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHealthProtect_MasterPlanCoverageSet'
ALTER TABLE [dbo].[GenHealthProtect_MasterPlanCoverageSet]
ADD CONSTRAINT [PK_GenHealthProtect_MasterPlanCoverageSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHealthLumpSum_MasterPricePlanSet'
ALTER TABLE [dbo].[GenHealthLumpSum_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenHealthLumpSum_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenHealthLumpSum_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenHealthLumpSum_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenHealthProtect_MasterPricePlanSet'
ALTER TABLE [dbo].[GenHealthProtect_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenHealthProtect_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenHealthProtect_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenHealthProtect_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenHealthProtect_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenExclusivePA_MasterPricePlanSet'
ALTER TABLE [dbo].[GenExclusivePA_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenExclusivePA_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenExclusivePA_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenExclusivePA_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenExclusivePA_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenShieldPA_MasterPricePlanSet'
ALTER TABLE [dbo].[GenShieldPA_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenShieldPA_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenShieldPA_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenShieldPA_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenShieldPA_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenMax7_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMax7_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMax7_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenMax7_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenMax7_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenMax7_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenLifePlus10_MasterPricePlanSet'
ALTER TABLE [dbo].[GenLifePlus10_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenLifePlus10_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenLifePlus10_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenLifePlus10_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenLifePlus10_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenSmart5_MasterPricePlanSet'
ALTER TABLE [dbo].[GenSmart5_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenSmart5_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenSmart5_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenSmart5_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenSmart5_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenSabye4_MasterPricePlanSet'
ALTER TABLE [dbo].[GenSabye4_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenSabye4_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenSabye4_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenSabye4_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenSabye4_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenHighLife3_MasterPricePlanSet'
ALTER TABLE [dbo].[GenHighLife3_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenHighLife3_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenHighLife3_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenHighLife3_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenHighLife3_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_AssuredSet'
ALTER TABLE [dbo].[GenFamilyPA_AssuredSet]
ADD CONSTRAINT [PK_GenFamilyPA_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [PK_GenFamilyPA_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_PlanSet'
ALTER TABLE [dbo].[GenFamilyPA_PlanSet]
ADD CONSTRAINT [PK_GenFamilyPA_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_DCRSet'
ALTER TABLE [dbo].[GenFamilyPA_DCRSet]
ADD CONSTRAINT [PK_GenFamilyPA_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenFamilyPA_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenFamilyPA_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_PaymentSet'
ALTER TABLE [dbo].[GenFamilyPA_PaymentSet]
ADD CONSTRAINT [PK_GenFamilyPA_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_AuditTrailSet'
ALTER TABLE [dbo].[GenFamilyPA_AuditTrailSet]
ADD CONSTRAINT [PK_GenFamilyPA_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenFamilyPA_MasterPricePlanSet'
ALTER TABLE [dbo].[GenFamilyPA_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenFamilyPA_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenFamilyPA_MasterCoverageSet'
ALTER TABLE [dbo].[GenFamilyPA_MasterCoverageSet]
ADD CONSTRAINT [PK_GenFamilyPA_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenFamilyPA_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenFamilyPA_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenFamilyPA_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_Subsidiary1Set'
ALTER TABLE [dbo].[GenFamilyPA_Subsidiary1Set]
ADD CONSTRAINT [PK_GenFamilyPA_Subsidiary1Set]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_Subsidiary2Set'
ALTER TABLE [dbo].[GenFamilyPA_Subsidiary2Set]
ADD CONSTRAINT [PK_GenFamilyPA_Subsidiary2Set]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_Subsidiary3Set'
ALTER TABLE [dbo].[GenFamilyPA_Subsidiary3Set]
ADD CONSTRAINT [PK_GenFamilyPA_Subsidiary3Set]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenFamilyPA_Subsidiary4Set'
ALTER TABLE [dbo].[GenFamilyPA_Subsidiary4Set]
ADD CONSTRAINT [PK_GenFamilyPA_Subsidiary4Set]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [PK_GenAomSub6_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_AssuredSet'
ALTER TABLE [dbo].[GenAomSub6_AssuredSet]
ADD CONSTRAINT [PK_GenAomSub6_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_PlanSet'
ALTER TABLE [dbo].[GenAomSub6_PlanSet]
ADD CONSTRAINT [PK_GenAomSub6_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_DCRSet'
ALTER TABLE [dbo].[GenAomSub6_DCRSet]
ADD CONSTRAINT [PK_GenAomSub6_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_FATCASet'
ALTER TABLE [dbo].[GenAomSub6_FATCASet]
ADD CONSTRAINT [PK_GenAomSub6_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_PaymentSet'
ALTER TABLE [dbo].[GenAomSub6_PaymentSet]
ADD CONSTRAINT [PK_GenAomSub6_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_AuditTrailSet'
ALTER TABLE [dbo].[GenAomSub6_AuditTrailSet]
ADD CONSTRAINT [PK_GenAomSub6_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenAomSub6_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenAomSub6_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenAomSub6_PayerSet'
ALTER TABLE [dbo].[GenAomSub6_PayerSet]
ADD CONSTRAINT [PK_GenAomSub6_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenAomSub6_MasterPricePlanSet'
ALTER TABLE [dbo].[GenAomSub6_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenAomSub6_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenAomSub6_MasterCoverageSet'
ALTER TABLE [dbo].[GenAomSub6_MasterCoverageSet]
ADD CONSTRAINT [PK_GenAomSub6_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenAomSub6_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenAomSub6_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenAomSub6_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL1_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_AssuredSet'
ALTER TABLE [dbo].[GenMotor_VOL1_AssuredSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_OldPolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_OldPolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL1_OldPolicySet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_PlanSet'
ALTER TABLE [dbo].[GenMotor_VOL1_PlanSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_TaxInvoiceSet'
ALTER TABLE [dbo].[GenMotor_VOL1_TaxInvoiceSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_TaxInvoiceSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL1_DCRSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_AuditTrailSet'
ALTER TABLE [dbo].[GenMotor_VOL1_AuditTrailSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_VehicleSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_VehicleSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_DriverSet'
ALTER TABLE [dbo].[GenMotor_VOL1_DriverSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_DriverSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotor_VOL1_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenMotor_VOL1_MasterCoverageSet'
ALTER TABLE [dbo].[GenMotor_VOL1_MasterCoverageSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [VehicleBrandId] in table 'GenMotor_VOL1_VehicleBrandSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleBrandSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_VehicleBrandSet]
    PRIMARY KEY CLUSTERED ([VehicleBrandId] ASC);
GO

-- Creating primary key on [VehicleModelId] in table 'GenMotor_VOL1_VehicleModelSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleModelSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_VehicleModelSet]
    PRIMARY KEY CLUSTERED ([VehicleModelId] ASC);
GO

-- Creating primary key on [VehicleTypeId] in table 'GenMotor_VOL1_VehicleTypeSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleTypeSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_VehicleTypeSet]
    PRIMARY KEY CLUSTERED ([VehicleTypeId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_PaymentSet'
ALTER TABLE [dbo].[GenMotor_VOL1_PaymentSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1_InspectionSet'
ALTER TABLE [dbo].[GenMotor_VOL1_InspectionSet]
ADD CONSTRAINT [PK_GenMotor_VOL1_InspectionSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenHealthLumpSum_Plan1Set'
ALTER TABLE [dbo].[GenHealthLumpSum_Plan1Set]
ADD CONSTRAINT [PK_GenHealthLumpSum_Plan1Set]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ValuePreferenceId] in table 'ValuePreferenceSet'
ALTER TABLE [dbo].[ValuePreferenceSet]
ADD CONSTRAINT [PK_ValuePreferenceSet]
    PRIMARY KEY CLUSTERED ([ValuePreferenceId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_AssuredSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_AssuredSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_PlanSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PlanSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_VehicleSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_VehicleSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_VehicleSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_OldPolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_OldPolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_OldPolicySet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_AuditTrailSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_AuditTrailSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_DriverSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_DriverSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_DriverSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_InspectionSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_InspectionSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_InspectionSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_PaymentSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PaymentSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_DCRSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL1Std_TaxInvoiceSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_TaxInvoiceSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_TaxInvoiceSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_AssuredSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_AssuredSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_VehicleSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_VehicleSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_VehicleSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_AuditTrailSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_AuditTrailSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_OldPolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_OldPolicySet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_OldPolicySet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_PaymentSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PaymentSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_DCRSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_PlanSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PlanSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_VOL2Plus_TaxInvoiceSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_TaxInvoiceSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_TaxInvoiceSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenMotor_VOL2Plus_MasterCoverageSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_MasterCoverageSet]
ADD CONSTRAINT [PK_GenMotor_VOL2Plus_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenMotor_VOL1Std_MasterCoverageSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_MasterCoverageSet]
ADD CONSTRAINT [PK_GenMotor_VOL1Std_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [PK_GenMotor_CMI_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_VehicleSet'
ALTER TABLE [dbo].[GenMotor_CMI_VehicleSet]
ADD CONSTRAINT [PK_GenMotor_CMI_VehicleSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_AuditTrailSet'
ALTER TABLE [dbo].[GenMotor_CMI_AuditTrailSet]
ADD CONSTRAINT [PK_GenMotor_CMI_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_PaymentSet'
ALTER TABLE [dbo].[GenMotor_CMI_PaymentSet]
ADD CONSTRAINT [PK_GenMotor_CMI_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_DCRSet'
ALTER TABLE [dbo].[GenMotor_CMI_DCRSet]
ADD CONSTRAINT [PK_GenMotor_CMI_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_PlanSet'
ALTER TABLE [dbo].[GenMotor_CMI_PlanSet]
ADD CONSTRAINT [PK_GenMotor_CMI_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_AssuredSet'
ALTER TABLE [dbo].[GenMotor_CMI_AssuredSet]
ADD CONSTRAINT [PK_GenMotor_CMI_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotor_CMI_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotor_CMI_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenMotor_CMI_MasterCoverageSet'
ALTER TABLE [dbo].[GenMotor_CMI_MasterCoverageSet]
ADD CONSTRAINT [PK_GenMotor_CMI_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotor_CMI_TaxInvoiceSet'
ALTER TABLE [dbo].[GenMotor_CMI_TaxInvoiceSet]
ADD CONSTRAINT [PK_GenMotor_CMI_TaxInvoiceSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [VehicleBrandId] in table 'VehicleBrandSet'
ALTER TABLE [dbo].[VehicleBrandSet]
ADD CONSTRAINT [PK_VehicleBrandSet]
    PRIMARY KEY CLUSTERED ([VehicleBrandId] ASC);
GO

-- Creating primary key on [MenuPreferenceId] in table 'MenuPreferenceSet'
ALTER TABLE [dbo].[MenuPreferenceSet]
ADD CONSTRAINT [PK_MenuPreferenceSet]
    PRIMARY KEY CLUSTERED ([MenuPreferenceId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [PK_GenCancer_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_AssuredSet'
ALTER TABLE [dbo].[GenCancer_AssuredSet]
ADD CONSTRAINT [PK_GenCancer_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_AuditTrailSet'
ALTER TABLE [dbo].[GenCancer_AuditTrailSet]
ADD CONSTRAINT [PK_GenCancer_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_PlanSet'
ALTER TABLE [dbo].[GenCancer_PlanSet]
ADD CONSTRAINT [PK_GenCancer_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_PayerSet'
ALTER TABLE [dbo].[GenCancer_PayerSet]
ADD CONSTRAINT [PK_GenCancer_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_PaymentSet'
ALTER TABLE [dbo].[GenCancer_PaymentSet]
ADD CONSTRAINT [PK_GenCancer_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_DCRSet'
ALTER TABLE [dbo].[GenCancer_DCRSet]
ADD CONSTRAINT [PK_GenCancer_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancer_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenCancer_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenCancer_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PlanId] in table 'GenCancer_MasterPricePlanSet'
ALTER TABLE [dbo].[GenCancer_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenCancer_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenCancer_MasterCoverageSet'
ALTER TABLE [dbo].[GenCancer_MasterCoverageSet]
ADD CONSTRAINT [PK_GenCancer_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ScriptId] in table 'GenCancer_LegalScriptTemplateSet'
ALTER TABLE [dbo].[GenCancer_LegalScriptTemplateSet]
ADD CONSTRAINT [PK_GenCancer_LegalScriptTemplateSet]
    PRIMARY KEY CLUSTERED ([ScriptId] ASC);
GO

-- Creating primary key on [AddressTypeId] in table 'AddressTypeSet'
ALTER TABLE [dbo].[AddressTypeSet]
ADD CONSTRAINT [PK_AddressTypeSet]
    PRIMARY KEY CLUSTERED ([AddressTypeId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [PK_GenMotorType1Std_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_AssuredSet'
ALTER TABLE [dbo].[GenMotorType1Std_AssuredSet]
ADD CONSTRAINT [PK_GenMotorType1Std_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_PlanSet'
ALTER TABLE [dbo].[GenMotorType1Std_PlanSet]
ADD CONSTRAINT [PK_GenMotorType1Std_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_CTPLSet'
ALTER TABLE [dbo].[GenMotorType1Std_CTPLSet]
ADD CONSTRAINT [PK_GenMotorType1Std_CTPLSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_AuditTrailSet'
ALTER TABLE [dbo].[GenMotorType1Std_AuditTrailSet]
ADD CONSTRAINT [PK_GenMotorType1Std_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_InspectionSet'
ALTER TABLE [dbo].[GenMotorType1Std_InspectionSet]
ADD CONSTRAINT [PK_GenMotorType1Std_InspectionSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_DriverSet'
ALTER TABLE [dbo].[GenMotorType1Std_DriverSet]
ADD CONSTRAINT [PK_GenMotorType1Std_DriverSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_OldPolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_OldPolicySet]
ADD CONSTRAINT [PK_GenMotorType1Std_OldPolicySet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_PaymentSet'
ALTER TABLE [dbo].[GenMotorType1Std_PaymentSet]
ADD CONSTRAINT [PK_GenMotorType1Std_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_VehicleSet'
ALTER TABLE [dbo].[GenMotorType1Std_VehicleSet]
ADD CONSTRAINT [PK_GenMotorType1Std_VehicleSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_PlanDiscountSet'
ALTER TABLE [dbo].[GenMotorType1Std_PlanDiscountSet]
ADD CONSTRAINT [PK_GenMotorType1Std_PlanDiscountSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_DCRSet'
ALTER TABLE [dbo].[GenMotorType1Std_DCRSet]
ADD CONSTRAINT [PK_GenMotorType1Std_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenGroupSME_PolicySet'
ALTER TABLE [dbo].[GenGroupSME_PolicySet]
ADD CONSTRAINT [PK_GenGroupSME_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_DCRSet'
ALTER TABLE [dbo].[GenGroupSME_DCRSet]
ADD CONSTRAINT [PK_GenGroupSME_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_PaymentSet'
ALTER TABLE [dbo].[GenGroupSME_PaymentSet]
ADD CONSTRAINT [PK_GenGroupSME_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_AssuredSet'
ALTER TABLE [dbo].[GenGroupSME_AssuredSet]
ADD CONSTRAINT [PK_GenGroupSME_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_EmployeeSet'
ALTER TABLE [dbo].[GenGroupSME_EmployeeSet]
ADD CONSTRAINT [PK_GenGroupSME_EmployeeSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_AuditTrailSet'
ALTER TABLE [dbo].[GenGroupSME_AuditTrailSet]
ADD CONSTRAINT [PK_GenGroupSME_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenGroupSME_PlanSet'
ALTER TABLE [dbo].[GenGroupSME_PlanSet]
ADD CONSTRAINT [PK_GenGroupSME_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [Coverage], [Plan], [SMESize] in table 'GenGroupSME_MasterPricePlanSet'
ALTER TABLE [dbo].[GenGroupSME_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenGroupSME_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([Coverage], [Plan], [SMESize] ASC);
GO

-- Creating primary key on [CreditCardClassTypeId] in table 'CreditCardClassTypeSet'
ALTER TABLE [dbo].[CreditCardClassTypeSet]
ADD CONSTRAINT [PK_CreditCardClassTypeSet]
    PRIMARY KEY CLUSTERED ([CreditCardClassTypeId] ASC);
GO

-- Creating primary key on [Id] in table 'MotorConstantSet'
ALTER TABLE [dbo].[MotorConstantSet]
ADD CONSTRAINT [PK_MotorConstantSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MotorPolicyNumberSet'
ALTER TABLE [dbo].[MotorPolicyNumberSet]
ADD CONSTRAINT [PK_MotorPolicyNumberSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [GroupCompanyId] in table 'GroupCompanySet'
ALTER TABLE [dbo].[GroupCompanySet]
ADD CONSTRAINT [PK_GroupCompanySet]
    PRIMARY KEY CLUSTERED ([GroupCompanyId] ASC);
GO

-- Creating primary key on [Code] in table 'PaymentMethodSet'
ALTER TABLE [dbo].[PaymentMethodSet]
ADD CONSTRAINT [PK_PaymentMethodSet]
    PRIMARY KEY CLUSTERED ([Code] ASC);
GO

-- Creating primary key on [PRODUCTNAME], [PROPOSALID] in table 'MasterPolicySet'
ALTER TABLE [dbo].[MasterPolicySet]
ADD CONSTRAINT [PK_MasterPolicySet]
    PRIMARY KEY CLUSTERED ([PRODUCTNAME], [PROPOSALID] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenMotorType1Std_ReferDocumentSet'
ALTER TABLE [dbo].[GenMotorType1Std_ReferDocumentSet]
ADD CONSTRAINT [PK_GenMotorType1Std_ReferDocumentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [MotorModelId] in table 'MotorModelSet'
ALTER TABLE [dbo].[MotorModelSet]
ADD CONSTRAINT [PK_MotorModelSet]
    PRIMARY KEY CLUSTERED ([MotorModelId] ASC);
GO

-- Creating primary key on [Id] in table 'MasterPolicy_DeliverySet'
ALTER TABLE [dbo].[MasterPolicy_DeliverySet]
ADD CONSTRAINT [PK_MasterPolicy_DeliverySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UWRejectReasonSet'
ALTER TABLE [dbo].[UWRejectReasonSet]
ADD CONSTRAINT [PK_UWRejectReasonSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenMotorType1SGL_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotorType1SGL_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotorType1SGL_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenMotorType2Plus_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotorType2Plus_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotorType2Plus_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenMotorType3_MasterPricePlanSet'
ALTER TABLE [dbo].[GenMotorType3_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenMotorType3_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [PK_GenProLife8_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_AssuredSet'
ALTER TABLE [dbo].[GenProLife8_AssuredSet]
ADD CONSTRAINT [PK_GenProLife8_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_DCRSet'
ALTER TABLE [dbo].[GenProLife8_DCRSet]
ADD CONSTRAINT [PK_GenProLife8_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_PaymentSet'
ALTER TABLE [dbo].[GenProLife8_PaymentSet]
ADD CONSTRAINT [PK_GenProLife8_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_FATCASet'
ALTER TABLE [dbo].[GenProLife8_FATCASet]
ADD CONSTRAINT [PK_GenProLife8_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenProLife8_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenProLife8_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_PlanSet'
ALTER TABLE [dbo].[GenProLife8_PlanSet]
ADD CONSTRAINT [PK_GenProLife8_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_PayerSet'
ALTER TABLE [dbo].[GenProLife8_PayerSet]
ADD CONSTRAINT [PK_GenProLife8_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenProLife8_AuditTrailSet'
ALTER TABLE [dbo].[GenProLife8_AuditTrailSet]
ADD CONSTRAINT [PK_GenProLife8_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenProLife8_MasterPricePlanSet'
ALTER TABLE [dbo].[GenProLife8_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenProLife8_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenProLife8_MasterCoverageSet'
ALTER TABLE [dbo].[GenProLife8_MasterCoverageSet]
ADD CONSTRAINT [PK_GenProLife8_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [PK_GenCancerLife_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_AssuredSet'
ALTER TABLE [dbo].[GenCancerLife_AssuredSet]
ADD CONSTRAINT [PK_GenCancerLife_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_PaymentSet'
ALTER TABLE [dbo].[GenCancerLife_PaymentSet]
ADD CONSTRAINT [PK_GenCancerLife_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_PlanSet'
ALTER TABLE [dbo].[GenCancerLife_PlanSet]
ADD CONSTRAINT [PK_GenCancerLife_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_DCRSet'
ALTER TABLE [dbo].[GenCancerLife_DCRSet]
ADD CONSTRAINT [PK_GenCancerLife_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_FATCASet'
ALTER TABLE [dbo].[GenCancerLife_FATCASet]
ADD CONSTRAINT [PK_GenCancerLife_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenCancerLife_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenCancerLife_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenCancerLife_MasterCoverageSet'
ALTER TABLE [dbo].[GenCancerLife_MasterCoverageSet]
ADD CONSTRAINT [PK_GenCancerLife_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_PayerSet'
ALTER TABLE [dbo].[GenCancerLife_PayerSet]
ADD CONSTRAINT [PK_GenCancerLife_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenCancerLife_AuditTrailSet'
ALTER TABLE [dbo].[GenCancerLife_AuditTrailSet]
ADD CONSTRAINT [PK_GenCancerLife_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenCancerLife_MasterPricePlanSet'
ALTER TABLE [dbo].[GenCancerLife_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenCancerLife_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenExclusivePA_PayerSet'
ALTER TABLE [dbo].[GenExclusivePA_PayerSet]
ADD CONSTRAINT [PK_GenExclusivePA_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenShieldPA_PayerSet'
ALTER TABLE [dbo].[GenShieldPA_PayerSet]
ADD CONSTRAINT [PK_GenShieldPA_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [TargetType] in table 'SupervisorTargetTypeSet'
ALTER TABLE [dbo].[SupervisorTargetTypeSet]
ADD CONSTRAINT [PK_SupervisorTargetTypeSet]
    PRIMARY KEY CLUSTERED ([TargetType] ASC);
GO

-- Creating primary key on [Id] in table 'SupervisorTargetSet'
ALTER TABLE [dbo].[SupervisorTargetSet]
ADD CONSTRAINT [PK_SupervisorTargetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [PK_GenSenior55_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_AssuredSet'
ALTER TABLE [dbo].[GenSenior55_AssuredSet]
ADD CONSTRAINT [PK_GenSenior55_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_PaymentSet'
ALTER TABLE [dbo].[GenSenior55_PaymentSet]
ADD CONSTRAINT [PK_GenSenior55_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_PlanSet'
ALTER TABLE [dbo].[GenSenior55_PlanSet]
ADD CONSTRAINT [PK_GenSenior55_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_DCRSet'
ALTER TABLE [dbo].[GenSenior55_DCRSet]
ADD CONSTRAINT [PK_GenSenior55_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_FATCASet'
ALTER TABLE [dbo].[GenSenior55_FATCASet]
ADD CONSTRAINT [PK_GenSenior55_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenSenior55_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenSenior55_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [CoverageId] in table 'GenSenior55_MasterCoverageSet'
ALTER TABLE [dbo].[GenSenior55_MasterCoverageSet]
ADD CONSTRAINT [PK_GenSenior55_MasterCoverageSet]
    PRIMARY KEY CLUSTERED ([CoverageId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_PayerSet'
ALTER TABLE [dbo].[GenSenior55_PayerSet]
ADD CONSTRAINT [PK_GenSenior55_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenSenior55_AuditTrailSet'
ALTER TABLE [dbo].[GenSenior55_AuditTrailSet]
ADD CONSTRAINT [PK_GenSenior55_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenSenior55_MasterPricePlanSet'
ALTER TABLE [dbo].[GenSenior55_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenSenior55_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [ProposalId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [PK_GenPABonus_PolicySet]
    PRIMARY KEY CLUSTERED ([ProposalId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_AssuredSet'
ALTER TABLE [dbo].[GenPABonus_AssuredSet]
ADD CONSTRAINT [PK_GenPABonus_AssuredSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_PaymentSet'
ALTER TABLE [dbo].[GenPABonus_PaymentSet]
ADD CONSTRAINT [PK_GenPABonus_PaymentSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_PlanSet'
ALTER TABLE [dbo].[GenPABonus_PlanSet]
ADD CONSTRAINT [PK_GenPABonus_PlanSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_DCRSet'
ALTER TABLE [dbo].[GenPABonus_DCRSet]
ADD CONSTRAINT [PK_GenPABonus_DCRSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_FATCASet'
ALTER TABLE [dbo].[GenPABonus_FATCASet]
ADD CONSTRAINT [PK_GenPABonus_FATCASet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_UnderWriteInfoSet'
ALTER TABLE [dbo].[GenPABonus_UnderWriteInfoSet]
ADD CONSTRAINT [PK_GenPABonus_UnderWriteInfoSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_PayerSet'
ALTER TABLE [dbo].[GenPABonus_PayerSet]
ADD CONSTRAINT [PK_GenPABonus_PayerSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'GenPABonus_AuditTrailSet'
ALTER TABLE [dbo].[GenPABonus_AuditTrailSet]
ADD CONSTRAINT [PK_GenPABonus_AuditTrailSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [PricePlanId] in table 'GenPABonus_MasterPricePlanSet'
ALTER TABLE [dbo].[GenPABonus_MasterPricePlanSet]
ADD CONSTRAINT [PK_GenPABonus_MasterPricePlanSet]
    PRIMARY KEY CLUSTERED ([PricePlanId] ASC);
GO

-- Creating primary key on [Id] in table 'TSRTargetSet'
ALTER TABLE [dbo].[TSRTargetSet]
ADD CONSTRAINT [PK_TSRTargetSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [OccupationClassOccupationClassId] in table 'OccupationSet'
ALTER TABLE [dbo].[OccupationSet]
ADD CONSTRAINT [FK_OccupationOccupationClass]
    FOREIGN KEY ([OccupationClassOccupationClassId])
    REFERENCES [dbo].[OccupationClassSet]
        ([OccupationClassId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OccupationOccupationClass'
CREATE INDEX [IX_FK_OccupationOccupationClass]
ON [dbo].[OccupationSet]
    ([OccupationClassOccupationClassId]);
GO

-- Creating foreign key on [GenHealthLumpSum_Payment_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payment]
    FOREIGN KEY ([GenHealthLumpSum_Payment_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payment'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payment]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_Payment_ItemId]);
GO

-- Creating foreign key on [GenHealthLumpSum_AuditTrail_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_AuditTrail]
    FOREIGN KEY ([GenHealthLumpSum_AuditTrail_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_AuditTrail'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_AuditTrail]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenHealthLumpSum_PolicyProposalId] in table 'GenHealthLumpSum_DCRSet'
ALTER TABLE [dbo].[GenHealthLumpSum_DCRSet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_DCR]
    FOREIGN KEY ([GenHealthLumpSum_PolicyProposalId])
    REFERENCES [dbo].[GenHealthLumpSum_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_DCR'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_DCR]
ON [dbo].[GenHealthLumpSum_DCRSet]
    ([GenHealthLumpSum_PolicyProposalId]);
GO

-- Creating foreign key on [GenHealthLumpSum_Assured_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Assured]
    FOREIGN KEY ([GenHealthLumpSum_Assured_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Assured'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Assured]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_Assured_ItemId]);
GO

-- Creating foreign key on [GenHealthLumpSum_UnderWriteInfo_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_UnderWriteInfo]
    FOREIGN KEY ([GenHealthLumpSum_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_UnderWriteInfo'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_UnderWriteInfo]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenHealthLumpSum_Plan_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Plan]
    FOREIGN KEY ([GenHealthLumpSum_Plan_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Plan'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Plan]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_Plan_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_Assured_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Assured]
    FOREIGN KEY ([GenHealthProtect_Assured_ItemId])
    REFERENCES [dbo].[GenHealthProtect_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_Assured'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_Assured]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_Assured_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_AuditTrail_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_AuditTrail]
    FOREIGN KEY ([GenHealthProtect_AuditTrail_ItemId])
    REFERENCES [dbo].[GenHealthProtect_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_AuditTrail'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_AuditTrail]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_Payment_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Payment]
    FOREIGN KEY ([GenHealthProtect_Payment_ItemId])
    REFERENCES [dbo].[GenHealthProtect_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_Payment'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_Payment]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_Payment_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_Plan_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Plan]
    FOREIGN KEY ([GenHealthProtect_Plan_ItemId])
    REFERENCES [dbo].[GenHealthProtect_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_Plan'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_Plan]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_Plan_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_UnderWriteInfo_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_UnderWriteInfo]
    FOREIGN KEY ([GenHealthProtect_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenHealthProtect_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_UnderWriteInfo'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_UnderWriteInfo]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_PolicyProposalId] in table 'GenHealthProtect_DCRSet'
ALTER TABLE [dbo].[GenHealthProtect_DCRSet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_DCR]
    FOREIGN KEY ([GenHealthProtect_PolicyProposalId])
    REFERENCES [dbo].[GenHealthProtect_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_DCR'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_DCR]
ON [dbo].[GenHealthProtect_DCRSet]
    ([GenHealthProtect_PolicyProposalId]);
GO

-- Creating foreign key on [ProductProductId] in table 'LeadSourceSet'
ALTER TABLE [dbo].[LeadSourceSet]
ADD CONSTRAINT [FK_LeadSourceProduct]
    FOREIGN KEY ([ProductProductId])
    REFERENCES [dbo].[ProductSet]
        ([ProductId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeadSourceProduct'
CREATE INDEX [IX_FK_LeadSourceProduct]
ON [dbo].[LeadSourceSet]
    ([ProductProductId]);
GO

-- Creating foreign key on [GenHealthLumpSum_Payer_ItemId] in table 'GenHealthLumpSum_PolicySet'
ALTER TABLE [dbo].[GenHealthLumpSum_PolicySet]
ADD CONSTRAINT [FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payer]
    FOREIGN KEY ([GenHealthLumpSum_Payer_ItemId])
    REFERENCES [dbo].[GenHealthLumpSum_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payer'
CREATE INDEX [IX_FK_GenHealthLumpSum_PolicyGenHealthLumpSum_Payer]
ON [dbo].[GenHealthLumpSum_PolicySet]
    ([GenHealthLumpSum_Payer_ItemId]);
GO

-- Creating foreign key on [GenHealthProtect_Payer_ItemId] in table 'GenHealthProtect_PolicySet'
ALTER TABLE [dbo].[GenHealthProtect_PolicySet]
ADD CONSTRAINT [FK_GenHealthProtect_PolicyGenHealthProtect_Payer]
    FOREIGN KEY ([GenHealthProtect_Payer_ItemId])
    REFERENCES [dbo].[GenHealthProtect_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHealthProtect_PolicyGenHealthProtect_Payer'
CREATE INDEX [IX_FK_GenHealthProtect_PolicyGenHealthProtect_Payer]
ON [dbo].[GenHealthProtect_PolicySet]
    ([GenHealthProtect_Payer_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_Assured_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Assured]
    FOREIGN KEY ([GenExclusivePA_Assured_ItemId])
    REFERENCES [dbo].[GenExclusivePA_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_Assured'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_Assured]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_Assured_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_AuditTrail_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_AuditTrail]
    FOREIGN KEY ([GenExclusivePA_AuditTrail_ItemId])
    REFERENCES [dbo].[GenExclusivePA_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_AuditTrail'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_AuditTrail]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_PolicyProposalId] in table 'GenExclusivePA_DCRSet'
ALTER TABLE [dbo].[GenExclusivePA_DCRSet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_DCR]
    FOREIGN KEY ([GenExclusivePA_PolicyProposalId])
    REFERENCES [dbo].[GenExclusivePA_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_DCR'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_DCR]
ON [dbo].[GenExclusivePA_DCRSet]
    ([GenExclusivePA_PolicyProposalId]);
GO

-- Creating foreign key on [GenExclusivePA_Payment_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Payment]
    FOREIGN KEY ([GenExclusivePA_Payment_ItemId])
    REFERENCES [dbo].[GenExclusivePA_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_Payment'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_Payment]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_Payment_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_Plan_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Plan]
    FOREIGN KEY ([GenExclusivePA_Plan_ItemId])
    REFERENCES [dbo].[GenExclusivePA_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_Plan'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_Plan]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_Plan_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_UnderWriteInfo_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_UnderWriteInfo]
    FOREIGN KEY ([GenExclusivePA_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenExclusivePA_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_UnderWriteInfo'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_UnderWriteInfo]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_Assured_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Assured]
    FOREIGN KEY ([GenShieldPA_Assured_ItemId])
    REFERENCES [dbo].[GenShieldPA_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_Assured'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_Assured]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_Assured_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_AuditTrail_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_AuditTrail]
    FOREIGN KEY ([GenShieldPA_AuditTrail_ItemId])
    REFERENCES [dbo].[GenShieldPA_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_AuditTrail'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_AuditTrail]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_PolicyProposalId] in table 'GenShieldPA_DCRSet'
ALTER TABLE [dbo].[GenShieldPA_DCRSet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_DCR]
    FOREIGN KEY ([GenShieldPA_PolicyProposalId])
    REFERENCES [dbo].[GenShieldPA_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_DCR'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_DCR]
ON [dbo].[GenShieldPA_DCRSet]
    ([GenShieldPA_PolicyProposalId]);
GO

-- Creating foreign key on [GenShieldPA_Payment_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Payment]
    FOREIGN KEY ([GenShieldPA_Payment_ItemId])
    REFERENCES [dbo].[GenShieldPA_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_Payment'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_Payment]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_Payment_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_Plan_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Plan]
    FOREIGN KEY ([GenShieldPA_Plan_ItemId])
    REFERENCES [dbo].[GenShieldPA_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_Plan'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_Plan]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_Plan_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_UnderWriteInfo_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_UnderWriteInfo]
    FOREIGN KEY ([GenShieldPA_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenShieldPA_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_UnderWriteInfo'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_UnderWriteInfo]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenMax7_Assured_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_Assured]
    FOREIGN KEY ([GenMax7_Assured_ItemId])
    REFERENCES [dbo].[GenMax7_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_Assured'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_Assured]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_Assured_ItemId]);
GO

-- Creating foreign key on [GenMax7_AuditTrail_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_AuditTrail]
    FOREIGN KEY ([GenMax7_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMax7_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_AuditTrail'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_AuditTrail]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMax7_PolicyProposalId] in table 'GenMax7_DCRSet'
ALTER TABLE [dbo].[GenMax7_DCRSet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_DCR]
    FOREIGN KEY ([GenMax7_PolicyProposalId])
    REFERENCES [dbo].[GenMax7_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_DCR'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_DCR]
ON [dbo].[GenMax7_DCRSet]
    ([GenMax7_PolicyProposalId]);
GO

-- Creating foreign key on [GenMax7_Payer_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_Payer]
    FOREIGN KEY ([GenMax7_Payer_ItemId])
    REFERENCES [dbo].[GenMax7_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_Payer'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_Payer]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_Payer_ItemId]);
GO

-- Creating foreign key on [GenMax7_Payment_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_Payment]
    FOREIGN KEY ([GenMax7_Payment_ItemId])
    REFERENCES [dbo].[GenMax7_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_Payment'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_Payment]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_Payment_ItemId]);
GO

-- Creating foreign key on [GenMax7_Plan_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_Plan]
    FOREIGN KEY ([GenMax7_Plan_ItemId])
    REFERENCES [dbo].[GenMax7_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_Plan'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_Plan]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_Plan_ItemId]);
GO

-- Creating foreign key on [GenMax7_UnderWriteInfo_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_UnderWriteInfo]
    FOREIGN KEY ([GenMax7_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenMax7_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_UnderWriteInfo'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_UnderWriteInfo]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenMax7_FATCA_ItemId] in table 'GenMax7_PolicySet'
ALTER TABLE [dbo].[GenMax7_PolicySet]
ADD CONSTRAINT [FK_GenMax7_PolicyGenMax7_FATCA]
    FOREIGN KEY ([GenMax7_FATCA_ItemId])
    REFERENCES [dbo].[GenMax7_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMax7_PolicyGenMax7_FATCA'
CREATE INDEX [IX_FK_GenMax7_PolicyGenMax7_FATCA]
ON [dbo].[GenMax7_PolicySet]
    ([GenMax7_FATCA_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_AuditTrail_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_AuditTrail]
    FOREIGN KEY ([GenLifePlus10_AuditTrail_ItemId])
    REFERENCES [dbo].[GenLifePlus10_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_AuditTrail'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_AuditTrail]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_Plan_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Plan]
    FOREIGN KEY ([GenLifePlus10_Plan_ItemId])
    REFERENCES [dbo].[GenLifePlus10_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_Plan'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_Plan]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_Plan_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_PolicyProposalId] in table 'GenLifePlus10_DCRSet'
ALTER TABLE [dbo].[GenLifePlus10_DCRSet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_DCR]
    FOREIGN KEY ([GenLifePlus10_PolicyProposalId])
    REFERENCES [dbo].[GenLifePlus10_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_DCR'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_DCR]
ON [dbo].[GenLifePlus10_DCRSet]
    ([GenLifePlus10_PolicyProposalId]);
GO

-- Creating foreign key on [GenLifePlus10_Assured_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Assured]
    FOREIGN KEY ([GenLifePlus10_Assured_ItemId])
    REFERENCES [dbo].[GenLifePlus10_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_Assured'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_Assured]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_Assured_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_FATCA_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_FATCA]
    FOREIGN KEY ([GenLifePlus10_FATCA_ItemId])
    REFERENCES [dbo].[GenLifePlus10_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_FATCA'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_FATCA]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_FATCA_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_Payer_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Payer]
    FOREIGN KEY ([GenLifePlus10_Payer_ItemId])
    REFERENCES [dbo].[GenLifePlus10_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_Payer'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_Payer]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_Payer_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_Payment_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_Payment]
    FOREIGN KEY ([GenLifePlus10_Payment_ItemId])
    REFERENCES [dbo].[GenLifePlus10_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_Payment'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_Payment]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_Payment_ItemId]);
GO

-- Creating foreign key on [GenLifePlus10_UnderWriteInfo_ItemId] in table 'GenLifePlus10_PolicySet'
ALTER TABLE [dbo].[GenLifePlus10_PolicySet]
ADD CONSTRAINT [FK_GenLifePlus10_PolicyGenLifePlus10_UnderwriterInfo]
    FOREIGN KEY ([GenLifePlus10_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenLifePlus10_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenLifePlus10_PolicyGenLifePlus10_UnderwriterInfo'
CREATE INDEX [IX_FK_GenLifePlus10_PolicyGenLifePlus10_UnderwriterInfo]
ON [dbo].[GenLifePlus10_PolicySet]
    ([GenLifePlus10_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenSmart5_AuditTrail_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_AuditTrail]
    FOREIGN KEY ([GenSmart5_AuditTrail_ItemId])
    REFERENCES [dbo].[GenSmart5_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_AuditTrail'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_AuditTrail]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenSmart5_FATCA_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_FATCA]
    FOREIGN KEY ([GenSmart5_FATCA_ItemId])
    REFERENCES [dbo].[GenSmart5_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_FATCA'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_FATCA]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_FATCA_ItemId]);
GO

-- Creating foreign key on [GenSmart5_Payer_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Payer]
    FOREIGN KEY ([GenSmart5_Payer_ItemId])
    REFERENCES [dbo].[GenSmart5_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_Payer'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_Payer]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_Payer_ItemId]);
GO

-- Creating foreign key on [GenSmart5_Plan_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Plan]
    FOREIGN KEY ([GenSmart5_Plan_ItemId])
    REFERENCES [dbo].[GenSmart5_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_Plan'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_Plan]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_Plan_ItemId]);
GO

-- Creating foreign key on [GenSmart5_UnderWriteInfo_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_UnderwriterInfo]
    FOREIGN KEY ([GenSmart5_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenSmart5_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_UnderwriterInfo'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_UnderwriterInfo]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenSmart5_Assured_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Assured]
    FOREIGN KEY ([GenSmart5_Assured_ItemId])
    REFERENCES [dbo].[GenSmart5_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_Assured'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_Assured]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_Assured_ItemId]);
GO

-- Creating foreign key on [GenSmart5_Payment_ItemId] in table 'GenSmart5_PolicySet'
ALTER TABLE [dbo].[GenSmart5_PolicySet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_Payment]
    FOREIGN KEY ([GenSmart5_Payment_ItemId])
    REFERENCES [dbo].[GenSmart5_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_Payment'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_Payment]
ON [dbo].[GenSmart5_PolicySet]
    ([GenSmart5_Payment_ItemId]);
GO

-- Creating foreign key on [GenSmart5_PolicyProposalId] in table 'GenSmart5_DCRSet'
ALTER TABLE [dbo].[GenSmart5_DCRSet]
ADD CONSTRAINT [FK_GenSmart5_PolicyGenSmart5_DCR]
    FOREIGN KEY ([GenSmart5_PolicyProposalId])
    REFERENCES [dbo].[GenSmart5_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSmart5_PolicyGenSmart5_DCR'
CREATE INDEX [IX_FK_GenSmart5_PolicyGenSmart5_DCR]
ON [dbo].[GenSmart5_DCRSet]
    ([GenSmart5_PolicyProposalId]);
GO

-- Creating foreign key on [GenSabye4_UnderWriteInfo_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_UnderWriteInfo]
    FOREIGN KEY ([GenSabye4_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenSabye4_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_UnderWriteInfo'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_UnderWriteInfo]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenSabye4_Plan_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Plan]
    FOREIGN KEY ([GenSabye4_Plan_ItemId])
    REFERENCES [dbo].[GenSabye4_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_Plan'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_Plan]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_Plan_ItemId]);
GO

-- Creating foreign key on [GenSabye4_Payment_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Payment]
    FOREIGN KEY ([GenSabye4_Payment_ItemId])
    REFERENCES [dbo].[GenSabye4_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_Payment'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_Payment]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_Payment_ItemId]);
GO

-- Creating foreign key on [GenSabye4_Assured_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Assured]
    FOREIGN KEY ([GenSabye4_Assured_ItemId])
    REFERENCES [dbo].[GenSabye4_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_Assured'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_Assured]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_Assured_ItemId]);
GO

-- Creating foreign key on [GenSabye4_FATCA_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_FATCA]
    FOREIGN KEY ([GenSabye4_FATCA_ItemId])
    REFERENCES [dbo].[GenSabye4_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_FATCA'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_FATCA]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_FATCA_ItemId]);
GO

-- Creating foreign key on [GenSabye4_PolicyProposalId] in table 'GenSabye4_DCRSet'
ALTER TABLE [dbo].[GenSabye4_DCRSet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_DCR]
    FOREIGN KEY ([GenSabye4_PolicyProposalId])
    REFERENCES [dbo].[GenSabye4_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_DCR'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_DCR]
ON [dbo].[GenSabye4_DCRSet]
    ([GenSabye4_PolicyProposalId]);
GO

-- Creating foreign key on [GenSabye4_AuditTrail_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_AuditTrail]
    FOREIGN KEY ([GenSabye4_AuditTrail_ItemId])
    REFERENCES [dbo].[GenSabye4_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_AuditTrail'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_AuditTrail]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenSabye4_Payer_ItemId] in table 'GenSabye4_PolicySet'
ALTER TABLE [dbo].[GenSabye4_PolicySet]
ADD CONSTRAINT [FK_GenSabye4_PolicyGenSabye4_Payer]
    FOREIGN KEY ([GenSabye4_Payer_ItemId])
    REFERENCES [dbo].[GenSabye4_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSabye4_PolicyGenSabye4_Payer'
CREATE INDEX [IX_FK_GenSabye4_PolicyGenSabye4_Payer]
ON [dbo].[GenSabye4_PolicySet]
    ([GenSabye4_Payer_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_Assured_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Assured]
    FOREIGN KEY ([GenHighLife3_Assured_ItemId])
    REFERENCES [dbo].[GenHighLife3_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_Assured'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_Assured]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_Assured_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_AuditTrail_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_AuditTrail]
    FOREIGN KEY ([GenHighLife3_AuditTrail_ItemId])
    REFERENCES [dbo].[GenHighLife3_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_AuditTrail'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_AuditTrail]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_PolicyProposalId] in table 'GenHighLife3_DCRSet'
ALTER TABLE [dbo].[GenHighLife3_DCRSet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_DCR]
    FOREIGN KEY ([GenHighLife3_PolicyProposalId])
    REFERENCES [dbo].[GenHighLife3_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_DCR'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_DCR]
ON [dbo].[GenHighLife3_DCRSet]
    ([GenHighLife3_PolicyProposalId]);
GO

-- Creating foreign key on [GenHighLife3_FATCA_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_FATCA]
    FOREIGN KEY ([GenHighLife3_FATCA_ItemId])
    REFERENCES [dbo].[GenHighLife3_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_FATCA'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_FATCA]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_FATCA_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_Plan_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Plan]
    FOREIGN KEY ([GenHighLife3_Plan_ItemId])
    REFERENCES [dbo].[GenHighLife3_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_Plan'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_Plan]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_Plan_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_UnderWriteInfo_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_UnderWriterInfo]
    FOREIGN KEY ([GenHighLife3_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenHighLife3_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_UnderWriterInfo'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_UnderWriterInfo]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_Payment_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Payment1]
    FOREIGN KEY ([GenHighLife3_Payment_ItemId])
    REFERENCES [dbo].[GenHighLife3_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_Payment1'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_Payment1]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_Payment_ItemId]);
GO

-- Creating foreign key on [GenHighLife3_Payer_ItemId] in table 'GenHighLife3_PolicySet'
ALTER TABLE [dbo].[GenHighLife3_PolicySet]
ADD CONSTRAINT [FK_GenHighLife3_PolicyGenHighLife3_Payer]
    FOREIGN KEY ([GenHighLife3_Payer_ItemId])
    REFERENCES [dbo].[GenHighLife3_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenHighLife3_PolicyGenHighLife3_Payer'
CREATE INDEX [IX_FK_GenHighLife3_PolicyGenHighLife3_Payer]
ON [dbo].[GenHighLife3_PolicySet]
    ([GenHighLife3_Payer_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Plan_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Plan]
    FOREIGN KEY ([GenFamilyPA_Plan_ItemId])
    REFERENCES [dbo].[GenFamilyPA_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Plan'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Plan]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Plan_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Assured_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Assured]
    FOREIGN KEY ([GenFamilyPA_Assured_ItemId])
    REFERENCES [dbo].[GenFamilyPA_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Assured'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Assured]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Assured_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_PolicyProposalId] in table 'GenFamilyPA_DCRSet'
ALTER TABLE [dbo].[GenFamilyPA_DCRSet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_DCR]
    FOREIGN KEY ([GenFamilyPA_PolicyProposalId])
    REFERENCES [dbo].[GenFamilyPA_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_DCR'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_DCR]
ON [dbo].[GenFamilyPA_DCRSet]
    ([GenFamilyPA_PolicyProposalId]);
GO

-- Creating foreign key on [GenFamilyPA_UnderWriteInfo_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_UnderWriteInfo]
    FOREIGN KEY ([GenFamilyPA_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenFamilyPA_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_UnderWriteInfo'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_UnderWriteInfo]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Payment_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Payment]
    FOREIGN KEY ([GenFamilyPA_Payment_ItemId])
    REFERENCES [dbo].[GenFamilyPA_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Payment'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Payment]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Payment_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_AuditTrail_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_AuditTrail]
    FOREIGN KEY ([GenFamilyPA_AuditTrail_ItemId])
    REFERENCES [dbo].[GenFamilyPA_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_AuditTrail'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_AuditTrail]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Subsidiary1_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary1]
    FOREIGN KEY ([GenFamilyPA_Subsidiary1_ItemId])
    REFERENCES [dbo].[GenFamilyPA_Subsidiary1Set]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary1'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary1]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Subsidiary1_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Subsidiary2_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary2]
    FOREIGN KEY ([GenFamilyPA_Subsidiary2_ItemId])
    REFERENCES [dbo].[GenFamilyPA_Subsidiary2Set]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary2'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary2]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Subsidiary2_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Subsidiary3_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary3]
    FOREIGN KEY ([GenFamilyPA_Subsidiary3_ItemId])
    REFERENCES [dbo].[GenFamilyPA_Subsidiary3Set]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary3'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary3]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Subsidiary3_ItemId]);
GO

-- Creating foreign key on [GenFamilyPA_Subsidiary4_ItemId] in table 'GenFamilyPA_PolicySet'
ALTER TABLE [dbo].[GenFamilyPA_PolicySet]
ADD CONSTRAINT [FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary4]
    FOREIGN KEY ([GenFamilyPA_Subsidiary4_ItemId])
    REFERENCES [dbo].[GenFamilyPA_Subsidiary4Set]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary4'
CREATE INDEX [IX_FK_GenFamilyPA_PolicyGenFamilyPA_Subsidiary4]
ON [dbo].[GenFamilyPA_PolicySet]
    ([GenFamilyPA_Subsidiary4_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_AuditTrail_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_AuditTrail]
    FOREIGN KEY ([GenAomSub6_AuditTrail_ItemId])
    REFERENCES [dbo].[GenAomSub6_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_AuditTrail'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_AuditTrail]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_Payment_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Payment]
    FOREIGN KEY ([GenAomSub6_Payment_ItemId])
    REFERENCES [dbo].[GenAomSub6_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_Payment'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_Payment]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_Payment_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_Plan_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Plan]
    FOREIGN KEY ([GenAomSub6_Plan_ItemId])
    REFERENCES [dbo].[GenAomSub6_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_Plan'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_Plan]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_Plan_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_PolicyProposalId] in table 'GenAomSub6_DCRSet'
ALTER TABLE [dbo].[GenAomSub6_DCRSet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_DCR]
    FOREIGN KEY ([GenAomSub6_PolicyProposalId])
    REFERENCES [dbo].[GenAomSub6_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_DCR'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_DCR]
ON [dbo].[GenAomSub6_DCRSet]
    ([GenAomSub6_PolicyProposalId]);
GO

-- Creating foreign key on [GenAomSub6_FATCA_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_FATCA]
    FOREIGN KEY ([GenAomSub6_FATCA_ItemId])
    REFERENCES [dbo].[GenAomSub6_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_FATCA'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_FATCA]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_FATCA_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_UnderWriteInfo_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_UnderWriteInfo]
    FOREIGN KEY ([GenAomSub6_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenAomSub6_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_UnderWriteInfo'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_UnderWriteInfo]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_Assured_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Assured]
    FOREIGN KEY ([GenAomSub6_Assured_ItemId])
    REFERENCES [dbo].[GenAomSub6_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_Assured'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_Assured]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_Assured_ItemId]);
GO

-- Creating foreign key on [GenAomSub6_Payer_ItemId] in table 'GenAomSub6_PolicySet'
ALTER TABLE [dbo].[GenAomSub6_PolicySet]
ADD CONSTRAINT [FK_GenAomSub6_PolicyGenAomSub6_Payer]
    FOREIGN KEY ([GenAomSub6_Payer_ItemId])
    REFERENCES [dbo].[GenAomSub6_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenAomSub6_PolicyGenAomSub6_Payer'
CREATE INDEX [IX_FK_GenAomSub6_PolicyGenAomSub6_Payer]
ON [dbo].[GenAomSub6_PolicySet]
    ([GenAomSub6_Payer_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Assured_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Assured]
    FOREIGN KEY ([GenMotor_VOL1_Assured_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Assured'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Assured]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Assured_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_AuditTrail_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_AuditTrail]
    FOREIGN KEY ([GenMotor_VOL1_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_AuditTrail'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_AuditTrail]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_PolicyProposalId] in table 'GenMotor_VOL1_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL1_DCRSet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_DCR]
    FOREIGN KEY ([GenMotor_VOL1_PolicyProposalId])
    REFERENCES [dbo].[GenMotor_VOL1_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_DCR'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_DCR]
ON [dbo].[GenMotor_VOL1_DCRSet]
    ([GenMotor_VOL1_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Driver_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Driver]
    FOREIGN KEY ([GenMotor_VOL1_Driver_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_DriverSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Driver'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Driver]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Driver_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_OldPolicy_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_OldPolicy]
    FOREIGN KEY ([GenMotor_VOL1_OldPolicy_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_OldPolicySet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_OldPolicy'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_OldPolicy]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_OldPolicy_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Plan_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Plan]
    FOREIGN KEY ([GenMotor_VOL1_Plan_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Plan'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Plan]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_TaxInvoice_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_TaxInvoice]
    FOREIGN KEY ([GenMotor_VOL1_TaxInvoice_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_TaxInvoiceSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_TaxInvoice'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_TaxInvoice]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_TaxInvoice_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Vehicle_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Vehicle]
    FOREIGN KEY ([GenMotor_VOL1_Vehicle_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_VehicleSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Vehicle'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Vehicle]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Vehicle_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Payment_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Payment]
    FOREIGN KEY ([GenMotor_VOL1_Payment_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Payment'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Payment]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Payment_ItemId]);
GO

-- Creating foreign key on [VehicleBrandVehicleBrandId] in table 'GenMotor_VOL1_VehicleModelSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleModelSet]
ADD CONSTRAINT [FK_VehicleBrandVehicleModel]
    FOREIGN KEY ([VehicleBrandVehicleBrandId])
    REFERENCES [dbo].[GenMotor_VOL1_VehicleBrandSet]
        ([VehicleBrandId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleBrandVehicleModel'
CREATE INDEX [IX_FK_VehicleBrandVehicleModel]
ON [dbo].[GenMotor_VOL1_VehicleModelSet]
    ([VehicleBrandVehicleBrandId]);
GO

-- Creating foreign key on [VehicleTypeVehicleTypeId] in table 'GenMotor_VOL1_VehicleModelSet'
ALTER TABLE [dbo].[GenMotor_VOL1_VehicleModelSet]
ADD CONSTRAINT [FK_VehicleTypeVehicleModel]
    FOREIGN KEY ([VehicleTypeVehicleTypeId])
    REFERENCES [dbo].[GenMotor_VOL1_VehicleTypeSet]
        ([VehicleTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleTypeVehicleModel'
CREATE INDEX [IX_FK_VehicleTypeVehicleModel]
ON [dbo].[GenMotor_VOL1_VehicleModelSet]
    ([VehicleTypeVehicleTypeId]);
GO

-- Creating foreign key on [GenMotor_VOL1_Inspection_ItemId] in table 'GenMotor_VOL1_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Inspection]
    FOREIGN KEY ([GenMotor_VOL1_Inspection_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1_InspectionSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Inspection'
CREATE INDEX [IX_FK_GenMotor_VOL1_PolicyGenMotor_VOL1_Inspection]
ON [dbo].[GenMotor_VOL1_PolicySet]
    ([GenMotor_VOL1_Inspection_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Assured_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Assured]
    FOREIGN KEY ([GenMotor_VOL1Std_Assured_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Assured'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Assured]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Assured_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_OldPolicy_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_OldPolicy]
    FOREIGN KEY ([GenMotor_VOL1Std_OldPolicy_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_OldPolicySet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_OldPolicy'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_OldPolicy]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_OldPolicy_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Plan_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Plan]
    FOREIGN KEY ([GenMotor_VOL1Std_Plan_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Plan'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Plan]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_AuditTrail_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_AuditTrail]
    FOREIGN KEY ([GenMotor_VOL1Std_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_AuditTrail'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_AuditTrail]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Vehicle_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Vehicle]
    FOREIGN KEY ([GenMotor_VOL1Std_Vehicle_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_VehicleSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Vehicle'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Vehicle]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Vehicle_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Driver_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Driver]
    FOREIGN KEY ([GenMotor_VOL1Std_Driver_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_DriverSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Driver'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Driver]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Driver_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Payment_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Payment]
    FOREIGN KEY ([GenMotor_VOL1Std_Payment_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Payment'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Payment]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Payment_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_Inspection_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Inspection]
    FOREIGN KEY ([GenMotor_VOL1Std_Inspection_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_InspectionSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Inspection'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_Inspection]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_Inspection_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_PolicyProposalId] in table 'GenMotor_VOL1Std_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_DCRSet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_DCR]
    FOREIGN KEY ([GenMotor_VOL1Std_PolicyProposalId])
    REFERENCES [dbo].[GenMotor_VOL1Std_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_DCR'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_DCR]
ON [dbo].[GenMotor_VOL1Std_DCRSet]
    ([GenMotor_VOL1Std_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotor_VOL1Std_TaxInvoice_ItemId] in table 'GenMotor_VOL1Std_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_TaxInvoice]
    FOREIGN KEY ([GenMotor_VOL1Std_TaxInvoice_ItemId])
    REFERENCES [dbo].[GenMotor_VOL1Std_TaxInvoiceSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_TaxInvoice'
CREATE INDEX [IX_FK_GenMotor_VOL1Std_PolicyGenMotor_VOL1Std_TaxInvoice]
ON [dbo].[GenMotor_VOL1Std_PolicySet]
    ([GenMotor_VOL1Std_TaxInvoice_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_Assured_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Assured]
    FOREIGN KEY ([GenMotor_VOL2Plus_Assured_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Assured'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Assured]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_Assured_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_AuditTrail_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_AuditTrail]
    FOREIGN KEY ([GenMotor_VOL2Plus_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_AuditTrail'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_AuditTrail]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_PolicyProposalId] in table 'GenMotor_VOL2Plus_DCRSet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_DCRSet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_DCR]
    FOREIGN KEY ([GenMotor_VOL2Plus_PolicyProposalId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_DCR'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_DCR]
ON [dbo].[GenMotor_VOL2Plus_DCRSet]
    ([GenMotor_VOL2Plus_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_Vehicle_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Vehicle]
    FOREIGN KEY ([GenMotor_VOL2Plus_Vehicle_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_VehicleSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Vehicle'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Vehicle]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_Vehicle_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_Payment_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Payment]
    FOREIGN KEY ([GenMotor_VOL2Plus_Payment_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Payment'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Payment]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_Payment_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_OldPolicy_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_OldPolicy]
    FOREIGN KEY ([GenMotor_VOL2Plus_OldPolicy_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_OldPolicySet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_OldPolicy'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_OldPolicy]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_OldPolicy_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_Plan_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Plan]
    FOREIGN KEY ([GenMotor_VOL2Plus_Plan_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Plan'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_Plan]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotor_VOL2Plus_TaxInvoice_ItemId] in table 'GenMotor_VOL2Plus_PolicySet'
ALTER TABLE [dbo].[GenMotor_VOL2Plus_PolicySet]
ADD CONSTRAINT [FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_TaxInvoice]
    FOREIGN KEY ([GenMotor_VOL2Plus_TaxInvoice_ItemId])
    REFERENCES [dbo].[GenMotor_VOL2Plus_TaxInvoiceSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_TaxInvoice'
CREATE INDEX [IX_FK_GenMotor_VOL2Plus_PolicyGenMotor_VOL2Plus_TaxInvoice]
ON [dbo].[GenMotor_VOL2Plus_PolicySet]
    ([GenMotor_VOL2Plus_TaxInvoice_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_Vehicle_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Vehicle]
    FOREIGN KEY ([GenMotor_CMI_Vehicle_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_VehicleSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_Vehicle'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_Vehicle]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_Vehicle_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_Assured_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Assured]
    FOREIGN KEY ([GenMotor_CMI_Assured_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_Assured'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_Assured]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_Assured_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_AuditTrail_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_AuditTrail]
    FOREIGN KEY ([GenMotor_CMI_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_AuditTrail'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_AuditTrail]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_Payment_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Payment]
    FOREIGN KEY ([GenMotor_CMI_Payment_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_Payment'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_Payment]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_Payment_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_Plan_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_Plan]
    FOREIGN KEY ([GenMotor_CMI_Plan_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_Plan'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_Plan]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotor_CMI_PolicyProposalId] in table 'GenMotor_CMI_DCRSet'
ALTER TABLE [dbo].[GenMotor_CMI_DCRSet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_DCR]
    FOREIGN KEY ([GenMotor_CMI_PolicyProposalId])
    REFERENCES [dbo].[GenMotor_CMI_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_DCR'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_DCR]
ON [dbo].[GenMotor_CMI_DCRSet]
    ([GenMotor_CMI_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotor_CMI_TaxInvoice_ItemId] in table 'GenMotor_CMI_PolicySet'
ALTER TABLE [dbo].[GenMotor_CMI_PolicySet]
ADD CONSTRAINT [FK_GenMotor_CMI_PolicyGenMotor_CMI_TaxInvoice]
    FOREIGN KEY ([GenMotor_CMI_TaxInvoice_ItemId])
    REFERENCES [dbo].[GenMotor_CMI_TaxInvoiceSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotor_CMI_PolicyGenMotor_CMI_TaxInvoice'
CREATE INDEX [IX_FK_GenMotor_CMI_PolicyGenMotor_CMI_TaxInvoice]
ON [dbo].[GenMotor_CMI_PolicySet]
    ([GenMotor_CMI_TaxInvoice_ItemId]);
GO

-- Creating foreign key on [GenCancer_Assured_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancel_Assured]
    FOREIGN KEY ([GenCancer_Assured_ItemId])
    REFERENCES [dbo].[GenCancer_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancel_Assured'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancel_Assured]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_Assured_ItemId]);
GO

-- Creating foreign key on [GenCancer_AuditTrail_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_AuditTrail]
    FOREIGN KEY ([GenCancer_AuditTrail_ItemId])
    REFERENCES [dbo].[GenCancer_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_AuditTrail'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_AuditTrail]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenCancer_Payer_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_Payer]
    FOREIGN KEY ([GenCancer_Payer_ItemId])
    REFERENCES [dbo].[GenCancer_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_Payer'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_Payer]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_Payer_ItemId]);
GO

-- Creating foreign key on [GenCancer_Payment_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_Payment]
    FOREIGN KEY ([GenCancer_Payment_ItemId])
    REFERENCES [dbo].[GenCancer_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_Payment'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_Payment]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_Payment_ItemId]);
GO

-- Creating foreign key on [GenCancer_Plan_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_Plan]
    FOREIGN KEY ([GenCancer_Plan_ItemId])
    REFERENCES [dbo].[GenCancer_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_Plan'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_Plan]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_Plan_ItemId]);
GO

-- Creating foreign key on [GenCancer_UnderWriteInfo_ItemId] in table 'GenCancer_PolicySet'
ALTER TABLE [dbo].[GenCancer_PolicySet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_UnderWriteInfo]
    FOREIGN KEY ([GenCancer_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenCancer_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_UnderWriteInfo'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_UnderWriteInfo]
ON [dbo].[GenCancer_PolicySet]
    ([GenCancer_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenCancer_PolicyProposalId] in table 'GenCancer_DCRSet'
ALTER TABLE [dbo].[GenCancer_DCRSet]
ADD CONSTRAINT [FK_GenCancer_PolicyGenCancer_DCR]
    FOREIGN KEY ([GenCancer_PolicyProposalId])
    REFERENCES [dbo].[GenCancer_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancer_PolicyGenCancer_DCR'
CREATE INDEX [IX_FK_GenCancer_PolicyGenCancer_DCR]
ON [dbo].[GenCancer_DCRSet]
    ([GenCancer_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotorType1Std_Assured_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Assured]
    FOREIGN KEY ([GenMotorType1Std_Assured_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Assured'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Assured]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Assured_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_AuditTrail_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_AuditTrail]
    FOREIGN KEY ([GenMotorType1Std_AuditTrail_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_AuditTrail'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_AuditTrail]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_CTPL_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_CTPL]
    FOREIGN KEY ([GenMotorType1Std_CTPL_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_CTPLSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_CTPL'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_CTPL]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_CTPL_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_Driver_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Driver]
    FOREIGN KEY ([GenMotorType1Std_Driver_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_DriverSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Driver'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Driver]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Driver_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_Inspection_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Inspection]
    FOREIGN KEY ([GenMotorType1Std_Inspection_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_InspectionSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Inspection'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Inspection]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Inspection_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_OldPolicy_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_OldPolicy]
    FOREIGN KEY ([GenMotorType1Std_OldPolicy_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_OldPolicySet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_OldPolicy'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_OldPolicy]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_OldPolicy_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_Payment_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Payment]
    FOREIGN KEY ([GenMotorType1Std_Payment_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Payment'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Payment]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Payment_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_Plan_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Plan]
    FOREIGN KEY ([GenMotorType1Std_Plan_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Plan'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Plan]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_Vehicle_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_Vehicle]
    FOREIGN KEY ([GenMotorType1Std_Vehicle_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_VehicleSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_Vehicle'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_Vehicle]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_Vehicle_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_PolicyProposalId] in table 'GenMotorType1Std_DCRSet'
ALTER TABLE [dbo].[GenMotorType1Std_DCRSet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_DCR]
    FOREIGN KEY ([GenMotorType1Std_PolicyProposalId])
    REFERENCES [dbo].[GenMotorType1Std_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_DCR'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_DCR]
ON [dbo].[GenMotorType1Std_DCRSet]
    ([GenMotorType1Std_PolicyProposalId]);
GO

-- Creating foreign key on [GenMotorType1Std_PlanDiscount_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_PlanDiscount]
    FOREIGN KEY ([GenMotorType1Std_PlanDiscount_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_PlanDiscountSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_PlanDiscount'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_PlanDiscount]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_PlanDiscount_ItemId]);
GO

-- Creating foreign key on [GenGroupSME_Payment_ItemId] in table 'GenGroupSME_PolicySet'
ALTER TABLE [dbo].[GenGroupSME_PolicySet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Payment]
    FOREIGN KEY ([GenGroupSME_Payment_ItemId])
    REFERENCES [dbo].[GenGroupSME_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_Payment'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_Payment]
ON [dbo].[GenGroupSME_PolicySet]
    ([GenGroupSME_Payment_ItemId]);
GO

-- Creating foreign key on [GenGroupSME_Assured_ItemId] in table 'GenGroupSME_PolicySet'
ALTER TABLE [dbo].[GenGroupSME_PolicySet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Assured]
    FOREIGN KEY ([GenGroupSME_Assured_ItemId])
    REFERENCES [dbo].[GenGroupSME_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_Assured'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_Assured]
ON [dbo].[GenGroupSME_PolicySet]
    ([GenGroupSME_Assured_ItemId]);
GO

-- Creating foreign key on [GenGroupSME_PolicyProposalId] in table 'GenGroupSME_EmployeeSet'
ALTER TABLE [dbo].[GenGroupSME_EmployeeSet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Employee]
    FOREIGN KEY ([GenGroupSME_PolicyProposalId])
    REFERENCES [dbo].[GenGroupSME_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_Employee'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_Employee]
ON [dbo].[GenGroupSME_EmployeeSet]
    ([GenGroupSME_PolicyProposalId]);
GO

-- Creating foreign key on [GenGroupSME_AuditTrail_ItemId] in table 'GenGroupSME_PolicySet'
ALTER TABLE [dbo].[GenGroupSME_PolicySet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_AuditTrail]
    FOREIGN KEY ([GenGroupSME_AuditTrail_ItemId])
    REFERENCES [dbo].[GenGroupSME_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_AuditTrail'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_AuditTrail]
ON [dbo].[GenGroupSME_PolicySet]
    ([GenGroupSME_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenGroupSME_PolicyProposalId] in table 'GenGroupSME_DCRSet'
ALTER TABLE [dbo].[GenGroupSME_DCRSet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_DCR]
    FOREIGN KEY ([GenGroupSME_PolicyProposalId])
    REFERENCES [dbo].[GenGroupSME_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_DCR'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_DCR]
ON [dbo].[GenGroupSME_DCRSet]
    ([GenGroupSME_PolicyProposalId]);
GO

-- Creating foreign key on [GenGroupSME_Plan_ItemId] in table 'GenGroupSME_PolicySet'
ALTER TABLE [dbo].[GenGroupSME_PolicySet]
ADD CONSTRAINT [FK_GenGroupSME_PolicyGenGroupSME_Plan]
    FOREIGN KEY ([GenGroupSME_Plan_ItemId])
    REFERENCES [dbo].[GenGroupSME_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenGroupSME_PolicyGenGroupSME_Plan'
CREATE INDEX [IX_FK_GenGroupSME_PolicyGenGroupSME_Plan]
ON [dbo].[GenGroupSME_PolicySet]
    ([GenGroupSME_Plan_ItemId]);
GO

-- Creating foreign key on [GenMotorType1Std_ReferDocument_ItemId] in table 'GenMotorType1Std_PolicySet'
ALTER TABLE [dbo].[GenMotorType1Std_PolicySet]
ADD CONSTRAINT [FK_GenMotorType1Std_PolicyGenMotorType1Std_ReferDocument]
    FOREIGN KEY ([GenMotorType1Std_ReferDocument_ItemId])
    REFERENCES [dbo].[GenMotorType1Std_ReferDocumentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenMotorType1Std_PolicyGenMotorType1Std_ReferDocument'
CREATE INDEX [IX_FK_GenMotorType1Std_PolicyGenMotorType1Std_ReferDocument]
ON [dbo].[GenMotorType1Std_PolicySet]
    ([GenMotorType1Std_ReferDocument_ItemId]);
GO

-- Creating foreign key on [PRODUCTNAME], [PROPOSALID] in table 'MasterPolicy_DeliverySet'
ALTER TABLE [dbo].[MasterPolicy_DeliverySet]
ADD CONSTRAINT [FK_MasterPolicy_DeliveryMasterPolicy]
    FOREIGN KEY ([PRODUCTNAME], [PROPOSALID])
    REFERENCES [dbo].[MasterPolicySet]
        ([PRODUCTNAME], [PROPOSALID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MasterPolicy_DeliveryMasterPolicy'
CREATE INDEX [IX_FK_MasterPolicy_DeliveryMasterPolicy]
ON [dbo].[MasterPolicy_DeliverySet]
    ([PRODUCTNAME], [PROPOSALID]);
GO

-- Creating foreign key on [GenProLife8_Assured_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Assured]
    FOREIGN KEY ([GenProLife8_Assured_ItemId])
    REFERENCES [dbo].[GenProLife8_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_Assured'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_Assured]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_Assured_ItemId]);
GO

-- Creating foreign key on [GenProLife8_AuditTrail_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_AuditTrail]
    FOREIGN KEY ([GenProLife8_AuditTrail_ItemId])
    REFERENCES [dbo].[GenProLife8_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_AuditTrail'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_AuditTrail]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenProLife8_PolicyProposalId] in table 'GenProLife8_DCRSet'
ALTER TABLE [dbo].[GenProLife8_DCRSet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_DCR]
    FOREIGN KEY ([GenProLife8_PolicyProposalId])
    REFERENCES [dbo].[GenProLife8_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_DCR'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_DCR]
ON [dbo].[GenProLife8_DCRSet]
    ([GenProLife8_PolicyProposalId]);
GO

-- Creating foreign key on [GenProLife8_FATCA_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_FATCA]
    FOREIGN KEY ([GenProLife8_FATCA_ItemId])
    REFERENCES [dbo].[GenProLife8_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_FATCA'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_FATCA]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_FATCA_ItemId]);
GO

-- Creating foreign key on [GenProLife8_Payer_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Payer]
    FOREIGN KEY ([GenProLife8_Payer_ItemId])
    REFERENCES [dbo].[GenProLife8_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_Payer'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_Payer]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_Payer_ItemId]);
GO

-- Creating foreign key on [GenProLife8_Payment_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Payment]
    FOREIGN KEY ([GenProLife8_Payment_ItemId])
    REFERENCES [dbo].[GenProLife8_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_Payment'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_Payment]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_Payment_ItemId]);
GO

-- Creating foreign key on [GenProLife8_UnderWriteInfo_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_UnderWriteInfo]
    FOREIGN KEY ([GenProLife8_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenProLife8_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_UnderWriteInfo'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_UnderWriteInfo]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenProLife8_Plan_ItemId] in table 'GenProLife8_PolicySet'
ALTER TABLE [dbo].[GenProLife8_PolicySet]
ADD CONSTRAINT [FK_GenProLife8_PolicyGenProLife8_Plan]
    FOREIGN KEY ([GenProLife8_Plan_ItemId])
    REFERENCES [dbo].[GenProLife8_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenProLife8_PolicyGenProLife8_Plan'
CREATE INDEX [IX_FK_GenProLife8_PolicyGenProLife8_Plan]
ON [dbo].[GenProLife8_PolicySet]
    ([GenProLife8_Plan_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_Assured_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Assured]
    FOREIGN KEY ([GenCancerLife_Assured_ItemId])
    REFERENCES [dbo].[GenCancerLife_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_Assured'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_Assured]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_Assured_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_AuditTrail_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_AuditTrail]
    FOREIGN KEY ([GenCancerLife_AuditTrail_ItemId])
    REFERENCES [dbo].[GenCancerLife_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_AuditTrail'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_AuditTrail]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_PolicyProposalId] in table 'GenCancerLife_DCRSet'
ALTER TABLE [dbo].[GenCancerLife_DCRSet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_DCR]
    FOREIGN KEY ([GenCancerLife_PolicyProposalId])
    REFERENCES [dbo].[GenCancerLife_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_DCR'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_DCR]
ON [dbo].[GenCancerLife_DCRSet]
    ([GenCancerLife_PolicyProposalId]);
GO

-- Creating foreign key on [GenCancerLife_FATCA_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_FATCA]
    FOREIGN KEY ([GenCancerLife_FATCA_ItemId])
    REFERENCES [dbo].[GenCancerLife_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_FATCA'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_FATCA]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_FATCA_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_Payer_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Payer]
    FOREIGN KEY ([GenCancerLife_Payer_ItemId])
    REFERENCES [dbo].[GenCancerLife_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_Payer'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_Payer]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_Payer_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_Payment_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Payment]
    FOREIGN KEY ([GenCancerLife_Payment_ItemId])
    REFERENCES [dbo].[GenCancerLife_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_Payment'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_Payment]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_Payment_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_Plan_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_Plan]
    FOREIGN KEY ([GenCancerLife_Plan_ItemId])
    REFERENCES [dbo].[GenCancerLife_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_Plan'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_Plan]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_Plan_ItemId]);
GO

-- Creating foreign key on [GenCancerLife_UnderWriteInfo_ItemId] in table 'GenCancerLife_PolicySet'
ALTER TABLE [dbo].[GenCancerLife_PolicySet]
ADD CONSTRAINT [FK_GenCancerLife_PolicyGenCancerLife_UnderWriteInfo]
    FOREIGN KEY ([GenCancerLife_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenCancerLife_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenCancerLife_PolicyGenCancerLife_UnderWriteInfo'
CREATE INDEX [IX_FK_GenCancerLife_PolicyGenCancerLife_UnderWriteInfo]
ON [dbo].[GenCancerLife_PolicySet]
    ([GenCancerLife_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenExclusivePA_Payer_ItemId] in table 'GenExclusivePA_PolicySet'
ALTER TABLE [dbo].[GenExclusivePA_PolicySet]
ADD CONSTRAINT [FK_GenExclusivePA_PolicyGenExclusivePA_Payer]
    FOREIGN KEY ([GenExclusivePA_Payer_ItemId])
    REFERENCES [dbo].[GenExclusivePA_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenExclusivePA_PolicyGenExclusivePA_Payer'
CREATE INDEX [IX_FK_GenExclusivePA_PolicyGenExclusivePA_Payer]
ON [dbo].[GenExclusivePA_PolicySet]
    ([GenExclusivePA_Payer_ItemId]);
GO

-- Creating foreign key on [GenShieldPA_Payer_ItemId] in table 'GenShieldPA_PolicySet'
ALTER TABLE [dbo].[GenShieldPA_PolicySet]
ADD CONSTRAINT [FK_GenShieldPA_PolicyGenShieldPA_Payer]
    FOREIGN KEY ([GenShieldPA_Payer_ItemId])
    REFERENCES [dbo].[GenShieldPA_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenShieldPA_PolicyGenShieldPA_Payer'
CREATE INDEX [IX_FK_GenShieldPA_PolicyGenShieldPA_Payer]
ON [dbo].[GenShieldPA_PolicySet]
    ([GenShieldPA_Payer_ItemId]);
GO

-- Creating foreign key on [TargetType] in table 'SupervisorTargetSet'
ALTER TABLE [dbo].[SupervisorTargetSet]
ADD CONSTRAINT [FK_SupervisorTargetSet_SupervisorTargetTypeSet]
    FOREIGN KEY ([TargetType])
    REFERENCES [dbo].[SupervisorTargetTypeSet]
        ([TargetType])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SupervisorTargetSet_SupervisorTargetTypeSet'
CREATE INDEX [IX_FK_SupervisorTargetSet_SupervisorTargetTypeSet]
ON [dbo].[SupervisorTargetSet]
    ([TargetType]);
GO

-- Creating foreign key on [GenSenior55_UnderWriteInfo_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_UnderWriteInfo]
    FOREIGN KEY ([GenSenior55_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenSenior55_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_UnderWriteInfo'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_UnderWriteInfo]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenSenior55_Plan_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Plan]
    FOREIGN KEY ([GenSenior55_Plan_ItemId])
    REFERENCES [dbo].[GenSenior55_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_Plan'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_Plan]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_Plan_ItemId]);
GO

-- Creating foreign key on [GenSenior55_Payment_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Payment]
    FOREIGN KEY ([GenSenior55_Payment_ItemId])
    REFERENCES [dbo].[GenSenior55_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_Payment'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_Payment]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_Payment_ItemId]);
GO

-- Creating foreign key on [GenSenior55_Payer_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGen_Payer]
    FOREIGN KEY ([GenSenior55_Payer_ItemId])
    REFERENCES [dbo].[GenSenior55_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGen_Payer'
CREATE INDEX [IX_FK_GenSenior55_PolicyGen_Payer]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_Payer_ItemId]);
GO

-- Creating foreign key on [GenSenior55_FATCA_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_FATCA]
    FOREIGN KEY ([GenSenior55_FATCA_ItemId])
    REFERENCES [dbo].[GenSenior55_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_FATCA'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_FATCA]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_FATCA_ItemId]);
GO

-- Creating foreign key on [GenSenior55_PolicyProposalId] in table 'GenSenior55_DCRSet'
ALTER TABLE [dbo].[GenSenior55_DCRSet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_DCR]
    FOREIGN KEY ([GenSenior55_PolicyProposalId])
    REFERENCES [dbo].[GenSenior55_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_DCR'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_DCR]
ON [dbo].[GenSenior55_DCRSet]
    ([GenSenior55_PolicyProposalId]);
GO

-- Creating foreign key on [GenSenior55_AuditTrail_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_AuditTrail]
    FOREIGN KEY ([GenSenior55_AuditTrail_ItemId])
    REFERENCES [dbo].[GenSenior55_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_AuditTrail'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_AuditTrail]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenSenior55_Assured_ItemId] in table 'GenSenior55_PolicySet'
ALTER TABLE [dbo].[GenSenior55_PolicySet]
ADD CONSTRAINT [FK_GenSenior55_PolicyGenSenior55_Assured]
    FOREIGN KEY ([GenSenior55_Assured_ItemId])
    REFERENCES [dbo].[GenSenior55_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenSenior55_PolicyGenSenior55_Assured'
CREATE INDEX [IX_FK_GenSenior55_PolicyGenSenior55_Assured]
ON [dbo].[GenSenior55_PolicySet]
    ([GenSenior55_Assured_ItemId]);
GO

-- Creating foreign key on [GenPABonus_UnderWriteInfo_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_UnderWriteInfo]
    FOREIGN KEY ([GenPABonus_UnderWriteInfo_ItemId])
    REFERENCES [dbo].[GenPABonus_UnderWriteInfoSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_UnderWriteInfo'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_UnderWriteInfo]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_UnderWriteInfo_ItemId]);
GO

-- Creating foreign key on [GenPABonus_Plan_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Plan]
    FOREIGN KEY ([GenPABonus_Plan_ItemId])
    REFERENCES [dbo].[GenPABonus_PlanSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_Plan'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_Plan]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_Plan_ItemId]);
GO

-- Creating foreign key on [GenPABonus_Payment_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Payment]
    FOREIGN KEY ([GenPABonus_Payment_ItemId])
    REFERENCES [dbo].[GenPABonus_PaymentSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_Payment'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_Payment]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_Payment_ItemId]);
GO

-- Creating foreign key on [GenPABonus_Payer_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGen_Payer]
    FOREIGN KEY ([GenPABonus_Payer_ItemId])
    REFERENCES [dbo].[GenPABonus_PayerSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGen_Payer'
CREATE INDEX [IX_FK_GenPABonus_PolicyGen_Payer]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_Payer_ItemId]);
GO

-- Creating foreign key on [GenPABonus_FATCA_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_FATCA]
    FOREIGN KEY ([GenPABonus_FATCA_ItemId])
    REFERENCES [dbo].[GenPABonus_FATCASet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_FATCA'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_FATCA]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_FATCA_ItemId]);
GO

-- Creating foreign key on [GenPABonus_PolicyProposalId] in table 'GenPABonus_DCRSet'
ALTER TABLE [dbo].[GenPABonus_DCRSet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_DCR]
    FOREIGN KEY ([GenPABonus_PolicyProposalId])
    REFERENCES [dbo].[GenPABonus_PolicySet]
        ([ProposalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_DCR'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_DCR]
ON [dbo].[GenPABonus_DCRSet]
    ([GenPABonus_PolicyProposalId]);
GO

-- Creating foreign key on [GenPABonus_AuditTrail_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_AuditTrail]
    FOREIGN KEY ([GenPABonus_AuditTrail_ItemId])
    REFERENCES [dbo].[GenPABonus_AuditTrailSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_AuditTrail'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_AuditTrail]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_AuditTrail_ItemId]);
GO

-- Creating foreign key on [GenPABonus_Assured_ItemId] in table 'GenPABonus_PolicySet'
ALTER TABLE [dbo].[GenPABonus_PolicySet]
ADD CONSTRAINT [FK_GenPABonus_PolicyGenPABonus_Assured]
    FOREIGN KEY ([GenPABonus_Assured_ItemId])
    REFERENCES [dbo].[GenPABonus_AssuredSet]
        ([ItemId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GenPABonus_PolicyGenPABonus_Assured'
CREATE INDEX [IX_FK_GenPABonus_PolicyGenPABonus_Assured]
ON [dbo].[GenPABonus_PolicySet]
    ([GenPABonus_Assured_ItemId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------