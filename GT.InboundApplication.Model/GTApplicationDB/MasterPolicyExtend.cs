﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Reflection;
using GT.InboundApplication.Model.Common;

namespace GT.InboundApplication.Model.GTApplicationDB
{
    public enum ApplicationStatuses
    {
        [Description("ทั้งหมด")]
        [LabelClass("default")]
        All=0,
        [Description("ร่าง")]
        [LabelClass("default")]
        Draft=1,
        [Description("รอ Underwrite")]
        [LabelClass("white")]
        WaitForUnderwrite = 2,
        [Description("รอตัดบัตร")]
        [LabelClass("white")]
        WaitForDCR=3,
        [Description("ตัดบัตรไม่ผ่าน")]
        [LabelClass("warning")]
        FailedDCR=4,
        [Description("รอ QC")]
        [LabelClass("white")]
        WaitForQC=5,
        [Description("ไม่ผ่าน QC")]
        [LabelClass("warning")]
        FailedQC=6,
        [Description("รอส่งงาน")]
        [LabelClass("info")]
        WaitForSubmit=7,
        [Description("รอออกกรมธรรม์")]
        [LabelClass("success")]
        WaitForPolicy=8,
        [Description("ไม่ผ่านการอนุมัติ")]
        [LabelClass("primary")]
        RejectPolicy=9,
        [Description("ออกกรมธรรม์เรียบร้อย")]
        [LabelClass("success")]
        Issued=10,
        [Description("ติด Memo")]
        [LabelClass("warning")]
        Memo=11,
        [Description("ยกเลิก")][LabelClass("default")]  
        Cancel=12,
        [Description("Refer")]
        [LabelClass("warning")]
        ReferUW = 13,
        [Description("ไม่ผ่าน Underwrite")]
        [LabelClass("default")]
        RejectUW = 14,
    }
    

    public class LabelClassAttribute : Attribute
    {
        internal LabelClassAttribute(string labelClass)
        {
            this.Style = labelClass;
        }
        public string Style { get; private set; }
    }

    public partial class MasterPolicy
    {
        public static class AutoUnderwriteStatuses
        {
            public const string Pass = "Pass";
            public const string NotPass = "NotPass";
            public const string Refer = "Refer";
            public const string Refer_Pass = "Refer_Pass";
            public const string Refer_Memo = "Refer_Memo";
            public const string Refer_NotPass = "Refer_NotPass";

            public static string GetDescription(string status)
            {
                switch(status)
                {
                    case AutoUnderwriteStatuses.Pass:
                        return "ผ่าน UW";
                    case AutoUnderwriteStatuses.NotPass:
                        return "ไม่ผ่าน UW";
                    case AutoUnderwriteStatuses.Refer:
                        return "Refer to UW";
                    case AutoUnderwriteStatuses.Refer_Pass:
                        return "Refer-ผ่าน";
                    case AutoUnderwriteStatuses.Refer_Memo:
                        return "Refer-Memo";
                    case AutoUnderwriteStatuses.Refer_NotPass:
                        return "Refer-ไม่ผ่าน";
                    default:
                        return "รอ UW";
                }
            }
            public static string GetLabelStyle(string status)
            {
                switch (status)
                {
                    case AutoUnderwriteStatuses.Pass:
                        return "success";
                    case AutoUnderwriteStatuses.NotPass:
                        return "danger";
                    case AutoUnderwriteStatuses.Refer:
                        return "warning";
                    case AutoUnderwriteStatuses.Refer_Pass:
                        return "success";
                    case AutoUnderwriteStatuses.Refer_Memo:
                        return "warning";
                    case AutoUnderwriteStatuses.Refer_NotPass:
                        return "danger";
                    default:
                        return "default";
                }
            }
        }

        [NotMapped]
        public String ProductDisplayName
        {
            get
            {
                if (POLICY_TYPE != null)
                {
                    switch (POLICY_TYPE.ToUpper())
                    {
                        case "TYPE1STD": return "Type 1";
                        case "TYPE1SGL": return "Type 1 Single Rate";
                        case "TYPE2P": return "Type 2+";
                        case "TYPE3P": return "Type 3";
                        default: return PRODUCTNAME;
                    }
                }
                return PRODUCTNAME;
            }
        }
        
        [NotMapped]
        public String AppStatusLabel
        {
            get
            {
                var labelClass = this.AppStatus.HasValue ? this.AppStatus.Value.GetAttribute<LabelClassAttribute>() : new LabelClassAttribute("default");
                return string.Format("<span class=\"label label-{1}\">{0}</span>", this.AppStatus.HasValue ? "" : "", labelClass.Style);
            }
        }
        [NotMapped]
        public String UWProcessStatusLabel
        {
            get
            {
                if (UWProcessStatus != null)
                {
                    switch (UWProcessStatus)
                    {
                        case "1": return "<span class=\"label label-warning\">เอกสารแจ้งงานไม่ถูกต้อง</span>";
                        case "2": return "<span class=\"label label-warning\">เบี้ยประกันภัยไม่ถูกต้อง</span>";
                        case "3": return "<span class=\"label label-info\">ระหว่างออกกรมธรรม์</span>";
                        case "4": return "<span class=\"label label-success\">ออกกรมธรรม์เรียบร้อย</span>";
                        case "5": return "<span class=\"label label-primary\">ยกเลิก</span>";
                        case "6": 
                            switch (InspectionStatus)
                            {
                                case "5": return "<span class=\"label label-warning\">แจ้งออกกรมธรรม์ล่วงหน้าเกิน 45 วัน (ได้รับรูปแล้ว)</span>";
                                case "6": return "<span class=\"label label-warning\">แจ้งออกกรมธรรม์ล่วงหน้าเกิน 45 วัน (รอส่งตรวจ)</span>";
                                default : return "<span class=\"label label-warning\">แจ้งออกกรมธรรม์ล่วงหน้าเกิน 45 วัน</span>";
                            }
                        case "7": return "<span class=\"label label-warning\">ติดเบี้ยประภันขั้นต่ำตามพิกัดฯ</span>";
                        case "8": return "<span class=\"label label-warning\">ปัญหาการตรวจสภาพรถ</span>";
                        case "9": return "<span class=\"label label-info\">รออนุมัติแผลเกิดก่อนการรับประกันภัย</span>";
                    }
                }
                else
                {
                    switch (InspectionStatus)
                    {
                        case "1": return "<span class=\"label label-info\">ผู้เอาประกันภัยถ่ายเอง (รอรูป)</span>";
                        case "2": return "<span class=\"label label-info\">รอนัดหมาย</span>";
                        case "3": return "<span class=\"label label-info\">รอผลตรวจสภาพรถยนต์</span>";
                        //case "4": return "<span class=\"label label-info\">รออนุมัติแผลเกิดก่อนการรับประกันภัย</span>";
                        case "5": return "<span class=\"label label-info\">ตรวจสภาพแล้ว</span>";
                        case "6": return "<span class=\"label label-info\">รอส่งตรวจ</span>";
                    }
                }
                return "<span class=\"label label-white\">งานใหม่</span>";
            }
        }
        [NotMapped]
        public String DeliveryStatusLabel
        {
            get
            {
                switch (DeliveryStatus)
                {
                    case "1": return "<span class=\"label label-info\">เตรียมส่งการเงิน</span>";
                    case "2": return "<span class=\"label label-info\">ส่งการเงิน</span>";
                    case "3": return "<span class=\"label label-warning\">ติดปัญหา</span>";
                    case "4": return "<span class=\"label label-warning\">รอเอกสาร</span>";
                    case "5": return "<span class=\"label label-warning\">ยกเลิก</span>";
                    case "6": return "<span class=\"label label-info\">เตรียมส่งไปรษณีย์</span>";
                    case "7": return "<span class=\"label label-success\">ส่งแล้ว</span>";
                    case "8": return "<span class=\"label label-warning\">เอกสารตีกลับจากไปรษณีย์</span>";
                    default: return "";
                }
            }
        }

        [NotMapped]
        public String UWProcessStatusDesc
        {
            get
            {
                return Regex.Replace(UWProcessStatusLabel, "<.*?>", String.Empty);
            }
        }
        [NotMapped]
        public String InspectionStatusLabel
        {
            get
            {
                    switch (InspectionStatus)
                    {
                        case "1": return "ผู้เอาประกันภัยถ่ายเอง (รอรูป)";
                        case "2": return "รอนัดหมาย";
                        case "3": return "รอผลตรวจสภาพรถยนต์";
                        //case "4": return "รออนุมัติแผลเกิดก่อนการรับประกันภัย";
                        case "5": return "ตรวจสภาพแล้ว";
                        case "6": return "รอส่งตรวจ";
                        default: return "";
                    }
            }
        }
        [NotMapped]
        public String AUWStatusDesc
        {
            get
            {
                return AutoUnderwriteStatuses.GetDescription(AUWStatusCode);
            }
        }

        [NotMapped]
        public String AUWStatusLabelStyle
        {
            get
            {
                return AutoUnderwriteStatuses.GetLabelStyle(AUWStatusCode);
            }
        }

        public void ClearAUWStatus()
        {
            this.AUWApproveCode = "";
            this.AUWProcessDate = null;
            this.AUWStatusCode = "";
        }
    }
}
