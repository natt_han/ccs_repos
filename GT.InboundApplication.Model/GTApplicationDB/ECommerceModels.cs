﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;


namespace GT.InboundApplication.Model.GTApplicationDB
{
    public class ECommerceModel
    {
        public class ECommerceDisplayApplicationClass
        {
            public int No { get; set; }
            public string PurchaseId { get; set; }
            public string ApplicationNo { get; set; }
            public string CustomerFullName { get; set; }
            public string CustomerAge { get; set; }
            public string ProductName { get; set; }
            public string ProductId { get; set; }
            public string PlanCode { get; set; }
            public string PlanName { get; set; }
            public string PlanNo { get; set; }
            public string InstallmentPremium { get; set; }
            public string API { get; set; }
            public string SalesResultMessage { get; set; }
            public string LeadStatus { get; set; }
            public string SalesResultCode { get; set; }
            public string AssignToWhom { get; set; }
            public string PurchasingFor { get; set; }
            public DateTime PurchaseDate { get; set; }
            public string PayerFullName { get; set; }
            public string PayPlan { get; set; }

            public string PhoneNumber { get; set; }
            public string OTPPhoneNumber { get; set; }
            public string EmailAddress { get; set; }

            // add new in 06/07/2017
            public string PaymentCode { get; set; }
            public string PaymentAmount { get; set; }
            public DateTime PaymentDate { get; set; }

            public string TSRID { get; set; }
            public string TSRNAME { get; set; }
        }

        public ECommerceDisplayApplicationClass _GetApplication(string connectionString, string purchaseid)
        {
            // Note : Get application by ID
            ECommerceDisplayApplicationClass app = new ECommerceDisplayApplicationClass();
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_purchaseid", purchaseid);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_GET_EAPP_BYPURCHASEID", param);

            if (ds.Tables[0].Rows.Count > 0)
            {
                app.FillByDataRow(ds.Tables[0].Rows[0]);
            }
            return app;
        }
        public List<ECommerceDisplayApplicationClass> _ListApplication(string connectionString, DateTime startdate, DateTime enddate)
        {
            // Note : List all application in range of date.
            List<ECommerceDisplayApplicationClass> apps = new List<ECommerceDisplayApplicationClass>();
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_startdate", startdate.ParseToDateString("yyyy-MM-dd"));
            param.Add("pr_enddate", enddate.ParseToDateString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_LIST_EAPP_FOR_ASSIGNMENT", param);
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ECommerceDisplayApplicationClass app = new ECommerceDisplayApplicationClass();
                    app.FillByDataRow<ECommerceDisplayApplicationClass>(dr);
                    apps.Add(app);
                }
            }
            return apps;
        }
        public List<ECommerceDisplayApplicationClass> _ListApplicationByUser(string connectionString, DateTime startdate, DateTime enddate, string userid)
        {
            // Note : List all application in range of date.
            List<ECommerceDisplayApplicationClass> apps = new List<ECommerceDisplayApplicationClass>();
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_startdate", startdate.ParseToDateString("yyyy-MM-dd"));
            param.Add("pr_enddate", enddate.ParseToDateString("yyyy-MM-dd"));
            param.Add("pr_userid", userid);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_LIST_EAPP_FOR_TSR", param);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ECommerceDisplayApplicationClass app = new ECommerceDisplayApplicationClass();
                    app.FillByDataRow<ECommerceDisplayApplicationClass>(dr);
                    apps.Add(app);
                }
            }
            return apps;
        }
        public int _AssignApplication(string connectionString, string receive_userid, string purchaseid, string assign_userid)
        {   
            // Note : Insert new assign user into e-commerce table.
            int retVal = 0;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_purchaseid", purchaseid);
            param.Add("pr_userid", receive_userid);
            param.Add("pr_assignuserid", assign_userid);
            retVal = ADONETHelper.ExecuteNonQuery(connectionString, "PROC_ECM_ASSIGN_EAPP_TO_TSR", param);
            return retVal;
        }
        public int _CreateNewApplication(string connectionString, string purchaseid, string applicationno, string userid, string productid)
        {  
            string returnVal = "";
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_purchaseid", purchaseid);
            param.Add("pr_applicationno", applicationno);
            param.Add("pr_userid", userid);
            param.Add("pr_productid", productid);
            var retVal = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_CREATE_APP_FROM_EAPP", param);
            var rowCount = retVal.Tables[0].Rows.Count;
            if(rowCount > 0)
            {
                returnVal = retVal.Tables[0].Rows[rowCount - 1][0].ToString();
            }
            switch(returnVal)
            {
                case "[SUCCESS]": return 1;
                case "[FAILED]": return 0;
                case "[IGNORE]": return -2;
                default: return -1;
            }
        }
        public int _CancelApplication(string connectionString, string purchaseid, string applicationno, string userid, string productid, out string returnmessage)
        {
            string returnVal = "";
            returnmessage = "";
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_purchaseid", purchaseid);
            param.Add("pr_applicationno", applicationno);
            var retVal = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_CANCEL_EAPP", param);
            var rowCount = retVal.Tables[0].Rows.Count;
            if (rowCount > 0)
            {
                returnVal = retVal.Tables[0].Rows[rowCount - 1][0].ToString();
            }
            if (rowCount > 1)
            {
                returnmessage = retVal.Tables[0].Rows[rowCount - 2][0].ToString();
            }
            switch (returnVal)
            {
                case "[SUCCESS]": return 1;
                case "[FAILED]": return 0;
                case "[IGNORE]": return -2;
                default: return -1;
            }
        }
        public DataTable _LoadCancellationReport(string connectionString, DateTime startdate, DateTime enddate)
        {
            // Note : List all application in range of date.
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", startdate.ParseToDateString("yyyy-MM-dd"));
            param.Add("PR_TODATE", enddate.ParseToDateString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_CANCELLATION_REPORT", param);
            return ds.Tables[0];
        }
        public int _RemoveApplication(string connectionString, string purchaseid, string assign_userid, out string returnmessage)
        {
            // Note : REMOVE LEAD FROM E-APPLICATION 
            string returnVal = "";
            returnmessage = "";
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_purchaseid", purchaseid);
            param.Add("pr_assignuserid", assign_userid);
            var retVal = ADONETHelper.ExecuteProcedure(connectionString, "PROC_ECM_REMOVE_EAPP", param);
            var rowCount = retVal.Tables[0].Rows.Count;
            if (rowCount > 0)
            {
                returnVal = retVal.Tables[0].Rows[rowCount - 1][0].ToString();
            }
            if (rowCount > 1)
            {
                returnmessage = retVal.Tables[0].Rows[rowCount - 2][0].ToString();
            }
            switch (returnVal)
            {
                case "[SUCCESS]": return 1;
                case "[FAILED]": return 0;
                case "[IGNORE]": return -2;
                default: return -1;
            }
        }
    }
}