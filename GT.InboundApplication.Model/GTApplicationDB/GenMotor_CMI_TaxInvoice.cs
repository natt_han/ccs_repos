//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GT.InboundApplication.Model.GTApplicationDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class GenMotor_CMI_TaxInvoice
    {
        public long ItemId { get; set; }
        public string Salutation { get; set; }
        public string SalutationCode { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string HomeNo { get; set; }
        public string Moo { get; set; }
        public string Village { get; set; }
        public string Building { get; set; }
        public string SubAlley { get; set; }
        public string Alley { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string District { get; set; }
        public string BranchDistrict { get; set; }
        public string Province { get; set; }
        public string PostCode { get; set; }
        public string SubDistrictId { get; set; }
        public string DistrictId { get; set; }
        public string BranchDistrictId { get; set; }
        public string ProvinceId { get; set; }
        public string ReceiptNo { get; set; }
        public Nullable<System.DateTime> ReceiptDate { get; set; }
    
        public virtual GenMotor_CMI_Policy GenMotor_CMI_Policy { get; set; }
    }
}
