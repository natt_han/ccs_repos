//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GT.InboundApplication.Model.GTApplicationDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class GenCancer_Policy
    {
        public GenCancer_Policy()
        {
            this.CRMActivityId = 0;
            this.GenCancer_DCR = new HashSet<GenCancer_DCR>();
        }
    
        public long ProposalId { get; set; }
        public string ProposalNo { get; set; }
        public string PolicyNo { get; set; }
        public Nullable<System.DateTime> InceptionDate { get; set; }
        public string InceptionTime { get; set; }
        public Nullable<System.DateTime> ProposalDate { get; set; }
        public Nullable<System.DateTime> PolicyExpiryDate { get; set; }
        public bool Active { get; set; }
        public Nullable<bool> Success { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> LeadSourceId { get; set; }
        public Nullable<long> CRMActivityId { get; set; }
        public string GroupPolicyNo { get; set; }
        public string ConsentFlag { get; set; }
        public string ConsentType { get; set; }
        public string ConsentDataSource { get; set; }
        public Nullable<System.DateTime> ConsentDate { get; set; }
        public string ConsentTime { get; set; }
        public string ConsentVersion { get; set; }
        public Nullable<int> ConsentId { get; set; }
        public Nullable<int> ConsentRevenueId { get; set; }
        public string ConsentRevenueFlag { get; set; }
        public string ConsentRevenueVersion { get; set; }
        public string GroupCompanyName { get; set; }
        public string ePolicyFlag { get; set; }
        public string ePolicyEmailAddress { get; set; }
        public string IsHealthCheck { get; set; }
        public Nullable<long> ProspectId { get; set; }
    
        public virtual GenCancer_Assured GenCancer_Assured { get; set; }
        public virtual GenCancer_AuditTrail GenCancer_AuditTrail { get; set; }
        public virtual GenCancer_Payer GenCancer_Payer { get; set; }
        public virtual GenCancer_Payment GenCancer_Payment { get; set; }
        public virtual GenCancer_Plan GenCancer_Plan { get; set; }
        public virtual GenCancer_UnderWriteInfo GenCancer_UnderWriteInfo { get; set; }
        public virtual ICollection<GenCancer_DCR> GenCancer_DCR { get; set; }
    }
}
