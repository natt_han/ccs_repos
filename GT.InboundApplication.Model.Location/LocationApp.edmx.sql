
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/07/2020 14:28:01
-- Generated from EDMX file: D:\sukasems\my_repos\sst_repos\GT.InboundApplication.Model.Location\LocationApp.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GTCRM_DB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_DistrictSubDistrict]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubDistrictSet] DROP CONSTRAINT [FK_DistrictSubDistrict];
GO
IF OBJECT_ID(N'[dbo].[FK_GeographyDistrict]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DistrictSet] DROP CONSTRAINT [FK_GeographyDistrict];
GO
IF OBJECT_ID(N'[dbo].[FK_ProvinceDistrict]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DistrictSet] DROP CONSTRAINT [FK_ProvinceDistrict];
GO
IF OBJECT_ID(N'[dbo].[FK_GeographyProvince]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProvinceSet] DROP CONSTRAINT [FK_GeographyProvince];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[DistrictSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DistrictSet];
GO
IF OBJECT_ID(N'[dbo].[GeographySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GeographySet];
GO
IF OBJECT_ID(N'[dbo].[ProvinceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProvinceSet];
GO
IF OBJECT_ID(N'[dbo].[SubDistrictSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubDistrictSet];
GO
IF OBJECT_ID(N'[dbo].[LegalScriptSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LegalScriptSet];
GO
IF OBJECT_ID(N'[dbo].[CallSourceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CallSourceSet];
GO
IF OBJECT_ID(N'[dbo].[ConsentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsentSet];
GO
IF OBJECT_ID(N'[dbo].[ConsentRevenueSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsentRevenueSet];
GO
IF OBJECT_ID(N'[dbo].[CreditCardIdentifierSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CreditCardIdentifierSet];
GO
IF OBJECT_ID(N'[dbo].[CreditCardBankNameSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CreditCardBankNameSet];
GO
IF OBJECT_ID(N'[dbo].[ConsentMappings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsentMappings];
GO
IF OBJECT_ID(N'[dbo].[CoverageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CoverageSet];
GO
IF OBJECT_ID(N'[dbo].[LifeOccupationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LifeOccupationSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'DistrictSet'
CREATE TABLE [dbo].[DistrictSet] (
    [DistrictId] int IDENTITY(1,1) NOT NULL,
    [ProvinceProvinceId] int  NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Name] nvarchar(300)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [GeographyGeographyId] int  NOT NULL
);
GO

-- Creating table 'GeographySet'
CREATE TABLE [dbo].[GeographySet] (
    [GeographyId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(300)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'ProvinceSet'
CREATE TABLE [dbo].[ProvinceSet] (
    [ProvinceId] int IDENTITY(1,1) NOT NULL,
    [GeographyGeographyId] int  NOT NULL,
    [Name] nvarchar(300)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Code] nvarchar(max)  NULL,
    [Abbreviation] nvarchar(max)  NULL
);
GO

-- Creating table 'SubDistrictSet'
CREATE TABLE [dbo].[SubDistrictSet] (
    [SubDistrictId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Name] nvarchar(300)  NOT NULL,
    [Description] nvarchar(500)  NOT NULL,
    [Note] nvarchar(500)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [ProvinceProvinceId] int  NOT NULL,
    [GeographyGeographyId] int  NOT NULL,
    [PostCode] nvarchar(max)  NULL,
    [DistrictDistrictId] int  NOT NULL
);
GO

-- Creating table 'LegalScriptSet'
CREATE TABLE [dbo].[LegalScriptSet] (
    [LegalScriptId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [HeaderParagraph] nvarchar(max)  NULL,
    [MainParagraph] nvarchar(max)  NULL,
    [BenefitParagraph] nvarchar(max)  NULL,
    [PaymentParagraph] nvarchar(max)  NOT NULL,
    [FooterParagraph] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [ProductProductId] int  NOT NULL,
    [ProductName] nvarchar(max)  NULL
);
GO

-- Creating table 'CallSourceSet'
CREATE TABLE [dbo].[CallSourceSet] (
    [CallSourceId] bigint IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [IsSupportInboundCall] bit  NOT NULL,
    [IsSupportOutboundCall] bit  NOT NULL,
    [IsSupportFollowupCall] bit  NOT NULL,
    [IsAdminLead] bit  NOT NULL,
    [MediaName] nvarchar(max)  NULL,
    [Seqn] int  NULL,
    [CallSourceType] nvarchar(max)  NULL,
    [CallSourceGroup] nvarchar(max)  NOT NULL,
    [ProjectName] nvarchar(max)  NULL,
    [AppName] nvarchar(50)  NULL,
    [SRC] nvarchar(10)  NULL,
    [MediaAgency] nvarchar(10)  NULL,
    [Owner] nvarchar(10)  NULL,
    [Intent] nvarchar(100)  NULL,
    [ProjectPriority] nvarchar(max)  NULL,
    [ProjectPriorityLife] nvarchar(max)  NULL,
    [CreditFor] nvarchar(20)  NULL,
    [IsReward] bit  NULL
);
GO

-- Creating table 'ConsentSet'
CREATE TABLE [dbo].[ConsentSet] (
    [ConsentId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [ConsentText] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [DataSource] nvarchar(max)  NOT NULL,
    [ConsentType] nvarchar(max)  NOT NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [Seqn] int  NULL
);
GO

-- Creating table 'ConsentRevenueSet'
CREATE TABLE [dbo].[ConsentRevenueSet] (
    [ConsentRevenueId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NULL,
    [ConsentText] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [ConsentVersion] nvarchar(max)  NULL,
    [Seqn] int  NULL
);
GO

-- Creating table 'CreditCardIdentifierSet'
CREATE TABLE [dbo].[CreditCardIdentifierSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [CreditCardPrefixNumber] nvarchar(max)  NOT NULL,
    [CreditCardType] nvarchar(max)  NOT NULL,
    [CreditCardTypeCode] nvarchar(max)  NOT NULL,
    [CreditCardClassType] nvarchar(max)  NOT NULL,
    [CreditCardClassTypeCode] nvarchar(max)  NOT NULL,
    [CreditCardBankName] nvarchar(max)  NOT NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [IsVerified] bit  NOT NULL
);
GO

-- Creating table 'CreditCardBankNameSet'
CREATE TABLE [dbo].[CreditCardBankNameSet] (
    [ItemId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL
);
GO

-- Creating table 'ConsentMappings'
CREATE TABLE [dbo].[ConsentMappings] (
    [ConsentMappingId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [AppName] nvarchar(max)  NOT NULL,
    [ConsentId] int  NULL,
    [IsActive] bit  NOT NULL,
    [ConsentTypeId] int  NOT NULL,
    [LeadSourceWording] nvarchar(max)  NULL
);
GO

-- Creating table 'CoverageSet'
CREATE TABLE [dbo].[CoverageSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ProductId] int  NOT NULL,
    [CoverageTitle] nvarchar(max)  NULL,
    [CoverageContent] nvarchar(max)  NULL,
    [Active] bit  NULL,
    [CreatedDate] datetime  NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL
);
GO

-- Creating table 'LifeOccupationSet'
CREATE TABLE [dbo].[LifeOccupationSet] (
    [LifeOccupationId] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Note] nvarchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [CreatedBy] nvarchar(max)  NULL,
    [CreatedDate] datetime  NULL,
    [UpdatedBy] nvarchar(max)  NULL,
    [UpdatedDate] datetime  NULL,
    [Seq] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [DistrictId] in table 'DistrictSet'
ALTER TABLE [dbo].[DistrictSet]
ADD CONSTRAINT [PK_DistrictSet]
    PRIMARY KEY CLUSTERED ([DistrictId] ASC);
GO

-- Creating primary key on [GeographyId] in table 'GeographySet'
ALTER TABLE [dbo].[GeographySet]
ADD CONSTRAINT [PK_GeographySet]
    PRIMARY KEY CLUSTERED ([GeographyId] ASC);
GO

-- Creating primary key on [ProvinceId] in table 'ProvinceSet'
ALTER TABLE [dbo].[ProvinceSet]
ADD CONSTRAINT [PK_ProvinceSet]
    PRIMARY KEY CLUSTERED ([ProvinceId] ASC);
GO

-- Creating primary key on [SubDistrictId] in table 'SubDistrictSet'
ALTER TABLE [dbo].[SubDistrictSet]
ADD CONSTRAINT [PK_SubDistrictSet]
    PRIMARY KEY CLUSTERED ([SubDistrictId] ASC);
GO

-- Creating primary key on [LegalScriptId] in table 'LegalScriptSet'
ALTER TABLE [dbo].[LegalScriptSet]
ADD CONSTRAINT [PK_LegalScriptSet]
    PRIMARY KEY CLUSTERED ([LegalScriptId] ASC);
GO

-- Creating primary key on [CallSourceId] in table 'CallSourceSet'
ALTER TABLE [dbo].[CallSourceSet]
ADD CONSTRAINT [PK_CallSourceSet]
    PRIMARY KEY CLUSTERED ([CallSourceId] ASC);
GO

-- Creating primary key on [ConsentId] in table 'ConsentSet'
ALTER TABLE [dbo].[ConsentSet]
ADD CONSTRAINT [PK_ConsentSet]
    PRIMARY KEY CLUSTERED ([ConsentId] ASC);
GO

-- Creating primary key on [ConsentRevenueId] in table 'ConsentRevenueSet'
ALTER TABLE [dbo].[ConsentRevenueSet]
ADD CONSTRAINT [PK_ConsentRevenueSet]
    PRIMARY KEY CLUSTERED ([ConsentRevenueId] ASC);
GO

-- Creating primary key on [ItemId] in table 'CreditCardIdentifierSet'
ALTER TABLE [dbo].[CreditCardIdentifierSet]
ADD CONSTRAINT [PK_CreditCardIdentifierSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ItemId] in table 'CreditCardBankNameSet'
ALTER TABLE [dbo].[CreditCardBankNameSet]
ADD CONSTRAINT [PK_CreditCardBankNameSet]
    PRIMARY KEY CLUSTERED ([ItemId] ASC);
GO

-- Creating primary key on [ConsentMappingId], [AppName], [IsActive], [ConsentTypeId] in table 'ConsentMappings'
ALTER TABLE [dbo].[ConsentMappings]
ADD CONSTRAINT [PK_ConsentMappings]
    PRIMARY KEY CLUSTERED ([ConsentMappingId], [AppName], [IsActive], [ConsentTypeId] ASC);
GO

-- Creating primary key on [Id] in table 'CoverageSet'
ALTER TABLE [dbo].[CoverageSet]
ADD CONSTRAINT [PK_CoverageSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LifeOccupationId] in table 'LifeOccupationSet'
ALTER TABLE [dbo].[LifeOccupationSet]
ADD CONSTRAINT [PK_LifeOccupationSet]
    PRIMARY KEY CLUSTERED ([LifeOccupationId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DistrictDistrictId] in table 'SubDistrictSet'
ALTER TABLE [dbo].[SubDistrictSet]
ADD CONSTRAINT [FK_DistrictSubDistrict]
    FOREIGN KEY ([DistrictDistrictId])
    REFERENCES [dbo].[DistrictSet]
        ([DistrictId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DistrictSubDistrict'
CREATE INDEX [IX_FK_DistrictSubDistrict]
ON [dbo].[SubDistrictSet]
    ([DistrictDistrictId]);
GO

-- Creating foreign key on [GeographyGeographyId] in table 'DistrictSet'
ALTER TABLE [dbo].[DistrictSet]
ADD CONSTRAINT [FK_GeographyDistrict]
    FOREIGN KEY ([GeographyGeographyId])
    REFERENCES [dbo].[GeographySet]
        ([GeographyId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GeographyDistrict'
CREATE INDEX [IX_FK_GeographyDistrict]
ON [dbo].[DistrictSet]
    ([GeographyGeographyId]);
GO

-- Creating foreign key on [ProvinceProvinceId] in table 'DistrictSet'
ALTER TABLE [dbo].[DistrictSet]
ADD CONSTRAINT [FK_ProvinceDistrict]
    FOREIGN KEY ([ProvinceProvinceId])
    REFERENCES [dbo].[ProvinceSet]
        ([ProvinceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProvinceDistrict'
CREATE INDEX [IX_FK_ProvinceDistrict]
ON [dbo].[DistrictSet]
    ([ProvinceProvinceId]);
GO

-- Creating foreign key on [GeographyGeographyId] in table 'ProvinceSet'
ALTER TABLE [dbo].[ProvinceSet]
ADD CONSTRAINT [FK_GeographyProvince]
    FOREIGN KEY ([GeographyGeographyId])
    REFERENCES [dbo].[GeographySet]
        ([GeographyId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GeographyProvince'
CREATE INDEX [IX_FK_GeographyProvince]
ON [dbo].[ProvinceSet]
    ([GeographyGeographyId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------