﻿using System.Collections;
using System.Data.Entity;
using System.Linq;
using DM.Utilities.NetFramework.Generics;


namespace GT.InboundApplication.Model.Location
{
    public static class LocationAppExtension
    {
        public static IEnumerable GetDistrictRelated(this DbSet<District> dbset, string provinceid)
        {
            var id = provinceid.ParseToPositiveInt();
            return from row in dbset.Where(row => row.ProvinceProvinceId == id).AsEnumerable()
                   select new
                   {
                       name = row.Name,
                       id = row.DistrictId
                   };
        }

        public static IEnumerable GetSubDistrictRelated(this DbSet<SubDistrict> dbset, string districtid)
        {
            var id = districtid.ParseToPositiveInt();
            return from row in dbset.Where(row => row.DistrictDistrictId == id).AsEnumerable()
                   select new
                   {
                       name = row.Name,
                       id = row.SubDistrictId,
                       postcode = row.PostCode
                   };
        }

        public static IQueryable<CallSource> ForProject(this DbSet<CallSource> dbSet, string projectName)
        {
            var results = from row in dbSet
                          where row.ProjectName == projectName || row.ProjectName == "ALL"
                          select row;
            return results;
        }

       
    }
}
