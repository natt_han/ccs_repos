﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using DM.Utilities.NetFramework.Excels.GT;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.GTApplicationDB;
using GT.CRMApplication.Helpers;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Identity;
using GT.CRMApplication.WebMVC5.Helpers;
using ClosedXML.Excel;
using System.Diagnostics;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;


namespace GT.CRMApplication.WebMVC5.Controllers
{
    public partial class ReportController
    {
        [HttpGet]
        public ActionResult ExportRPT_KPI_TSR_ALL(string startdate, string enddate)
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/PerformanceReport.xlsx";
                DataSet ds = _ReportModel.loadRPT_KPI_TSR_ALL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate());


                // open excel template and append it.
                ExcelWriter writer = new ExcelWriter();
                try
                {
                    writer.StartExcel(false);
                    writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                    int worksheetindex = 1;
                    int startrow = 8;
                    int startcol = 1;
                    //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);

                    int trow = startrow;

                    foreach (DataTable table in ds.Tables)
                    {
                        var suplist = table.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                        foreach (var sup in suplist)
                        {
                            var data = table.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());
                            foreach (DataRow row in data)
                            {
                                TimeSpan talkTime = new TimeSpan(0, 0, row["total_talk_time"].ToString().ParseToPositiveInt());
                                int col = startcol;
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["supervisorname"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["fullname"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_lead"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["old_lead"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_existing"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_used"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_call"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["call_attempt"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call_attempt"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["nocontact"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["followup"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["wait"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["inprogress"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["reject"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_update"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_target"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["success"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc_rate"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc_rate"].ToString());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["list_conv"].ToString().ParseToPositiveDouble());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["respond_rate"].ToString().ParseToPositiveDouble());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_respond_rate"].ToString().ParseToPositiveDouble());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit"].ToString().ParseToPositiveInt());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit"].ToString().ParseToPositiveDecimal());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved"].ToString().ParseToPositiveInt());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved"].ToString().ParseToPositiveDecimal());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["case_size"].ToString().ParseToPositiveDecimal());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_approved"].ToString().ParseToPositiveInt());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sumnew_approved"].ToString().ParseToPositiveDecimal());
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["lead_conv"].ToString().ParseToPositiveDouble());
                                trow++;
                            }
                            if (table.Rows.Count > 1)
                            {
                                int col = startcol + 1;
                                var total_lead = data.Sum(row => row.Field<int>("total_lead"));
                                var new_lead = data.Sum(row => row.Field<int>("new_lead"));
                                var total_used = data.Sum(row => row.Field<int>("total_used"));
                                var new_used = data.Sum(row => row.Field<int?>("new_used"));
                                var total_call = data.Sum(row => row.Field<int>("total_call"));
                                var total_talk_time = data.Sum(row => row.Field<int>("total_talk_time"));

                                TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
                                var new_lead_call = data.Sum(row => row.Field<int>("new_lead_call"));
                                var success = data.Sum(row => row.Field<int>("success"));
                                var new_success = data.Sum(row => row.Field<int?>("new_success"));
                                var dmc = data.Sum(row => row.Field<int?>("dmc"));
                                var new_dmc = data.Sum(row => row.Field<int?>("new_dmc"));
                                var approved = data.Sum(row => row.Field<int?>("approved"));
                                var sum_approved = data.Sum(row => row.Field<decimal?>("sum_approved"));
                                var new_approved = data.Sum(row => row.Field<int?>("new_approved"));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Total");
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("old_lead")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("new_existing")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("nocontact")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("followup")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("wait")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("inprogress")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("reject")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("not_update")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("not_target")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int>("success")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<int?>("submit")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<decimal?>("sum_submit")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), data.Sum(row => row.Field<decimal?>("sumnew_approved")));
                                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
                                trow++;
                            }
                            trow++;
                        }

                        if (suplist.Count() > 1)
                        {
                            int col = startcol + 1;
                            var dt = table.AsEnumerable();
                            var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
                            var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
                            var total_used = dt.Sum(row => row.Field<int>("total_used"));
                            var new_used = dt.Sum(row => row.Field<int?>("new_used"));
                            var total_call = dt.Sum(row => row.Field<int>("total_call"));
                            var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

                            TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
                            var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
                            var success = dt.Sum(row => row.Field<int>("success"));
                            var new_success = dt.Sum(row => row.Field<int?>("new_success"));
                            var dmc = dt.Sum(row => row.Field<int?>("dmc"));
                            var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
                            var approved = dt.Sum(row => row.Field<int?>("approved"));
                            var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
                            var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Project Total");
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
                        }
                        trow += 2;
                    }

                    var union = ds.Tables[0].AsEnumerable().Union(ds.Tables[1].AsEnumerable());
                    if (union.Count() > 1)
                    {
                        int col = startcol + 1;
                        var dt = union;//.AsEnumerable();
                        var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
                        var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
                        var total_used = dt.Sum(row => row.Field<int>("total_used"));
                        var new_used = dt.Sum(row => row.Field<int?>("new_used"));
                        var total_call = dt.Sum(row => row.Field<int>("total_call"));
                        var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

                        TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
                        var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
                        var success = dt.Sum(row => row.Field<int>("success"));
                        var new_success = dt.Sum(row => row.Field<int?>("new_success"));
                        var dmc = dt.Sum(row => row.Field<int?>("dmc"));
                        var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
                        var approved = dt.Sum(row => row.Field<int?>("approved"));
                        var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
                        var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Grand Total");
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
                    }


                    //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);
                    int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                    int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                    // update format painter.
                    ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                    ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                    writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                    string username = User.Identity.GetUserName();
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                    var project = AppConfigHelper.ProjectName();
                    //if (project == "IGM")
                    //{
                    //    writer.DeleteEntireColumn(worksheetindex, "AE1");
                    //    writer.DeleteEntireColumn(worksheetindex, "AD1");
                    //    writer.DeleteEntireColumn(worksheetindex, "AC1");
                    //}
                    string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                    string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                    if (_startdate == _enddate)
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate);
                    }
                    else
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                    }

                    // set password
                    writer.ProtectFile(password);

                    // save as file to the new directory.
                    string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                    tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                    writer.SaveFileAs(Server.MapPath(tempfilename));
                    writer.Close();
                    writer.ReleaseAllComObject();

                    // Delete Older Temporary File on server.
                    try
                    {
                        FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
                    }
                    catch { }
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    writer.Close();
                    writer.ReleaseAllComObject();
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    freecachedball();
                    return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult ExportRPT_KPI_TSR(string startdate, string enddate)
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/PerformanceReport.xlsx";
                DataTable datasource = null;
                if (User.IsTSR())
                {
                    datasource = _ReportModel.loadRPT_KPI_TSR(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
                }
                else if (User.IsInRole("SUPERVISOR"))
                {
                    datasource = _ReportModel.loadRPT_KPI_TSR(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, User.Identity.GetUserId());
                }
                else if (User.IsManager())
                {
                    datasource = _ReportModel.loadRPT_KPI_TSR(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, null);
                }

                // open excel template and append it.
                ExcelWriter writer = new ExcelWriter();
                try
                {
                    writer.StartExcel(false);
                    writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                    int worksheetindex = 1;
                    int startrow = 8;
                    int startcol = 1;
                    //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);

                    int trow = startrow;

                    var suplist = datasource.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                    foreach (var sup in suplist)
                    {
                        var data = datasource.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());
                        foreach (DataRow row in data)
                        {
                            TimeSpan talkTime = new TimeSpan(0, 0, row["total_talk_time"].ToString().ParseToPositiveInt());
                            int col = startcol;
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["supervisorname"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["fullname"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_lead"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["old_lead"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_existing"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_used"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_call"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["call_attempt"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call_attempt"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["nocontact"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["followup"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["wait"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["inprogress"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["reject"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_update"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_target"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["success"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc_rate"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc_rate"].ToString());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["list_conv"].ToString().ParseToPositiveDouble());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["respond_rate"].ToString().ParseToPositiveDouble());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_respond_rate"].ToString().ParseToPositiveDouble());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit"].ToString().ParseToPositiveInt());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit"].ToString().ParseToPositiveDecimal());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved"].ToString().ParseToPositiveInt());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved"].ToString().ParseToPositiveDecimal());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["case_size"].ToString().ParseToPositiveDecimal());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_approved"].ToString().ParseToPositiveInt());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sumnew_approved"].ToString().ParseToPositiveDecimal());
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["lead_conv"].ToString().ParseToPositiveDouble());
                            trow++;
                        }
                        if (datasource.Rows.Count > 1)
                        {
                            int col = startcol + 1;
                            var dt = data;
                            var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
                            var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
                            var total_used = dt.Sum(row => row.Field<int>("total_used"));
                            var new_used = dt.Sum(row => row.Field<int?>("new_used"));
                            var total_call = dt.Sum(row => row.Field<int>("total_call"));
                            var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

                            TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
                            var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
                            var success = dt.Sum(row => row.Field<int>("success"));
                            var new_success = dt.Sum(row => row.Field<int?>("new_success"));
                            var dmc = dt.Sum(row => row.Field<int?>("dmc"));
                            var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
                            var approved = dt.Sum(row => row.Field<int?>("approved"));
                            var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
                            var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Total");
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
                            trow++;
                        }
                        trow++;
                    }

                    if (suplist.Count() > 1)
                    {
                        int col = startcol + 1;
                        var dt = datasource.AsEnumerable();
                        var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
                        var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
                        var total_used = dt.Sum(row => row.Field<int>("total_used"));
                        var new_used = dt.Sum(row => row.Field<int?>("new_used"));
                        var total_call = dt.Sum(row => row.Field<int>("total_call"));
                        var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

                        TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
                        var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
                        var success = dt.Sum(row => row.Field<int>("success"));
                        var new_success = dt.Sum(row => row.Field<int?>("new_success"));
                        var dmc = dt.Sum(row => row.Field<int?>("dmc"));
                        var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
                        var approved = dt.Sum(row => row.Field<int?>("approved"));
                        var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
                        var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Grand Total");
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
                    }

                    //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);
                    int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                    int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                    // update format painter.
                    ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                    ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                    writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                    string username = User.Identity.GetUserName();
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                    var project = AppConfigHelper.ProjectName();
                    //if (project == "IGM")
                    //{
                    //    writer.DeleteEntireColumn(worksheetindex, "AE1");
                    //    writer.DeleteEntireColumn(worksheetindex, "AD1");
                    //    writer.DeleteEntireColumn(worksheetindex, "AC1");
                    //}
                    string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                    string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                    if (_startdate == _enddate)
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate);
                    }
                    else
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                    }

                    // set password
                    writer.ProtectFile(password);

                    // save as file to the new directory.
                    string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                    tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                    writer.SaveFileAs(Server.MapPath(tempfilename));
                    // Delete Older Temporary File on server.
                    try
                    {
                        FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
                    }
                    catch { }
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {

                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    freecachedb();
                    return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                finally
                {
                    writer.Close();
                    writer.ReleaseAllComObject();
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportRPT_TSR_KPIbyProductAll(string startdate, string enddate)
        {
            if (ProjectSetupModel.GetProjectType() == "NL")
            {
                return ExportRPT_KPI_TSR_NL_ALL(startdate, enddate);
            }
            else
            {
                return ExportRPT_KPI_TSR_OL_ALL(startdate, enddate);
            }
        }

        [HttpGet]
        public ActionResult ExportRPT_KPI_TSR_NL_ALL(string startdate, string enddate)
        {
            ExcelWriter writer = new ExcelWriter();
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/KPIbyTSR_NL.xlsx";
                DataSet ds = _ReportModel.loadRPT_TSR_KPIbyProductNLAll(connectionstring, startdate.ParseToDate(), enddate.ParseToDate());


                // open excel template and append it.                
                writer.StartExcel(false);
                writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                int worksheetindex = 1;
                int startrow = 8;
                int startcol = 1;

                int trow = startrow;

                foreach (DataTable table in ds.Tables)
                {
                    var suplist = table.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                    foreach (var sup in suplist)
                    {
                        var data = table.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());                        
                        trow = WriteDetailNL(writer, worksheetindex, startcol, trow, data);
                        
                        if (table.Rows.Count > 1)
                        {
                            WriteSummaryRowNL(data, writer, worksheetindex, startcol, trow);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(startcol + 1, trow), "Total");
                            trow++;
                        }
                        trow++;
                    }

                    if (suplist.Count() > 1)
                    {
                        int col = startcol + 1;
                        WriteSummaryRowNL(table.AsEnumerable(), writer, worksheetindex, startcol, trow);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(startcol + 1, trow), "Project Total");
                    }
                    trow += 2;
                }

                var union = ds.Tables[0].AsEnumerable().Union(ds.Tables[1].AsEnumerable());
                if (union.Count() > 1)
                {
                    WriteSummaryRowNL(union.AsEnumerable(), writer, worksheetindex, startcol, trow);
                }


                //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);
                int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                // update format painter.
                ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                string username = User.Identity.GetUserName();
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                var project = AppConfigHelper.ProjectName();
                string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                if (_startdate == _enddate)
                {
                    writer.RenameWorkSheet(worksheetindex, _startdate);
                }
                else
                {
                    writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                }

                // set password
                writer.ProtectFile(password);

                // save as file to the new directory.
                string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                writer.SaveFileAs(Server.MapPath(tempfilename));

                return Json(tempfilename, JsonRequestBehavior.AllowGet);           
            }
            catch (Exception e)
            {
                freecachedball();
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }finally{
                writer.Close();
                writer.ReleaseAllComObject();
            }
        }

        [HttpGet]
        public ActionResult ExportRPT_KPI_TSR_OL_ALL(string startdate, string enddate)
        {
            ExcelWriter writer = new ExcelWriter();
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/KPIbyTSR_OL.xlsx";
                DataSet ds = _ReportModel.loadRPT_TSR_KPIbyProductOLAll(connectionstring, startdate.ParseToDate(), enddate.ParseToDate());

                // open excel template and append it.                
                
                writer.StartExcel(false);
                writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                int worksheetindex = 1;
                int startrow = 8;
                int startcol = 1;

                int trow = startrow;

                foreach (DataTable table in ds.Tables)
                {
                    var suplist = table.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                    foreach (var sup in suplist)
                    {
                        var data = table.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());
                        trow = WriteDetailOL(writer, worksheetindex, startcol, trow, data);
                        if (table.Rows.Count > 1)
                        {
                            WriteSummaryRowOL(data, writer, worksheetindex, startcol, trow);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(startcol + 1, trow), "Total");
                            trow++;
                        }
                        trow++;
                    }

                    if (suplist.Count() > 1)
                    {
                        int col = startcol + 1;
                        WriteSummaryRowOL(table.AsEnumerable(), writer, worksheetindex, startcol, trow);
                        writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col, trow), "Project Total");
                    }
                    trow += 2;
                }

                var union = ds.Tables[0].AsEnumerable().Union(ds.Tables[1].AsEnumerable());
                if (union.Count() > 1)
                {
                    WriteSummaryRowOL(union.AsEnumerable(), writer, worksheetindex, startcol, trow);
                }


                //writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);
                int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                // update format painter.
                ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                string username = User.Identity.GetUserName();
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                var project = AppConfigHelper.ProjectName();
                string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                if (_startdate == _enddate)
                {
                    writer.RenameWorkSheet(worksheetindex, _startdate);
                }
                else
                {
                    writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                }

                // set password
                writer.ProtectFile(password);

                // save as file to the new directory.
                string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                writer.SaveFileAs(Server.MapPath(tempfilename));

                return Json(tempfilename, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                freecachedball();
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                writer.Close();
                writer.ReleaseAllComObject();
            }
        }

        public ActionResult ExportRPT_TSR_KPIbyProduct(string startdate, string enddate)
        {
            if (ProjectSetupModel.GetProjectType() == "NL")
            {
                return ExportRPT_KPI_TSR_NL(startdate, enddate);
            }
            else
            {
                return ExportRPT_KPI_TSR_OL(startdate, enddate);
            }
        }
                
        public ActionResult ExportRPT_KPI_TSR_NL(string startdate, string enddate)
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/KPIbyTSR_NL.xlsx";
                DataTable datasource = null;
                if (User.IsTSR())
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductNL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
                }
                else if (User.IsInRole("SUPERVISOR"))
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductNL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, User.Identity.GetUserId());
                }
                else if (User.IsManager())
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductNL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, null);
                }

                // open excel template and append it.
                ExcelWriter writer = new ExcelWriter();
                try
                {
                    writer.StartExcel(false);
                    writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                    int worksheetindex = 1;
                    int startrow = 8;
                    int startcol = 1;

                    int trow = startrow;

                    var suplist = datasource.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                    foreach (var sup in suplist)
                    {
                        var data = datasource.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());
                        trow = WriteDetailNL(writer, worksheetindex, startcol, trow, data);

                        if (datasource.Rows.Count > 1)
                        {
                            //Sup Total
                            WriteSummaryRowNL(data, writer, worksheetindex, startcol, trow);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(startcol + 1, trow), "Total");
                            trow++;
                        }
                        trow++;
                    }

                    if (suplist.Count() > 1)
                    {
                        //Grand Total
                        WriteSummaryRowNL(datasource.AsEnumerable(), writer, worksheetindex, startcol, trow);
                    }

                    int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                    int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                    // update format painter.
                    ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                    ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                    writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                    string username = User.Identity.GetUserName();
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                    var project = AppConfigHelper.ProjectName();

                    string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                    string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                    if (_startdate == _enddate)
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate);
                    }
                    else
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                    }

                    // set password
                    writer.ProtectFile(password);

                    // save as file to the new directory.
                    string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                    tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                    writer.SaveFileAs(Server.MapPath(tempfilename));
                    // Delete Older Temporary File on server.
                    try
                    {
                        FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
                    }
                    catch { }
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {

                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    freecachedb();
                    return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                finally
                {
                    writer.Close();
                    writer.ReleaseAllComObject();
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }

            
        }

        private int WriteDetailNL(ExcelWriter writer, int worksheetindex, int startcol, int trow, EnumerableRowCollection<DataRow> data)
        {
            foreach (DataRow row in data)
            {
                TimeSpan talkTime = new TimeSpan(0, 0, row["total_talk_time"].ToString().ParseToPositiveInt());
                int col = startcol;
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["supervisorname"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["fullname"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_hls"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_cancer"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_hlsbundle"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["old_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_existing"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_used"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_call"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["call_attempt"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call_attempt"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["nocontact"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["followup"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["wait"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["inprogress"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["reject"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_update"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_target"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["success"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc_rate"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc_rate"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["list_conv"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["respond_rate"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_respond_rate"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_hls"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_hls"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_hls_ipd"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_hls_ipd"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_hls_iopd"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_hls_iopd"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_cancer"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_cancer"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_hls"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_hls"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_hls_ipd"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_hls_ipd"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_hls_iopd"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_hls_iopd"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_cancer"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_cancer"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["case_size"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_approved"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sumnew_approved"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["lead_conv"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_hls"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_hls"].ToString().ParseToPositiveInt() + row["new_lead_hlsbundle"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_hls_ipd"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_hls"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_hls_iopd"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_hls"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_cancer"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_cancer"].ToString().ParseToPositiveInt()));
                trow++;
            }

            return trow;
        }

        private void WriteSummaryRowNL(IEnumerable<DataRow> data, ExcelWriter writer, int worksheetindex, int startcol, int trow)
        {
            int col = startcol + 1;
            var dt = data;
            var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
            var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
            var new_lead_hls = dt.Sum(row => row.Field<int>("new_lead_hls"));
            var new_lead_cancer = dt.Sum(row => row.Field<int>("new_lead_cancer"));
            var new_lead_hlsbundle = dt.Sum(row => row.Field<int>("new_lead_hlsbundle"));
            var total_used = dt.Sum(row => row.Field<int>("total_used"));
            var new_used = dt.Sum(row => row.Field<int?>("new_used"));
            var total_call = dt.Sum(row => row.Field<int>("total_call"));
            var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

            TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
            var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
            var success = dt.Sum(row => row.Field<int>("success"));
            var new_success = dt.Sum(row => row.Field<int?>("new_success"));
            var dmc = dt.Sum(row => row.Field<int?>("dmc"));
            var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
            var approved = dt.Sum(row => row.Field<int?>("approved"));
            var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
            var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Grand Total");
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_hls);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_cancer);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_hlsbundle);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_hls")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_hls")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_hls_ipd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_hls_ipd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_hls_iopd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_hls_iopd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_cancer")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_cancer")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_hls")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_hls")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_hls_ipd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_hls_ipd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_hls_iopd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_hls_iopd")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_cancer")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_cancer")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_hls")) * 100.0, new_lead_hls + new_lead_hlsbundle));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_hls_ipd")) * 100.0, new_lead_hls + new_lead_hlsbundle));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_hls_iopd")) * 100.0, new_lead_hls + new_lead_hlsbundle));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_cancer")) * 100.0, new_lead_cancer));
        }

        public ActionResult ExportRPT_KPI_TSR_OL(string startdate, string enddate)
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/KPIbyTSR_OL.xlsx";
                DataTable datasource = null;
                if (User.IsTSR())
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductOL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
                }
                else if (User.IsInRole("SUPERVISOR"))
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductOL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, User.Identity.GetUserId());
                }
                else if (User.IsManager())
                {
                    datasource = _ReportModel.loadRPT_TSR_KPIbyProductOL(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, null);
                }

                // open excel template and append it.
                ExcelWriter writer = new ExcelWriter();
                try
                {
                    writer.StartExcel(false);
                    writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                    int worksheetindex = 1;
                    int startrow = 8;
                    int startcol = 1;

                    int trow = startrow;

                    var suplist = datasource.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
                    foreach (var sup in suplist)
                    {
                        var data = datasource.AsEnumerable().Where(c => sup["SupervisorName"].ToString() == c["SupervisorName"].ToString());
                        trow = WriteDetailOL(writer, worksheetindex, startcol, trow, data);

                        if (datasource.Rows.Count > 1)
                        {
                            //Sup Total
                            WriteSummaryRowOL(data, writer, worksheetindex, startcol, trow);
                            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(startcol + 1, trow), "Total");
                            trow++;
                        }
                        trow++;
                    }

                    if (suplist.Count() > 1)
                    {
                        //Grand Total
                        WriteSummaryRowOL(datasource.AsEnumerable(), writer, worksheetindex, startcol, trow);
                    }

                    int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                    int lastrow = writer.GetUsedRangeRow(worksheetindex) + 1;

                    // update format painter.
                    ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                    ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                    writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                    string username = User.Identity.GetUserName();
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A1"), "รายงาน KPI by TSR");
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A2"), "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A3"), "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate);
                    writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell("A4"), "ผู้ออกรายงาน " + username);

                    var project = AppConfigHelper.ProjectName();

                    string _startdate = startdate.ParseToDate().ToString("dd-MM-yy");
                    string _enddate = enddate.ParseToDate().ToString("dd-MM-yy");

                    if (_startdate == _enddate)
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate);
                    }
                    else
                    {
                        writer.RenameWorkSheet(worksheetindex, _startdate + "-" + _enddate);
                    }

                    // set password
                    writer.ProtectFile(password);

                    // save as file to the new directory.
                    string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                    tempfilename = Url.Content("~/Downloads/") + string.Format("sst-kpibytsrreport_{0}.xlsx", fileidentity);

                    writer.SaveFileAs(Server.MapPath(tempfilename));
                    // Delete Older Temporary File on server.
                    try
                    {
                        FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
                    }
                    catch { }
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {

                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    freecachedb();
                    return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                finally
                {
                    writer.Close();
                    writer.ReleaseAllComObject();
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private int WriteDetailOL(ExcelWriter writer, int worksheetindex, int startcol, int trow, EnumerableRowCollection<DataRow> data)
        {
            foreach (DataRow row in data)
            {
                TimeSpan talkTime = new TimeSpan(0, 0, row["total_talk_time"].ToString().ParseToPositiveInt());
                int col = startcol;
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["supervisorname"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["fullname"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_gccl"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_gs55"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_gpab"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_gpl8"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["old_lead"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_existing"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_used"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["total_call"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["call_attempt"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_lead_call_attempt"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["nocontact"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["followup"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["wait"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["inprogress"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["reject"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_update"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["not_target"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["success"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["dmc_rate"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_dmc_rate"].ToString());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["list_conv"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["respond_rate"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_respond_rate"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_gccl"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_gccl"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_gs55"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_gs55"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_gpab"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_gpab"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["submit_gpl8"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_submit_gpl8"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_gccl"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_gccl"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_gs55"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_gs55"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_gpab"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_gpab"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["approved_gpl8"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sum_approved_gpl8"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["case_size"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["new_approved"].ToString().ParseToPositiveInt());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["sumnew_approved"].ToString().ParseToPositiveDecimal());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), row["lead_conv"].ToString().ParseToPositiveDouble());
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_gccl"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_gccl"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_gs55"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_gs55"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_gpab"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_gpab"].ToString().ParseToPositiveInt()));
                writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(row["new_approved_gpl8"].ToString().ParseToPositiveInt() * 100.0, row["new_lead_gpl8"].ToString().ParseToPositiveInt()));
                trow++;
            }

            return trow;
        }

        private void WriteSummaryRowOL(IEnumerable<DataRow> data, ExcelWriter writer, int worksheetindex, int startcol, int trow)
        {
            int col = startcol + 1;
            var dt = data;
            var total_lead = dt.Sum(row => row.Field<int>("total_lead"));
            var new_lead = dt.Sum(row => row.Field<int>("new_lead"));
            var new_lead_gccl = dt.Sum(row => row.Field<int>("new_lead_gccl"));
            var new_lead_gs55 = dt.Sum(row => row.Field<int>("new_lead_gs55"));
            var new_lead_gpab = dt.Sum(row => row.Field<int>("new_lead_gpab"));
            var new_lead_gpl8 = dt.Sum(row => row.Field<int>("new_lead_gpl8"));
            var total_used = dt.Sum(row => row.Field<int>("total_used"));
            var new_used = dt.Sum(row => row.Field<int?>("new_used"));
            var total_call = dt.Sum(row => row.Field<int>("total_call"));
            var total_talk_time = dt.Sum(row => row.Field<int>("total_talk_time"));

            TimeSpan talkTime = new TimeSpan(0, 0, total_talk_time);
            var new_lead_call = dt.Sum(row => row.Field<int>("new_lead_call"));
            var success = dt.Sum(row => row.Field<int>("success"));
            var new_success = dt.Sum(row => row.Field<int?>("new_success"));
            var dmc = dt.Sum(row => row.Field<int?>("dmc"));
            var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc"));
            var approved = dt.Sum(row => row.Field<int?>("approved"));
            var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved"));
            var new_approved = dt.Sum(row => row.Field<int?>("new_approved"));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), "Grand Total");
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_lead);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_gccl);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_gs55);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_gpab);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_gpl8);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("old_lead")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("new_existing")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_used);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), total_call);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), string.Format("{0}:{1}", Math.Floor(talkTime.TotalHours).ToString().PadLeft(2, '0'), talkTime.Minutes.ToString().PadLeft(2, '0'), talkTime.Seconds.ToString().PadLeft(2, '0')));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(total_call, total_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_lead_call);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_lead_call, new_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("nocontact")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("followup")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("wait")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("inprogress")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("reject")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_update")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("not_target")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int>("success")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dmc);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_dmc);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dmc * 100.0, total_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_dmc * 100.0, new_used));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(approved * 100.0, total_lead));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(success * 100.0, dmc));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_success * 100.0, new_dmc));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_gccl")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_gccl")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_gs55")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_gs55")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_gpab")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_gpab")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("submit_gpl8")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_submit_gpl8")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), sum_approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_gccl")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_gccl")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_gs55")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_gs55")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_gpab")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_gpab")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<int?>("approved_gpl8")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sum_approved_gpl8")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(sum_approved, approved));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), new_approved);
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), dt.Sum(row => row.Field<decimal?>("sumnew_approved")));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(new_approved * 100.0, new_lead));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_gccl")) * 100.0, new_lead_gccl));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_gs55")) * 100.0, new_lead_gs55));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_gpab")) * 100.0, new_lead_gpab));
            writer.WriteCellData(worksheetindex, new ExcelWriter.ExcelCell(col++, trow), SolutionHelper.divide(dt.Sum(row => row.Field<int?>("new_approved_gpl8")) * 100.0, new_lead_gpl8));
        }
    }
}