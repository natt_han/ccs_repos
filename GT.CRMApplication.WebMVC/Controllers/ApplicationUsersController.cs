﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.GTApplicationDB;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public class ApplicationUsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /ApplicationUsers/
        public ActionResult Index()
        {
            ViewBag.Roles = db.Roles.ToList();
            List<ApplicationUser> data;
            if(User.IsSuperAdmin()){
                data = db.Users.Where(item => item.IsBlocked == false && item.IsVerified == true).ToList();
            }
            else
            {
                //hidden non-sales team user from sales team
                var salesteamroles = new string[]{"TSR", "SUPERVISOR", "QC", "ADMIN", "PM","INBOUNDHEAD" };
                var salesroles = db.Roles.Where(c => salesteamroles.Contains(c.Name)).Select(s => s.Id).ToList();
                data = db.Users.Where(item => item.IsBlocked == false && item.IsVerified == true && item.Roles.Any(ic=>salesroles.Contains(ic.RoleId)) ).ToList();
            }
            return View(data);
        }

        // GET: /ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            var selectedrole = db.Roles.Find(applicationUser.Roles.FirstOrDefault().RoleId);
            ViewBag.UserRoleName = selectedrole.Name;
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: /ApplicationUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            var selectedrole = db.Roles.Find(applicationUser.Roles.FirstOrDefault().RoleId);
            var salesteamroles = new string[] { "TSR", "SUPERVISOR", "QC", "ADMIN", "PM", };
            var salesroles = db.Roles.Where(c => salesteamroles.Contains(c.Name));
            var result = db.Roles.ToList();
            if (!User.IsSuperAdmin())
            {
                result = salesroles.ToList();
            }
            ViewBag.Roles = new SelectList(result, "Id", "Name", selectedrole.Id);
            ViewBag.Supervisors = new SelectList(db.GetAdminSelectList(), "Id", "Name",applicationUser.SupervisorId);
            
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        
        // POST: /ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(ApplicationUser applicationUser, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                applicationUser.FillInModelWithDefaultValue();

                var user = db.Users.Find(applicationUser.Id);
                user.DataUserName = applicationUser.DataUserName;
                user.UserName = applicationUser.DataUserName;
                
                user.FirstName = applicationUser.FirstName;
                user.LastName = applicationUser.LastName;
                user.NickName = applicationUser.NickName;
                user.Email = applicationUser.Email;
                user.SupervisorId = applicationUser.SupervisorId;
                user.SupervisorName = applicationUser.SupervisorName;
                user.ExtensionCode = applicationUser.ExtensionCode;

                var selectedrole  = form["UserRole"].ToString();
                var userrole = user.Roles.FirstOrDefault();
                if (userrole.RoleId != selectedrole)
                {
                    user.Roles.Remove(userrole);

                    var newuserrole = new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole();
                    newuserrole.RoleId = selectedrole;
                    newuserrole.UserId = user.Id;

                    user.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole() { RoleId = selectedrole, UserId = user.Id });
                }

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationUser);
        }

        // GET: /ApplicationUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            var selectedrole = db.Roles.Find(applicationUser.Roles.FirstOrDefault().RoleId);
            ViewBag.UserRoleName = selectedrole.Name;
            if (applicationUser == null)
            {
                return HttpNotFound();
            }            
            return View(applicationUser);
        }

        // POST: /ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            var user = db.Users.Find(id);
            user.IsBlocked = true;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
