using GT.CRMApplication.WebMVC5.Helpers;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.GTApplicationDB;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GT.CRMApplication.WebMVC5.Controllers
{

    [Authorize]
    public class ProspectController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        private enum Source
        {
            None = 0,
            LeadAssignment = 1,
            ECommerce = 2
        }

        public ProspectController()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            db.Database.Log = sql => System.Diagnostics.Debug.Write(sql);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        public long LeadAssignmentId
        {
            get
            {
                var laid = Session["LeadAssignmentId"];
                if (laid != null)
                {
                    return Convert.ToInt64(laid);
                }
                return 0;
            }
            set
            {
                Session["LeadAssignmentId"] = value;
            }
        }

        private Source ActivitySource
        {
            get
            {
                var s = Session["Prospect/ActivitySource"];
                if (s != null) return (Source)s;
                return Source.None;
            }
            set
            {
                Session["Prospect/ActivitySource"] = value;
            }
        }

        private long? ProspectId
        {
            get
            {
                var s = Session["Prospect/ProspectId"];
                if (s != null) return (long)s;
                return null;
            }
            set
            {
                Session["Prospect/ProspectId"] = value;
            }
        }

        private string CallFrom
        {
            get
            {
                var s = Session["Prospect/CallFrom"];
                if (s != null) return (string)s;
                return null;
            }
            set
            {
                Session["Prospect/CallFrom"] = value;
            }
        }

        #region GET

        public ActionResult Index()
        {
            ViewBag.ResponseCategory = new SelectList(db.ResponseCategorySet.ToList(), "ResponseCategoryId", "Name");
            ViewBag.TotalProspect = db.Prospect_ProfileSet.Count();

            //var callsource = this._GetCallSource();
            //ViewBag.CallSource = new SelectList(callsource, "name", "name");
            _LoadCallSource();
            //_LoadResponse();

            return View();
        }

        public ActionResult Search()
        {
            ViewBag.ResponseCategory = new SelectList(db.ResponseCategorySet.ToList(), "ResponseCategoryId", "Name");
            ViewBag.TotalProspect = db.Prospect_ProfileSet.Count();

            _LoadCallSource();
            _LoadResponse();
            ViewBag.OfferProduct = new SelectList(db.OfferProductSet.Where(x => x.Active.Value).ToList(), "OfferProductId", "Name");

            return View();
        }

        public ActionResult CreateProfile()
        {
            Prospect_Profile newprofile = new Prospect_Profile();
            this._PopulateProfile();
            return View(newprofile);
        }

        public ActionResult EditProfile(int? id)
        {
            if (id.IsNullOrEmpty())
            {
                return HttpNotFound("Id is Null.");
            }
            Prospect_Profile profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }
            this._PopulateProfile(profile);
            return View(profile);
        }

        public ActionResult Detail(long id)
        {
            var profile = db.Prospect_ProfileSet.SingleOrDefault(c => c.ProspectId == id);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }
            var lead = db.LeadProfileSet.ActiveSet().FirstOrDefault(c => c.ProspectId == profile.ProspectId);
            if (lead != null)
            {
                ViewBag.LeadProfile = db.LeadProfileSet.ActiveSet().FirstOrDefault(c => c.ProspectId == profile.ProspectId);                
                if (profile.Prospect_Activity.Any())
                {
                    using (var appdb = new ApplicationDbContext())
                    {
                        ViewBag.User = appdb.Users.Find(profile.Prospect_Activity.OrderByDescending(o => o.CreatedDate).First().CreatedBy);
                    }
                }
            }
            ConsentHelper.GetConsent(ViewBag, profile);
            return View("IncomingCall", "_EmptyLayout", profile);
            //return PartialView("IncomingCall", profile);
        }
        
        private Prospect_Activity prepareCreateActivity(long? id,string callfrom,out ActionResult result)
        {
            if (id.IsNullOrEmpty())
            {
                result = HttpNotFound("Id is Null.");
                return null;
            }
            Prospect_Profile profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                result = HttpNotFound("Profile Not Found!");
                return null;
            }
            callfrom = getCallFrom(id, callfrom);
            Prospect_Activity newactivity = new Prospect_Activity()
            {
                Prospect_Profile = profile,
                CallTime = DateTime.Now.ToString("HH:mm"),
                CallFrom = callfrom
            };

            ViewBag.RunScript = "AfterScript();";
            this._PopulateProfileActivity();
            //if (!User.IsSuperAdmin())
            {
                var noassignment = _CheckAssignment(newactivity);
                if (noassignment != null)
                {
                    result = noassignment;
                    return null;
                }
            }
            var leadProfile = db.LeadAssignmentSet.Find(newactivity.LeadAssignmentId).LeadProfile;
            ViewBag.LeadProfile = leadProfile;
                        
            using (var appdb = new ApplicationDbContext())
            {
                ViewBag.User = appdb.Users.Find(User.Identity.GetUserId());
            }
            ConsentHelper.GetConsent(ViewBag, newactivity.Prospect_Profile);
            result = null;
            return newactivity;
        }
        

        public ActionResult CreateActivity(long? id,string callfrom=""){
            ActionResult result;
            Prospect_Activity newactivity = prepareCreateActivity(id, "Application", out result);
            if (result != null) return result;
            return View(newactivity);
        }

       

        public ActionResult DeleteActivity(int? id, int? activitycode)
        {
            var act = db.Prospect_ActivitySet.Find(activitycode);
            return View(act);
        }

        [HttpDelete]
        public ActionResult DeleteActivity(Prospect_Activity entity)
        {
            var act = db.Prospect_ActivitySet.Find(entity.ItemId);
            if (act == null)
            {
                return RedirectToAction("MainProfile", new { id = entity.Prospect_ProfileProspectId, message = "Activity is not found." });
            }
            var profileid = act.Prospect_ProfileProspectId;
            if (!act.AppointmentId.IsNullOrEmpty())
            {
                var appointment = db.Prospect_AppointmentSet.Find(act.AppointmentId);
                if (appointment != null)
                {
                    if (db.Prospect_ActivitySet.Any(c => c.ItemId > appointment.Prospect_Activity.ItemId))
                    {
                        appointment.IsReply = false;
                    }
                }
            }
            //var profile = act.Prospect_Profile;
            //profile.Prospect_Activity.Remove(act);
            if (act.Prospect_Appointment != null) db.Entry(act.Prospect_Appointment).State = EntityState.Deleted;
            db.Entry(act).State = EntityState.Deleted;
            db.SaveChanges();
            return RedirectToAction("MainProfile", new { @id = profileid, message = "Delete Success" });
        }

        public ActionResult CreateFollowUpActivity(long? id, int appointmentcode)
        {
            string connectionString = db.Database.Connection.ConnectionString;
            var prospectid = id;
            if (prospectid.IsNullOrEmpty())
            {
                return HttpNotFound("Id is Null.");
            }
            Session.Remove("Appointment");
            Prospect_Profile profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId == prospectid.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Specific Profile is Not Found!");
            }

            getCallFrom(id, CRMExtension.CallFrom.Appointment);
            Prospect_Activity newactivity = new Prospect_Activity()
            {
                Prospect_Profile = profile,
                AppointmentId = appointmentcode,
                CallTime = DateTime.Now.ToString("HH:mm"),
                CallType = "FLWUP",
                CallTypeCode = "FLWUP",
                CallFrom = CRMExtension.CallFrom.Appointment
            };

            // update appointment change (incase follow-up)
            if (!appointmentcode.IsNullOrEmpty() && appointmentcode > 0)
            {
                var appointment = db.Prospect_AppointmentSet.SingleOrDefault(row => row.ItemId == newactivity.AppointmentId.Value);
                appointment.IsRead = true;


                LeadAssignmentId = appointment.Prospect_Activity.LeadAssignmentId.HasValue ? appointment.Prospect_Activity.LeadAssignmentId.Value : 0;
                if (LeadAssignmentId > 0) this.ActivitySource = Source.LeadAssignment;
                else this.ActivitySource = Source.None;
                if (LeadAssignmentId > 0 && !db.LeadAssignmentSet.Any(c => c.LeadAssignmentId == LeadAssignmentId && c.UnAssignDate == null))
                {
                    appointment.IsCancel = true;
                    appointment.CanceledDate = DateTime.Now;
                    appointment.CanceledBy = "Automatically Cancel";
                }
                var noassignment = _CheckAssignment(newactivity);
                if (User.IsTSR() && (noassignment != null || this.ActivitySource == Source.None))
                {
                    appointment.IsCancel = true;
                    appointment.CanceledDate = DateTime.Now;
                    appointment.CanceledBy = "Automatically Cancel";
                }
                db.SaveChanges();
                if (noassignment != null)
                {
                    return noassignment;
                }
            }

            ViewBag.RunScript = "AfterScript();";

            // bind default / outbound
            this._PopulateProfileActivity();
            var leadProfile = db.LeadAssignmentSet.Find(newactivity.LeadAssignmentId).LeadProfile;
            ViewBag.LeadProfile = leadProfile;

            using (var appdb = new ApplicationDbContext())
            {
                ViewBag.User = appdb.Users.Find(User.Identity.GetUserId());
            }
            ConsentHelper.GetConsent(ViewBag, newactivity.Prospect_Profile);
            return View("CreateActivity", newactivity);
        }

        public ActionResult CancelAppointment(int appointmentid)
        {
            var appointment = db.Prospect_AppointmentSet.SingleOrDefault(row => row.ItemId == appointmentid);
            appointment.IsCancel = true;
            appointment.CanceledDate = DateTime.Now;
            appointment.CanceledBy = User.Identity.GetUserId();
            _UpdateLeadAssignment(appointment.Prospect_Activity,false);
            db.SaveChanges();
            Session["Appointment"] = null;
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditActivity(int? id, int activitycode)
        {
            string connectionString = db.Database.Connection.ConnectionString;
            if (id.IsNullOrEmpty())
            {
                return HttpNotFound("Id is Null.");
            }
            Prospect_Profile profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }

            var editactivity = profile.Prospect_Activity.SingleOrDefault(row => row.ItemId == activitycode);
            var noassignment = _CheckAssignment(editactivity);
            if (noassignment != null)
            {
                return noassignment;
            }

            this._PopulateProfileActivity(editactivity);
            var leadProfile = db.LeadAssignmentSet.Find(editactivity.LeadAssignmentId).LeadProfile;
            ViewBag.LeadProfile = leadProfile;
            using (var appdb = new ApplicationDbContext())
            {
                ViewBag.User = appdb.Users.Find(User.Identity.GetUserId());
            }                       

            ConsentHelper.GetConsent(ViewBag, editactivity.Prospect_Profile);
            ViewBag.RunScript = "EditScript();";
            ViewBag.ExtendMessage = "";
            return View(editactivity);
        }


        public ActionResult UpdateAppointment(long leadprofileid, string message, DateTime apmdate)
        {
            var leadprofile = db.LeadProfileSet.Find(leadprofileid);
            var prospect = db.Prospect_ProfileSet.Find(leadprofile.ProspectId);
            var lastact = prospect.Prospect_Activity.OrderBy(o => o.ItemId).Last();
            var apm = lastact.Prospect_Appointment;
            if (apm == null)
            {
                apm = new Prospect_Appointment();
                lastact.Prospect_Appointment = apm;
                apm.AppointmentDate = apmdate;
                apm.Note = message;
                apm.IsCancel = false;
                apm.IsRead = false;
                apm.IsReply = false;
                db.SaveChanges();
                apm = db.Prospect_AppointmentSet.Find(apm.ItemId);
            }
            return View("EditAppointment", apm);
        }

        public ActionResult EditAppointment(long id)
        {
            var apm = db.Prospect_AppointmentSet.Find(id);
            return View("EditAppointment", apm);
        }

        [HttpPost]
        public ActionResult EditAppointment(Prospect_Appointment vm)
        {
            var apm = db.Prospect_AppointmentSet.Find(vm.ItemId);
            apm.AppointmentDate = vm.AppointmentDate;
            apm.AppointmentTime = vm.AppointmentTime;
            apm.Topic = vm.Topic;
            apm.Note = vm.Note;
            db.SaveChanges();
            return RedirectToAction("MainProfile", new { @id = apm.Prospect_Activity.Prospect_ProfileProspectId, @message = "Save Success" });
        }

        private void SetAssignment(long? id)
        {
            var userid = User.Identity.GetUserId();
            LeadProfile leadprofile = db.LeadProfileSet.ActiveSet().FirstOrDefault(item => item.ProspectId == id.Value & item.RecentLeadAssignment.ReceiverUserId == userid);

            if (!leadprofile.IsNullOrEmpty())
            {
                ActivitySource = Source.LeadAssignment;
                LeadAssignmentId = leadprofile.RecentLeadAssignment.LeadAssignment_LeadAssignmentId.Value;
            }
        }


        private string getCallFrom(long? prospectid,string callfrom){
            //check with value in session
            if(this.ProspectId != prospectid){
                this.ProspectId = prospectid;
                this.CallFrom = callfrom;
            }else if (!callfrom.IsNullOrEmpty()){
                this.CallFrom = callfrom;
            }
            return this.CallFrom;
        }

        public ActionResult MainProfile(long? id, string returnUrl = "", string message = "",string callfrom="")
        {
            if (id.IsNullOrEmpty())
            {
                return HttpNotFound("Id is Null.");
            }
            Prospect_Profile profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }
            if (profile.Status == false)
            {
                ViewBag.RunScript = "ShowDisabledProfileScript('" + returnUrl + "');";
            }
            if (message.Length > 0)
            {
                ViewBag.RunScript = "ShowSuccessScript(\"" + message + "\");";
            }
            if (User.IsTSR())
            {
                SetAssignment(id);
            }
            getCallFrom(id, callfrom);
            var lead = db.LeadProfileSet.ActiveSet().FirstOrDefault(c => c.ProspectId == profile.ProspectId);
            if(lead != null)
            {
                ViewBag.LeadProfile = db.LeadProfileSet.ActiveSet().FirstOrDefault(c => c.ProspectId == profile.ProspectId);
                if (profile.Prospect_Activity.Any())
                {
                    using (var appdb = new ApplicationDbContext())
                    {
                        ViewBag.User = appdb.Users.Find(profile.Prospect_Activity.OrderByDescending(o => o.CreatedDate).First().CreatedBy);
                    }
                }                
            }            

            return View(profile);
        }
                        
        public PartialViewResult ViewMainProfile(int? id, string telephone, string telephone2, string tel2ex)
        {
            Prospect_Profile profile = null;
            if (id != null && id.Value > 0)
            {
                profile = db.Prospect_ProfileSet.Find(id);
            }
            else //if (!telephone.IsNullOrEmpty())
            {
                profile = db.Prospect_ProfileSet.Search(telephone, null, telephone2, tel2ex);
            }

            ViewBag.isReadOnly = true;
            return PartialView("_MainProfile", profile);
        }



        public ActionResult ViewAppointment()
        {
            return View();
        }
       
        public ActionResult Notification()
        {
            return View();
        }
        public ActionResult NotificationCalendar()
        {
            // id : user id
            if (User.IsInRole("ADMIN") || User.IsInRole("PM"))
            {
                var queryresult = from row in db.Prospect_AppointmentSet.Where(c => c.IsReply == false && c.IsCancel == false).AsEnumerable()
                                  orderby row.AppointmentDate, row.AppointmentTime descending
                                  select new
                                  {
                                      title = row.Topic,
                                      start = row.AppointmentDate.Value.ToString("yyyy-MM-dd") + " " + row.AppointmentTime,
                                      end = row.AppointmentDate.Value.ToString("yyyy-MM-dd") + " " + (row.AppointmentTime.Split(':').First().ParseToPositiveInt() + 1).ToString().PadLeft(2, '0') + ":" + row.AppointmentTime.Split(':').Last(),
                                      url = @Url.Content("~/Prospect/CreateFollowupActivity") + "/?id=" + row.Prospect_Activity.Prospect_ProfileProspectId + "&appointmentcode=" + row.ItemId
                                  };
                var json = new JavaScriptSerializer().Serialize(queryresult);
                ViewBag.Json = json;
            }
            else
            {
                var uid = User.Identity.GetUserId();
                var queryresult = from row in db.Prospect_AppointmentSet.Where(c => c.IsReply == false && c.IsCancel == false && c.Prospect_Activity.CreatedBy == uid).AsEnumerable()
                                  orderby row.AppointmentDate, row.AppointmentTime descending
                                  select new
                                  {
                                      title = row.Topic,
                                      start = row.AppointmentDate.Value.ToString("yyyy-MM-dd") + " " + row.AppointmentTime,
                                      end = row.AppointmentDate.Value.ToString("yyyy-MM-dd") + " " + (row.AppointmentTime.Split(':').First().ParseToPositiveInt() + 1).ToString().PadLeft(2, '0') + ":" + row.AppointmentTime.Split(':').Last(),
                                      url = @Url.Content("~/Prospect/CreateFollowupActivity") + "/?id=" + row.Prospect_Activity.Prospect_ProfileProspectId + "&appointmentcode=" + row.ItemId
                                  };
                var json = new JavaScriptSerializer().Serialize(queryresult);
                ViewBag.Json = json;
            }
            return View();
        }


        public ActionResult OpenFromLead(int? id, string nextaction = "MainProfile", string callfrom = "")
        {
            ActivitySource = Source.LeadAssignment;
            LeadProfile leadprofile = db.LeadProfileSet.SingleOrDefault(item => item.LeadProfileId == id.Value);
            if (leadprofile.IsNullOrEmpty())
            {
                return HttpNotFound("Lead Profile Not Found!");
            }

            LeadAssignmentId = leadprofile.RecentLeadAssignment.LeadAssignment_LeadAssignmentId.Value;
            if (leadprofile.ProspectId.HasValue && leadprofile.ProspectId.Value > 0)
            {
                return RedirectToAction(nextaction, new { @id = leadprofile.ProspectId, @callfrom = callfrom });
            }            

            try
            {
                var newprofile = new Prospect_Profile();
                newprofile.FirstName = leadprofile.FirstName;
                newprofile.LastName = leadprofile.LastName;
                newprofile.GenderCode = leadprofile.GenderCode;
                newprofile.GenderName = leadprofile.GenderName;
                newprofile.TelPhone = leadprofile.TelPhone;
                newprofile.Email = leadprofile.Email;

                newprofile.FillInModelWithDefaultValue();
                newprofile.Status = true;
                this._FulFil_ProspectProfile(newprofile, true);


                Prospect_Profile profile = db.Prospect_ProfileSet.Add(newprofile);
                db.SaveChanges();
                leadprofile.ProspectId = profile.ProspectId;
                db.SaveChanges();
                return RedirectToAction(nextaction, new { @id = profile.ProspectId, @callfrom = callfrom });
            }
            catch (DbEntityValidationException e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                foreach (var eve in e.EntityValidationErrors)
                {
                    ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                        ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return RedirectToAction("MyLead", "LeadAssignment");
        }

        public ActionResult OptOutList()
        {
            return View();
        }

        
        public ActionResult OptOutApprove(long id)
        {
            var prospect = db.Prospect_ProfileSet.Find(id);
            prospect.OptOut_ApproveBy = User.Identity.GetUserId();
            var leads = db.LeadProfileSet.ActiveSet().Where(c => c.ProspectId == id).ToList();
            leads.ForEach(i => i.Status = false);
            db.SaveChanges();
            return RedirectToAction("OptOutList");
        }

        public ActionResult OptOutDeny(long id)
        {
            var prospect = db.Prospect_ProfileSet.Find(id);
            prospect.OptOut_ApproveBy = User.Identity.GetUserId();
            prospect.OptOut = null;
            prospect.OptOut_Date = null;
            db.SaveChanges();
            return RedirectToAction("OptOutList");
        }

        #endregion

        #region AJAX

        public class SearchProfileClass
        {
            public string phonenumber { get; set; }
            public string callsource { get; set; }
            public string name { get; set; }
            public string activitydate { get; set; }
            public string calltype { get; set; }
            public string activitytype { get; set; }
            public string response { get; set; }
            public string product { get; set; }
        }
        public class SearchResultProfileClass
        {
            public Int64 id { get; set; }
            public string phonenumber { get; set; }
            public string email { get; set; }
            public string name { get; set; }
            public int age { get; set; }
            //public string gender { get; set; }
            //public string income { get; set; }
            public string last_calltype { get; set; }
            public string last_callsource { get; set; }
            public string last_activity { get; set; }
            public string last_contactdate { get; set; }
            public int numberofcontact { get; set; }
            public string response { get; set; }
            public string product { get; set; }
        }

        [HttpPost]
        public ActionResult SearchProfile(SearchProfileClass criteria)
        {
            LeadAssignmentId = 0;//clear leadAssignmentId;
            criteria.name = criteria.name.IsNullOrEmpty() ? string.Empty : criteria.name;
            criteria.FillInModelWithDefaultValue();

            var searchresult = db.Database.SqlQuery<SearchResultProfileClass>
                (string.Format("dbo.PROC_PROSPECT_SEARCH '{0}','{1}','{2}','{3}','{4}','{5}'", criteria.phonenumber, criteria.callsource, criteria.name, criteria.activitydate.IsNullOrEmpty() ? null : criteria.activitydate.ParseToDate().ToString("yyyy-MM-dd"), criteria.calltype, criteria.activitytype, DBNull.Value));

            return Json(searchresult.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult SearchProspect(SearchProfileClass criteria)
        {
            LeadAssignmentId = 0;//clear leadAssignmentId;
            criteria.name = criteria.name.IsNullOrEmpty() ? string.Empty : criteria.name;
            criteria.FillInModelWithDefaultValue();
            var actdate = criteria.activitydate.ParseToDate();
            IQueryable<Prospect_Profile> profileSet = (IQueryable<Prospect_Profile>)db.Prospect_ProfileSet;
            if (!string.IsNullOrEmpty(criteria.phonenumber)) profileSet = profileSet.Where(x => x.TelPhone.Contains(criteria.phonenumber.Replace(" ", "")));
            //if (!string.IsNullOrEmpty(criteria.phonenumber)) profileSet = profileSet.Where(x => x.Prospect_PhoneNumber.Any(c => c.Active && c.PhoneNumber == criteria.phonenumber.Replace(" ", "")));
            if (!string.IsNullOrEmpty(criteria.name)) profileSet = profileSet.Where(c => (c.FirstName + " " + c.LastName).Contains(criteria.name));
            
            if (!string.IsNullOrEmpty(criteria.activitydate)) profileSet = profileSet.Where(c => DbFunctions.TruncateTime(c.Prospect_Activity.OrderByDescending(o => o.CreatedDate).FirstOrDefault().CreatedDate) == actdate);
            if (!string.IsNullOrEmpty(criteria.response)) profileSet = profileSet.Where(c => c.Prospect_Activity.OrderByDescending(o => o.CreatedDate).FirstOrDefault().ResponseCategoryCode == criteria.response);
            
            var showsimplesource = User.IsSimpleSource();
            var searchresult = from row in profileSet.ToList()
                               let lastact = row.Prospect_Activity.OrderByDescending(o => o.CreatedDate).FirstOrDefault()
                               select new SearchResultProfileClass
                               {
                                   id = row.ProspectId,
                                   phonenumber = string.IsNullOrEmpty(row.TelPhone) ? row.TelPhone2 : row.TelPhone,
                                   email = row.Email,
                                   name = row.FirstName + " " + row.LastName,
                                   age = row.BirthDate.HasValue ? ((row.BirthDate.Value.DayOfYear > DateTime.Today.DayOfYear) ? DateTime.Today.Year - row.BirthDate.Value.Year + 1 : DateTime.Today.Year - row.BirthDate.Value.Year) : 0,
                                   last_contactdate = lastact != null ? lastact.CreatedDate.Value.ToShortDateString() + " " + lastact.CallTime : "",
                                   numberofcontact = lastact != null ? db.Prospect_ActivitySet.Where(c => c.Prospect_ProfileProspectId == row.ProspectId).Count() : 0,
                                   response = lastact != null ? lastact.ResponseCategoryCode  : "",
                               };
            return PartialView("_SearchResult", searchresult);
        }

        [HttpGet]
        public ActionResult IsDuplicateCustomerByName(string SalutationCode, string FirstName, string LastName)
        {
            var id = this._CheckDuplicateCustomerByName(SalutationCode, FirstName, LastName);
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IsDuplicateCustomerByHomePhone(string TelPhone2, string TelPhone2_Extension)
        {
            var id = this._CheckDuplicateCustomerByHomePhone(TelPhone2, TelPhone2_Extension);
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IsDuplicateCustomerByEmail(string Email)
        {
            var id = this._CheckDuplicateCustomerByEmail(Email);
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IsDuplicateCustomerByCellPhone(string TelPhone)
        {
            var id = this._CheckDuplicateCustomerByCellPhone(TelPhone);
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IsDuplicateCustomer(string SalutationCode, string FirstName, string LastName, string TelPhone2, string TelPhone2_Extension, string TelPhone, string Email, string ProspectId)
        {
            string message = string.Empty;
            if (!FirstName.IsNullOrEmpty() && !LastName.IsNullOrEmpty())
            {
                var customerid = this._CheckDuplicateCustomerByName(SalutationCode, FirstName.Trim(), LastName.Trim());
                if (customerid.ParseToPositiveInt() > 0 && customerid != ProspectId)
                {
                    message += "<li> ชื่อ-นามสกุล นี้เคยมีการบันทึกแล้วในระบบ </li>";
                }
            }
            //if(!TelPhone2.IsNullOrEmpty())
            //{
            //    var customerid = this._CheckDuplicateCustomerByHomePhone(TelPhone2, TelPhone2_Extension);
            //    if (customerid.ParseToPositiveInt() > 0 && customerid != ProspectId)
            //    {
            //        var profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId.ToString().Equals(customerid));
            //        message += "<li> เบอร์โทรศัพท์บ้าน นี้เคยมีการบันทึกแล้วในระบบ ภายใต้ชิ่อ " + (!profile.IsNullOrEmpty() ? string.Format("{0} {1} {2}", profile.Salutation, profile.FirstName, profile.LastName) : string.Empty) + " </li>";
            //    }
            //}
            //if (!TelPhone.IsNullOrEmpty())
            //{
            //    var customerid = this._CheckDuplicateCustomerByCellPhone(TelPhone);
            //    if (customerid.ParseToPositiveInt() > 0 && customerid != ProspectId)
            //    {
            //        var profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId.ToString().Equals(customerid));
            //        message += "<li> เบอร์โทรศัพท์มือถือ นี้เคยมีการบันทึกแล้วในระบบ ภายใต้ชิ่อ " + (!profile.IsNullOrEmpty() ? string.Format("{0} {1} {2}", profile.Salutation, profile.FirstName, profile.LastName) : string.Empty) + " </li>";
            //    }
            //}
            if (!Email.IsNullOrEmpty())
            {
                var customerid = this._CheckDuplicateCustomerByEmail(Email);
                if (customerid.ParseToPositiveInt() > 0 && customerid != ProspectId)
                {
                    var profile = db.Prospect_ProfileSet.SingleOrDefault(item => item.ProspectId.ToString().Equals(customerid));
                    message += "<li> อีเมล์ นี้เคยมีการบันทึกแล้วในระบบ ภายใต้ชิ่อ " + (!profile.IsNullOrEmpty() ? string.Format("{0} {1} {2}", profile.Salutation, profile.FirstName, profile.LastName) : string.Empty) + " </li>";
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
                

        [HttpPost]
        public ActionResult GetActivityType(string id)
        {
            var calltypecode = id;
            var result = this._GetActivityType(calltypecode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetResponseCategory(string id)
        {
            var calltypecode = id;
            var result = from row in db.ResponseCategorySet.AsEnumerable()
                         select new
                         {
                             id = row.ResponseCategoryId,
                             name = row.Name,
                             support = row.IsSupportInboundCall && row.IsSupportOutboundCall && row.IsSupportFollowupCall ? "ALL" : row.IsSupportFollowupCall ? "FLWUP" : row.IsSupportInboundCall ? "IB" : row.IsSupportOutboundCall ? "OB" : ""
                         };
            var result2 = result.Where(row => row.support == calltypecode || row.support == "ALL");
            return Json(result2, JsonRequestBehavior.AllowGet);
        }

        

        [HttpGet]
        public ActionResult GetResponseReason(long id,long? prospectid)
        {
            var result = this._GetResponseReason(id,prospectid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetResponseSubReason(long id)
        {
            var responsereasonid = id;
            var result = this._GetResponseSubReason(responsereasonid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAppointmentAlert(string id)
        {
            // id : user id
            var tomorrow = DateTime.Today.AddDays(1);
            if (User.IsInRole("PM") || User.IsInRole("SYSTEMADMIN"))
            {

                var queryresult = from row in db.Prospect_AppointmentSet//.AsNoTracking().Include(i => i.Prospect_Activity).Include(i => i.Prospect_Activity.Prospect_Profile)
                                      .Where(c => c.AppointmentDate < tomorrow
                                          && c.IsReply == false
                                          && c.IsCancel == false)
                                          .Select(s => new {
                                              s.ItemId,
                                              s.AppointmentDate,
                                              s.AppointmentTime,
                                              ProspectId = s.Prospect_Activity.Prospect_Profile.ProspectId,
                                              GenderCode = s.Prospect_Activity.Prospect_Profile.GenderCode,
                                              name = s.Prospect_Activity.Prospect_Profile.Salutation + " " + s.Prospect_Activity.Prospect_Profile.FirstName + " " + s.Prospect_Activity.Prospect_Profile.LastName,
                                              Calldate = s.Prospect_Activity.CreatedDate,
                                              CallTime = s.Prospect_Activity.CallTime,
                                              s.Topic
                                          }).AsEnumerable()
                                  orderby row.AppointmentDate descending, row.AppointmentTime ascending
                                  select new
                                  {
                                      appointmentid = row.ItemId,
                                      prospectid = row.ProspectId,
                                      imagepath = row.GenderCode == "F" ? "Content/images/profile_female.png" : row.GenderCode == "M" ? "Content/images/profile_male.png" : "Content/images/profile_unknown.png",
                                      appointmenttime = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "Today - " + row.AppointmentTime : row.AppointmentDate.Value.ToString("dd/MM/yyyy") + " - " + row.AppointmentTime,
                                      name = row.name,
                                      lastactivitydate = row.Calldate.GetValueOrDefault().ToString("dd/MM/yyyy") + " - " + row.CallTime,
                                      appointmenttopic = row.Topic,
                                      backcolor = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "#FFF" : "#FFF",
                                      delay = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "" : "DELAY",
                                      TSR = ""
                                  };
                return Json(queryresult, JsonRequestBehavior.AllowGet);
            }
            else if (User.IsInRole("SUPERVISOR"))
            {
                var udb = new ApplicationDbContext();
                var supid = User.Identity.GetUserId();
                var activeuser = udb.ActiveTSRs().ToList();
                var team = udb.ActiveTSRs().Where(d => d.SupervisorId == supid).Select(s => s.Id).ToList();
                var queryresult = from row in db.Prospect_AppointmentSet
                                      .Where(c => c.AppointmentDate < tomorrow
                                          && c.IsReply == false
                                          && c.IsCancel == false
                                          && team.Contains(c.Prospect_Activity.CreatedBy))
                                          .Select(s => new
                                          {
                                              s.ItemId,
                                              s.AppointmentDate,
                                              s.AppointmentTime,
                                              ProspectId = s.Prospect_Activity.Prospect_Profile.ProspectId,
                                              GenderCode = s.Prospect_Activity.Prospect_Profile.GenderCode,
                                              name = s.Prospect_Activity.Prospect_Profile.Salutation + " " + s.Prospect_Activity.Prospect_Profile.FirstName + " " + s.Prospect_Activity.Prospect_Profile.LastName,
                                              Calldate = s.Prospect_Activity.CreatedDate,
                                              CallTime = s.Prospect_Activity.CallTime,
                                              s.Topic,
                                              CreatedBy = s.Prospect_Activity.CreatedBy
                                          }).AsEnumerable()
                                  orderby row.AppointmentDate descending, row.AppointmentTime ascending
                                  select new
                                  {
                                      appointmentid = row.ItemId,
                                      prospectid = row.ProspectId,
                                      imagepath = row.GenderCode == "F" ? "Content/images/profile_female.png" : row.GenderCode == "M" ? "Content/images/profile_male.png" : "Content/images/profile_unknown.png",
                                      appointmenttime = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "Today - " + row.AppointmentTime : row.AppointmentDate.Value.ToString("dd/MM/yyyy") + " - " + row.AppointmentTime,
                                      name = row.name,
                                      lastactivitydate = row.Calldate.GetValueOrDefault().ToString("dd/MM/yyyy") + " - " + row.CallTime,
                                      appointmenttopic = row.Topic,
                                      backcolor = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "#FFF" : "#FFF",
                                      delay = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "" : "DELAY",
                                      TSR = "นัดหมายโดย " + activeuser.Where(c => c.Id == row.CreatedBy).Select(t => t.FirstName).FirstOrDefault()
                                  };
                return Json(queryresult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Session["Appointment"] == null)
                {
                    var queryresult = from row in db.Prospect_AppointmentSet.AsNoTracking().Include(i => i.Prospect_Activity).Include(i => i.Prospect_Activity.Prospect_Profile)
                                          .Where(c => c.AppointmentDate.Value < tomorrow
                                              && c.IsReply == false
                                              && c.IsCancel == false
                                              && c.Prospect_Activity.CreatedBy == id).AsEnumerable()
                                      orderby row.AppointmentDate descending, row.AppointmentTime ascending
                                      select new
                                      {
                                          appointmentid = row.ItemId,
                                          prospectid = row.Prospect_Activity.Prospect_Profile.ProspectId,
                                          imagepath = row.Prospect_Activity.Prospect_Profile.GenderCode == "F" ? "Content/images/profile_female.png" : row.Prospect_Activity.Prospect_Profile.GenderCode == "M" ? "Content/images/profile_male.png" : "Content/images/profile_unknown.png",
                                          appointmenttime = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "Today - " + row.AppointmentTime : row.AppointmentDate.Value.ToString("dd/MM/yyyy") + " - " + row.AppointmentTime,
                                          name = row.Prospect_Activity.Prospect_Profile.Salutation + " " + row.Prospect_Activity.Prospect_Profile.FirstName + " " + row.Prospect_Activity.Prospect_Profile.LastName,
                                          lastactivitydate = row.Prospect_Activity.CreatedDate.Value.ToString("dd/MM/yyyy") + " - " + row.Prospect_Activity.CallTime,
                                          appointmenttopic = row.Topic,
                                          backcolor = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "#FFF" : "#FFF",
                                          delay = row.AppointmentDate.Value.Date == DateTime.Now.Date ? "" : "DELAY",
                                          TSR = ""
                                      };
                    Session["Appointment"] = queryresult.ToList();
                    return Json(queryresult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(Session["Appointment"], JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult GetAllAppointment(string id)
        {
            // id : user id
            if (User.IsAdmin())
            {
                var queryresult = from row in db.Prospect_AppointmentSet
                                      .Where(c => c.IsReply == false && c.IsCancel == false).AsEnumerable()
                                  orderby row.AppointmentDate, row.AppointmentTime descending
                                  select new
                                  {
                                      appointmentid = row.ItemId,
                                      prospectid = row.Prospect_Activity.Prospect_Profile.ProspectId,
                                      name = row.Prospect_Activity.Prospect_Profile.Salutation + " " + row.Prospect_Activity.Prospect_Profile.FirstName + " " + row.Prospect_Activity.Prospect_Profile.LastName,
                                      imagepath = !row.Prospect_Activity.Prospect_Profile.GenderCode.IsNullOrEmpty() ? "../../Content/images/profile_unknow.png" : row.Prospect_Activity.Prospect_Profile.GenderCode == "F" ? "../../Content/images/profile_female.png" : "../../Content/images/profile_male.png",
                                      appointmentdate = row.AppointmentDate.Value.ToString("dd/MM/yyyy") + " - " + row.AppointmentTime,
                                      replydate = row.ReplyDate.HasValue ? row.ReplyDate.Value.ToString("dd/MM/yyyy") + " - " + (row.ReplyTime.IsNullOrEmpty() ? "" : row.ReplyTime) : "Waiting for Reply...",
                                      delay = row.ReplyDate.HasValue ? row.ReplyDate.Value.Subtract(row.AppointmentDate.Value).Days.ToString() : DateTime.Now.Date.Subtract(row.AppointmentDate.Value).Days.ToString(),
                                      topic = row.Topic,
                                      lastactivity = row.Prospect_Activity.ActivityType

                                  };

                return Json(queryresult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var queryresult = from row in db.Prospect_AppointmentSet
                                  .Where(c => c.Prospect_Activity.CreatedBy == id && c.IsReply == false && c.IsCancel == false).AsEnumerable()
                                  orderby row.AppointmentDate, row.AppointmentTime descending
                                  select new
                                  {
                                      appointmentid = row.ItemId,
                                      prospectid = row.Prospect_Activity.Prospect_Profile.ProspectId,
                                      name = row.Prospect_Activity.Prospect_Profile.Salutation + " " + row.Prospect_Activity.Prospect_Profile.FirstName + " " + row.Prospect_Activity.Prospect_Profile.LastName,
                                      imagepath = !row.Prospect_Activity.Prospect_Profile.GenderCode.IsNullOrEmpty() ? "../../Content/images/profile_unknow.png" : row.Prospect_Activity.Prospect_Profile.GenderCode == "F" ? "../../Content/images/profile_female.png" : "../../Content/images/profile_male.png",
                                      appointmentdate = row.AppointmentDate.Value.ToString("dd/MM/yyyy") + " - " + row.AppointmentTime,
                                      replydate = row.ReplyDate.HasValue ? row.ReplyDate.Value.ToString("dd/MM/yyyy") + " - " + (row.ReplyTime.IsNullOrEmpty() ? "" : row.ReplyTime) : "Waiting for Reply...",
                                      delay = row.ReplyDate.HasValue ? row.ReplyDate.Value.Subtract(row.AppointmentDate.Value).Days.ToString() : DateTime.Now.Date.Subtract(row.AppointmentDate.Value).Days.ToString(),
                                      topic = row.Topic,
                                      lastactivity = row.Prospect_Activity.ActivityType
                                  };
                return Json(queryresult, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetActivityInfo(int id)
        {
            var activity_id = id;// Helper.GenericExtensionClass.ParseToPositiveInt(id);
            var showsimplesource = false;
            if (User.Identity.IsAuthenticated)
            {
                showsimplesource = User.IsSimpleSource();
            }
            var act = db.Prospect_ActivitySet.Where(c => c.ItemId == id).AsEnumerable();            
            var query = from row in act
                        select new
                        {
                            prospectname = row.Prospect_Profile.FirstName + " " + row.Prospect_Profile.LastName,
                            response_category = row.ResponseCategory,
                            response_reason = row.ResponseReason,
                            response_detail = row.Detail,
                            appointment_date = row.Prospect_Appointment.IsNullOrEmpty() ? "" : row.Prospect_Appointment.AppointmentDate.Value.ToString("dd/MM/yyyy")
                            ,
                            appointment_time = row.Prospect_Appointment.IsNullOrEmpty() ? "" : row.Prospect_Appointment.AppointmentTime
                            ,
                            appointment_topic = row.Prospect_Appointment.IsNullOrEmpty() ? "" : row.Prospect_Appointment.Topic
                            ,
                            appointment_detail = row.Prospect_Appointment.IsNullOrEmpty() ? "" : row.Prospect_Appointment.Note
                            ,
                            note = row.Note
                            ,
                            activitytypename = row.ActivityType
                            ,
                            activitytypecode = row.ActivityTypeCode
                            ,
                            calltype = row.CallType
                            ,                            
                            calldate = row.CreatedDate.Value.ToString("dd/MM/yyyy") + " " + row.CallTime
                            ,
                            callfrom = row.CallFrom
                        };
            if (query.Count() > 0)
            {
                return Json(query.First(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult SaveTelephonyRequest(string unique_id)
        {
            db.TelephonyRequestSet.Add(new TelephonyRequest() { CallId = unique_id, CallDate = DateTime.Now });
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region POST

        [HttpPost]
        public ActionResult CreateProfile(Prospect_Profile newprofile)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    newprofile.FillInModelWithDefaultValue();
                    newprofile.Status = true;
                    this._FulFil_ProspectProfile(newprofile, true);
                                        
                    Prospect_Profile profile = db.Prospect_ProfileSet.Add(newprofile);
                    db.SaveChanges();

                    return RedirectToAction("MainProfile", new { @id = profile.ProspectId, @message = "Save Success" });
                }
                catch (DbEntityValidationException e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    ModelState.AddModelError("", ex.Message);
                }
            }
            this._PopulateProfile();
            return View(newprofile);
        }

        [HttpPost]
        public ActionResult EditProfile(Prospect_Profile updateprofile)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    updateprofile.FillInModelWithDefaultValue();
                    this._FulFil_ProspectProfile(updateprofile, false);
                    if (updateprofile.OptOut.IsNullOrEmpty())
                    {
                        updateprofile.OptOut_ApproveBy = null;
                        updateprofile.OptOut_Date = null;
                    }
                    var originalprofile = db.Prospect_ProfileSet.AsNoTracking().First(c => c.ProspectId == updateprofile.ProspectId);
                    if (originalprofile.OptOut.IsNullOrEmpty() && updateprofile.OptOut == "Y")
                    {
                        updateprofile.OptOut_ApproveBy = User.Identity.GetUserId();
                        if (updateprofile.OptOut_Date == null) updateprofile.OptOut_Date = DateTime.Today;
                    }

                    db.SaveChanges();

                    db.Entry(updateprofile).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(updateprofile).Property(x => x.TelPhone).IsModified = false;

                    var leads = db.LeadProfileSet.ActiveSet().Where(c => c.ProspectId == updateprofile.ProspectId);
                    foreach (var item in leads)
                    {
                        item.FirstName = updateprofile.FirstName;
                        item.LastName = updateprofile.LastName;
                    }

                    db.SaveChanges();

                    return RedirectToAction("MainProfile", new { @id = updateprofile.ProspectId.ToString() });
                }
                catch (DbEntityValidationException e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    ModelState.AddModelError("", ex.Message);
                }
            }
            this._PopulateProfile();
            return View(updateprofile);
        }

        [HttpPost]
        public ActionResult CreateActivity(Prospect_Activity newactivity, FormCollection form)
        {
            Session["Appointment"] = null;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var project = AppConfigHelper.ProjectName();
                    newactivity.FillInModelWithDefaultValue();
                    this._FulFil_ActivityProfile(newactivity);

                    // find its parent.
                    var profile = db.Prospect_ProfileSet.SingleOrDefault(row => row.ProspectId == newactivity.Prospect_Profile.ProspectId);                    

                    newactivity.Prospect_Profile = profile;

                    // update appointment that waiting for reply
                    var obsoleted_appointments = db.Prospect_AppointmentSet.Where(row => row.IsReply == false
                                                 && row.Prospect_Activity.Prospect_ProfileProspectId == newactivity.Prospect_Profile.ProspectId);

                    obsoleted_appointments.ToList().ForEach(item =>
                    {
                        item.IsCancel = true;
                        item.CanceledDate = DateTime.Now;
                        item.CanceledBy = "Automatically Cancel";
                    });
                    db.SaveChanges();

                    // update model by source of lead of last activity
                    // update the previous information when the activity came from following up
                    if (!newactivity.AppointmentId.IsNullOrEmpty() && newactivity.AppointmentId > 0)
                    {
                        var appointment = db.Prospect_AppointmentSet.SingleOrDefault(row => row.ItemId == newactivity.AppointmentId.Value);
                        appointment.ReplyDate = newactivity.CreatedDate;
                        appointment.ReplyTime = newactivity.CallTime;
                        appointment.IsRead = true;
                        appointment.IsReply = true;
                        appointment.IsCancel = false;
                        db.SaveChanges();

                        // update current source of lead by last source of lead.
                        var last_activity = db.Prospect_ActivitySet.SingleOrDefault(row => row.ItemId == appointment.Prospect_Activity.ItemId);
                        newactivity.BaseActivityId = last_activity.BaseActivityId;
                    }

                    // save main activity into database first.
                    db.Entry<Prospect_Activity>(newactivity).State = System.Data.Entity.EntityState.Added;

                    // save its children data into database.
                    if (!newactivity.Prospect_Appointment.IsNullOrEmpty()) db.Entry<Prospect_Appointment>(newactivity.Prospect_Appointment).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();

                    // set current activity as basement, if it not came from any appointment.
                    if (newactivity.AppointmentId.IsNullOrEmpty())
                    {
                        newactivity.BaseActivityId = newactivity.ItemId;
                        db.Entry<Prospect_Activity>(newactivity).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }


                    //update lead assignment
                    _UpdateLeadAssignment(newactivity, true);

                    if (!newactivity.ActivityType.IsNullOrEmpty())
                    {
                        try
                        {
                            SendCustomerRequestEmail(newactivity);
                        }
                        catch(Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    return RedirectToAction("MainProfile", new { @id = newactivity.Prospect_ProfileProspectId });
                    
                }
                catch (DbEntityValidationException e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    ModelState.AddModelError("", ex.Message);
                }
            }
            this._PopulateProfileActivity(newactivity);
            return View(newactivity);
        }

        private void SendCustomerRequestEmail(Prospect_Activity activity)
        {
            var mailto = db.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.Equals("Email") && row.Name.Equals("CustomerRequestTo"));
            var lead = db.LeadProfileSet.Where(c=>c.ProspectId==activity.Prospect_ProfileProspectId).FirstOrDefault();
            if(lead != null)
            {
                var subject = string.Format("{0}:{1}/{2}/{3}/{4}",lead.TM, activity.ActivityType,lead.PolicyNo,lead.FirstName,lead.TelPhone);
                var body = string.Format(@"
<p>{0}</p>
<table border='1' cellspacing='0'>
<thead>
    <tr>
        <th>TM</th><th>กรมธรรม์</th><th>รายชื่อ</th><th>เบอร์โทรศัพท์</th><th>Remark</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td>
    </tr>
</tbody>
</table>
", activity.ActivityType, lead.TM, lead.PolicyNo, lead.FirstName, lead.TelPhone,activity.Detail);
                var to = new List<string>();// { "witsaruth@generali.co.th","wansiris@generali.co.th"};
                to.AddRange(mailto.Value.Split(',').Select(p => p.Trim()).ToList());
                var cc = new List<string>();
                using (var appdb = new ApplicationDbContext())
                {
                    var u = appdb.Users.Find(User.Identity.GetUserId());
                    cc.Add(u.Email);
                }
                EmailHelper.DoSendEmail(to,cc,subject,body);
            }            
        }

        [HttpPost]
        public ActionResult CreateFollowupActivity(Prospect_Activity newactivity, FormCollection form)
        {
            return this.CreateActivity(newactivity, form);
        }

        [HttpPost]
        public ActionResult EditActivity(Prospect_Activity activity, FormCollection form)
        {
            Session["Appointment"] = null;
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var project = AppConfigHelper.ProjectName();
                    activity.FillInModelWithDefaultValue();
                    this._FulFil_ActivityProfile(activity, false);

                    // find its parent.
                    var profile = db.Prospect_ProfileSet.SingleOrDefault(row => row.ProspectId == activity.Prospect_Profile.ProspectId);
                    activity.Prospect_Profile = profile;
                    db.SaveChanges();

                    var extactivity = profile.Prospect_Activity.SingleOrDefault(row => row.ItemId == activity.ItemId);
                    // update appointment change (incase follow-up)
                    // update current source of lead by recent source of lead.
                    if (!activity.AppointmentId.IsNullOrEmpty() && activity.AppointmentId > 0)
                    {
                        var appointment = db.Prospect_AppointmentSet.SingleOrDefault(row => row.ItemId == activity.AppointmentId.Value);
                        appointment.ReplyDate = activity.CreatedDate;
                        appointment.ReplyTime = activity.CallTime;
                        appointment.IsRead = true;
                        appointment.IsReply = true;
                        appointment.IsCancel = false;
                        db.SaveChanges();

                        // update current source of lead by recent source of lead. (when this activity came from following up)
                        var last_activity = db.Prospect_ActivitySet.SingleOrDefault(row => row.ItemId == appointment.Prospect_Activity.ItemId);
                        if (!last_activity.IsNullOrEmpty())
                        {
                            activity.CallSource = last_activity.CallSource;
                            activity.CallSourceCode = last_activity.CallSourceCode;
                            activity.BaseActivityId = last_activity.BaseActivityId; // add new 4 Jan 2016
                        }
                    }

                    // update lead assignment
                    _UpdateLeadAssignment(activity,  false);

                    // Delete other one.
                    var lastactivity = extactivity;
                    if (!lastactivity.Prospect_Appointment.IsNullOrEmpty()) db.Entry(lastactivity.Prospect_Appointment).State = System.Data.Entity.EntityState.Deleted;

                    db.SaveChanges();

                    // edit main activity into database first.
                    string createby = extactivity.CreatedBy;
                    DateTime createdate = extactivity.CreatedDate.Value;
                    long? basementid = extactivity.BaseActivityId; // add new 4Jan2016 (add basementid)
                    extactivity.EvacateValueModel(activity);
                    extactivity.Prospect_Profile = profile;
                    extactivity.CreatedBy = createby;
                    extactivity.CreatedDate = createdate;
                    extactivity.BaseActivityId = basementid; // add new 4Jan2016 (add basementid)
                    extactivity.Prospect_ProfileProspectId = profile.ProspectId;
                    db.Entry<Prospect_Activity>(extactivity).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();

                    // add children data into database (either one).
                    if (!activity.Prospect_Appointment.IsNullOrEmpty())
                    {
                        activity.Prospect_Appointment.Prospect_Activity = extactivity;
                        db.Entry<Prospect_Appointment>(activity.Prospect_Appointment).State = System.Data.Entity.EntityState.Added;
                    }
                    
                    db.SaveChanges();                    

                    // back to main profile page
                    return RedirectToAction("MainProfile", new { @id = extactivity.Prospect_ProfileProspectId, @message = "Save Success" });
                }
                catch (DbEntityValidationException e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    ModelState.AddModelError("", ex.Message);
                }
            }
            this._PopulateProfileActivity(activity);
            return View(activity);
        }

        #endregion

        #region Helper Method

        private void _PopulateProfile()
        {
            //ViewBag.Salutation = new SelectList(appdb.SalutationSet.ToList(), "Code", "Name");
        }

        private void _PopulateProfile(Prospect_Profile profile)
        {
            
            //ViewBag.Salutation = new SelectList(appdb.SalutationSet.ToList(), "Code", "Name");
        }

        private void _loadActivity()
        {
            ViewBag.Activity = new SelectList(new List<SelectListItem>
            {
                new SelectListItem() {Value= "ลูกค้าปฏิเสธการติดต่อเพื่อขายประกันทางโทรศัพท์(DO not call)",Text="ลูกค้าปฏิเสธการติดต่อเพื่อขายประกันทางโทรศัพท์(DO not call)" },
                new SelectListItem() {Value= "ลูกค้าต้องการให้เจ้าหน้าที่ฝ่ายขายติดต่อกลับ" ,Text="ลูกค้าต้องการให้เจ้าหน้าที่ฝ่ายขายติดต่อกลับ"},
                new SelectListItem() {Value= "ลูกค้ามีความประสงค์เปลี่ยนแปลง/แก้ไขข้อมูล",Text="ลูกค้ามีความประสงค์เปลี่ยนแปลง/แก้ไขข้อมูล" }
            }, "Value", "Text");
        }
        private void _PopulateProfileActivity()
        {
            ViewBag.ResponseCategory = new SelectList(db.ResponseCategorySet.Where(x => x.Active).ToList(), "ResponseCategoryId", "Name");
            ViewBag.ResponseReason = new SelectList(db.ResponseReasonSet.Take(0).ToList(), "ResponseReasonId", "Name");
            _loadActivity();
        }
        private void _PopulateProfileActivity(Prospect_Activity activity)
        {
            ViewBag.ResponseCategory = new SelectList(db.ResponseCategorySet.ToList(), "ResponseCategoryId", "Name");
            if (!activity.IsNullOrEmpty())
            {
                var responsereason = this._GetResponseReason(activity.ResponseCategoryCode.ParseToPositiveInt(),activity.Prospect_ProfileProspectId);
                ViewBag.ResponseReason = new SelectList(responsereason, "id", "name");
            }
            else
            {
                ViewBag.ResponseReason = new SelectList(db.ResponseReasonSet.Take(0).ToList(), "ResponseReasonId", "Name");
            }
            _loadActivity();
        }
        private void _FulFil_ProspectProfile(Prospect_Profile profile, bool IsNewProfile = true)
        {            
            if (!profile.GenderCode.IsNullOrEmpty())
            {
                switch (profile.GenderCode)
                {
                    case "M": profile.GenderName = "ชาย"; break;
                    case "F": profile.GenderName = "หญิง"; break;
                    default: break;
                }
            }
            
            profile.IsCustomer = false;
            profile.UpdatedBy = User.Identity.GetUserId();
            profile.UpdatedDate = DateTime.Now;
            if (IsNewProfile)
            {
                profile.CreatedBy = profile.UpdatedBy;
                profile.CreatedDate = profile.UpdatedDate;
                profile.ConsentDate = null;
                profile.OptOut_Date = null;
            }
            profile.BirthDate = (profile.BirthDate.IsNullOrEmpty() || profile.BirthDate.Value.Year == 1900) ? null : profile.BirthDate;
            profile.Note = profile.Note.IsNullOrEmpty() ? "" : profile.Note;
            profile.Occupation = profile.Occupation.IsNullOrEmpty() ? "" : profile.Occupation;
            profile.Remark = profile.Remark.IsNullOrEmpty() ? "" : profile.Remark;
            profile.AnnualIncome = profile.AnnualIncome.IsNullOrEmpty() ? 0 : profile.AnnualIncome.Value;
        }
        private void _FulFil_ActivityProfile(Prospect_Activity activity, bool IsNewActivity = true)
        {
            if (IsNewActivity)
            {
                activity.Prospect_ProfileProspectId = activity.Prospect_Profile.ProspectId;
                activity.CreatedBy =
                activity.UpdatedBy = User.Identity.GetUserId();
                activity.CreatedDate =
                activity.UpdatedDate = DateTime.Now;
                activity.AppointmentId = activity.AppointmentId <= 0 ? null : activity.AppointmentId;
            }
            else
            {
                activity.UpdatedBy = User.Identity.GetUserId();
                activity.UpdatedDate = DateTime.Now;
            }

            if (activity.IsNullOrEmpty()) return;
                        
            // Emotional
            if (!activity.EmotionCode.IsNullOrEmpty())
            {
                activity.Emotion = activity.EmotionCode;
            }

            // Appointment
            if (!activity.Prospect_Appointment.IsNullOrEmpty())
            {
                if (activity.Prospect_Appointment.AppointmentDate.IsNullOrEmpty())
                {
                    activity.Prospect_Appointment = null;
                }
                else
                {
                    activity.Prospect_Appointment.FillInModelWithDefaultValue();
                    activity.Prospect_Appointment.ReplyDate = null;
                    activity.Prospect_Appointment.ReplyTime = string.Empty;
                }
            }

                if (!activity.ResponseCategoryCode.IsNullOrEmpty())
                {
                    var id = activity.ResponseCategoryCode;
                    var result = db.ResponseCategorySet.SingleOrDefault(item => item.ResponseCategoryId.ToString().Equals(id));
                    if (result != null) activity.ResponseCategory = result.Name;
                }
                if (!activity.ResponseReasonCode.IsNullOrEmpty())
                {
                    var id = activity.ResponseReasonCode;
                    var result = db.ResponseReasonSet.SingleOrDefault(item => item.ResponseReasonId.ToString().Equals(id));
                    if (result != null) activity.ResponseReason = result.Name;
                }                
        }

        private void _UpdateLeadAssignment(Prospect_Activity activity, bool isCreate)
        {
            if (!activity.LeadAssignmentId.IsNullOrEmpty() && activity.LeadAssignmentId > 0)
            {
                var leadAssignment = db.LeadAssignmentSet.Find(activity.LeadAssignmentId);
                leadAssignment.CallDate = activity.CreatedDate;
                leadAssignment.ActivityCode = activity.ActivityTypeCode;
                leadAssignment.Activity = activity.ActivityType;

                
                if (isCreate || activity.CreatedDate >= leadAssignment.CallDate)
                {
                    leadAssignment.ResponseCategoryCode = activity.ResponseCategoryCode;
                    leadAssignment.ResponseCategoryName = activity.ResponseCategory;
                    leadAssignment.ResponseReasonCode = activity.ResponseReasonCode;
                    leadAssignment.ResponseReason = activity.ResponseReason;
                    leadAssignment.Detail = activity.Detail;
                                
                    if (activity.Prospect_Appointment != null && activity.Prospect_Appointment.IsCancel != true)
                    {
                        leadAssignment.AppointmentDate = activity.Prospect_Appointment.AppointmentDate;
                        leadAssignment.AppointmentTime = activity.Prospect_Appointment.AppointmentTime;
                        leadAssignment.AppointmentTopic = activity.Prospect_Appointment.Topic;
                        leadAssignment.AppointmentContent = activity.Prospect_Appointment.Note;
                    }
                    else
                    {
                        leadAssignment.AppointmentDate = null;
                        leadAssignment.AppointmentTime = "";
                        leadAssignment.AppointmentTopic = "";
                        leadAssignment.AppointmentContent = "";
                    }
                }


                var recent = leadAssignment.LeadProfile.RecentLeadAssignment;
                recent.CallDate = leadAssignment.CallDate;
                recent.ActivityCode = activity.ActivityTypeCode;
                recent.Activity = activity.ActivityType;
                recent.ResponseCategoryCode = leadAssignment.ResponseCategoryCode;
                recent.ResponseCategoryName = leadAssignment.ResponseCategoryName;
                recent.ResponseReasonCode = leadAssignment.ResponseReasonCode;
                recent.ResponseReason = leadAssignment.ResponseReason;
                recent.ResponseSubReasonCode = leadAssignment.ResponseSubReasonCode;
                recent.ReaponseSubReason = leadAssignment.ReaponseSubReason;
                recent.Detail = leadAssignment.Detail;
                recent.AppointmentId = leadAssignment.AppointmentId;
                recent.AppointmentDate = leadAssignment.AppointmentDate;
                recent.AppointmentTime = leadAssignment.AppointmentTime;
                recent.AppointmentTopic = leadAssignment.AppointmentTopic;
                recent.AppointmentContent = leadAssignment.AppointmentTime;
                if (isCreate) recent.CallCount++;                

                db.SaveChanges();
            }
        }

        private ActionResult NoLeadAssignmentAction()
        {
            return RedirectToAction("MyLead", "LeadAssignment", new { @message = "NoLeadAssignment" });
        }
              

        private ActionResult _CheckAssignment(Prospect_Activity activity)
        {
            LeadAssignment leadAssignment = null;
            if (User.IsTSR())
            {
                var userid = User.Identity.GetUserId();
                var lead = db.LeadProfileSet.ActiveSet().Where(c => c.ProspectId == activity.Prospect_Profile.ProspectId && c.RecentLeadAssignment.ReceiverUserId == userid).FirstOrDefault();
                if (lead != null)
                {
                    this.ActivitySource = Source.LeadAssignment;
                    this.LeadAssignmentId = lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId.Value;
                }
            }
            if (this.ActivitySource == Source.LeadAssignment)
            {
                if (LeadAssignmentId > 0)
                {
                    leadAssignment = db.LeadAssignmentSet.Find(LeadAssignmentId);
                    ViewBag.LeadProduct = leadAssignment.LeadProfile.Product;
                    activity.LeadAssignmentId = LeadAssignmentId;
                    activity.CallSourceCode = leadAssignment.LeadProfile.CallSourceCode.ParseToPositiveLong();
                    activity.CallSource = leadAssignment.LeadProfile.CallSource;
                    //var callsource = locationDB.CallSourceSet.Find(leadAssignment.LeadProfile.CallSourceCode.ParseToPositiveLong());
                    //activity.CallSourceSet = callsource;
                    ViewBag.LeadProduct = leadAssignment.LeadProfile.Product;
                    ViewBag.SourceInformFlag = leadAssignment.LeadProfile.LeadsourceInformFlag ?? false;
                    

                    //Priority
                    activity.Priority = leadAssignment.Priority;
                    activity.SubPriority = leadAssignment.SubPriority;
                    activity.PriorityVersion = leadAssignment.PriorityVersion;
                    activity.PriorityTime = leadAssignment.PriorityTime;
                    if (_CheckLeadAssignment(activity.Prospect_Profile, leadAssignment) == false)
                    {
                        return NoLeadAssignmentAction();
                    }
                }
                else
                {
                    return NoLeadAssignmentAction();
                }
            }
            else
            {
                ViewBag.NoAssignment = true;
            }
            return null;
        }

        private bool _CheckECommerceAssignment(Prospect_Profile profile, ECommerceModel.ECommerceDisplayApplicationClass app)
        {
            if (!User.Identity.GetUserId().Equals(app.TSRID))
            {
                return false;
            }
            int pid = _GetProspectFromECommerce(app);
            if (profile.ProspectId != pid)
            {
                return false;
            }
            return true;
        }

        private bool _CheckLeadAssignment(Prospect_Profile profile, LeadAssignment assign)
        {
            if (assign.LeadAssignmentId != assign.LeadProfile.RecentLeadAssignment.LeadAssignment_LeadAssignmentId)
            {
                return false;
            }
            if (profile.ProspectId != assign.LeadProfile.ProspectId)
            {
                return false;
            }
            return true;
        }

        private int _GetProspectFromECommerce(ECommerceModel.ECommerceDisplayApplicationClass app)
        {
            int pid = 0;
            if (!app.PhoneNumber.IsNullOrEmpty())
            {
                pid = this._CheckDuplicateCustomerByCellPhone(app.PhoneNumber.FormatPhonenumber()).ParseToPositiveInt();
            }
            if (pid == 0 && !app.EmailAddress.IsNullOrEmpty())
            {
                pid = this._CheckDuplicateCustomerByEmail(app.EmailAddress).ParseToPositiveInt();
            }
            return pid;
        }

        #endregion

        #region Core Retrieve Method

        private void _LoadCallSource(string value = null)
        {
            
        }

        private void _LoadResponse(string value = null)
        {
            var response = from row in db.ResponseCategorySet
                           where row.Active
                           select new { code = row.ResponseCategoryId, name = row.Name };
            ViewBag.Responses = new SelectList(response.ToList(), "code", "name", value);
        }

        
        

        private System.Collections.IEnumerable _GetResponseCategory(string calltypecode)
        {
            var result = from row in db.ResponseCategorySet.AsEnumerable()
                         select new
                         {
                             id = row.ResponseCategoryId,
                             name = row.Name,
                             support = row.IsSupportInboundCall && row.IsSupportOutboundCall && row.IsSupportFollowupCall ? "ALL" : row.IsSupportFollowupCall ? "FLWUP" : row.IsSupportInboundCall ? "IB" : row.IsSupportOutboundCall ? "OB" : ""
                         };
            var result2 = result.Where(row => row.support == calltypecode || row.support == "ALL");
            return result2;
        }
        private System.Collections.IEnumerable _GetActivityType(string calltypecode)
        {
            var result = from row in db.ActivitySet.AsEnumerable()
                         where row.Active.Equals(true)
                         && row.IsSupportProspectPerson.Equals(true)
                         select new
                         {
                             id = row.Code,
                             name = row.Name,
                             support = (row.IsSupportFollowupCall ? "FLWUP" : "") + "|" + (row.IsSupportInboundCall ? "IB" : "") + "|" + (row.IsSupportOutboundCall ? "OB" : "")
                         };
            var result2 = result.Where(row => row.support.Contains(calltypecode));
            return result2.ToList();
        }
        private System.Collections.IEnumerable _GetResponseReason(long responsecategorycode,long? prospectid)
        {
            var query = db.ResponseReasonSet.Where(c => c.ResponseCategoryId == responsecategorycode);
            query = query.Where(c => c.Active == true);
            if (prospectid > 0)
            {
                if (db.Prospect_ProfileSet.Find(prospectid).Prospect_Activity.Count(c=>c.ResponseCategoryCode=="1") < 3)
                {
                    query = query.Where(c => c.ResponseReasonId != 87);//exclude follow up เกิน 3 ครั้ง
                }
            }

            var result = from row in query
                         select new
                         {
                             id = row.ResponseReasonId,
                             name = row.Name
                         };
            return result.ToList();
        }
        private System.Collections.IEnumerable _GetResponseSubReason(long responsereasonid)
        {
            var query = db.ResponseSubReasonSet.Where(c => c.ResponseReasonId == responsereasonid);
            
            query = query.Where(c => c.Active == true);
            
            var result = from row in query
                         select new
                         {
                             id = row.ResponseSubReasonId,
                             name = row.Name
                         };
            return result.ToList();
        }
        
        private string _CheckDuplicateCustomerByCellPhone(string TelPhone)
        {
            var result = from row in db.Prospect_ProfileSet
                         where row.TelPhone == TelPhone
                         select new
                         {
                             id = row.ProspectId
                         };
            string outputid = "0";
            if (result.Count() > 0) outputid = result.ToList().First().id.ToString();
            return outputid;
        }
        private string _CheckDuplicateCustomerByEmail(string Email)
        {
            var result = from row in db.Prospect_ProfileSet
                         where row.Email == Email
                         select new
                         {
                             id = row.ProspectId
                         };
            string outputid = "0";
            if (result.Count() > 0) outputid = result.ToList().First().id.ToString();
            return outputid;
        }
        private string _CheckDuplicateCustomerByHomePhone(string TelPhone2, string TelPhone2_Extension)
        {
            var result = from row in db.Prospect_ProfileSet
                         where row.TelPhone2 == TelPhone2 && row.TelPhone2_Extension == TelPhone2_Extension
                         select new
                         {
                             id = row.ProspectId
                         };
            string outputid = "0";
            if (result.Count() > 0) outputid = result.ToList().First().id.ToString();
            return outputid;
        }
        private string _CheckDuplicateCustomerByName(string SalutationCode, string FirstName, string LastName)
        {
            var result = from row in db.Prospect_ProfileSet
                         where row.FirstName == FirstName && row.LastName == LastName
                         select new
                         {
                             id = row.ProspectId
                         };
            string outputid = "0";
            if (result.Count() > 0) outputid = result.ToList().First().id.ToString();
            return outputid;
        }

        #endregion

        #region Private Method

        public void CallDMGalaxyAsync(float activityId, string projectName)
        {
            try
            {
                if (projectName.ToUpper() == "STAGING") return;
                // send data to dm-galaxy via dm-sphere
                string leadType = "SST";
                string getURL = string.Format(@"http://192.168.10.26/DMSphere/api/v1/shooting_star?id={0}&lead_type={1}&project={2}", activityId, leadType, projectName.ToUpper());
                var client = new RestSharp.RestClient(getURL);
                var request = new RestSharp.RestRequest(getURL, RestSharp.Method.GET);
                client.ExecuteAsync(request, null);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
        }

        private void CallDashboardRefreshAsync()
        {
            try
            {
                var client = new RestSharp.RestClient(Request.SalesLink());
                var request = new RestSharp.RestRequest("Dashboard/RefreshTrigger", RestSharp.Method.GET);
                client.ExecuteAsync(request, null);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
        }
              
        #endregion

        #region OneApp Integration

        [AllowAnonymous]
        public ActionResult IncommingCall(string id)
        {
            return RedirectToAction("IncomingCall", new { @id = id });
        }
        [AllowAnonymous]
        public ActionResult IncomingCall(string id)
        {
            id = id.Replace("params=", "");
            Prospect_Profile profile = null;
            if (id != null)
            {
                id = id.FormatPhonenumber();
                profile = db.Prospect_ProfileSet.Search(id, null, id, "");
            }
            return View(profile);
        }
       
        #endregion

        #region QCall Integration

        public class QCallResultClass
        {
            /*  Unique_ID String Generate from SST(เป็น text 30 characters)
                Caller Number String Customer Phone Number(เบอร์โทร 10 หลัก ไม่มี space)
                Dial_Time Datetime Dial Time from Telephone System(วนัเวลาทโี่ ทรออก)
                Ringing_Duration INT(Second) Ringing Duration(Second) (เวลารอสาย)
                Hangup_Time Datetime Hangup time from Telephone System(วันเวลาที่วางสาย)
                Hangup_Case String Hangup_Case(สถานะของสาย Call Reason ในข้อที่ 2)
                Call_Duration INT(Second) (เวลาในการสนทนา) หน่วยเป็นวินาที เช่น 72 วินาที = 1.12 นาที
                Voicename Text voiceXXX(ชื่อ voice file จะใช้งานคู่กับ URL ที่ทาง Q-Call ส่งให้เพื่อใช้ในการค้นหาไฟล์)*/

            /*
             UniqueId  : prospectId-userId-telephoneNo-timestamp
            */

            public string Unique_ID { get; set; }
            public string Caller_Number { get; set; }
            public DateTime Dial_Time { get; set; }
            public int Ringing_Duration { get; set; }
            public DateTime Hangup_Time { get; set; }
            public string Hangup_Case { get; set; }
            public int Call_Duration { get; set; }
            public string Voicename { get; set; }
            public string Agent_ID { get; set; }
        }

        [Route("QCallResult")]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult QCallResult()
        {
            try
            {
                // get body as json raw data.
                Stream req = HttpContext.Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();

                // deserializing json-input to define-object.
                var model = new QCallResultClass();
                model = JsonConvert.DeserializeObject<QCallResultClass>(json);

                // validate mandatory fields.(unique Id must be contained 5 argument as least).
                if (!ValidateQCallResultModel(model, out string invalidMessage))
                {
                    var ex = new Exception(invalidMessage);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    Response.StatusCode = 400;
                    return Json(new { Status = "400", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

                // break arguments from uniqueId
                string[] uniqueIdArguments = model.Unique_ID.Split('-');

                /*
                 * UniqueId Arguments.
                 [0] port number
                 [1] prospect Id
                 [2] extension
                 [3] lead assignment id
                 [4] timestamp
                */

                string extension = uniqueIdArguments[2];
                if (!long.TryParse(uniqueIdArguments[1], out long prospectId)) prospectId = 0;
                if (!long.TryParse(uniqueIdArguments[3], out long leadAssignmentId)) leadAssignmentId = 0;

                // get username from database.
                var username = "";
                var userId = "";
                using (var appdb = new ApplicationDbContext())
                {
                    var user = appdb.ActiveUsers().Where(c => c.ExtensionCode == extension);
                    if (user.Any())
                    {
                        userId = user.First().Id;
                        username = user.First().UserName;
                    }
                }

                // transform model into entity.
                TelephonyResponse entity = new TelephonyResponse()
                {
                    CallId = model.Unique_ID,
                    CallDate = model.Dial_Time,
                    TalkTime = model.Call_Duration,
                    ResponseCode = GetResponseCode(model.Hangup_Case),
                    ResponseDesc = model.Hangup_Case,
                    ExtensionCode = model.Agent_ID,
                    CallType = "OUTBOUND",
                    TelephoneNumber = model.Caller_Number,
                    Ringtime = model.Ringing_Duration,
                    VoiceName = model.Voicename,
                    UserId = userId,
                    ProspectId = prospectId,
                    LeadAssignmentId = leadAssignmentId
                };

                // save new entity.
                db.TelephonyResponseSet.Add(entity);
                db.SaveChanges();

                // connect signalr (clicktocallhub).
                var signalr = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<Hubs.ClickToCallHub>();
                var clicktocall = Hubs.ClickToCall.Instance;

                // boardcasting message to client.
                clicktocall.UpdateResponse(username, entity.CallId, entity.ResponseDesc, (entity.TalkTime.HasValue ? entity.TalkTime.Value : 0), (entity.Ringtime.HasValue ? entity.Ringtime.Value : 0));

                // response.
                return Json(new { status = "Success", call_id = entity.CallId, call_date = entity.CallDate }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException ex)
            {
                // database error
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Response.StatusCode = 500;
                return Json(new { Status = "Error", Message = ex.EntityValidationErrors.First().ValidationErrors.First().ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // unexception error
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Response.StatusCode = 500;
                return Json(new { Status = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool ValidateQCallResultModel(QCallResultClass model, out string message)
        {
            message = "";
            if (model.Unique_ID == null || model.Unique_ID == string.Empty)
            {
                message += "UniqueId is required.";
                return false;
            }
            else if (model.Caller_Number == null || model.Caller_Number == string.Empty)
            {
                message += "Caller_Number is required.";
                return false;
            }
            else if (model.Unique_ID.Split('-').Length < 5)
            {
                message += "Argument of UniqueId mush be contained as least 5 arguments.";
                return false;
            }
            else return true;
        }

        public short GetResponseCode(string responseDescription)
        {
            /*
                ANSWERED Call is answered. A successful dial. The caller reached the callee.
                NOANSWERED No answer. The dial command reached its number, the number rang for too long, then
                the dial timed out.
                BUSY Busy signal. The dial command reached its number but the number is busy.
                FAILED
                This status will occur when send the dial call but it facing the network issues
                eg. Problem in telephone link, voice gateway, network connection between asterisk
                system and provide.
                CONGESTION Congestion. This status is usually a sign that the recogn number is not recognized
             */
            switch (responseDescription)
            {
                case "ANSWERED": return 1;
                case "NOANSWERED": return 2;
                case "BUSY": return 3;
                case "CONGESTION": return 4;
                default: return 0;
            }
        }

        #endregion
    }
}
