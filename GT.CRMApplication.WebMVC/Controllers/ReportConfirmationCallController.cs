﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using DM.Utilities.NetFramework.Excels.GT;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.GTApplicationDB;
using GT.CRMApplication.Helpers;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Identity;
using GT.CRMApplication.WebMVC5.Helpers;
using ClosedXML.Excel;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GT.CRMApplication.WebMVC5.ViewModels;
using System.Data.Entity;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public partial class ReportController
    {
        [HttpGet]
        public ActionResult ConfirmationCall()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ConfirmationCall(string date)
        {
            var reportdate = date.ParseToDate();
            var data = from row in db.LeadProfileSet.Include("RecentLeadAssignment").Where(c => c.ExpireDate == reportdate).GroupBy(g => new { g.ProductGroup, g.IssueDate, g.ExpireDate }).ToList()
                       select new
                       {
                           row.Key.ProductGroup,
                           IssueDate = row.Key.IssueDate.Value.ToString("dd/MM/yyyy"),
                           ExpireDate = row.Key.ExpireDate.Value.ToString("dd/MM/yyyy"),
                           Success = row.Count(c=>c.RecentLeadAssignment.ResponseCategoryCode == "2"),
                           Unsuccess = row.Count(c => c.RecentLeadAssignment.ResponseCategoryCode == "10"),
                           Letter = row.Count(c => c.RecentLeadAssignment.ResponseCategoryCode == "11"),
                           Followup = row.Count(c => c.RecentLeadAssignment.ResponseCategoryCode == "1" || c.RecentLeadAssignment.ResponseCategoryCode==""),
                       };
            return Json(data,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLetter(string product, string issue, string expire)
        {
            DateTime expiredate = expire.ParseToDate();
            DateTime issuedate = issue.ParseToDate();
            var list = db.LeadProfileSet.Where(c => c.ProductGroup == product && c.ExpireDate == expiredate && DbFunctions.TruncateTime(c.IssueDate) == issuedate && (c.RecentLeadAssignment.ResponseCategoryCode == "1" || c.RecentLeadAssignment.ResponseCategoryCode == ""));
            foreach(var item in list)
            {
                item.RecentLeadAssignment.ResponseCategoryCode = "11";
                item.RecentLeadAssignment.ResponseCategoryName = "Letter";
                item.SystemRemark = "Update Letter before report";
            }
            db.SaveChanges();
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportConfirmationCall(string product,string issue,string expire)
        {
            if (product == "NL")
            {
                return ExportConfirmationCallNL(issue, expire);
            }
            else if (product == "LIFE")
            {
                return ExportConfirmationCallOL(issue, expire);
            }
            return Json(new { error = "Wrong product type" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportConfirmationCallNL(string issue, string expire)
        {
            DateTime expiredate = expire.ParseToDate();
            DateTime issuedate = issue.ParseToDate() ;
            try
            {
                //var tsrs = iddb.ActiveTSRs().Select(s => new { CODE = s.Id, NAME = s.FirstName + " " + s.LastName });
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/Confirmation Call PA.xlsx"), XLEventTracking.Disabled))
                {
                    var list = db.LeadProfileSet.Where(c => c.ProductGroup == "NL" && c.ExpireDate == expiredate && DbFunctions.TruncateTime(c.IssueDate) == issuedate).ToList();
                    var data = from row in list
                               let raw = JObject.Parse(row.Raw)
                               select new
                               {
                                   TM = row.TM,
                                   Item = raw["Item"]?.ToString(),
                                   Status = raw["ContractStatus"]?.ToString(),
                                   row.PolicyNo,
                                   row.ApplicationNo,
                                   row.PlanCode,
                                   row.AgentCode,
                                   row.AgentName,
                                   row.FirstName,
                                   row.TelPhone,
                                   Inception = row.InceptionDate.Value.ToString("yyyy-MM-dd"),
                                   Issue = row.IssueDate.Value.ToString("yyyy-MM-dd"),
                                   row.Address,
                                   ct = raw["ContractType"].ToString(),
                                   rt = raw["RiskType"].ToString(),
                                   row.NetPremium,
                                   Gross = raw["GrossPremium"].ToString().ParseToPositiveDecimal(),
                                   Stamp = raw["Stamp"].ToString().ParseToPositiveDecimal(),
                                   Disc = raw["Disc"].ToString().ParseToPositiveDecimal(),
                                   SBT = raw["SBT"].ToString().ParseToPositiveDecimal(),
                                   VAT = raw["VAT"].ToString().ParseToPositiveDecimal(),
                                   row.PostTracking,
                                   PostDate = row.PostDate,//.HasValue ? row.PostDate.Value.ToString("yyyy-MM-dd") : "",
                                   CallDate = raw["CallDate"].ToString(),
                                   row.RecentLeadAssignment.ConfirmationCallResult,
                                   ExpireDate = row.ExpireDate,//.Value.ToString("yyyy-MM-dd"),
                                   row.RecentLeadAssignment.ReceiverUserName,
                                   row.RecentLeadAssignment.ResponseReason,
                                   row.RecentLeadAssignment.CallCount
                               };
                    wb.Worksheet(1).Cell(2, 1).InsertData(data);
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Confirmation Call Non Life {0} Reply {1}.xlsx", issuedate.ToString("ddMMyyyy"), expiredate.ToString("ddMMyyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename));
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error=ex.Message}, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult ExportConfirmationCallOL(string issue, string expire)
        {
            DateTime expiredate = expire.ParseToDate();
            DateTime issuedate = issue.ParseToDate();
            try
            {
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/Confirmation Call OL.xlsx"), XLEventTracking.Disabled))
                {
                    var data = from row in db.LeadProfileSet.Where(c => c.ProductGroup == "LIFE" && c.ExpireDate == expiredate && DbFunctions.TruncateTime(c.IssueDate) == issuedate).ToList()
                               let raw = JsonConvert.DeserializeObject<LifeInputLead>(row.Raw) //JObject.Parse(row.Raw)
                               select new
                               {
                                   TM = row.TM,
                                   Case = "",
                                   Item = raw.Item,
                                   Status = raw.Status,
                                   row.PolicyNo,
                                   row.ApplicationNo,
                                   row.Planname,
                                   row.NetPremium,
                                   row.SumInsure,
                                   row.FirstName,
                                   row.TelPhone,
                                   row.AgentName,
                                   M="",
                                   raw.IssueDate,
                                   raw.Payplan,
                                   row.Address,
                                   row.PostTracking,
                                   row.PostDate,//.HasValue ? row.PostDate.Value.ToString("yyyy-MM-dd") : "",
                                   CallDate = raw.CallDate,
                                   row.RecentLeadAssignment.ConfirmationCallResult,
                                   ExpireDate = row.ExpireDate.Value.ToString("yyyy-MM-dd"),
                                   row.RecentLeadAssignment.ReceiverUserName,
                                   row.RecentLeadAssignment.ResponseReason,
                                   //row.RecentLeadAssignment.CallCount
                               };
                    wb.Worksheet(1).Cell(2, 1).InsertData(data);
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Confirmation Call Life {0} Reply {1}.xlsx", issuedate.ToString("ddMMyyyy"), expiredate.ToString("ddMMyyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename));
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult RawDataReport()
        {
            return View();
        }
        public ActionResult ExportRawData(string startdate,string enddate)
        {
            try
            {
                var d1 = startdate.ParseToDate();
                var d2 = enddate.ParseToDate().AddHours(23);
                var data = from row in db.LeadProfileSet
                           where row.IssueDate>=d1 && row.IssueDate<=d2
                           group row by new { row.RecentLeadAssignment.ReceiverUserId, row.RecentLeadAssignment.ReceiverUserName, row.IssueDate,row.ProductGroup,row.Product,row.ExpireDate, CreatedDate=DbFunctions.TruncateTime(row.CreatedDate) } into g
                           select new
                           {
                               g.Key.IssueDate,
                               g.Key.ProductGroup,
                               g.Key.Product,
                               g.Key.ReceiverUserId,
                               g.Key.ReceiverUserName,
                               g.Key.CreatedDate,
                               g.Key.ExpireDate,
                               policy = g.Count(),
                               success = g.Where(c=>c.RecentLeadAssignment.ResponseCategoryCode == "2").Count(),
                               unsuccess = g.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "10").Count(),
                               letter = g.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "11").Count(),
                           };
                if (User.IsTSR())
                {
                    var userid = User.Identity.GetUserId();
                    data = data.Where(c => c.ReceiverUserId == userid);
                }
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/Raw.xlsx"), XLEventTracking.Disabled))
                {
                    var ws = wb.Worksheet(1);
                    var irow = 2;
                    foreach(var row in data.OrderBy(o=>o.IssueDate))
                    {
                        ws.Cell(irow, 3).Value = row.IssueDate;
                        ws.Cell(irow, 4).Value = row.ProductGroup;
                        ws.Cell(irow, 5).Value = row.Product ?? row.ProductGroup;
                        ws.Cell(irow, 6).Value = row.ReceiverUserName;
                        ws.Cell(irow, 7).Value = row.CreatedDate;
                        ws.Cell(irow, 8).Value = row.ExpireDate;
                        ws.Cell(irow, 10).Value = row.ExpireDate;
                        ws.Cell(irow, 13).Value = row.policy;
                        ws.Cell(irow, 14).Value = row.success;
                        ws.Cell(irow, 15).Value = row.unsuccess;
                        ws.Cell(irow, 16).Value = row.letter;
                        ws.Cell(irow, 20).Value = row.IssueDate.Value.ToString("yyyyMM");
                        ws.Cell(irow, 21).Value = row.ExpireDate.Value.ToString("yyyyMM");
                        ws.Cell(irow, 22).Value = row.ExpireDate.Value.ToString("yyyyMM");
                        irow++;
                    }

                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Raw_{0}_{1}.xlsx", d1.ToString("ddMMyyyy"), d2.ToString("ddMMyyyy"));
                    
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename));
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DailyReport()
        {
            return View();
        }

        public ActionResult RetrieveDailyReport(string reportdate)
        {
            try
            {
                var rdate = reportdate.ParseToDate();
                var data = from row in db.LeadProfileSet
                           where DbFunctions.TruncateTime(row.CreatedDate) == rdate
                           group row by new { row.RecentLeadAssignment.ReceiverUserId, row.RecentLeadAssignment.ReceiverUserName, CreatedDate = DbFunctions.TruncateTime(row.CreatedDate) } into g
                           select new
                           {
                               g.Key.ReceiverUserName,
                               Life = g.Where(c => c.ProductGroup == "LIFE").Count(),
                               Cancer = g.Where(c => c.Product == "CANCER").Count(),
                               Health = g.Where(c => c.Product == "HEALTH").Count(),
                               PA = g.Where(c => c.Product == "PA").Count(),
                               Total = g.Count()
                           };
                var pdata = data.ToList();
                if (pdata.Count > 0)
                {
                    pdata.Add(new
                    {
                        ReceiverUserName = "Total",
                        Life = data.Sum(s => s.Life),
                        Cancer = data.Sum(s => s.Cancer),
                        Health = data.Sum(s => s.Health),
                        PA = data.Sum(s => s.PA),
                        Total = data.Sum(s => s.Total),
                    });
                }
                return Json(pdata, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportDailyReport(string reportdate)
        {
            try
            {
                var rdate = reportdate.ParseToDate();
                var data = from row in db.LeadProfileSet
                           where DbFunctions.TruncateTime(row.CreatedDate) == rdate
                           group row by new { row.RecentLeadAssignment.ReceiverUserId, row.RecentLeadAssignment.ReceiverUserName, CreatedDate = DbFunctions.TruncateTime(row.CreatedDate) } into g
                           select new
                           {
                               g.Key.ReceiverUserName,
                               Life = g.Where(c => c.ProductGroup == "LIFE").Count(),
                               Cancer = g.Where(c => c.Product == "CANCER").Count(),
                               Health = g.Where(c => c.Product == "HEALTH").Count(),
                               PA = g.Where(c => c.Product == "PA").Count(),                               
                               Total = g.Count()
                           };
                var pdata = data.ToList();
                if (pdata.Count > 0)
                {
                    pdata.Add(new
                    {
                        ReceiverUserName = "Total",
                        Life = data.Sum(s => s.Life),
                        Cancer = data.Sum(s => s.Cancer),
                        Health = data.Sum(s => s.Health),
                        PA = data.Sum(s => s.PA),
                        Total = data.Sum(s => s.Total),
                    });
                }
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/DailyAssign.xlsx"), XLEventTracking.Disabled))
                {
                    wb.Worksheet(1).Cell(1, 1).Value = reportdate;
                    wb.Worksheet(1).Cell(2, 1).InsertData(pdata);
                    
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Daily report confirmation call {0}.xlsx", rdate.ToString("ddMMyyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename));
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult RetrieveInvoice(string startdate, string enddate)
        {
            try
            {
                var d1 = startdate.ParseToDate();
                var d2 = enddate.ParseToDate().AddHours(23);
                var tsr = "";
                if (User.IsTSR())
                {
                    tsr = User.Identity.GetUserId();
                }

                IEnumerable<InvoiceVM> data = QueryInvoiceReport(d1, d2, tsr);
                var pdata = data;

                return Json(pdata, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<InvoiceVM> QueryInvoiceReport(DateTime d1, DateTime d2, string tsr)
        {
            return db.LeadProfileSet.Include(i => i.RecentLeadAssignment).Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "2"
                                && c.RecentLeadAssignment.CallDate >= d1 && c.RecentLeadAssignment.CallDate < d2
                                && (tsr == "" || c.RecentLeadAssignment.ReceiverUserId == tsr)
                            ).Select(s => new { s.RecentLeadAssignment.ReceiverUserId, s.RecentLeadAssignment.ReceiverUserName, s.ProductGroup, s.Product })
                            .ToList().GroupBy(g => new { g.ReceiverUserId, g.ReceiverUserName })
                                .Select((g, index) => new InvoiceVM(
            index + 1,
            g.Key.ReceiverUserName,
            g.Where(c => c.ProductGroup == "LIFE").Count(),
            g.Where(c => c.Product == "CANCER").Count(),
            g.Where(c => c.Product == "HEALTH").Count(),
            g.Where(c => c.Product == "PA").Count(),
            g.Count(),
            g.Count() * 3,
            g.Count() * 3
                                ));
        }

        public ActionResult ExportInvoice(string startdate,string enddate)
        {
            try
            {
                var d1 = startdate.ParseToDate();
                var d2 = enddate.ParseToDate().AddHours(23);
                var tsr = "";
                if (User.IsTSR())
                {
                    tsr = User.Identity.GetUserId();
                }
                
                IEnumerable<InvoiceVM> data = QueryInvoiceReport(d1, d2, tsr);
                var pdata = data;

                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/InvoiceTemplate.xlsx"), XLEventTracking.Disabled))
                {
                    var ws = wb.Worksheet(1);
                    ws.Cell(2, 1).InsertData(pdata);
                    if (tsr == "")
                    {
                        var irow = ws.LastRowUsed().RowNumber()+1;
                        ws.Cell(irow, 2).Value = "Total";
                        ws.Cell(irow, 3).Value = pdata.Sum(s => s.Life);
                        ws.Cell(irow, 4).Value = pdata.Sum(s => s.Cancer);
                        ws.Cell(irow, 5).Value = pdata.Sum(s => s.Health);
                        ws.Cell(irow, 6).Value = pdata.Sum(s => s.PA);
                        ws.Cell(irow, 7).Value = pdata.Sum(s => s.TotalCase);
                        ws.Cell(irow, 8).Value = pdata.Sum(s => s.Incentive);
                        ws.Cell(irow, 9).Value = pdata.Sum(s => s.Total);
                    }
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Invoice {0}_{1}.xlsx", d1.ToString("ddMMyyyy"), d2.ToString("ddMMyyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename));
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Productivity()
        {
            return View();
        }

        public ActionResult RetrieveProductivity(string reportmonth)
        {
            try
            {
                var reportdate = DateTime.ParseExact(reportmonth, "MMMM yyyy", CultureInfo.InvariantCulture);
                var d1 = reportdate;
                var d2 = reportdate.AddMonths(1).AddDays(-1).AddHours(23);
                IEnumerable<ProductivityVM> data = QueryProductivityReport(d1, d2);
                return Json(data, JsonRequestBehavior.AllowGet);                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ExportProductivity(string reportmonth)
        {
            try
            {
                var reportdate = DateTime.ParseExact(reportmonth, "MMMM yyyy", CultureInfo.InvariantCulture);
                var d1 = reportdate;
                var d2 = reportdate.AddMonths(1).AddDays(-1).AddHours(23);
                IEnumerable<ProductivityVM> data = QueryProductivityReport(d1, d2);
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/Productivity.xlsx"), XLEventTracking.Disabled))
                {
                    var ws = wb.Worksheet(1);
                    if (data.Count() > 2)
                    {
                        ws.Row(9).InsertRowsBelow(data.Count() - 2);
                        ws.Row(3).InsertRowsBelow(data.Count() - 2);
                        var irow = 9 + (data.Count() - 2);
                        for (var i = 1; i <= data.Count() - 2; i++)
                        {
                            ws.Row(irow).CopyTo(ws.Row(irow + i));
                        }
                    }

                    wb.Worksheet(1).Cell(1, 1).Value = d1;
                    wb.Worksheet(1).Cell(3, 1).InsertData(data);
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Productivity report {0}.xlsx", d1.ToString("MMM-yyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename), false, true);
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<ProductivityVM> QueryProductivityReport(DateTime d1, DateTime d2)
        {
            return db.LeadProfileSet.Include(i => i.RecentLeadAssignment).Where(c => c.IssueDate >= d1 && c.IssueDate < d2
            && (c.RecentLeadAssignment.ResponseCategoryCode == "2" || c.RecentLeadAssignment.ResponseCategoryCode == "10" || c.RecentLeadAssignment.ResponseCategoryCode == "11")
            ).Select(s => new { s.RecentLeadAssignment.ReceiverUserId, s.RecentLeadAssignment.ReceiverUserName, s.RecentLeadAssignment.ResponseCategoryCode, s.Product, s.ProductGroup })
            .ToList().GroupBy(g => new { g.ReceiverUserId, g.ReceiverUserName })
            .Select((g, index) => new ProductivityVM(
                    g.Key.ReceiverUserName,
                    g.Where(c => c.ProductGroup == "LIFE" && c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.ProductGroup == "LIFE" && c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.ProductGroup == "LIFE" && c.ResponseCategoryCode == "11").Count(),
                    g.Where(c => c.ProductGroup == "LIFE").Count(),
                    g.Where(c => c.Product == "CANCER" && c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.Product == "CANCER" && c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.Product == "CANCER" && c.ResponseCategoryCode == "11").Count(),
                    g.Where(c => c.Product == "CANCER").Count(),
                    g.Where(c => c.Product == "HEALTH" && c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.Product == "HEALTH" && c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.Product == "HEALTH" && c.ResponseCategoryCode == "11").Count(),
                    g.Where(c => c.Product == "HEALTH").Count(),
                    g.Where(c => c.Product == "PA" && c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.Product == "PA" && c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.Product == "PA" && c.ResponseCategoryCode == "11").Count(),
                    g.Where(c => c.Product == "PA").Count(),
                    g.Where(c => c.ProductGroup == "NL" && c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.ProductGroup == "NL" && c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.ProductGroup == "NL" && c.ResponseCategoryCode == "11").Count(),
                    g.Where(c => c.ProductGroup == "NL").Count(),
                    g.Where(c => c.ResponseCategoryCode == "2").Count(),
                    g.Where(c => c.ResponseCategoryCode == "10").Count(),
                    g.Where(c => c.ResponseCategoryCode == "11").Count(),
                    g.Count()
                ));
        }

        public ActionResult UnSuccess()
        {
            return View();
        }

        public ActionResult ExportUnSuccess(string startdate, string enddate)
        {
            try
            {
                var d1 = startdate.ParseToDate();
                var d2 = enddate.ParseToDate().AddHours(23);
                var basedata = db.LeadProfileSet.Where(c => c.IssueDate >= d1 && c.IssueDate < d2);//.Select(s=> new { s.TM,s.ProductGroup,s.RecentLeadAssignment.ResponseCategoryCode }).ToList();
                var summary = basedata.GroupBy(g => new { g.TM, g.ProductGroup })
                              .Select(g => new {
                                              g.Key.ProductGroup,
                                              g.Key.TM,
                                              s_case = g.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "2").Count(),
                                              u_case = g.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "10").Count(),
                                              l_case = g.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == "11").Count(),
                                              total = g.Count()
                              }).Select(s => new {                                  
                                  s.TM,
                                  s.s_case,
                                  s_per = 1.0 * s.s_case/s.total,
                                  s.u_case,
                                  u_per = 1.0 * s.u_case/s.total,
                                  s.l_case,
                                  l_per = 1.0 * s.l_case/s.total,
                                  s.total,
                                  total_per = 1,
                                  s.ProductGroup
                              });
                var ulife = from row in db.UnsuccessLifeSet.Where(c => c.IssueDate >= d1 && c.IssueDate < d2).ToList()
                          select new
                          {
                              row.Product,
                              ReportMonth = string.Format("{0:MM}{0:MMM}", row.IssueDate),
                              IssueDate = row.IssueDate.ToString("dd/MM/yyyy"),
                              row.TM,
                              row.PolicyNo,
                              row.ApplicationNo,
                              row.AgentName,
                              row.FirstName,
                              Status = "U(Unsuccessful)",
                              Calldate = row.CallDate.ToString("dd/MM/yyyy"),
                              row.StatCode,
                              row.BusinessDate
                          };
                var unl = from row in db.UnsuccessNLSet.Where(c => c.IssueDate >= d1 && c.IssueDate < d2).ToList()
                          select new
                          {
                              row.Product,
                              ReportMonth = string.Format("{0:MM}{0:MMM}", row.IssueDate),
                              IssueDate = row.IssueDate.ToString("dd/MM/yyyy"),
                              row.TM,
                              row.PolicyNo,
                              row.ApplicationNo,
                              row.FirstName,
                              row.AgentName,                              
                              Status = "U(Unsuccessful)",
                              Calldate = row.CallDate.ToString("dd/MM/yyyy"),
                              row.StatCode,
                              row.BusinessDate
                          };
                using (var wb = new XLWorkbook(Server.MapPath("~/Excels/Unsuccess.xlsx"), XLEventTracking.Disabled))
                {
                    var ws = wb.Worksheet(1);
                    var slife = summary.Where(c => c.ProductGroup == "LIFE");
                    var snl = summary.Where(c => c.ProductGroup == "NL");
                    var nlrow = 11;//nl data start row
                    if (snl.Count() > 2)
                    {
                        ws.Row(nlrow).InsertRowsBelow(snl.Count() - 2);
                    }
                    if (slife.Count() > 2)
                    {
                        ws.Row(5).InsertRowsBelow(slife.Count() - 2);
                        nlrow += slife.Count() - 2;
                    }
                    ws.Cell(5, 1).InsertData(slife);
                    ws.Cell(nlrow, 1).InsertData(snl);

                    //Life
                    if (ulife.Count() > 0)
                    {
                        var bizdate = DateTime.ParseExact(ulife.First().BusinessDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                        ws = wb.Worksheet(2);
                        ws.Cell(1, 11).Value = string.Format("Status as of {0:dd/MM/yyyy}", bizdate);
                        ws.Cell(2, 1).InsertData(ulife);
                    }
                    //NL
                    if (unl.Count() > 0)
                    {
                        var bizdate = DateTime.ParseExact(unl.First().BusinessDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                        ws = wb.Worksheet(3);
                        ws.Cell(1, 12).Value = string.Format("Status as of {0:dd/MM/yyyy}",bizdate);
                        ws.Cell(2, 1).InsertData(unl);
                    }
                        

                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Unsuccess {0}_{1}.xlsx", d1.ToString("ddMMyyyy"), d2.ToString("ddMMyyyy"));
                    DeleteTemp();
                    wb.SaveAs(Server.MapPath(tempfilename),false,true);
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }            
        }

        public ActionResult DailyTrackingCallReport()
        {
            return View();
        }

        public ActionResult ExportDailyTrackingCall(string startdate, string enddate)
        {
            try
            {
                var d1 = startdate.ParseToDate();
                var d2 = enddate.ParseToDate().AddHours(23);
                using (var wb = new XLWorkbook( XLEventTracking.Disabled))
                {
                    var ws = wb.AddWorksheet("Data");

                    var data = db.QueryAsModel<DailyTrackingViewModel>("select * from VW_DAILY_TRACKING where CreatedDate between @fromdate and @todate order by CreatedDate",new { fromdate=d1,todate=d2});
                    ws.Cell(1, 1).InsertTable(data,false);
                    ws.Cell(1, 1).InsertData(new string[] { "Time", "ผู้รับมอบหมาย", "วันที่นำเข้า", "Issue date", "Reply within date", "Product", "TM", "ชื่อ", "เลขกรมธรรม์", "เบอร์", "ผลการตอบรับ", "เหตุผล", "หมายเหตุ", "นัดหมาย", "จำนวนที่ติดต่อ(วันนี้)", "จำนวนที่ติดต่อ(ทั้งหมด)" }, true);
                    ws.RangeUsed().CreateTable();
                    ws.ColumnsUsed().AdjustToContents();

                    ws = wb.AddWorksheet("Sheet1",1);
                    var sum = from row in data
                              group row by row.TSR into g
                              select new
                              {
                                  TSR = g.Key,
                                  Follow = g.Count(c => c.ResponseCategory == "Follow up"),
                                  Letter = g.Count(c => c.ResponseCategory == "Letter"),
                                  Success = g.Count(c => c.ResponseCategory == "Success"),
                                  Unsuccess = g.Count(c => c.ResponseCategory == "Unsuccess"),
                                  Total = g.Count()
                              };
                    ws.Cell("A1").Value = "Daily Tracking Call Report";
                    ws.Cell("A2").Value = "สร้าง Report เมื่อเวลา :" + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    ws.Cell("A3").Value = "ช่วงวันที่ระบุ " + startdate + " ถึง " + enddate;

                    var tb = ws.Cell(5, 1).InsertTable(sum);
                    ws.Cell(5, 2).Value = "Follow up";
                    tb.SetShowTotalsRow(true);
                    tb.Field(0).TotalsRowLabel = "Total";
                    tb.Field(1).TotalsRowFunction = XLTotalsRowFunction.Sum;
                    tb.Field(2).TotalsRowFunction = XLTotalsRowFunction.Sum;
                    tb.Field(3).TotalsRowFunction = XLTotalsRowFunction.Sum;
                    tb.Field(4).TotalsRowFunction = XLTotalsRowFunction.Sum;
                    tb.Field(5).TotalsRowFunction = XLTotalsRowFunction.Sum;

                    ws.Column(1).AdjustToContents();

                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Report Daily Tracking Call {0}_{1}.xlsx", d1.ToString("ddMMyyyy"), d2.ToString("ddMMyyyy"));
                    wb.SaveAs(Server.MapPath(tempfilename), false, true);
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TimeAttendance()
        {
            return View();
        }

        public ActionResult ExportTimeAttendance(string reportmonth)
        {
            try
            {
                var ts1 = TimeSpan.Parse("9:00");
                var ts2 = TimeSpan.Parse("18:00");
                var reportdate = DateTime.ParseExact(reportmonth, "MMMM yyyy", CultureInfo.InvariantCulture);
                var d1 = reportdate;
                var d2 = reportdate.AddMonths(1).AddDays(-1).AddHours(23);
                var data = db.QueryAsModel<TimeAttendanceViewModel>("EXEC PROC_RPT_TIME_ATTENDANCE @reportmonth",new { reportmonth=reportdate.ToString("yyyy-MM") });
                var TSRs = data.Where(c=>c.TSRName!=null).Select(s => new { s.UserId, s.TSRName } ).Distinct();
                var cal = db.SpecialCalendarSet.Where(c => c.CalendarDate >= d1 && c.CalendarDate <= d2);

                using (var wb = new XLWorkbook(XLEventTracking.Disabled))
                {
                    var ws = wb.AddWorksheet("template");
                    //prepare template
                    ws.Columns(1, 12).Width = 12;
                    ws.Cell("A1").Value= "Time Attendance Report";
                    ws.Cell("A2").Value = "ข้อมูลเดือน " + reportdate.ToString("MMM yyyy");

                    ws.Cell("A5").InsertData(new string[] { "วันที่", "วัน", "เข้า", "ออก", "สาย", "ออกก่อน", "โทรสายแรก", "โทรสายสุดท้าย", "โทรทั้งหมด", "TalkTime", "หมายเหตุ" },true);
                    ws.Range("A5","K37").Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range("A5:K5").Style.Fill.SetBackgroundColor(XLColor.FromTheme(XLThemeColor.Accent3, 0.8));

                    var startrow = 6;
                    for(var d = d1; d <= d2;d = d.AddDays(1))
                    {
                        ws.Cell(startrow, 1).Value = d.ToString("dd/MM/yyyy");
                        ws.Cell(startrow, 2).Value = d.ToString("ddd");

                        
                        if (cal.Any(c => c.CalendarDate == d))
                        {
                            var specialdate = cal.FirstOrDefault(c => c.CalendarDate == d);
                            ws.Cell(startrow, 11).Value = string.IsNullOrEmpty(specialdate.Description) ? (specialdate.HolidayCount == 1 ? "วันหยุด": "วันทำงาน") : specialdate.Description;
                            if (specialdate.HolidayCount == 1)
                            {
                                ws.Row(startrow).CellsUsed(XLCellsUsedOptions.All).Style.Fill.SetBackgroundColor(XLColor.AntiFlashWhite);
                            }
                        }
                        if ((d.IsWeekend()))
                        {
                            ws.Row(startrow).CellsUsed(XLCellsUsedOptions.All).Style.Fill.SetBackgroundColor(XLColor.AntiFlashWhite);
                        }
                        startrow++;
                    }
                    var totalrow = startrow;
                    ws.Cell(totalrow, 1).Value = "Total";

                    //write data
                    foreach(var tsr in TSRs)
                    {
                        startrow = 6;
                        
                        ws = wb.Worksheet(1).CopyTo(tsr.TSRName);
                        ws.Cell("A3").Value = "TSR: " + tsr.TSRName;
                        var tsrdata = data.Where(c => c.UserId == tsr.UserId).OrderBy(o => o.Date);
                        var ds = from row in tsrdata
                                 select new
                                 {
                                     LoggedIn = row.LoggedIn?.ToString("HH:mm:ss"),
                                     LoggedOut = row.LoggedOut?.ToString("HH:mm:ss"),
                                     Late = row.Late(ts1)?.ToString(@"hh\:mm\:ss"),
                                     EarlyLeave = row.EarlyLeave(ts2)?.ToString(@"hh\:mm\:ss"),
                                     FirstCall = row.FirstCall.HasValue ? row.FirstCall.Value.ToString("HH:mm:ss") : "",
                                     LastCall = row.LastCall.HasValue ? row.LastCall.Value.ToString("HH:mm:ss") : "",
                                     row.TotalCall,
                                     TalkTime = row.TalkTime.HasValue? TimeSpan.FromSeconds(row.TalkTime ?? 0).ToString("c") : "",
                                     Remark = row.LeaveRemark ?? row.DateRemark ?? ((row.LoggedIn == null && row.FirstCall == null && row.HolidayCount != 1 && !row.Date.IsWeekend() && row.Date < DateTime.Today) ? "ขาด" : "")
                                 };
                        ws.Cell("C6").InsertData(ds);
                        ws.Cell(totalrow, 2).Value = tsrdata.Count(c => c.LoggedIn.HasValue || c.FirstCall.HasValue);
                        ws.Cell(totalrow, 5).Value = TimeSpan.FromSeconds(tsrdata.Sum(s => s.Late(ts1)?.TotalSeconds ?? 0)).ToString(@"hh\:mm\:ss");
                        ws.Cell(totalrow, 6).Value = TimeSpan.FromSeconds(tsrdata.Sum(s => s.EarlyLeave(ts2)?.TotalSeconds ?? 0)).ToString(@"hh\:mm\:ss");
                        ws.Cell(totalrow, 9).Value = tsrdata.Sum(s => s.TotalCall);
                        ws.Cell(totalrow, 10).Value = TimeSpan.FromSeconds(tsrdata.Sum(s => s.TalkTime ?? 0)).ToString("c");

                        foreach (var item in tsrdata)
                        {
                            if (item.LeaveRemark != null)
                            {
                                ws.Row(startrow + item.Date.Day - 1).CellsUsed(XLCellsUsedOptions.All).Style.Fill.SetBackgroundColor(XLColor.FromTheme(XLThemeColor.Accent6, 0.8));
                            }else if (item.IsAbsent())
                            {
                                ws.Row(startrow + item.Date.Day - 1).CellsUsed(XLCellsUsedOptions.All).Style.Fill.SetBackgroundColor(XLColor.FromTheme(XLThemeColor.Accent2, 0.8));
                            }
                        }
                    }
                    wb.Worksheet(1).Delete();
                    var filename = Url.Content("~/Downloads/") + string.Format("TimeAttendance {0}.xlsx", d1.ToString("MMM yyyy"));
                    wb.SaveAs(Server.MapPath(filename));
                    return Json(filename, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }

    internal class InvoiceVM
    {
        public int Index { get; }
        public string TSR { get; }
        public int Life { get; }
        public int Cancer { get; }
        public int Health { get; }
        public int PA { get; }
        public int TotalCase { get; }
        public int Incentive { get; }
        public int Total { get; }

        public InvoiceVM(int index, string tSR, int life, int cancer, int health, int pA, int totalCase, int incentive, int total)
        {
            Index = index;
            TSR = tSR;
            Life = life;
            Cancer = cancer;
            Health = health;
            PA = pA;
            TotalCase = totalCase;
            Incentive = incentive;
            Total = total;
        }

        public override bool Equals(object obj)
        {
            return obj is InvoiceVM other &&
                   Index == other.Index &&
                   TSR == other.TSR &&
                   Life == other.Life &&
                   Cancer == other.Cancer &&
                   Health == other.Health &&
                   PA == other.PA &&
                   TotalCase == other.TotalCase &&
                   Incentive == other.Incentive &&
                   Total == other.Total;
        }

        public override int GetHashCode()
        {
            int hashCode = -360900210;
            hashCode = hashCode * -1521134295 + Index.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TSR);
            hashCode = hashCode * -1521134295 + Life.GetHashCode();
            hashCode = hashCode * -1521134295 + Cancer.GetHashCode();
            hashCode = hashCode * -1521134295 + Health.GetHashCode();
            hashCode = hashCode * -1521134295 + PA.GetHashCode();
            hashCode = hashCode * -1521134295 + TotalCase.GetHashCode();
            hashCode = hashCode * -1521134295 + Incentive.GetHashCode();
            hashCode = hashCode * -1521134295 + Total.GetHashCode();
            return hashCode;
        }
    }

    internal class ProductivityVM
    {
        public string TSR { get; }
        public int Life1 { get; }
        public int Life2 { get; }
        public int Life3 { get; }
        public int Life { get; }
        public int CANCER1 { get; }
        public int CANCER2 { get; }
        public int CANCER3 { get; }
        public int Cancer { get; }
        public int HEALTH1 { get; }
        public int HEALTH2 { get; }
        public int HEALTH3 { get; }
        public int Health { get; }
        public int PA1 { get; }
        public int PA2 { get; }
        public int PA3 { get; }
        public int PA { get; }
        public int NL1 { get; }
        public int NL2 { get; }
        public int NL3 { get; }
        public int NL { get; }
        public int Total1 { get; }
        public int Total2 { get; }
        public int Total3 { get; }
        public int Total { get; }

        public ProductivityVM(string tSR, int life1, int life2, int life3, int life, int cANCER1, int cANCER2, int cANCER3, int cancer, int hEALTH1, int hEALTH2, int hEALTH3, int health, int pA1, int pA2, int pA3, int pA, int nL1, int nL2, int nL3, int nL, int total1, int total2, int total3, int total)
        {
            TSR = tSR;
            Life1 = life1;
            Life2 = life2;
            Life3 = life3;
            Life = life;
            CANCER1 = cANCER1;
            CANCER2 = cANCER2;
            CANCER3 = cANCER3;
            Cancer = cancer;
            HEALTH1 = hEALTH1;
            HEALTH2 = hEALTH2;
            HEALTH3 = hEALTH3;
            Health = health;
            PA1 = pA1;
            PA2 = pA2;
            PA3 = pA3;
            PA = pA;
            NL1 = nL1;
            NL2 = nL2;
            NL3 = nL3;
            NL = nL;
            Total1 = total1;
            Total2 = total2;
            Total3 = total3;
            Total = total;
        }

        public override bool Equals(object obj)
        {
            return obj is ProductivityVM other &&
                   TSR == other.TSR &&
                   Life1 == other.Life1 &&
                   Life2 == other.Life2 &&
                   Life3 == other.Life3 &&
                   Life == other.Life &&
                   CANCER1 == other.CANCER1 &&
                   CANCER2 == other.CANCER2 &&
                   CANCER3 == other.CANCER3 &&
                   Cancer == other.Cancer &&
                   HEALTH1 == other.HEALTH1 &&
                   HEALTH2 == other.HEALTH2 &&
                   HEALTH3 == other.HEALTH3 &&
                   Health == other.Health &&
                   PA1 == other.PA1 &&
                   PA2 == other.PA2 &&
                   PA3 == other.PA3 &&
                   PA == other.PA &&
                   NL1 == other.NL1 &&
                   NL2 == other.NL2 &&
                   NL3 == other.NL3 &&
                   NL == other.NL &&
                   Total1 == other.Total1 &&
                   Total2 == other.Total2 &&
                   Total3 == other.Total3 &&
                   Total == other.Total;
        }

        public override int GetHashCode()
        {
            int hashCode = -2064271392;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TSR);
            hashCode = hashCode * -1521134295 + Life1.GetHashCode();
            hashCode = hashCode * -1521134295 + Life2.GetHashCode();
            hashCode = hashCode * -1521134295 + Life3.GetHashCode();
            hashCode = hashCode * -1521134295 + Life.GetHashCode();
            hashCode = hashCode * -1521134295 + CANCER1.GetHashCode();
            hashCode = hashCode * -1521134295 + CANCER2.GetHashCode();
            hashCode = hashCode * -1521134295 + CANCER3.GetHashCode();
            hashCode = hashCode * -1521134295 + Cancer.GetHashCode();
            hashCode = hashCode * -1521134295 + HEALTH1.GetHashCode();
            hashCode = hashCode * -1521134295 + HEALTH2.GetHashCode();
            hashCode = hashCode * -1521134295 + HEALTH3.GetHashCode();
            hashCode = hashCode * -1521134295 + Health.GetHashCode();
            hashCode = hashCode * -1521134295 + PA1.GetHashCode();
            hashCode = hashCode * -1521134295 + PA2.GetHashCode();
            hashCode = hashCode * -1521134295 + PA3.GetHashCode();
            hashCode = hashCode * -1521134295 + PA.GetHashCode();
            hashCode = hashCode * -1521134295 + NL1.GetHashCode();
            hashCode = hashCode * -1521134295 + NL2.GetHashCode();
            hashCode = hashCode * -1521134295 + NL3.GetHashCode();
            hashCode = hashCode * -1521134295 + NL.GetHashCode();
            hashCode = hashCode * -1521134295 + Total1.GetHashCode();
            hashCode = hashCode * -1521134295 + Total2.GetHashCode();
            hashCode = hashCode * -1521134295 + Total3.GetHashCode();
            hashCode = hashCode * -1521134295 + Total.GetHashCode();
            return hashCode;
        }
    }
}