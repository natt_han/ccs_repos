﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.Common;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class NotificationController : Controller
    {
        public class NotificationViewModel
        {
            public string Title { get; set; }
            public string Body { get; set; }
            public string Url { get; set; }
            public string Tag { get; set; }
            public bool requireInteraction { get; set; }
            public int Timeout { get; set; }
            public int Priority { get; set; }
            public string Type { get; set; }

            public NotificationViewModel()
            {
                Type = "alert";
            }
        }

        public class NotyViewModel
        {
            public string tag { get; set; }
            public string type { get; set; } 
            public string text { get; set; }
            public string url { get; set; }
            
            public int timeout { get; set; }

            public NotyViewModel()
            {
                type="";
            }
        }

        public Dictionary<string,NotificationViewModel> Notifications {
            get { return (Dictionary<string,NotificationViewModel>)Session["Notification"]; }
            set { Session["Notification"] = value; }
        }

        public long LastFetch
        {
            get {
                var temp = Session["lastnotifetch"];
                return (temp == null ? 0 : ParseLong(Session["lastnotifetch"].ToString()));                
            }
            set { Session["lastnotifetch"] = value; }
        }
        
       
	

        private long onesec = 10000000;
        // GET: Notification
        public ActionResult Fetch(bool force=true)
        {
            string[] motor = { "13", "15" };//{ "12" };
            var noti = new List<NotificationViewModel>();
            if (User.Identity.IsAuthenticated)
            {
                var lastfetch = LastFetch;
                var today = DateTime.Today;
                if (force) lastfetch = 0;
                var timelapse = DateTime.Now.Ticks - lastfetch;                
                if (force || timelapse > (300 * onesec))
                {
                    var lastfetchtime = new DateTime(lastfetch);
                    if (lastfetch == 0) lastfetch = DateTime.Now.AddMinutes(-5).Ticks;
                    if (User.IsInRole("SUPERVISOR"))
                    {
                        
                    }
                    else if (User.IsTSR())
                    {
                        using (var db = new IGTCRMContext())
                        {
                            var userid = User.Identity.GetUserId();
                            var lasthour = DateTime.Now.AddHours(-10);

                            
                            var time = DateTime.Now.ToString("HH:mm");
                            var appointment = db.Prospect_AppointmentSet
                                      .Where(c => c.AppointmentDate == DateTime.Today && c.AppointmentTime.CompareTo(time) <= 0
                                          && c.IsReply == false
                                          && c.IsCancel == false
                                          && c.Prospect_Activity.CreatedBy == userid).OrderBy(o=>o.AppointmentTime);
                            foreach (var item in appointment)
                            {
                                
                                noti.Add(new NotificationViewModel() { Title = String.Format("{4} ถึงเวลานัดหมายลูกค้า {0} {1} {2} เรื่อง {3}",item.Prospect_Activity.Prospect_Profile.Salutation,
                                                                       item.Prospect_Activity.Prospect_Profile.FirstName , item.Prospect_Activity.Prospect_Profile.LastName,item.Topic, item.AppointmentTime),
                                                                       Url = Url.Action("CreateFollowUpActivity", "Prospect", new { id = item.Prospect_Activity.Prospect_ProfileProspectId, appointmentcode=item.ItemId }),
                                    Tag = "appointment-" + item.ItemId
                                });
                            
                            }
                        }
                    }                    
                }
                LastFetch = DateTime.Now.Ticks;
            }
            return Json(noti, JsonRequestBehavior.AllowGet);
        }

        private long ParseLong(string val)
        {
            try{
                return long.Parse(val);
            }
            catch
            {
                return 0;
            }
            
        }
    }
}