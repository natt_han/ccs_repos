﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class ResponseCategoryController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /ResponseCategory/
        public ActionResult Index()
        {
            return View(db.ResponseCategorySet.ToList());
        }

        // GET: /ResponseCategory/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseCategory responseCategory = db.ResponseCategorySet.Find(id);
            if (responseCategory == null)
            {
                return HttpNotFound();
            }
            return View(responseCategory);
        }

        // GET: /ResponseCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ResponseCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ResponseCategoryId,Code,Name,Note,Active,Icon,IsSupportInboundCall,IsSupportOutboundCall,IsSupportFollowupCall,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate")] ResponseCategory responseCategory)
        {
            if (ModelState.IsValid)
            {
				responseCategory.CreatedBy =
				responseCategory.UpdatedBy = User.Identity.GetUserId();
				responseCategory.CreatedDate =
                responseCategory.UpdatedDate = DateTime.Now;
                db.ResponseCategorySet.Add(responseCategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(responseCategory);
        }

        // GET: /ResponseCategory/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseCategory responseCategory = db.ResponseCategorySet.Find(id);
            if (responseCategory == null)
            {
                return HttpNotFound();
            }
            return View(responseCategory);
        }

        // POST: /ResponseCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ResponseCategoryId,Code,Name,Note,Active,Icon,IsSupportInboundCall,IsSupportOutboundCall,IsSupportFollowupCall,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate")] ResponseCategory responseCategory)
        {
            if (ModelState.IsValid)
            {
				responseCategory.UpdatedBy = User.Identity.GetUserId();
			    responseCategory.UpdatedDate = DateTime.Now;
                db.Entry(responseCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(responseCategory);
        }

        // GET: /ResponseCategory/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseCategory responseCategory = db.ResponseCategorySet.Find(id);
            if (responseCategory == null)
            {
                return HttpNotFound();
            }
            return View(responseCategory);
        }

        // POST: /ResponseCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ResponseCategory responseCategory = db.ResponseCategorySet.Find(id);
            db.ResponseCategorySet.Remove(responseCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
