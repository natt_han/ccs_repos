﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.GTApplicationDB;
using GT.CRMApplication.WebMVC5.Helpers;
using System.Data.Entity.Validation;
using System.Threading;
using System.Globalization;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Threading.Tasks;
using System.Net;

using ExcelDataReader;
using System.Data;
using System.Text.RegularExpressions;
using GT.CRMApplication.WebMVC5.ViewModels;
using ClosedXML.Excel;
using Newtonsoft.Json.Linq;
using SimpleJson;
using Newtonsoft.Json;
using System.Text;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    //[Authorize]
    public class LeadAssignmentController : Controller
    {        
        private ApplicationDbContext appdb = new ApplicationDbContext();
        IGTCRMContext db = new IGTCRMContext();
        IGTCRMContext crm = new IGTCRMContext();
        ActiveUserModel _ActiveUserModel = new ActiveUserModel();

        public class SearchLeadClass
        {
            public string phonenumber { get; set; }
            public string name { get; set; }
            public string createdate { get; set; }
            public string response { get; set; }
            public string[] responses { get; set; }
            public string reason { get; set; }
            public string leadflag { get; set; }
            public string sup { get; set; }
            public string tsr { get; set; }
            public string[] tsrs { get; set; }
            public string receiver { get; set; }
            public string callcount { get; set; }
            public int? leadage1 { get; set; }
            public int? leadage2 { get; set; }
            public string daterange { get; set; }
            public string product { get; set; }
            public string tag { get; set; }
            public string leaddb { get; set; }
            public int? expirein { get; set; }
            public string calldate { get; set; }
            public string expiredate { get; set; }
            public string issuedate { get; set; }
            public string policyno { get; set; }
        }

        public class MyLeadDisplayClass
        {
            public string TM { get; set; }
            public string Name { get; set; }
            public string Telphone { get; set; }
            public string PolicyNo { get; set; }
            public string CreatedDate { get; set; }
            public string IssueDate { get; set; }
            public string ExpireDate { get; set; }
            public string CallSource { get; set; }
            public string Product { get; set; }
            public string ResponseCategoryCode { get; set; }
            public string ResponseReason { get; set; }
            public dynamic Remark { get; set; }
            public string CallDate { get; set; }
            public int? CallCount { get; set; }
            public int TodayCall { get; set; }
            public string Appointment { get; set; }
            public string AppointmentTime { get; set; }
            public string AppointmentTopic { get; set; }
            public long LeadProfileId { get; set; }
            public int Countdown { get; set; }
            public string Priority { get; set; }
            public bool IsDMC { get; set; }
            public string XSellLabel { get; set; }
            public string LeadFlag { get; set; }
            public decimal? TopConversion { get; set; }
            public string TopTsr { get; set; }
            public decimal? MyConversion { get; set; }
            public string RemainingHotTime { get; set; }
            public bool IsDownloaded { get; set; }
        }

        public class DistributeLeadViewModel
        {
            public ActiveUserModel.ActiveUserClass tsr { get; set; }
            public int leadqty { get; set; }
            public int currentlead { get; set; }
            public int OpenLead { get; set; }
            public bool isSelected { get; set; }
        }

        public LeadAssignmentController()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            db.Database.Log = sql => System.Diagnostics.Debug.Write(sql);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            appdb.Dispose();
            db.Dispose();
            crm.Dispose();
        }

        #region Action

        // GET: LeadProfile
        public ActionResult Index(string tag)
        {
            if (!User.IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            SearchLeadClass criteria = new SearchLeadClass() { createdate = DateTime.Today.ToShortDateString() };
            
            LoadTsr();
            LoadResponseExtend();
            LoadReason();
            ViewBag.SearchData = criteria;
            return View();
        }

        [HttpPost]
        public ActionResult Search(SearchLeadClass criteria)
        {
            var createdate = criteria.createdate.ParseToDate();
            var leadset = db.LeadProfileSet.ActiveSet();
            if (!String.IsNullOrEmpty(criteria.createdate)) leadset = leadset.CreateDateInRange(criteria.createdate);// leadset.Where(x => DbFunctions.TruncateTime(x.CreatedDate) == createdate);
            if (!String.IsNullOrEmpty(criteria.name)) leadset = leadset.Where(x => (x.FirstName + " " + x.LastName).Contains(criteria.name));
            if (!String.IsNullOrEmpty(criteria.phonenumber))
            {
                var telephone = criteria.phonenumber.Replace(" ", "");
                List<long> prospectids = new List<long>();
                leadset = leadset.Where(x => x.TelPhone.Replace(" ", "").Contains(telephone));
            }
            if (criteria.responses != null) leadset = leadset.Where(c => criteria.responses.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
            if (!String.IsNullOrEmpty(criteria.leadflag)) leadset = leadset.Where(c => c.LeadFlag == criteria.leadflag);
            if (!String.IsNullOrEmpty(criteria.tsr)) leadset = leadset.LastAssignTo(criteria.tsr);
            if (!String.IsNullOrEmpty(criteria.receiver)) leadset = leadset.Where(c => c.RecentLeadAssignment.ReceiverUserId == criteria.receiver);
            ViewBag.SearchData = criteria;
            LoadTsr();
            LoadLeadFlag("N");
            LoadResponseExtend(criteria.response);
            return View("Index",leadset.ToList());
        }

        public ActionResult RetrieveLeads(SearchLeadClass criteria)
        {
            var createdate = criteria.createdate.ParseToDate();
            var leadset = db.LeadProfileSet.ActiveSet();
            if (!string.IsNullOrEmpty(criteria.product)) leadset = leadset.Where(c => c.ProductGroup == criteria.product);
            if (!String.IsNullOrEmpty(criteria.createdate)) leadset = leadset.CreateDateInRange(criteria.createdate);
            if (!String.IsNullOrEmpty(criteria.issuedate)) leadset = leadset.IssueDateInRange(criteria.issuedate);
            if (!String.IsNullOrEmpty(criteria.expiredate)) leadset = leadset.ExpireDateInRange(criteria.expiredate);
            if (!String.IsNullOrEmpty(criteria.name)) leadset = leadset.Where(x => x.FirstName.Contains(criteria.name));
            if (!String.IsNullOrEmpty(criteria.phonenumber))
            {
                var telephone = criteria.phonenumber.Replace(" ", "");
                leadset = leadset.Where(x => x.TelPhone.Contains(telephone));
            }
            
            //if (criteria.responses != null) leadset = leadset.Where(c => criteria.responses.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
            if (!String.IsNullOrEmpty(criteria.response))
            {
                var responses = criteria.response.Replace("[", "").Replace("]", "").Split(',');
                leadset = leadset.Where(c => responses.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
            }
            if (!String.IsNullOrEmpty(criteria.reason))
            {
                var reasons = criteria.reason.Replace("[", "").Replace("]", "").Split(',');
                leadset = leadset.Where(c => reasons.Contains(c.RecentLeadAssignment.ResponseReasonCode));
            }
            if (!String.IsNullOrEmpty(criteria.tsr))
            {
                var tsrs = criteria.tsr.Split(',');
                leadset = leadset.LastAssignTo(tsrs);
            }
            if (!String.IsNullOrEmpty(criteria.receiver))
            {
                var tsrs = criteria.receiver.Split(',');
                leadset = leadset.Where(c => tsrs.Contains(c.RecentLeadAssignment.ReceiverUserId));
            }
            //if (!String.IsNullOrEmpty(criteria.receiver)) leadset = leadset.Where(c => c.RecentLeadAssignment.ReceiverUserId == criteria.receiver);

            if (criteria.tag == "newlead")
            {
                leadset = db.LeadProfileSet.WaitingForAssign(30);
            }
            else if (criteria.tag == "expire")
            {
                var today = DateTime.Today;
                leadset = db.LeadProfileSet.ActiveSet().Where(c=>c.ExpireDate == today && (c.RecentLeadAssignment.ResponseCategoryCode==""||c.RecentLeadAssignment.ResponseCategoryCode=="1"));
            }
            var showsimplesource = User.IsSimpleSource();
            var usecol = from row in leadset
                         select new
                         {
                             row.LeadProfileId,
                             row.TM,
                             row.FirstName,
                             row.LastName,
                             row.TelPhone,
                             row.PolicyNo,
                             row.CreatedDate,
                             row.ExpireDate,
                             row.ProductGroup,
                             row.RecentLeadAssignment.ResponseCategoryCode,
                             row.RecentLeadAssignment.ResponseReason,
                             row.RecentLeadAssignment.Detail,
                             row.RecentLeadAssignment.CallDate,
                             row.RecentLeadAssignment.CallCount,
                             row.RecentLeadAssignment.ReceiverUserId,
                             row.RecentLeadAssignment.ReceiverUserName,
                         };
            var result = from row in usecol.ToList()
                         select new
                         {
                             row.LeadProfileId,
                             row.TM,
                             fullname = row.FirstName + " " + row.LastName,
                             telphone = row.TelPhone,
                             policyno = row.PolicyNo,
                             CreatedDate = row.CreatedDate.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture),
                             ExpireDate = row.ExpireDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                             row.ProductGroup,
                             row.ResponseCategoryCode,
                             row.ResponseReason,
                             row.Detail,
                             CallDate = row.CallDate.HasValue ? row.CallDate.Value.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture) : "",
                             row.CallCount,
                             ReceiverUserName = row.ReceiverUserId==""?"":row.ReceiverUserName
                         };
            return Json(result,JsonRequestBehavior.AllowGet);
        }



        public ActionResult CreateLeadProfile()
        {
            LeadProfile newprofile = new LeadProfile();
            this.PopulateProfile(newprofile);            
            return View(newprofile);
        }
        public ActionResult EditProfile(int? id)
        {
            if (id.IsNullOrEmpty())
            {
                return HttpNotFound("Id that you search is not found.");
            }
            LeadProfile profile = db.LeadProfileSet.SingleOrDefault(item => item.LeadProfileId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }
            this.PopulateProfile(profile);
            return View(profile);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeadProfile lead = await db.LeadProfileSet.FindAsync(id);
            if (lead == null)
            {
                return HttpNotFound();
            }
            return View(lead);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            LeadProfile lead = await db.LeadProfileSet.FindAsync(id);
            ClearWaitingAppointment(lead);
            lead.Status = false;
            //lead.LeadFlag = "D";
            lead.UpdatedDate = DateTime.Now;
            lead.UpdatedBy = User.Identity.GetUserId();
            lead.UnAssign(db);
            db.Database.ExecuteSqlCommand("UPDATE [dbo].LeadNotificationSet SET IsActive=0 where CurrentLeadProfileId="+id);

            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteClear(int id)
        {
            LeadProfile lead = await db.LeadProfileSet.FindAsync(id);
            ClearWaitingAppointment(lead);
            lead.Status = false;
            lead.LeadFlag = "D";
            lead.UpdatedDate = DateTime.Now;
            lead.UpdatedBy = User.Identity.GetUserId();
            lead.UnAssign(db);
            db.Database.ExecuteSqlCommand("UPDATE [dbo].LeadNotificationSet SET IsActive=0 where CurrentLeadProfileId=" + id);
            await db.SaveChangesAsync();
            db.Database.ExecuteSqlCommand("DELETE [dbo].LeadAssignmentSet where [LeadProfileLeadProfileId]=" + id);
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAssignment(int id)
        {
            var item = db.LeadAssignmentSet.Find(id);
            var profileid = item.LeadProfileLeadProfileId;
            db.LeadAssignmentSet.Remove(item);
            db.SaveChanges();
            return RedirectToAction("AssignLead", new{id=profileid});
        }

        public ActionResult ToggleAssignmentFlag(int id)
        {
            var item = db.LeadAssignmentSet.Find(id);
            if (item.LeadFlag == "O")
            {
                item.LeadFlag = "N";
            }
            else
            {
                item.LeadFlag = "O";
            }
            db.SaveChanges();
            return RedirectToAction("AssignLead", new { id = item.LeadProfileLeadProfileId });
        }

        public ActionResult ToggleLeadFlag(int id)
        {
            var item = db.LeadProfileSet.Find(id);
            item.LeadFlag = "N";
            db.SaveChanges();
            return RedirectToAction("AssignLead", new { id = item.LeadProfileId });
        }

        public ActionResult MakeActiveAssignment(int id)
        {
            var item = db.LeadAssignmentSet.Find(id);
            item.UnAssignDate = null;
            var recent = item.LeadProfile.RecentLeadAssignment;
            recent.LeadAssignment_LeadAssignmentId = id;
            recent.ReceiverUserId = item.ReceiverUserId;
            recent.ReceiverUserName = item.ReceiverUserName;
            recent.AssignUserId = item.AssignUserId;
            recent.AssignDate = item.AssignDate;
            recent.CallCount = db.Prospect_ActivitySet.Count(c => c.LeadAssignmentId == item.LeadAssignmentId);
            recent.CallDate = item.CallDate;
            recent.ActivityCode = item.ActivityCode;
            recent.Activity = item.Activity;
            recent.ResponseCategoryCode = item.ResponseCategoryCode;
            recent.ResponseCategoryName = item.ResponseCategoryName;
            recent.ResponseReasonCode = item.ResponseReasonCode;
            recent.ResponseReason = item.ResponseReason;
            recent.Detail = item.Detail;

            db.SaveChanges();
            return RedirectToAction("AssignLead", new { id = item.LeadProfileLeadProfileId });
        }

        [HttpPost]
        public ActionResult CreateLeadProfile(LeadProfile entity)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    entity.FillInModelWithDefaultValue();
                    entity.Status = true;
                    this.FulFil_LeadProfile(entity, true);


                    entity.RecentLeadAssignment = new RecentLeadAssignment();
                    entity.RecentLeadAssignment.FillInModelWithDefaultValue();
                    entity.RecentLeadAssignment.CallDate = null;
                    entity.RecentLeadAssignment.AppointmentDate = null;

                    Prospect_Profile prospect = null;
                    if (entity.ProspectId.HasValue && entity.ProspectId.Value > 0)
                    {
                        prospect = db.Prospect_ProfileSet.Find(entity.ProspectId);
                        entity.Salutation = prospect.Salutation;
                        entity.SalutationCode = prospect.SalutationCode;
                        entity.FirstName = prospect.FirstName;
                        entity.LastName = prospect.LastName;
                        entity.Email = prospect.Email;
                    }

                    entity.LeadFlag = "N";
                    checkLeadFlag(entity);

                    LeadProfile profile = db.LeadProfileSet.Add(entity);
                    db.SaveChanges();

                    //return RedirectToAction("MainProfile", new { @id = profile.LeadProfileId, @message = "Save Success" });
                    if (User.IsAdmin())
                    {
                        TempData["result-lead"] = profile;
                        return RedirectToAction("CreateLeadProfile");
                    }
                    else if (User.IsTSR())
                    {
                        return RedirectToAction("MyLead", new { @message = "Save Success" });
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException==null ? ex.Message : ex.InnerException.Message);
                }
            }
            this.PopulateProfile();
            return View(entity);
        }

        [HttpPost]
        public ActionResult EditLeadProfile(LeadProfile updateprofile)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    updateprofile.FillInModelWithDefaultValue();
                    this.FulFil_LeadProfile(updateprofile, false);

                    db.Entry(updateprofile).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    //return RedirectToAction("MainProfile", new { @id = updateprofile.LeadProfileId.ToString() });
                    return View("Index");
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException.Message);
                }
            }
            this.PopulateProfile();
            return View(updateprofile);
        }

        [HttpGet]
        public ActionResult AssignLead(int? id)
        {
            if (id.IsNullOrEmpty())
            {
                return HttpNotFound("Id that you search is not found.");
            }
            LeadProfile profile = db.LeadProfileSet.SingleOrDefault(item => item.LeadProfileId == id.Value);
            if (profile.IsNullOrEmpty())
            {
                return HttpNotFound("Profile Not Found!");
            }
            this.PopulateProfile(profile);

            var tsrlist = appdb.ActiveTSRs().ToList();
            ViewBag.ActiveUser = new SelectList(tsrlist.OrderBy(o => o.FullName), "Id", "FullName");
            var user = appdb.Users.Find(profile.CreatedBy);
            if (user != null)
            {
                ViewBag.CreateByName = user.FirstName + " " + user.LastName;
            }
            else
            {
                ViewBag.CreateByName = profile.CreatedBy;
            }
            
            return View(profile);
        }

        [HttpPost]
        public ActionResult AssignLead(LeadProfile vm,bool? keepnew)
        {
            var lead = db.LeadProfileSet.Find(vm.LeadProfileId);

            //if assign not change, skip this part
            if (vm.RecentLeadAssignment.ReceiverUserId != lead.RecentLeadAssignment.ReceiverUserId)
            {
                // update appointment that waiting for reply
                ClearWaitingAppointment(lead);
                if (!(keepnew == true) && lead.LeadAssignmentSet.Any() && !String.IsNullOrEmpty(lead.RecentLeadAssignment.ResponseCategoryCode))
                {
                    lead.LeadFlag = "O";
                }
                lead.UnAssign(db,User.Identity.GetUserId());
                LeadAssignment leadAssignment = lead.AssignLead(User.Identity.GetUserId(), vm.RecentLeadAssignment.ReceiverUserId, vm.RecentLeadAssignment.ReceiverUserName);
                db.SaveChanges();
                lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = (leadAssignment != null) ? leadAssignment.LeadAssignmentId : 0;
            }

            lead.CallSourceCode = vm.CallSourceCode;
            lead.CallSource = vm.CallSource;
            
            lead.Product = vm.Product;
            lead.ProductCode = vm.ProductCode;
            //lead.LeadSource = vm.LeadSource;
            lead.Remark = vm.Remark;
            lead.UpdatedDate = DateTime.Now;
            lead.UpdatedBy = User.Identity.GetUserId();
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult MyLead(string message = "",string tag = "")
        {
            var userid = User.Identity.GetUserId();
            IQueryable<LeadProfile> leadset = null;
            
            if (message.Equals("NoLeadAssignment")) {
                ViewBag.RunScript = "ShowNoAssign();";
            }
            else if (message.Length > 0)
            {
                ViewBag.RunScript = "ShowSuccessScript();";
            }
            LoadResponseExtend();
            LoadCallCount();
            return View(leadset);
        }

        public ActionResult TsrLeadInspect()
        {
            LoadResponseExtend();
            LoadCallCount();
            LoadTsr();
            return View("MyLead");
        }


        public ActionResult GetMyLead(SearchLeadClass criteria)//( string assigndate, string response, string callcount, string product,string tag,string tsr,string leadflag)
        {
            string connectionString = db.Database.Connection.ConnectionString;
            var userid = User.Identity.GetUserId();
            if (User.IsAdmin() && !criteria.tsr.IsNullOrEmpty())
            {
                userid = criteria.tsr;
            }
            string[] responses = { "" };
            if (!String.IsNullOrEmpty(criteria.response)) responses = criteria.response.Split(',');
            IQueryable<ActiveLead> leadset = null;
            if (String.IsNullOrEmpty(criteria.tag))
            {
                leadset = crm.ActiveLeadSet.Where(p => p.ReceiverUserId.Equals(userid));
                if (!String.IsNullOrEmpty(criteria.createdate)) leadset = leadset.CreateDateInRange(criteria.createdate);
                if (!String.IsNullOrEmpty(criteria.issuedate)) leadset = leadset.IssueDateInRange(criteria.issuedate);
                if (!String.IsNullOrEmpty(criteria.expiredate)) leadset = leadset.ExpireDateInRange(criteria.expiredate);

                if (responses != null) leadset = leadset.Where(c => responses.Contains(c.ResponseCategoryCode));
                if (!String.IsNullOrEmpty(criteria.product)) leadset = leadset.Where(c => c.ProductGroup == criteria.product);
                leadset = _filterCallCount(leadset, criteria.callcount);

            }
            else if (criteria.tag == "newlead")
            {
                leadset = crm.ActiveLeadSet.WaitingForContact(userid, 30);
            }
            else if (criteria.tag == "expire")
            {
                var today = DateTime.Today;
                leadset = crm.ActiveLeadSet.Where(c => c.RecentLeadAssignment.ReceiverUserId == userid && c.ExpireDate == today && (c.RecentLeadAssignment.ResponseCategoryCode == "" || c.RecentLeadAssignment.ResponseCategoryCode == "1"));
            }
            //var todaycalls = db.Prospect_ActivitySet.Where(c => c.CreatedDate >= DateTime.Today && c.CreatedBy == userid);
            var data = from row in leadset.AsEnumerable()
                       select new MyLeadDisplayClass
                       {
                           TM = row.TM,
                           Name = row.FirstName + " " + row.LastName,
                           Telphone = row.TelPhone,
                           PolicyNo = row.PolicyNo,
                           CreatedDate = row.CreatedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           IssueDate = row.IssueDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           ExpireDate = row.ExpireDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           Product = (row.ProductGroup=="NL"&&!row.Product.IsNullOrEmpty())?row.Product:row.ProductGroup,
                           ResponseCategoryCode = row.ResponseCategoryCode.ResponseLabel(),
                           ResponseReason = row.ResponseReason,
                           Remark =  row.Detail ,
                           CallDate = row.CallDate.HasValue ? row.CallDate.Value.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : "",
                           CallCount = row.CallCount,
                           TodayCall = row.TodayCall,

                           Appointment = row.AppointmentDate.HasValue ? row.AppointmentDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                           AppointmentTime = row.AppointmentTime,
                           AppointmentTopic = row.AppointmentTopic,
                           LeadProfileId = row.LeadProfileId,
                           Countdown = row.Countdown,
                           IsDownloaded = row.LeadsourceInformFlag==true,
                       };

            List<MyLeadDisplayClass> displayData = data.ToList();           

            return Json(displayData, JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult UploadLead()
        {
            if (!User.IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View();
        }

        [HttpPost]
        public ActionResult UploadLead(HttpPostedFileBase leadsfile)
        {
            var leads = new List<LeadProfile>();
            
                
            using (var stream = leadsfile.InputStream)
            {
                if (leadsfile.FileName.Contains("Non Life"))
                {
                    leads = ReadNLFile(leadsfile.FileName, stream);
                }
                else
                {
                    leads = ReadOLFile(leadsfile.FileName, stream);
                }
            }
            
            foreach (var entity in leads.Where(c=>c.Status.Value))
            {
                entity.FillInModelWithDefaultValue();
                this.FulFil_LeadProfile(entity, true);

                entity.RecentLeadAssignment = new RecentLeadAssignment();
                entity.RecentLeadAssignment.FillInModelWithDefaultValue();
                entity.RecentLeadAssignment.CallDate = null;
                entity.RecentLeadAssignment.AppointmentDate = null;
                if (entity.ProductGroup == "NL")
                {
                    var plan = db.MasterPlanSet.Find(entity.PlanCode);
                    if (plan != null)
                    {
                        entity.Planname = plan.PlanName;
                        entity.SumInsure = plan.SA;
                    }
                }

                entity.LeadFlag = "N";                
                db.LeadProfileSet.Add(entity);
            }
            db.SaveChanges();
            if (leadsfile.FileName.Contains("Non Life"))
            {
                db.Database.ExecuteSqlCommand("exec [dbo].[PROC_UPDATE_PAYPLAN_NL]");
            }
            else
            {
                db.Database.ExecuteSqlCommand("exec [dbo].[PROC_UPDATE_PAYPLAN_OL]");                
            }
            
            TempData["uploadresult"] = leads;
            return RedirectToAction("UploadResult");
        }

        public ActionResult UploadResult()
        {
            var leads = (List<LeadProfile>)TempData["uploadresult"];
            if (leads == null) return RedirectToAction("UploadLead");
            return View(leads);
        }

        private List<LeadProfile> ReadNLFile(string filename,System.IO.Stream stream)
        {
            var leads = new List<LeadProfile>();
            var dt = new DataTable();
            if (filename.EndsWith("xlsx"))
            {
                dt = ImportExceltoDataset(stream).Tables[0];
            }
            else if (filename.EndsWith("xls"))
            {
                dt = XlsToDT(stream);
            }
            else if (filename.EndsWith("xml"))
            {
                dt = XMLtoDataTable.ImportExcelXML(stream, true, true).Tables[0];
            }
            if (dt != null)
            {
                //Change column name
                foreach(DataColumn dc in dt.Columns)
                {
                    dc.ColumnName = dc.ColumnName.Replace(" ", "");// dc.ColumnName.Trim();
                }
                dt.Columns["วันที่ส่งให้GT"].ColumnName = "ExpireDate";
                dt.Columns["วันที่ส่งกรมธรรม์"].ColumnName = "PostDate";
                dt.Columns["วันที่เริ่มโทรติดตาม"].ColumnName = "CallDate";


                foreach (DataRow dr in dt.Rows)
                {
                    var lead = new LeadProfile();
                    lead.TelPhone = dr["InsuredTelNo"].ToString().Trim().Replace(" ", "");
                    var name = dr["InsuredName"].ToString().Trim();

                    lead.ProductGroup = "NL";
                    switch (dr["Contracttype"].ToString())
                    {
                        case "ACR": lead.Product = "CANCER";break;
                        case "API": lead.Product = "HEALTH"; break;
                        case "AAI": lead.Product = "PA"; break;
                        case "AAS": lead.Product = "PA"; break;
                    }
                    lead.PlanCode = dr["Contracttype"].ToString() + dr["Risktype"].ToString() + dr["Plancode"].ToString();
                    lead.NetPremium = dr["NetPremium"].ToString().ParseToPositiveDecimal();
                    lead.FirstName = name;
                    lead.TM = dr["TM"].ToString();
                    lead.PolicyNo = dr["ContractNo"].ToString();
                    lead.ApplicationNo = dr["ApplicationNo"].ToString();
                    lead.InceptionDate = dr["InceptionDate"].ToString().ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.IssueDate = dr["TransDate(IssueDate)"].ToString().ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.ExpireDate = dr["ExpireDate"].ToString().ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.AgentCode = dr["AgentNo"].ToString();
                    lead.AgentName = dr["AgentName"].ToString();
                    lead.Address = dr["InsuredAddress"].ToString();
                    lead.PostTracking = dr["PostPolicy"].ToString();
                    lead.PostDate = dr["PostDate"].ToString().ParseSpecifyDateFormat("yyyy-MM-dd");

                    lead.Status = true;
                    lead.Raw = new JObject(
                        dt.Columns.Cast<DataColumn>().Select(c => new JProperty(c.ColumnName, JToken.FromObject(dr[c])))).ToString(Formatting.None);

                    //validation
                    lead.ValidateImport();
                    if (lead.Status.Value && db.LeadProfileSet.Any(c => c.PolicyNo == lead.PolicyNo && c.Status == true))
                    {
                        lead.Status = false;
                        lead.LeadFlag = "D";
                        lead.Remark = "เลขกรมธรรม์ซ้ำ";
                    }
                    //check in file duplicate
                    if (lead.Status.Value && leads.Any(c => c.Status.Value && c.PolicyNo == lead.PolicyNo))
                    {
                        lead.Status = false;
                        lead.Remark = "เลขกรมธรรม์ซ้ำภายในไฟล์";
                    }

                    lead.Filename = filename;
                    leads.Add(lead);
                }
            }
            return leads;
        }

        private List<LeadProfile> ReadOLFile0(string filename, System.IO.Stream stream)
        {
            var leads = new List<LeadProfile>();
            var dt = new DataTable();
            if (filename.EndsWith("xlsx"))
            {
                dt = ImportExceltoDataset(stream).Tables[0];
            }
            else if (filename.EndsWith("xls"))
            {
                dt = XlsToDT(stream);
            }
            else if (filename.EndsWith("xml"))
            {
                dt = XMLtoDataTable.ImportExcelXML(stream, true, true).Tables[0];
            }
            if (dt != null)
            {
                //Change column name
                foreach (DataColumn dc in dt.Columns)
                {
                    dc.ColumnName = ConvertLatinToThai(dc.ColumnName.Replace(" ", ""));// dc.ColumnName.Trim();
                }

                List<LifeInputLead> data = new List<LifeInputLead>();
                string tmteam = "";
                foreach (DataRow dr in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(dr[0].ToString())) tmteam = dr[0].ToString();
                    var lead = new LifeInputLead()
                    {
                        TM = tmteam,
                        Item = dr[2].ToString(),
                        Status = dr[3].ToString(),
                        PolicyNo = dr[4].ToString(),
                        ApplicationNo = dr[5].ToString(),
                        Planname = ConvertLatinToThai(dr[6].ToString()),
                        NetPremium = dr[7].ToString(),
                        SumInsured = dr[8].ToString(),
                        InsuredName = ConvertLatinToThai(dr[9].ToString()),
                        InsuredTelNo = dr[10].ToString(),
                        AgentName = ConvertLatinToThai(dr[11].ToString()),
                        IssueDate = dr[13].ToString(),
                        Payplan = dr[14].ToString(),
                        Address1 = ConvertLatinToThai(dr[15].ToString()),
                        Address2 = ConvertLatinToThai(dr[16].ToString()),
                        Address3 = ConvertLatinToThai(dr[17].ToString()),
                        Address4 = ConvertLatinToThai(dr[18].ToString()),
                        Province = ConvertLatinToThai(dr[19].ToString()),
                        Zipcode = dr[20].ToString(),
                        PostPolicy = dr[21].ToString(),
                        PostDate = dr[22].ToString(),
                        CallDate = dr[23].ToString(),
                        ExpireDate = dr[25].ToString()
                    };
                    data.Add(lead);
                }


                foreach (var row in data)
                {
                    var lead = new LeadProfile();
                    lead.TelPhone = row.InsuredTelNo.Trim().Replace(" ", "");
                    var name = row.InsuredName.Trim();

                    lead.ProductGroup = "LIFE";
                    lead.Planname = row.Planname;
                    lead.NetPremium = row.NetPremium.ParseToPositiveDecimal();
                    lead.FirstName = name;
                    lead.TM = row.TM;
                    lead.PolicyNo = row.PolicyNo;
                    lead.IssueDate = row.IssueDate.ParseSpecifyDateFormat("yyyyMMdd");
                    lead.ExpireDate = row.ExpireDate.ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.AgentName = row.AgentName;
                    lead.Address = string.Format("{0} {1} {2} {3} {4} {5}", row.Address1, row.Address2, row.Address3, row.Address4, row.Province, row.Zipcode);
                    lead.PostTracking = row.PostPolicy;
                    lead.PostDate = row.PostDate.ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.Filename = filename;

                    lead.Status = true;
                    lead.Raw = JsonConvert.SerializeObject(row);

                    //validation
                    lead.ValidateImport();
                    if (lead.Status.Value && db.LeadProfileSet.Any(c => c.PolicyNo == lead.PolicyNo && c.Status == true))
                    {
                        lead.Status = false;
                        lead.LeadFlag = "D";
                        lead.Remark = "เลขกรมธรรม์ซ้ำ";
                    }
                    //check in file duplicate
                    if (lead.Status.Value && leads.Any(c => c.Status.Value && c.PolicyNo == lead.PolicyNo))
                    {
                        lead.Status = false;
                        lead.Remark = "เลขกรมธรรม์ซ้ำภายในไฟล์";
                    }

                    leads.Add(lead);

                }
            }
            return leads;
        }
        private List<LeadProfile> ReadOLFile(string filename, System.IO.Stream stream)
        {
            var leads = new List<LeadProfile>();
            var dt = new DataTable();
            if (filename.EndsWith("xlsx"))
            {
                dt = ImportExceltoDataset(stream).Tables[0];
            }
            else if (filename.EndsWith("xls"))
            {
                dt = XlsToDT(stream);
            }
            else if (filename.EndsWith("xml"))
            {
                dt = XMLtoDataTable.ImportExcelXML(stream, true, true).Tables[0];
            }
            if (dt != null)
            {
                //Change column name
                foreach (DataColumn dc in dt.Columns)
                {
                    dc.ColumnName = (dc.ColumnName.Replace(" ", ""));// dc.ColumnName.Trim();
                }

                List<LifeInputLead> data = new List<LifeInputLead>();
                string tmteam="";
                foreach (DataRow dr in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(dr[0].ToString())) tmteam = dr[0].ToString();
                    var lead = new LifeInputLead()
                    {                        
                        TM = tmteam,
                        Item = dr[2].ToString(),
                        Status = dr[3].ToString(),
                        PolicyNo = dr[4].ToString(),
                        ApplicationNo = dr[5].ToString(),
                        Planname = dr[6].ToString(),
                        NetPremium = dr[7].ToString(),
                        SumInsured = dr[8].ToString(),
                        InsuredName = (dr[9].ToString()),
                        InsuredTelNo = dr[10].ToString(),
                        AgentName = (dr[11].ToString()),
                        IssueDate = dr[13].ToString(),
                        Payplan = dr[14].ToString(),
                        Address = (dr[15].ToString()),
                        PostPolicy = dr[16].ToString(),
                        PostDate = dr[17].ToString(),
                        CallDate = dr[18].ToString(),
                        ExpireDate = dr[20].ToString()
                    };
                    data.Add(lead);
                }


                foreach (var row in data)
                {
                    var lead = new LeadProfile();
                    lead.TelPhone = row.InsuredTelNo.Trim().Replace(" ", "");
                    var name = row.InsuredName.Trim();

                    lead.ProductGroup = "LIFE";
                    lead.Planname = row.Planname;
                    lead.NetPremium = row.NetPremium.ParseToPositiveDecimal();
                    lead.SumInsure = row.SumInsured.ParseToPositiveDecimal();
                    lead.Payplan = row.Payplan;
                    lead.FirstName = name;
                    lead.TM = row.TM;
                    lead.ApplicationNo = row.ApplicationNo;
                    lead.PolicyNo = row.PolicyNo;
                    lead.IssueDate = row.IssueDate.ParseSpecifyDateFormat("yyyyMMdd");
                    lead.ExpireDate = row.ExpireDate.ParseSpecifyDateFormat("yyyy-MM-dd");
                    lead.AgentName = row.AgentName;
                    lead.Address = row.Address;
                    lead.PostTracking = row.PostPolicy;
                    lead.PostDate = row.PostDate.ParseSpecifyDateFormat("yyyy-MM-dd");
                    
                    lead.Filename = filename;

                    lead.Status = true;
                    lead.Raw = JsonConvert.SerializeObject(row);

                    //validation
                    lead.ValidateImport();
                    if (lead.Status.Value && db.LeadProfileSet.Any(c => c.PolicyNo == lead.PolicyNo && c.Status == true))
                    {
                        lead.Status = false;
                        lead.LeadFlag = "D";
                        lead.Remark = "เลขกรมธรรม์ซ้ำ";
                    }
                    //check in file duplicate
                    if (lead.Status.Value && leads.Any(c => c.Status.Value && c.PolicyNo == lead.PolicyNo))
                    {
                        lead.Status = false;
                        lead.Remark = "เลขกรมธรรม์ซ้ำภายในไฟล์";
                    }

                    leads.Add(lead);

                }
            }
            return leads;
        }

        private string ConvertLatinToThai(string val)
        {
            return Encoding.GetEncoding("ISO-8859-11").GetString(Encoding.GetEncoding("ISO-8859-1").GetBytes(val));
        }


        [HttpPost]
        public PartialViewResult ParseLeadsFile(HttpPostedFileBase leadsfile)
        {
            try
            {
                var leads = new List<LeadProfile>();
                using (var stream = leadsfile.InputStream)
                {
                    if (leadsfile.FileName.Contains("Non Life"))
                    {
                        leads = ReadNLFile(leadsfile.FileName, stream);
                    }
                    else
                    {
                        leads = ReadOLFile(leadsfile.FileName, stream);
                    }                    
                }

                return PartialView("_ParseLeadsFile", leads);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }			
        }

        private DataTable XlsToDT(System.IO.Stream stream)
        {
            IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            var ds = excelReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            });
            return ds.Tables[0];
        }
        private DataSet ImportExceltoDataset(System.IO.Stream stream)
        {
            // Open the Excel file using ClosedXML.
            // Keep in mind the Excel file cannot be open when trying to read it
            using (XLWorkbook workBook = new XLWorkbook(stream))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(1);

                //Create a new DataTable.
                DataTable dt = new DataTable();

                //Loop through the Worksheet rows.
                bool firstRow = true;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //Use the first row to add columns to DataTable.
                    if (firstRow)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            dt.Columns.Add(cell.Value.ToString());
                        }
                        firstRow = false;
                    }
                    else
                    {
                        //check empty row
                        if (row.FirstCellUsed() == null) { break; }
                        //Add rows to DataTable.
                        dt.Rows.Add();
                        int i = 0;
                        
                        foreach (IXLCell cell in row.Cells(row.FirstCellUsed().Address.ColumnNumber, row.LastCellUsed().Address.ColumnNumber))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                            i++;
                        }
                    }
                }
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
        }

        private IEnumerable<LeadProfile> _ReadyToAssignLead(SearchLeadClass criteria)
        {
            var leadset = db.LeadProfileSet.ActiveSet().Include(i=>i.RecentLeadAssignment).Where(c => c.RecentLeadAssignment.LeadAssignment_LeadAssignmentId==0);
            if (!String.IsNullOrEmpty(criteria.createdate)) leadset = leadset.CreateDateInRange(criteria.createdate);
            if (!String.IsNullOrEmpty(criteria.expiredate)) leadset = leadset.ExpireDateInRange(criteria.expiredate);
            if (!String.IsNullOrEmpty(criteria.issuedate)) leadset = leadset.IssueDateInRange(criteria.issuedate);
            if (!String.IsNullOrEmpty(criteria.tsr))
            {
                var tsrs = criteria.tsr.Split(',');
                leadset = leadset.LastAssignTo(tsrs);
            }
            if (!String.IsNullOrEmpty(criteria.product)) leadset = leadset.Where(c => c.ProductGroup == criteria.product);
            if (!String.IsNullOrEmpty(criteria.response))
            {
                var responses = criteria.response.Replace("[", "").Replace("]", "").Split(',');
                leadset = leadset.Where(c => responses.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
            }
            
            //leadset = leadset.Where(c => !db.Prospect_ProfileSet.Any(ic => ic.OptOut=="Y" && ic.ProspectId == c.ProspectId));
            var leadlist = leadset.AsEnumerable();           
            return leadset;
        }

        public ActionResult DistributeLead()
        {
            if (!User.IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            var criteria = new SearchLeadClass() { leadflag = "N", response="I",responses = new string[]{"","1","5"} };
            return DistributeLead(criteria);
        }

        [HttpPost]
        public ActionResult DistributeLead(SearchLeadClass criteria)
        {
            if (!User.IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            ViewBag.SearchData = criteria;
            var suplist = appdb.GetSupervisorSelectList().ToList();
            ViewBag.suplist = new SelectList(suplist, "CODE", "NAME");
            ViewBag.tsrViewModel = LoadTsrsLead();
            //LoadLeadFlag(criteria.leadflag);
            LoadTsr(criteria.tsr);
            LoadResponseExtend(criteria.response);
            return View();
        }

        public List<DistributeLeadViewModel> LoadTsrsLead(){
            var active = appdb.ActiveTSRs().AsEnumerable();
            var tsrlist = (from row in active orderby row.SupervisorId select new ActiveUserModel.ActiveUserClass() { CODE = row.Id, NAME = row.FirstName + " " + row.LastName, SUPERVISERID = row.SupervisorId }).ToList();
            ViewBag.tsrlist = tsrlist;
            var leadbytsr = db.LeadProfileSet.ActiveSet().GroupBy(g => g.RecentLeadAssignment.ReceiverUserId).Select(s => new {tsr=s.Key,currentlead=s.Count(),OpenLead = s.Count(c=>c.RecentLeadAssignment.ResponseCategoryCode==""|| c.RecentLeadAssignment.ResponseCategoryCode=="1") });

            var tsrvm = (from row in tsrlist
                         orderby row.NAME
                         let userlead = leadbytsr.Where(c=>c.tsr==row.CODE).ToList()
                         select new DistributeLeadViewModel()
                         {
                             tsr = row,
                             leadqty = 0,
                             currentlead = userlead.Sum(s => s.currentlead),
                             OpenLead = userlead.Sum(s => s.OpenLead)
                         }).ToList();
            return tsrvm;
        }

        public ActionResult RetrieveTsrsLead()
        {
            var tsrlist = LoadTsrsLead();
            return Json(tsrlist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RetrieveAvailableLeads(SearchLeadClass criteria)
        {            
            var leadset = from row in _ReadyToAssignLead(criteria)
                          select new {
                              row.ProductGroup,
                              expire = row.ExpireDate.Value.ToString("dd/MM/yyyy"),
                              CreatedDate = row.CreatedDate.HasValue ? row.CreatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture) : "",
                              row.TM,
                              fullname = row.FirstName ,
                            telphone = row.TelPhone,
                            row.PolicyNo,
                            ResponseCategoryCode = row.RecentLeadAssignment.ResponseCategoryCode.ResponseLabel(),
                          };
            return Json(leadset.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DoDistribute(List<DistributeLeadViewModel> vm,SearchLeadClass criteria)
        {
            var createdate = criteria.createdate.ParseToDate();
            var leadset = _ReadyToAssignLead(criteria).OrderBy(x => Guid.NewGuid());
            var tsrindex=0;
            //ActiveUserModel.ActiveUserClass tsr=null;
            DistributeLeadViewModel model=null;
            foreach (var lead in leadset.ToList())
            {
                while(model == null||model.leadqty==0){
                    if (tsrindex >= vm.Count) {
                        model = null;
                        break;
                    }
                    model = vm.ElementAt(tsrindex++);
                    if (model.isSelected == false)model=null;
                    //if (model.leadqty == 0)continue;
                }
                if (model == null) break;
                //if (String.IsNullOrEmpty(lead.SupervisorId)) lead.SupervisorId = model.tsr.SUPERVISERID;
                LeadAssignment leadAssignment = lead.AssignLead(User.Identity.GetUserId(),model.tsr.CODE, model.tsr.NAME);
                db.SaveChanges();
                lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = leadAssignment.LeadAssignmentId;
                model.leadqty--;
            }
            db.SaveChanges();
            return RedirectToAction("DistributeLead",criteria);
        }

        public ActionResult UnAssignLead(SearchLeadClass criteria)
        {
            if (!User.IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            var tsrlist = appdb.ActiveTSRs().ToList();            

            ViewBag.ActiveUser = new SelectList(tsrlist, "Id", "FullName", criteria.tsr);
            LoadResponseExtend(criteria.response);
            LoadCallCount(criteria.callcount);

            if (Request.HttpMethod=="GET")
            {
                criteria = new SearchLeadClass() {};
                ViewBag.SearchData = criteria;
                return View(new List<LeadProfile>());
            }
            else
            {
                ViewBag.SearchData = criteria;
                var leadset = db.LeadProfileSet.ActiveSet().Where(c => !String.IsNullOrEmpty(c.RecentLeadAssignment.ReceiverUserId));                
                if (criteria.responses != null) leadset = leadset.Where(c => criteria.responses.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
                if (criteria.tsrs != null) leadset = leadset.Where(c => criteria.tsrs.Contains(c.RecentLeadAssignment.ReceiverUserId));
                if (!String.IsNullOrEmpty(criteria.createdate)) leadset = leadset.CreateDateInRange(criteria.createdate);
                if (!String.IsNullOrEmpty(criteria.issuedate)) leadset = leadset.IssueDateInRange(criteria.issuedate);
                if (!String.IsNullOrEmpty(criteria.expiredate)) leadset = leadset.ExpireDateInRange(criteria.expiredate);
                if (!String.IsNullOrEmpty(criteria.product)) leadset = leadset.Where(c => c.ProductGroup == criteria.product);
                leadset = _filterCallCount(leadset, criteria.callcount);
                if (!criteria.calldate.IsNullOrEmpty())
                {
                    var dates = criteria.calldate.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
                    var endDate = DateTime.Parse(dates[1], System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
                    leadset = leadset.Where(c => c.RecentLeadAssignment.CallDate >= startDate && c.RecentLeadAssignment.CallDate <= endDate);
                }
                return View(leadset);
            }
        }

        [HttpPost]
        public ActionResult DoUnAssign(long[] leadid,bool? keepnew)
        {

            var leadset = from row in db.LeadProfileSet where leadid.Contains(row.LeadProfileId) select row;
            foreach (var lead in leadset.ToList())
            {
                lead.RecentLeadAssignment.CallBackOf = appdb.GetSupervisor(lead.RecentLeadAssignment.ReceiverUserId).Id;
                ClearWaitingAppointment(lead);
                lead.UnAssign(db,User.Identity.GetUserId());
                if (!(keepnew==true) && !String.IsNullOrEmpty(lead.RecentLeadAssignment.ResponseCategoryCode)) {
                    lead.LeadFlag = "O";
                }

            }
            db.SaveChanges();
            return RedirectToAction("UnAssignLead");
        }

        [HttpPost]
        public ActionResult DoTransfer(long[] leadid,string tsrid)
        {
            try
            {
                var tsrname = appdb.Users.Find(tsrid).FullName;
                var userid = User.Identity.GetUserId();
                var leadset = from row in db.LeadProfileSet where leadid.Contains(row.LeadProfileId) select row;
                foreach (var lead in leadset.ToList())
                {
                    if (lead.RecentLeadAssignment.ReceiverUserId == tsrid) continue;//skip if same tsr
                    lead.RecentLeadAssignment.CallBackOf = appdb.GetSupervisor(lead.RecentLeadAssignment.ReceiverUserId).Id;
                    ClearWaitingAppointment(lead);
                    lead.UnAssign(db, User.Identity.GetUserId());
                    if (!String.IsNullOrEmpty(lead.RecentLeadAssignment.ResponseCategoryCode))
                    {
                        lead.LeadFlag = "O";
                    }
                }
                db.SaveChanges();
                
                foreach (var lead in leadset.ToList())
                {
                    if (lead.RecentLeadAssignment.ReceiverUserId == tsrid) continue;//skip if same tsr
                    LeadAssignment leadAssignment = lead.AssignLead(userid, tsrid, tsrname);
                    db.SaveChanges();
                    lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = leadAssignment.LeadAssignmentId;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Response.StatusCode = 500;
                throw;
            }
            return RedirectToAction("UnAssignLead");
        }

        public ActionResult LeadNotification()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LeadNotification(string createdate)
        {
            IQueryable<LeadNotification> leads = null;
            if (User.IsAdmin())
            {
                leads = db.LeadNotificationSet.Where(c => c.IsActive == true && c.IsAssign == false);
            }
            else
            {
                var userid = User.Identity.GetUserId();
                leads = db.LeadNotificationSet.Where(c => c.IsActive == true && c.ReceiverUserId == userid);
            }
            if (!string.IsNullOrEmpty(createdate))
            {
                var dates = createdate.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
                var endDate = DateTime.Parse(dates[1], System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
                leads = leads.Where(c => c.CreatedDate >= startDate && c.CreatedDate < endDate);
            }

            ViewBag.NotiCount = leads.Count(c => c.IsRead == false);
            return View(leads.OrderByDescending(c => c.CreatedDate));
        }

        public ActionResult GetLeadNotification(string userid)
        {
            IQueryable<LeadNotification> leads = null;
            if (User.IsAdmin())
            {
                leads = db.LeadNotificationSet.Where(c => c.IsActive == true && c.IsAssign == false);
            }
            else
            {
                userid = User.Identity.GetUserId();
                leads = db.LeadNotificationSet.Where(c => c.IsActive == true && c.ReceiverUserId == userid);
            }

            ViewBag.NotiCount = leads.Count(c => c.IsRead == false);
            return PartialView(leads.OrderByDescending(c=>c.CreatedDate).Take(20));
        }

        public ActionResult OpenNotification(int? notiId, int? id)
        {
            if (notiId != null)
            {
                var noti = db.LeadNotificationSet.Find(notiId);
                if (noti != null)
                {
                    noti.IsRead = true;
                    db.SaveChangesAsync();
                }
                if (User.IsAdmin())
                {
                    return RedirectToAction("AssignLead", new { id = id });
                }
                else
                {
                    
                    return RedirectToAction("OpenFromLead", "Prospect", new { id = id,callfrom = CRMExtension.CallFrom.NewLeadNoti });
                    
                }
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult ReadAllNotification()
        {
            if (User.IsAdmin())
            {
                db.Database.ExecuteSqlCommand("UPDATE [dbo].LeadNotificationSet SET IsRead=1 where IsActive=1 and IsRead=0 and IsAssign=0");
            }
            else
            {
                db.Database.ExecuteSqlCommand(string.Format("UPDATE [dbo].LeadNotificationSet SET IsRead=1 where IsActive=1 and IsRead=0 and ReceiverUserId='{0}' ", User.Identity.GetUserId()));
            }
            return RedirectToAction("Index","Home");
        }
        public ActionResult DeleteNotification(int? notiId)
        {
            if (notiId != null)
            {
                var noti = db.LeadNotificationSet.Find(notiId);
                if (noti != null)
                {
                    noti.IsActive = false;
                    db.SaveChanges();
                }
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DuplicateLeads(string searchdate)
        {
            if (searchdate != null)
            {
                var leads = db.LeadProfileSet.Where(c => c.LeadFlag == "D" && c.Status == true).OrderByDescending(c => c.UpdatedDate);
                leads = leads.UpdateDateInRange(searchdate).OrderByDescending(c => c.UpdatedDate);
                return View(leads);
            }
            return View();
        }

        public ActionResult HideHotLead(long leadprofileid)
        {
            var lead = db.LeadProfileSet.Find(leadprofileid);
            lead.RecentLeadAssignment.HideFlag = true;
            db.SaveChanges();
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region AJAX services
        [HttpGet]
        public ActionResult GetAllLeads(string dateFrom = null, string dateTo = null)
        {
            ///List<LeadProfile> leadprofiles = new List<LeadProfile>();
            DateTime dF = dateFrom != null ? dateFrom.ParseToDate() : DateTime.Now.Date;
            DateTime dT = dateTo != null ? dateTo.ParseToDate() : DateTime.Now.Date.AddHours(23).AddMinutes(59);
            var leadprofiles = from row in db.LeadProfileSet
                               where row.CreatedDate.Value >= dF
                               && row.CreatedDate.Value <= dT
                               select row;

            var lc = from row in leadprofiles.AsEnumerable()
                    select new
                    {
                         id                     = row.LeadProfileId
                        ,fullName               = row.FirstName + " " + row.LastName
                        ,gender                 = row.GenderName
                        ,telPhone               = row.TelPhone
                        ,Email                  = row.Email
                        ,UpdatedDate            = row.UpdatedDate
                        ,CallSource             = row.CallSource
                        ,RecentActivty          = row.RecentLeadAssignment.Activity
                        ,RecentResponseCategory = row.RecentLeadAssignment.ResponseCategoryName
                        ,RecentApplicationNo    = row.RecentLeadAssignment.ApplicationNo
                    };
        return Json(lc, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NewLeadAmount()
        {
            var last7day = DateTime.Today.AddDays(-7);
            var today = DateTime.Today;
            var vm = new NewLeadAmountViewModel();
            if (User.IsAdmin())
            {
                vm.NewLead = db.LeadProfileSet.WaitingForAssign(30).Count();
                vm.ExpireLead = db.LeadProfileSet.ActiveSet().Where(c => c.ExpireDate == today && (c.RecentLeadAssignment.ResponseCategoryCode == "" || c.RecentLeadAssignment.ResponseCategoryCode == "1")).Count();
            }
            else
            {
                var userid=User.Identity.GetUserId();
                this.UpdateAccessTimeStamp(userid);
                vm.NewLead = db.LeadProfileSet.WaitingForContact(userid,30).Count();
                vm.ExpireLead = db.LeadProfileSet.ActiveSet().Where(c => c.ExpireDate==today && c.RecentLeadAssignment.ReceiverUserId == userid && (c.RecentLeadAssignment.ResponseCategoryCode == "" || c.RecentLeadAssignment.ResponseCategoryCode=="1" )).Count();
            }
            return PartialView(vm);
        }

        [HttpPost]
        public ActionResult FlagDownload(long leadid,bool downloaded)
        {           
            var lead = db.LeadProfileSet.Find(leadid);
            if(lead != null)
            {
                lead.LeadsourceInformFlag = downloaded;
                db.SaveChanges();
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public class TriggerNewLeadOutputClass
        {
            public int LeadProfileId { get; set; }
        }

        //public ActionResult GetTsrByProject(string project)
        //{
        //    //var tsrinfo = igtdb.Database.SqlQuery<ApplicationUser>("select [USERID] Id,JoinDate CreatedDate from [VW_ALL_PROJECT_USERS] where IsBlocked=0 and ROLEID in (2,3,4)").ToList();
        //    var tsr = igtdb.Database.SqlQuery<ApplicationUser>("select [USERID] Id,FirstName,LastName from [VW_ALL_PROJECT_USERS] where IsBlocked=0 and ROLEID in (2,3,4) and project=@project", new SqlParameter("@project", project)).ToList();
        //    var result = from row in tsr
        //              select new {
        //                row.FullName
        //              };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Private Functions

        private void ClearWaitingAppointment(LeadProfile profile)
        {
            if (profile.RecentLeadAssignment.LeadAssignment_LeadAssignmentId > 0)
            {
                var obsoleted_appointments = db.Prospect_AppointmentSet.Where(row => row.IsReply == false && row.IsCancel == false
                                             && row.Prospect_Activity.LeadAssignmentId == profile.RecentLeadAssignment.LeadAssignment_LeadAssignmentId);

                obsoleted_appointments.ToList().ForEach(item =>
                {
                    item.IsCancel = true;
                    item.CanceledDate = DateTime.Now;
                    item.CanceledBy = "Automatically Cancel";
                });
            }
        }

        private void PopulateProfile(LeadProfile profile = null)
        {        
        }


        private void LoadResponse(string value = null)
        {
            var response = from row in db.ResponseCategorySet
                           where row.Active
                           select new { code = row.ResponseCategoryId, name = row.Name };
            ViewBag.Response = new SelectList(response.ToList(), "code", "name", value);
        }

        private void LoadResponseExtend(string value = null)
        {
            var response =  (from row in db.ResponseCategorySet
                            where row.Active
                            select new { code = row.ResponseCategoryId.ToString(), name = row.Name }).ToList();
            response.Insert(0, new { code = "", name = "ไม่มีผลการตอบรับ" });

            ViewBag.ResponseList = new SelectList(response.ToList(), "code", "name", value);
        }

        private void LoadReason(string value = null)
        {
            var Reason = (from row in db.ResponseReasonSet
                            where row.Active==true && row.ResponseCategory.Active == true
                            orderby row.ResponseCategoryId
                            select new { code = row.ResponseReasonId.ToString(), name = row.ResponseCategory.Name + " - "+ row.Name }).ToList();
            Reason.Insert(0, new { code = "", name = "ไม่มีเหตุผลการตอบรับ" });

            ViewBag.ReasonList = new SelectList(Reason.ToList(), "code", "name", value);
        }

        private void LoadLeadFlag(string value = "N")
        {
            var list = new List<SelectListItem>
            {
                new SelectListItem{ Text="ลีดใหม่", Value = "N" },
                new SelectListItem{ Text="ลีดเก่า", Value = "O" }
            };
            ViewBag.LeadFlagList = new SelectList(list,"Value","Text", value);
        }


        private void LoadCallCount(string value = "N")
        {
            var list = new List<SelectListItem>();
            for(var i = 0; i <= 9; i++)
            {
                list.Add(new SelectListItem { Text=i.ToString(),Value=i.ToString() });
            }
            list.Add(new SelectListItem { Text = "10+", Value = "10+" });

            ViewBag.CallCount = new SelectList(list, "Value", "Text", value);
        }

        private void LoadTsr(string tsr = null)
        {
            var tsrlist = appdb.ActiveTSRs().AsEnumerable();
            ViewBag.ActiveUser = new SelectList(tsrlist, "Id", "FullName", tsr);
        }

        private void FulFil_LeadProfile(LeadProfile profile, bool isNewProfile = true)
        {            
            if (!profile.GenderCode.IsNullOrEmpty())
            {
                switch (profile.GenderCode)
                {
                    case "M": profile.GenderName = "ชาย"; break;
                    case "F": profile.GenderName = "หญิง"; break;
                    default: break;
                }
            }

            profile.IsCustomer = false;
            profile.UpdatedBy = User.Identity.GetUserId();
            profile.UpdatedDate = DateTime.Now;
            if (isNewProfile)
            {
                profile.CreatedBy = profile.UpdatedBy;
                profile.CreatedDate = profile.UpdatedDate;
            }
            profile.BirthDate = (profile.BirthDate.IsNullOrEmpty() || profile.BirthDate.Value.Year == 1900) ? null : profile.BirthDate;
            profile.Note = profile.Note.IsNullOrEmpty() ? "" : profile.Note;
            profile.Occupation = profile.Occupation.IsNullOrEmpty() ? "" : profile.Occupation;
            profile.Remark = profile.Remark.IsNullOrEmpty() ? "" : profile.Remark;
            profile.AnnualIncome = profile.AnnualIncome.IsNullOrEmpty() ? 0 : profile.AnnualIncome.Value;
        }

        private IQueryable<LeadProfile> _filterCallCount(IQueryable<LeadProfile> leadset, string callcount)
        {
            if (callcount.IsNullOrEmpty()) return leadset;
            if (callcount.Length == 1)
            {
                var icallcount = callcount.ParseToPositiveInt();
                leadset = leadset.Where(c => c.RecentLeadAssignment.CallCount == icallcount);
            }
            else
            {
                leadset = leadset.Where(c => c.RecentLeadAssignment.CallCount >= 10);
            }
            return leadset;
        }

        private IQueryable<ActiveLead> _filterCallCount(IQueryable<ActiveLead> leadset, string callcount)
        {
            if (callcount.IsNullOrEmpty()) return leadset;
            if (callcount.Length == 1)
            {
                var icallcount = callcount.ParseToPositiveInt();
                leadset = leadset.Where(c => c.CallCount == icallcount);
            }
            else
            {
                leadset = leadset.Where(c => c.CallCount >= 10);
            }
            return leadset;
        }

        [AllowAnonymous]
        private void checkLeadFlag(LeadProfile entity,bool notify = true){
            entity.checkLeadFlag(db, AppConfig.LeadExpireDays, notify);
        }

        #endregion

    }
}
