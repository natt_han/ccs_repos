﻿using GT.InboundApplication.Model.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.Entity;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize(Roles = "SYSTEMADMIN,PM")]
    public class SpecialCalendarController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        public SpecialCalendarController()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
        }

        // GET: SpecialCalendars
        public ActionResult Index()
        {
            return View(db.SpecialCalendarSet.ToList().OrderByDescending(o=>o.CalendarDate));
        }

        // GET: SpecialCalendars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialCalendar specialCalendar = db.SpecialCalendarSet.Find(id);
            if (specialCalendar == null)
            {
                return HttpNotFound();
            }
            return View(specialCalendar);
        }

        // GET: SpecialCalendars/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SpecialCalendars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CalendarDate,Description,HolidayCount")] SpecialCalendar specialCalendar)
        {
            if (ModelState.IsValid)
            {
                db.SpecialCalendarSet.Add(specialCalendar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(specialCalendar);
        }

        // GET: SpecialCalendars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialCalendar specialCalendar = db.SpecialCalendarSet.Find(id);
            if (specialCalendar == null)
            {
                return HttpNotFound();
            }
            return View(specialCalendar);
        }

        // POST: SpecialCalendars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CalendarDate,Description,HolidayCount")] SpecialCalendar specialCalendar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialCalendar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(specialCalendar);
        }

        // GET: SpecialCalendars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialCalendar specialCalendar = db.SpecialCalendarSet.Find(id);
            if (specialCalendar == null)
            {
                return HttpNotFound();
            }
            return View(specialCalendar);
        }

        // POST: SpecialCalendars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecialCalendar specialCalendar = db.SpecialCalendarSet.Find(id);
            db.SpecialCalendarSet.Remove(specialCalendar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}