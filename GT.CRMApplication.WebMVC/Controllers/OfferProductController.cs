﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class OfferProductController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /OfferProduct/
        public ActionResult Index()
        {
            return View(db.OfferProductSet.ToList());
        }

        // GET: /OfferProduct/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferProduct offerProduct = db.OfferProductSet.Find(id);
            if (offerProduct == null)
            {
                return HttpNotFound();
            }
            return View(offerProduct);
        }

        // GET: /OfferProduct/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /OfferProduct/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="OfferProductId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,Category,ProductController")] OfferProduct offerProduct)
        {
            if (ModelState.IsValid)
            {
				offerProduct.CreatedBy =
				offerProduct.UpdatedBy = User.Identity.GetUserId();
				offerProduct.CreatedDate =
                offerProduct.UpdatedDate = DateTime.Now;
                db.OfferProductSet.Add(offerProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(offerProduct);
        }

        // GET: /OfferProduct/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferProduct offerProduct = db.OfferProductSet.Find(id);
            if (offerProduct == null)
            {
                return HttpNotFound();
            }
            return View(offerProduct);
        }

        // POST: /OfferProduct/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include= "OfferProductId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,Category,ProductController")] OfferProduct offerProduct)
        {
            if (ModelState.IsValid)
            {
				offerProduct.UpdatedBy = User.Identity.GetUserId();
			    offerProduct.UpdatedDate = DateTime.Now;
                db.Entry(offerProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(offerProduct);
        }

        // GET: /OfferProduct/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferProduct offerProduct = db.OfferProductSet.Find(id);
            if (offerProduct == null)
            {
                return HttpNotFound();
            }
            return View(offerProduct);
        }

        // POST: /OfferProduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            OfferProduct offerProduct = db.OfferProductSet.Find(id);
            db.OfferProductSet.Remove(offerProduct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
