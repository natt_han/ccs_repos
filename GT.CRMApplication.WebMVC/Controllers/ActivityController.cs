﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class ActivityController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /Activity/
        public ActionResult Index()
        {
            return View(db.ActivitySet.ToList());
        }

        // GET: /Activity/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.ActivitySet.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // GET: /Activity/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Activity/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create([Bind(Include="ActivityId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsSupportInboundCall,IsSupportOutboundCall,IsSupportFollowupCall,IsSupportUnknownPerson,IsSupportProspectPerson")] Activity activity)
        {
            if (ModelState.IsValid)
            {
				activity.CreatedBy =
				activity.UpdatedBy = User.Identity.GetUserId();
				activity.CreatedDate =
                activity.UpdatedDate = DateTime.Now;
                db.ActivitySet.Add(activity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(activity);
        }

        // GET: /Activity/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.ActivitySet.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: /Activity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include="ActivityId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsSupportInboundCall,IsSupportOutboundCall,IsSupportFollowupCall,IsSupportUnknownPerson,IsSupportProspectPerson")] Activity activity)
        {
            if (ModelState.IsValid)
            {
				activity.UpdatedBy = User.Identity.GetUserId();
			    activity.UpdatedDate = DateTime.Now;
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(activity);
        }

        // GET: /Activity/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.ActivitySet.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: /Activity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Activity activity = db.ActivitySet.Find(id);
            db.ActivitySet.Remove(activity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
