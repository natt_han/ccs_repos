﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class ResponseReasonController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /ResponseReason/
        public ActionResult Index()
        {
            var responsereasonset = db.ResponseReasonSet.Include(r => r.ResponseCategory);
            return View(responsereasonset.ToList());
        }

        // GET: /ResponseReason/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseReason responseReason = db.ResponseReasonSet.Find(id);
            if (responseReason == null)
            {
                return HttpNotFound();
            }
            return View(responseReason);
        }

        // GET: /ResponseReason/Create
        public ActionResult Create()
        {
            ViewBag.ResponseCategories = new SelectList(db.ResponseCategorySet, "ResponseCategoryId", "Name");
            return View();
        }

        // POST: /ResponseReason/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ResponseReasonId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ResponseCategoryId,MotorActive")] ResponseReason responseReason)
        {
            if (ModelState.IsValid)
            {
				responseReason.CreatedBy =
				responseReason.UpdatedBy = User.Identity.GetUserId();
				responseReason.CreatedDate =
                responseReason.UpdatedDate = DateTime.Now;
                db.ResponseReasonSet.Add(responseReason);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ResponseCategories = new SelectList(db.ResponseCategorySet, "ResponseCategoryId", "Name");
            return View(responseReason);
        }

        // GET: /ResponseReason/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseReason responseReason = db.ResponseReasonSet.Find(id);
            ViewBag.ResponseCategories = new SelectList(db.ResponseCategorySet, "ResponseCategoryId", "Name");
            if (responseReason == null)
            {
                return HttpNotFound();
            }
            return View(responseReason);
        }

        // POST: /ResponseReason/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ResponseReasonId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ResponseCategoryId,MotorActive")] ResponseReason responseReason)
        {
            if (ModelState.IsValid)
            {
				responseReason.UpdatedBy = User.Identity.GetUserId();
			    responseReason.UpdatedDate = DateTime.Now;
                db.Entry(responseReason).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ResponseCategories = new SelectList(db.ResponseCategorySet, "ResponseCategoryId", "Name");
            return View(responseReason);
        }

        // GET: /ResponseReason/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseReason responseReason = db.ResponseReasonSet.Find(id);
            if (responseReason == null)
            {
                return HttpNotFound();
            }
            return View(responseReason);
        }

        // POST: /ResponseReason/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ResponseReason responseReason = db.ResponseReasonSet.Find(id);
            db.ResponseReasonSet.Remove(responseReason);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
