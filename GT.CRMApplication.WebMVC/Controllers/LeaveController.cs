﻿using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public class LeaveController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        public LeaveController()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Leave
        public ActionResult Index(int? year=null)
        {
            if (year == null) year = DateTime.Today.Year;
            ViewBag.year = year;
            if (User.IsTSR())
            {
                var userid = User.Identity.GetUserId();
                var vm = db.LeaveRequestSet.Where(c => c.UserId == userid).ToList().Where(c=>c.Date.Year == year).OrderByDescending(o=>o.Date);
                return View(vm);
            }
            else
            {
                var vm = db.LeaveRequestSet.ToList().ToList().Where(c => c.Date.Year == year).OrderByDescending(o => o.Date);
                return View(vm);
            }            
        }

        public ActionResult Create()
        {
            var vm = new LeaveRequest();
            vm.Date = DateTime.Today;
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeaveRequest vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                vm.UserId = User.Identity.GetUserId();
                using (var iddb = new ApplicationDbContext())
                {
                    vm.UserName = iddb.Users.Find(vm.UserId).FullName;
                }
                vm.Status = 0;
                db.LeaveRequestSet.Add(vm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {                
                if(ex.GetBaseException().Message.Contains("duplicate key"))
                {
                    ModelState.AddModelError("", "วันลาซ้ำ");
                }
                return View(vm);
            }            
        }

        public ActionResult Edit(int Id)
        {
            var vm = db.LeaveRequestSet.Find(Id);
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LeaveRequest vm)
        {
            try
            {
                var en = db.LeaveRequestSet.Find(vm.Id);
                en.Date = vm.Date;
                en.Remark = vm.Remark;
                en.Status = 0;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.GetBaseException().Message.Contains("duplicate key"))
                {
                    ModelState.AddModelError("", "วันลาซ้ำ");
                }
                return View(vm);
            }
            
        }

        [Authorize(Roles = "PM,SYSTEMADMIN")]
        public ActionResult Approve(int Id)
        {
            var en = db.LeaveRequestSet.Find(Id);
            en.Status = 1;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "PM,SYSTEMADMIN")]
        public ActionResult Reject(int Id)
        {
            var en = db.LeaveRequestSet.Find(Id);
            en.Status = 2;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int Id)
        {
            var en = db.LeaveRequestSet.Find(Id);
            return View(en);
        }

        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int Id)
        {
            var en = db.LeaveRequestSet.Find(Id);
            db.LeaveRequestSet.Remove(en);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}