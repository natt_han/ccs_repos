﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public class CoveragesController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /Coverages/
        public ActionResult Index()
        {
            var dbset = db.CoverageSet;//.Include(l => l.Product);
            return View(dbset.ToList());
        }

        // GET: /Coverages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coverage Coverage = db.CoverageSet.Find(id);
            if (Coverage == null)
            {
                return HttpNotFound();
            }
            return View(Coverage);
        }

        // GET: /Coverages/Create
        public ActionResult Create()
        {
            var Coverage = new Coverage();
            return View(Coverage);
        }

        // POST: /Coverages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include= "CoverageTitle,CoverageContent, Active,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy,ProductId")] Coverage Coverage)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
				Coverage.CreatedBy =
				Coverage.UpdatedBy = User.Identity.GetUserId();
				Coverage.CreatedDate =
                Coverage.UpdatedDate = DateTime.Now;
                db.CoverageSet.Add(Coverage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Coverage);
        }

        // GET: /Coverages/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coverage Coverage = db.CoverageSet.Find(id);
            if (Coverage == null)
            {
                return HttpNotFound();
            }
            return View(Coverage);
        }

        // POST: /Coverages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CoverageTitle,CoverageContent, Active,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy")] Coverage Coverage)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var item in errors)
            {
                string message = item.ErrorMessage;
                ModelState.AddModelError("", item.Exception);
            }
            if (ModelState.IsValid)
            {
				Coverage.UpdatedBy = User.Identity.GetUserId();
			    Coverage.UpdatedDate = DateTime.Now;
                db.Entry(Coverage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Coverage);
        }

        // GET: /Coverages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Coverage Coverage = db.CoverageSet.Find(id);
            if (Coverage == null)
            {
                return HttpNotFound();
            }
            return View(Coverage);
        }

        // POST: /Coverages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Coverage Coverage = db.CoverageSet.Find(id);
            db.CoverageSet.Remove(Coverage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
