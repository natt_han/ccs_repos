﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class MasterPlansController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /MasterPlans/
        public ActionResult Index()
        {
            return View(db.MasterPlanSet.ToList());
        }

        // GET: /MasterPlans/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlan masterPlan = db.MasterPlanSet.Find(id);
            if (masterPlan == null)
            {
                return HttpNotFound();
            }
            return View(masterPlan);
        }

        // GET: /MasterPlans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /MasterPlans/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="PlanCode,PlanName,SA,CoverageId")] MasterPlan masterPlan)
        {
            if (ModelState.IsValid)
            {
                db.MasterPlanSet.Add(masterPlan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(masterPlan);
        }

        // GET: /MasterPlans/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlan masterPlan = db.MasterPlanSet.Find(id);
            if (masterPlan == null)
            {
                return HttpNotFound();
            }
            return View(masterPlan);
        }

        // POST: /MasterPlans/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="PlanCode,PlanName,SA,CoverageId")] MasterPlan masterPlan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(masterPlan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(masterPlan);
        }

        // GET: /MasterPlans/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlan masterPlan = db.MasterPlanSet.Find(id);
            if (masterPlan == null)
            {
                return HttpNotFound();
            }
            return View(masterPlan);
        }

        // POST: /MasterPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MasterPlan masterPlan = db.MasterPlanSet.Find(id);
            db.MasterPlanSet.Remove(masterPlan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
