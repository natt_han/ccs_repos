﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ActionResult HttpError403()
        {
            return View();
        }

        public ActionResult HttpError404()
        {
            return View();
        }

        public ActionResult HttpError500()
        {
            return View();
        }

        public ActionResult GeneralError(int httpcode)
        {
            return View();
        }

        public ViewResult Index()
        {
            return View("Error");
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            return View("HttpError404");
        }
        public ActionResult NotificationRequire()
        {
            return View();
        }
	}
}