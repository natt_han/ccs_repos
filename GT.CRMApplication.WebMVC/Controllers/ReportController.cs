﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using DM.Utilities.NetFramework.Excels.GT;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.GTApplicationDB;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Identity;
using GT.CRMApplication.WebMVC5.Helpers;
using ClosedXML.Excel;
using System.Diagnostics;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using WebGrease.Css.Extensions;
using Westwind.Utilities.Extensions;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public partial class ReportController : Controller
    {
        IGTCRMContext db = new IGTCRMContext();
        private ApplicationDbContext iddb = new ApplicationDbContext();
        ReportModel _ReportModel;

        public ReportController()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            db.Database.Log = sql => System.Diagnostics.Debug.Write(sql);
            this._ReportModel = new ReportModel();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
            iddb.Dispose();
        }

        private void DeleteTemp()
        {
            try
            {
                FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
            }
            catch { }
        }

        private void _loadSup_TsrList(){
            ViewBag.SupervisorList = new SelectList(iddb.GetSupervisorSelectList(), "CODE", "NAME");
            ViewBag.TsrList = new SelectList(iddb.ActiveTSRs().Select(s => new { CODE = s.Id, NAME = s.FirstName + " " + s.LastName }), "CODE", "NAME");
            if (User.IsSupervisor())
            {
                var supid= User.Identity.GetUserId();
                ViewBag.TsrList = new SelectList(iddb.ActiveTSRs().Where(c => c.SupervisorId == supid).Select(s => new { CODE = s.Id, NAME = s.FirstName + " " + s.LastName }), "CODE", "NAME");
            }
        }

        #region GET

        public ActionResult Dashboard()
        {
            return View("Index");
        }

        public ActionResult Index()
        {
            return RedirectToActionPermanent("SalesReport");
        }

        public ActionResult SalesReport()
        {
            return View("Index");
        }
        
        public ActionResult MarketingReport()
        {
            return View("MarketingReport");
        }

        public ActionResult IntegratedReport()
        {
            return View("IntegratedReport");
        }

        

        public ActionResult CallTrackingReport()
        {
            return View();
        }


        public ActionResult DailySalesReport()
        {
            return View();
        }


        public ActionResult DailySalesSummaryReport()
        {
            return View();
        }

        public ActionResult InboundCallReport()
        {
            return View();
        }


        public ActionResult LeadPerformanceByProductReport()
        {
            ViewBag.SupervisorList = new SelectList(iddb.GetSupervisorSelectList(), "CODE", "NAME");
            ViewBag.TsrList = new SelectList(iddb.ActiveTSRs().Select(s => new { CODE = s.Id, NAME = s.FirstName + " " + s.LastName }), "CODE", "NAME");
            LoadLeadProduct();
            return View();
        }

        
        public ActionResult KPIByTSRReport()
        {
            return View();
        }

        public ActionResult LeadConversion()
        {
            return View();
        }

       

        public ActionResult SalesPerformanceByTSRReport()
        {
            return View();
        }

        public ActionResult TSRSummaryReport()
        {
            return View();
        }
      

        public ActionResult TSRSalesByProductReport()
        {
            return View();
        }

        public ActionResult TSRTrackingReport()
        {
            _loadSup_TsrList();
            return View();
        }
        
        public ActionResult SubStatusTracking(string reportdate)
        {
            return View();
        }

        public ActionResult ExportSubStatusTracking(string reportdate)
        {
            try
            {
                var enddate = reportdate.ParseToDate().AddHours(23).AddMinutes(59);
                var td = _ReportModel.loadRPT_SubStatusTracking(db.Database.Connection.ConnectionString, reportdate.ParseToDate(), enddate).AsEnumerable();
                var mtd = _ReportModel.loadRPT_SubStatusTracking(db.Database.Connection.ConnectionString, new DateTime(enddate.Year,enddate.Month,1), enddate).AsEnumerable();
                using (var wb = new XLWorkbook(XLEventTracking.Disabled))
                {
                    var ws = wb.Worksheets.Add("Sheet1");
                    ws.Cell(1, 2).Value = string.Format("Sub Status Tracking : {0}", AppConfigHelper.ProjectName());
                    ws.Cell(2, 2).Value = string.Format("Report as of {0}", reportdate);
                    ws.Cell(3, 2).Value = string.Format("วันที่ออกรายงาน {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm"));

                    var irow = 4;
                    var mtdlead = mtd.Sum(s => s["Cnt"].ToString().ParseToPositiveInt());
                    var tdlead = td.Sum(s => s["Cnt"].ToString().ParseToPositiveInt());

                    ws.Cell(irow, 2).Value = "Total Leads MTD" ;
                    ws.Cell(irow, 3).Value = "As of " + enddate.ToString("dd MMM yyyy");
                    ws.Cell(irow, 4).Value = mtd.Sum(s => s["Cnt"].ToString().ParseToPositiveInt());
                    ws.Cell(irow, 5).Value = "1";
                    ws.Cell(irow, 5).Style.NumberFormat.Format = "0%";
                    ws.Range(irow, 2, irow, 5).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Fill.SetBackgroundColor(XLColor.FromHtml("#9C0006")).Font.SetFontColor(XLColor.White);
                    irow++;
                    foreach (DataRow dr in mtd)
                    {
                        if (dr["ResponseCategory"].ToString().IsNullOrEmpty()) continue;
                        ws.Cell(irow, 2).Value = dr["ResponseCategory"].ToString();
                        ws.Cell(irow, 3).Value = dr["ResponseSubReason"].ToString();
                        ws.Cell(irow, 4).Value = dr["Cnt"].ToString();
                        ws.Cell(irow, 5).FormulaA1 = string.Format("={0}/{1}", dr["Cnt"].ToString(),mtdlead);
                        ws.Cell(irow, 5).Style.NumberFormat.Format = "0%";
                        ws.Range(irow, 2, irow, 5).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                        irow++;
                    }
                    irow++;

                    ws.Cell(irow, 2).Value = "Total Leads today " ;
                    ws.Cell(irow, 3).Value = "'"+enddate.ToString("dd MMM yyyy");
                    ws.Cell(irow, 4).Value = td.Sum(s => s["Cnt"].ToString().ParseToPositiveInt());
                    ws.Cell(irow, 5).Value = "1";
                    ws.Cell(irow, 5).Style.NumberFormat.Format = "0%";
                    ws.Range(irow, 2, irow, 5).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Fill.SetBackgroundColor(XLColor.FromHtml("#9C0006")).Font.SetFontColor(XLColor.White);
                    irow++;
                    foreach (DataRow dr in td)
                    {
                        if (dr["ResponseCategory"].ToString().IsNullOrEmpty()) continue;
                        ws.Cell(irow, 2).Value = dr["ResponseCategory"].ToString();
                        ws.Cell(irow, 3).Value = dr["ResponseSubReason"].ToString();
                        ws.Cell(irow, 4).Value = dr["Cnt"].ToString();
                        ws.Cell(irow, 5).FormulaA1 = string.Format("={0}/{1}", dr["Cnt"].ToString(), tdlead);
                        ws.Cell(irow, 5).Style.NumberFormat.Format = "0%";
                        ws.Range(irow, 2, irow, 5).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin).Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                        irow++;
                    }
                    ws.Columns().AdjustToContents();
                    string tempfilename = Url.Content("~/Downloads/") + string.Format("Sub Status Tracking {1} {0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"), AppConfigHelper.ProjectName());
                    wb.SaveAs(Server.MapPath(tempfilename));

                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("error :" + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

      
        private object GetValueIfExist(DataRow dr,string colname){
            if(dr.Table.Columns.Contains(colname)){
                return dr[colname];
            }
            return null;
        }

        private DataTable Transpose(DataTable dt)
        {
            DataTable dtNew = new DataTable();

            //adding columns    
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtNew.Columns.Add(i.ToString());
            }



            //Changing Column Captions: 
            dtNew.Columns[0].ColumnName = "Type";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //For dateTime columns use like below
                //dtNew.Columns[i + 1].ColumnName = Convert.ToDateTime(dt.Rows[i].ItemArray[0].ToString()).ToString("MM/dd/yyyy");
                dtNew.Columns[i + 1].ColumnName = dt.Rows[i].ItemArray[0].ToString();
                //Else just assign the ItermArry[0] to the columnName prooperty
            }

            //Adding Row Data
            for (int k = 1; k < dt.Columns.Count; k++)
            {
                DataRow r = dtNew.NewRow();
                r[0] = dt.Columns[k].ToString();
                for (int j = 1; j <= dt.Rows.Count; j++)
                    r[j] = dt.Rows[j - 1][k];
                dtNew.Rows.Add(r);
            }

            return dtNew;
        }


        [HttpGet]
        public ActionResult RetrieveRPT_KPI_TSR(string startdate, string enddate)
        {
            string conectionstring = db.Database.Connection.ConnectionString;
            DataTable datasource = null;
            if (User.IsTSR())
            {
                datasource = _ReportModel.loadRPT_KPI_TSR(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
            }
            else if (User.IsInRole("SUPERVISOR"))
            {
                datasource = _ReportModel.loadRPT_KPI_TSR(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, User.Identity.GetUserId());
            }
            else if (User.IsManager())
            {
                datasource = _ReportModel.loadRPT_KPI_TSR(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, null);
            }
            var suplist = datasource.AsEnumerable().GroupBy(g => g["SupervisorName"].ToString()).Select(g => g.First());
            var tdataset = from sup in suplist
                           select new
                           {
                               dt = (from row in datasource.AsEnumerable()
                                     where row["SupervisorName"].ToString() == sup["SupervisorName"].ToString()
                                     select new
                                     {
                                         Fullname = row["Fullname"].ToString(),
                                         Supervisor = row["SupervisorName"].ToString(),
                                         total_lead = row["total_lead"].ToString().ParseToPositiveInt(),
                                         new_lead = row["new_lead"].ToString().ParseToPositiveInt(),
                                         old_lead = row["old_lead"].ToString().ParseToPositiveInt(),
                                         new_used = row["new_used"].ToString().ParseToPositiveInt(),
                                         old_used = row["old_used"].ToString().ParseToPositiveInt(),
                                         total_used = row["total_used"].ToString().ParseToPositiveInt(),
                                         total_call = row["total_call"].ToString().ParseToPositiveInt(),
                                         total_talk_time = row["total_talk_time"].ToString().ParseToPositiveInt(),
                                         new_lead_call = row["new_lead_call"].ToString().ParseToPositiveInt(),
                                         call_attempt = row["call_attempt"].ToString().ParseToPositiveDouble(),
                                         new_lead_call_attempt = row["new_lead_call_attempt"].ToString().ParseToPositiveDouble(),
                                         nocontact = row["nocontact"].ToString().ParseToPositiveInt(),
                                         followup = row["followup"].ToString().ParseToPositiveInt(),
                                         wait = row["wait"].ToString().ParseToPositiveInt(),
                                         inprogress = row["inprogress"].ToString().ParseToPositiveInt(),
                                         refuse = row["refuse"].ToString().ParseToPositiveInt(),
                                         unsuccess = row["unsuccess"].ToString().ParseToPositiveInt(),
                                         reject = row["reject"].ToString().ParseToPositiveInt(),
                                         notupdate = row["not_update"].ToString().ParseToPositiveInt(),
                                         nottarget = row["not_target"].ToString().ParseToPositiveInt(),
                                         success = row["success"].ToString().ParseToPositiveInt(),
                                         dmc = row["dmc"].ToString().ParseToPositiveInt(),
                                         dmc_rate = row["dmc_rate"].ToString().ParseToPositiveDecimal().ToString("0.00"),
                                         list_conv = row["list_conv"].ToString().ParseToPositiveDouble(),
                                         respond_rate = row["respond_rate"].ToString().ParseToPositiveDouble(),
                                         submit = row["submit"].ToString().ParseToPositiveInt(),
                                         approve = row["approved"].ToString().ParseToPositiveInt(),
                                         sum_submit = row["sum_submit"].ToString().ParseToPositiveDecimal(),
                                         sum_approve = row["sum_approved"].ToString().ParseToPositiveDecimal(),
                                         new_approve = row["new_approved"].ToString().ParseToPositiveInt(),
                                         sumnew_approve = row["sumnew_approved"].ToString().ParseToPositiveDecimal(),
                                     }).ToList()
                           };
            var dataset = from row in tdataset
                          select new
                          {
                              dt = row.dt,
                              summary = new
                              {
                                  Supervisor = row.dt.First().Supervisor,
                                  total_lead = row.dt.Sum(r => r.total_lead),
                                  new_lead = row.dt.Sum(r => r.new_lead),
                                  old_lead = row.dt.Sum(r => r.old_lead),
                                  new_used = row.dt.Sum(r => r.new_used),
                                  old_used = row.dt.Sum(r => r.old_used),
                                  total_used = row.dt.Sum(r => r.total_used),
                                  total_call = row.dt.Sum(r => r.total_call),
                                  total_talk_time = row.dt.Sum(r => r.total_talk_time),
                                  new_lead_call = row.dt.Sum(r => r.new_lead_call),
                                  nocontact = row.dt.Sum(r => r.nocontact),
                                  followup = row.dt.Sum(r => r.followup),
                                  wait = row.dt.Sum(r => r.wait),
                                  inprogress = row.dt.Sum(r => r.inprogress),
                                  refuse = row.dt.Sum(r => r.refuse),
                                  unsuccess = row.dt.Sum(r => r.unsuccess),
                                  reject = row.dt.Sum(r => r.reject),
                                  notupdate = row.dt.Sum(r => r.notupdate),
                                  nottarget = row.dt.Sum(r => r.nottarget),
                                  success = row.dt.Sum(r => r.success),
                                  dmc = row.dt.Sum(r => r.dmc),
                                  submit = row.dt.Sum(r => r.submit),
                                  approve = row.dt.Sum(r => r.approve),
                                  sum_submit = row.dt.Sum(r => r.sum_submit),
                                  sum_approve = row.dt.Sum(r => r.sum_approve),
                                  new_approve = row.dt.Sum(r => r.new_approve),
                                  sumnew_approve = row.dt.Sum(r => r.sumnew_approve),
                              }
                          };
            return Json(dataset, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RetrieveRPT_KPI_DB(string startdate, string enddate, string tsr, string supervisor, string[] source)
        {
            string conectionstring = db.Database.Connection.ConnectionString;
            DataTable datasource = null;
            if (User.IsTSR())
            {
                datasource = _ReportModel.loadRPT_KPI_DB(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
            }
            else if (User.IsInRole("SUPERVISOR"))
            {
                datasource = _ReportModel.loadRPT_KPI_DB(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), tsr, User.Identity.GetUserId());
            }
            else if (User.IsManager())
            {
                datasource = _ReportModel.loadRPT_KPI_DB(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), tsr, supervisor);
            }
            var data = datasource.AsEnumerable().Where(c => source.Contains(c["sourcecode"].ToString()));
            if (source.Length == 1 && source[0] == "all") data = datasource.AsEnumerable();
            var result = from row in data
                         select new
                         {
                             callsource = string.IsNullOrEmpty(row["sourcename"].ToString()) ? "Unknown" : row["sourcename"].ToString(),
                             total_lead = row["total_lead"].ToString().ParseToPositiveInt(),
                             new_lead = row["new_lead"].ToString().ParseToPositiveInt(),
                             old_lead = row["old_lead"].ToString().ParseToPositiveInt(),
                             new_used = row["new_used"].ToString().ParseToPositiveInt(),
                             old_used = row["old_used"].ToString().ParseToPositiveInt(),
                             total_used = row["total_used"].ToString().ParseToPositiveInt(),
                             total_call = row["total_call"].ToString().ParseToPositiveInt(),
                             call_attempt = row["call_attempt"].ToString().ParseToPositiveDouble(),
                             nocontact = row["nocontact"].ToString().ParseToPositiveInt(),
                             followup = row["followup"].ToString().ParseToPositiveInt(),
                             wait = row["wait"].ToString().ParseToPositiveInt(),
                             inprogress = row["inprogress"].ToString().ParseToPositiveInt(),
                             refuse = row["refuse"].ToString().ParseToPositiveInt(),
                             unsuccess = row["unsuccess"].ToString().ParseToPositiveInt(),
                             reject = row["reject"].ToString().ParseToPositiveInt(),
                             notupdate = row["not_update"].ToString().ParseToPositiveInt(),
                             nottarget = row["not_target"].ToString().ParseToPositiveInt(),
                             success = row["success"].ToString().ParseToPositiveInt(),
                             dmc = row["dmc"].ToString().ParseToPositiveInt(),
                             dmc_rate = row["dmc_rate"].ToString(),
                             list_conv = row["list_conv"].ToString().ParseToPositiveDouble(),
                             respond_rate = row["respond_rate"].ToString().ParseToPositiveDouble(),
                             submit = row["submit"].ToString().ParseToPositiveInt(),
                             approve = row["approved"].ToString().ParseToPositiveInt(),
                             sum_submit = row["sum_submit"].ToString().ParseToPositiveDecimal(),
                             sum_approve = row["sum_approved"].ToString().ParseToPositiveDecimal(),
                             new_approve = row["new_approved"].ToString().ParseToPositiveInt(),
                             sumnew_approve = row["sumnew_approved"].ToString().ParseToPositiveDecimal(),
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
        [HttpGet]
        public ActionResult RetrieveRPT_LeadConv(string startdate, string enddate, string tsr, string supervisor)
        {
            string conectionstring = db.Database.Connection.ConnectionString;
            DataTable datasource = null;

            datasource = _ReportModel.loadRPT_LeadConversion(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), null, null);


            var result = from row in datasource.AsEnumerable()
                         select new
                         {
                             fullname = row["fullname"].ToString(),
                             supervisor = row["supervisorname"].ToString(),
                             new_lead = row["new_lead"].ToString().ParseToPositiveInt(),
                             success = row["success"].ToString().ParseToPositiveInt(),
                             lead_conv = row["lead_conv"].ToString().ParseToPositiveDouble(),
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Display Report on screen
        [HttpGet]
        public ActionResult RetrieveTSRSummaryReport(string startdate, string enddate)
        {
            string connectionString = db.Database.Connection.ConnectionString;
            DataTable datasource = _ReportModel.loadRPT_TSRSummaryReport(connectionString, startdate.ParseToDate(), enddate.ParseToDate());

            var result = from row in datasource.AsEnumerable()
                         select new
                         {
                             team = row["Team"].ToString(),
                             supervisor = row["SupervisorName"].ToString(),
                             tsr = row["TSRName"].ToString(),
                             total_call = row["TotalCall"].ToString().ParseToPositiveInt(),
                             total_talk_time = row["TotalTalkTime"].ToString().ParseToPositiveInt(),
                             no_contact = row["NoContact"].ToString().ParseToPositiveInt(),
                             not_update = row["NotUpdate"].ToString().ParseToPositiveInt(),
                             not_target = row["NotTarget"].ToString().ParseToPositiveInt(),
                             reject = row["Reject"].ToString().ParseToPositiveInt(),
                             follow_up = row["FollowUp"].ToString().ParseToPositiveInt(),
                             success = row["Success"].ToString().ParseToPositiveInt(),
                             submit_policy = row["SubmitPolicy"].ToString().ParseToPositiveInt(),
                             submit_premium = row["SubmitPremium"].ToString().ParseToPositiveDecimal(),
                             approve_policy = row["ApprovePolicy"].ToString().ParseToPositiveInt(),
                             approve_premium = row["ApprovePremium"].ToString().ParseToPositiveDecimal()
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet]
        public ActionResult RetrieveDailySalesSummaryReport(string startdate, string enddate, string productcat)
        {
            try
            {
                bool allproject = GetIsAllProject();

                string connectionstring = db.Database.Connection.ConnectionString;
                DataTable datasource = _ReportModel.loadRPT_DailySalesSummary(connectionstring, startdate.ParseToDate(), enddate.ParseToDate(), productcat, allproject);
                int workingTsrs = _ReportModel.GetWorkingTsrs(connectionstring, startdate.ParseToDate());
                int saleTsrs = _ReportModel.GetSaleTsrs(connectionstring, startdate.ParseToDate());
                var result = (from row in datasource.AsEnumerable()
                              select new
                              {
                                  productname = row["PRODUCTNAME"].ToString(),
                                  submit = row["submit"].ToString(),
                                  submit_api = row["submit_api"],
                                  approve = row["approve"].ToString(),
                                  approve_api = row["approve_api"],
                                  submitgt = row["submitgt"].ToString(),
                                  submitgt_api = row["submitgt_api"],
                                  
                              });
                var summary = (from row in datasource.AsEnumerable().Take(1)
                               let dt = datasource.AsEnumerable()
                               select new
                               {
                                   productname = "Total",
                                   submit = dt.Sum(r => r["submit"].ToString().ParseToPositiveInt()),
                                   submit_api = dt.Sum(r => r["submit_api"].ToString().ParseToPositiveDecimal()),
                                   approve = dt.Sum(r => r["approve"].ToString().ParseToPositiveInt()),
                                   approve_api = dt.Sum(r => r["approve_api"].ToString().ParseToPositiveDecimal()),
                                   submitgt = dt.Sum(r => r["submitgt"].ToString().ParseToPositiveInt()),
                                   submitgt_api = dt.Sum(r => r["submitgt_api"].ToString().ParseToPositiveDecimal()),
                               });

                //var json = new json
                var j = new Dictionary<string, object>();
                j.Add("workingtsrs", workingTsrs);
                j.Add("saletsrs", saleTsrs);
                j.Add("data", result);
                j.Add("summary", summary);
                return Json(j, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }            
        }

        [HttpGet]
        public ActionResult RetrieveDailySalesReport(string startdate, string enddate)
        {
            int i = 1;
            string conectionstring = db.Database.Connection.ConnectionString;
            DataTable datasource = _ReportModel.loadRPT_DailySales(conectionstring, startdate.ParseToDate(), enddate.ParseToDate());

            var result = from row in datasource.AsEnumerable()
                         select new
                         {
                             SEQ = i++,
                             APPLICATIONNO = row["APPLICATIONNO"].ToString(),
                             CREATE_DATE = row["CREATE_DATE"].ToString(),
                             PRODUCTNAME = row["PRODUCTNAME"].ToString(),
                             PLANCODE = row["PLANCODE"].ToString(),
                             INSTALMENTPREMIUM = row["INSTALMENTPREMIUM"].ToString(),
                             APP_STATUS = row["APP_STATUS"].ToString(),
                             ASSURED_FULLNAME = row["ASSURED_FULLNAME"].ToString(),
                             SOURCEOFLEAD = row["SOURCEOFLEAD"].ToString(),
                             TSRNAME = row["TSRNAME"].ToString(),
                             SUPERVISOR_NAME = row["SUPERVISOR_NAME"].ToString()
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet]
        public ActionResult RetrieveLeadPerformance(string startdate, string enddate, string tsr, string supervisor, string[] source)
        {
            string conectionstring = db.Database.Connection.ConnectionString;
            DataTable datasource = null;
            if (User.IsTSR())
            {
                datasource = _ReportModel.loadRPT_LeadPerformance(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), User.Identity.GetUserId(), null);
            }
            else if (User.IsInRole("SUPERVISOR"))
            {
                datasource = _ReportModel.loadRPT_LeadPerformance(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), tsr, User.Identity.GetUserId());
            }
            else if (User.IsManager())
            {
                datasource = _ReportModel.loadRPT_LeadPerformance(conectionstring, startdate.ParseToDate(), enddate.ParseToDate(), tsr, supervisor);
            }
            var data = datasource.AsEnumerable().Where(c => source.Contains(c["source"].ToString()));
            if (source.Length == 1 && source[0] == "all") data = datasource.AsEnumerable();
            var result = from row in data
                         select new
                         {
                             callsource = string.IsNullOrEmpty(row["SourceName"].ToString()) ? "Unknown" : row["SourceName"].ToString(),
                             product = row["Product"].ToString(),
                             income_lead = row["TotalIncomeLead"].ToString().ParseToPositiveInt(),
                             assigned = row["Assigned"].ToString().ParseToPositiveInt(),
                             unassigned = row["Unassigned"].ToString().ParseToPositiveInt(),
                             total_lead = row["TotalLead"].ToString().ParseToPositiveInt(),
                             new_lead = row["NewLead"].ToString().ParseToPositiveInt(),
                             old_lead = row["OldLead"].ToString().ParseToPositiveInt(),
                             new_used = row["NewUsed"].ToString().ParseToPositiveInt(),
                             old_used = row["OldUsed"].ToString().ParseToPositiveInt(),
                             total_used = row["TotalUsed"].ToString().ParseToPositiveInt(),
                             total_call = row["TotalCall"].ToString().ParseToPositiveInt(),
                             call_attempt = row["CallAttempt"].ToString().ParseToPositiveDouble(),
                             nocontact = row["NoContact"].ToString().ParseToPositiveInt(),
                             followup = row["FollowUp"].ToString().ParseToPositiveInt(),
                             wait = row["Wait"].ToString().ParseToPositiveInt(),
                             inprogress = row["InProgress"].ToString().ParseToPositiveInt(),
                             refuse = row["Refuse"].ToString().ParseToPositiveInt(),
                             unsuccess = row["unsuccess"].ToString().ParseToPositiveInt(),
                             reject = row["Reject"].ToString().ParseToPositiveInt(),
                             notupdate = row["NotUpdate"].ToString().ParseToPositiveInt(),
                             nottarget = row["NotTarget"].ToString().ParseToPositiveInt(),
                             success = row["Success"].ToString().ParseToPositiveInt(),
                             dmc = row["DMC"].ToString().ParseToPositiveInt(),
                             dmc_rate = row["DMCRate"].ToString(),
                             list_conv = row["ListConv"].ToString().ParseToPositiveDouble(),
                             respond_rate = row["RespondRate"].ToString().ParseToPositiveDouble(),
                             submit = row["Submit"].ToString().ParseToPositiveInt(),
                             approve = row["Approved"].ToString().ParseToPositiveInt(),
                             sum_submit = row["SumSubmit"].ToString().ParseToPositiveDecimal(),
                             sum_approve = row["SumApproved"].ToString().ParseToPositiveDecimal(),
                             new_approve = row["NewApproved"].ToString().ParseToPositiveInt(),
                             sumnew_approve = row["SumNewApproved"].ToString().ParseToPositiveDecimal(),
                         };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    
       
        [HttpGet]
        public ActionResult ExportCallTrackingReport(string startdate, string enddate)
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                string password = ConfigurationManager.AppSettings["excelpassword"].ToString();
                string tempfilename = "";
                string templatefilepath = "~/Excels/CallTrackingReport.xls";
                DataTable datasource = _ReportModel.loadRPT_CallTracking(connectionstring, startdate.ParseToDate(), enddate.ParseToDate());

                // open excel template and append it.
                ExcelWriter writer = new ExcelWriter();
                try
                {
                    writer.StartExcel(false);
                    writer.OpenExcelWorkBook(Server.MapPath(templatefilepath));

                    var items = writer.GetWorkSheetInfo();
                    int worksheetindex = 1;
                    int startrow = 9;
                    int startcol = 1;

                    writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);
                    int lastcolumn = writer.GetUsedRangeColumn(worksheetindex);
                    int lastrow = writer.GetUsedRangeRow(worksheetindex)+1;

                    // update format painter.
                    ExcelWriter.ExcelCell copytopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell copybottomright = new ExcelWriter.ExcelCell(lastcolumn, startrow);
                    ExcelWriter.ExcelCell pastetopleft = new ExcelWriter.ExcelCell(startcol, startrow);
                    ExcelWriter.ExcelCell pastebottomright = new ExcelWriter.ExcelCell(lastcolumn, lastrow);
                    writer.CopyFormatPainterByRange(worksheetindex, copytopleft, copybottomright, pastetopleft, pastebottomright);

                    writer.WriteWholeDataTable(worksheetindex, datasource, startrow, startcol);

                    writer.WriteCellData(1, new ExcelWriter.ExcelCell("D3"), startdate.ParseSpecifyDateFormat("d/M/yyyy"));
                    writer.WriteCellData(1, new ExcelWriter.ExcelCell("D4"), enddate.ParseSpecifyDateFormat("d/M/yyyy"));
                    writer.WriteCellData(1, new ExcelWriter.ExcelCell("D5"), DateTime.Now.Date);

                    // set password
                    writer.ProtectFile(password);

                    // save as file to the new directory.
                    string fileidentity = string.Format("{0}_{1}", startdate.ParseToDate().ToString("yyyyMMdd"), enddate.ParseToDate().ToString("yyyyMMdd"));
                    tempfilename = Url.Content("~/Downloads/") + string.Format("CALLTRACKING_REPORT_{0}.xls", fileidentity);

                    writer.SaveFileAs(Server.MapPath(tempfilename));
                    writer.Close();
                    writer.ReleaseAllComObject();

                    // Delete Older Temporary File on server.
                    try
                    {
                        FileSystemHelperExtension.DeleteTemporaryFiles(Server.MapPath(Url.Content(string.Format("~/Downloads"))));
                    }
                    catch { }
                    return Json(tempfilename, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    writer.Close();
                    writer.ReleaseAllComObject();

                    return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json("error : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

       
      
     
        internal class KPIbyDBReportVM
        {
            public string Sourcename { get; }
            public int Total_lead { get; }
            public int New_lead { get; }
            public int Old_lead { get; }
            public int Total_used { get; }
            public int Total_call { get; }
            public double Call_attempt { get; }
            public int Nocontact { get; }
            public int Followup { get; }
            public int Wait { get; }
            public int Inprogress { get; }
            public int Reject { get; }
            public int Not_update { get; }
            public int Not_target { get; }
            public int Success { get; }
            public int Dmc { get; }
            public int New_dmc { get; }
            public double Dmc_rate { get; }
            public double New_dmc_rate { get; }
            public double List_conv { get; }
            public double Respond_rate { get; }
            public double New_respond_rate { get; }
            public int Submit { get; }
            public decimal Sum_submit { get; }
            public int Approved { get; }
            public decimal Sum_approved { get; }
            public decimal Case_size { get; }
            public int New_approved { get; }
            public decimal Sumnew_approved { get; }
            public double Lead_conv { get; }

            public KPIbyDBReportVM(string sourcename, int total_lead, int new_lead, int old_lead, int total_used, int total_call, double call_attempt, int nocontact, int followup, int wait, int inprogress, int reject, int not_update, int not_target, int success,
                int dmc, int new_dmc, double dmc_rate, double new_dmc_rate, double list_conv, double respond_rate, double new_respond_rate, int submit, decimal sum_submit, int approved, decimal sum_approved, decimal case_size, int new_approved, decimal sumnew_approved, double lead_conv)
            {
                Sourcename = sourcename;
                Total_lead = total_lead;
                New_lead = new_lead;
                Old_lead = old_lead;
                Total_used = total_used;
                Total_call = total_call;
                Call_attempt = call_attempt;
                Nocontact = nocontact;
                Followup = followup;
                Wait = wait;
                Inprogress = inprogress;
                Reject = reject;
                Not_update = not_update;
                Not_target = not_target;
                Success = success;
                Dmc = dmc;
                New_dmc = new_dmc;
                Dmc_rate = dmc_rate;
                New_dmc_rate = new_dmc_rate;
                List_conv = list_conv;
                Respond_rate = respond_rate;
                New_respond_rate = new_respond_rate;
                Submit = submit;
                Sum_submit = sum_submit;
                Approved = approved;
                Sum_approved = sum_approved;
                Case_size = case_size;
                New_approved = new_approved;
                Sumnew_approved = sumnew_approved;
                Lead_conv = lead_conv;
            }
            
        }

        private List<KPIbyDBReportVM> parseToKPIDBVM(IEnumerable<DataRow> datasource)
        {
            var result = (from row in datasource
                          select new KPIbyDBReportVM(
                            string.IsNullOrEmpty(row["sourcename"].ToString()) ? "Unknown" : row["sourcename"].ToString(),
                            row["total_lead"].ToString().ParseToPositiveInt(),
                            row["new_lead"].ToString().ParseToPositiveInt(),
                            row["old_lead"].ToString().ParseToPositiveInt(),
                            row["total_used"].ToString().ParseToPositiveInt(),
                            row["total_call"].ToString().ParseToPositiveInt(),
                            row["call_attempt"].ToString().ParseToPositiveDouble(),
                            row["nocontact"].ToString().ParseToPositiveInt(),
                            row["followup"].ToString().ParseToPositiveInt(),
                            row["wait"].ToString().ParseToPositiveInt(),
                            row["inprogress"].ToString().ParseToPositiveInt(),
                            row["reject"].ToString().ParseToPositiveInt(),
                            row["not_update"].ToString().ParseToPositiveInt(),
                            row["not_target"].ToString().ParseToPositiveInt(),
                            row["success"].ToString().ParseToPositiveInt(),
                            row["dmc"].ToString().ParseToPositiveInt(),
                            row["new_dmc"].ToString().ParseToPositiveInt(),
                            row["dmc_rate"].ToString().ParseToPositiveDouble(),
                            row["new_dmc_rate"].ToString().ParseToPositiveDouble(),
                            row["list_conv"].ToString().ParseToPositiveDouble(),
                            row["respond_rate"].ToString().ParseToPositiveDouble(),
                            row["new_respond_rate"].ToString().ParseToPositiveDouble(),
                            row["submit"].ToString().ParseToPositiveInt(),
                            row["sum_submit"].ToString().ParseToPositiveDecimal(),
                            row["approved"].ToString().ParseToPositiveInt(),
                            row["sum_approved"].ToString().ParseToPositiveDecimal(),
                            row["case_size"].ToString().ParseToPositiveDecimal(),
                            row["new_approved"].ToString().ParseToPositiveInt(),
                            row["sumnew_approved"].ToString().ParseToPositiveDecimal(),
                            row["lead_conv"].ToString().ParseToPositiveDouble()
                         )).ToList();
            var dt = datasource;
            var total_lead = dt.Sum(row => row.Field<int?>("total_lead")).GetValueOrDefault(0);
            var new_lead = dt.Sum(row => row.Field<int?>("new_lead")).GetValueOrDefault(0);
            var total_used = dt.Sum(row => row.Field<int?>("total_used")).GetValueOrDefault(0);
            var new_used = dt.Sum(row => row.Field<int?>("new_used")).GetValueOrDefault(0);
            var total_call = dt.Sum(row => row.Field<int?>("total_call")).GetValueOrDefault(0);
            var success = dt.Sum(row => row.Field<int?>("success")).GetValueOrDefault(0);
            var new_success = dt.Sum(row => row.Field<int?>("new_success")).GetValueOrDefault(0);
            var dmc = dt.Sum(row => row.Field<int?>("dmc")).GetValueOrDefault(0);
            var new_dmc = dt.Sum(row => row.Field<int?>("new_dmc")).GetValueOrDefault(0);
            var approved = dt.Sum(row => row.Field<int?>("approved")).GetValueOrDefault(0);
            var sum_approved = dt.Sum(row => row.Field<decimal?>("sum_approved")).GetValueOrDefault(0);
            var new_approved = dt.Sum(row => row.Field<int?>("new_approved")).GetValueOrDefault(0);
            result.Add( new KPIbyDBReportVM
            (
                "Total",
                total_lead,
                new_lead,
                dt.Sum(row => row.Field<int>("old_lead")),
                total_used,
                total_call,
                SolutionHelper.divide(total_call, total_used),
                dt.Sum(row => row.Field<int>("nocontact")),
                dt.Sum(row => row.Field<int>("followup")),
                dt.Sum(row => row.Field<int>("wait")),
                dt.Sum(row => row.Field<int>("inprogress")),
                dt.Sum(row => row.Field<int>("reject")),
                dt.Sum(row => row.Field<int>("not_update")),
                dt.Sum(row => row.Field<int>("not_target")),
                success = dt.Sum(row => row.Field<int>("success")),
                dmc,
                new_dmc,
                SolutionHelper.divide(dmc * 100.0, total_used),
                SolutionHelper.divide(new_dmc * 100.0, new_used),
                SolutionHelper.divide(approved * 100.0, total_lead),
                SolutionHelper.divide(success * 100.0, dmc),
                SolutionHelper.divide(new_success * 100.0, new_dmc),
                dt.Sum(row => row.Field<int?>("submit")).GetValueOrDefault(0),
                dt.Sum(row => row.Field<decimal?>("sum_submit")).GetValueOrDefault(0),
                approved,
                sum_approved,
                SolutionHelper.divide(sum_approved, approved),
                new_approved,
                dt.Sum(row => row.Field<decimal?>("sumnew_approved")).GetValueOrDefault(0),
                SolutionHelper.divide(new_approved * 100.0, new_lead)
            ));
            return result;
        }
        
       
       
      
             
    
        private void freecachedb(){
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                _ReportModel.FreeProcCache(connectionstring);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        private void freecachedball()
        {
            try
            {
                string connectionstring = db.Database.Connection.ConnectionString;
                _ReportModel.FreeProcCacheAll(connectionstring);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        [HttpGet]
        public ActionResult FreeProcCache()
        {
            try
            {
                freecachedb();
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FreeProcCacheAll()
        {
            try
            {
                freecachedball();
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error : " + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
   
      
        #endregion

        private void LoadLeadProduct(string value = null)
        {
            var products = from row in db.OfferProductSet
                              where row.Active.Value
                              select new
                              {
                                  code = row.OfferProductId,
                                  name = row.Name
                              };
            ViewBag.Products = new SelectList(products.ToList(), "code", "name", value);
        }

     
        private bool GetIsAllProject()
        {
            string[] projects = new string[] { "IGL", "STAGING" };
            return projects.Contains(AppConfigHelper.ProjectName());
        }
    }

    
}