﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize(Roles = "SYSTEMADMIN,PM")]
    public class ResponseSubReasonController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /ResponseSubReason/
        public ActionResult Index()
        {
            var responsesubreasonset = db.ResponseSubReasonSet.Include(r => r.ResponseReason);
            return View(responsesubreasonset.ToList());
        }

        // GET: /ResponseSubReason/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseSubReason responseSubReason = db.ResponseSubReasonSet.Find(id);
            if (responseSubReason == null)
            {
                return HttpNotFound();
            }
            return View(responseSubReason);
        }

        // GET: /ResponseSubReason/Create
        public ActionResult Create()
        {
            LoadComponent();
            return View();
        }

        // POST: /ResponseSubReason/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ResponseSubReasonId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ResponseReasonId,MotorActive")] ResponseSubReason responseSubReason)
        {
            if (ModelState.IsValid)
            {
				responseSubReason.CreatedBy =
				responseSubReason.UpdatedBy = User.Identity.GetUserId();
				responseSubReason.CreatedDate =
                responseSubReason.UpdatedDate = DateTime.Now;
                db.ResponseSubReasonSet.Add(responseSubReason);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            LoadComponent();
            return View(responseSubReason);
        }

        // GET: /ResponseSubReason/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseSubReason responseSubReason = db.ResponseSubReasonSet.Find(id);
            LoadComponent();
            if (responseSubReason == null)
            {
                return HttpNotFound();
            }
            return View(responseSubReason);
        }

        // POST: /ResponseSubReason/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ResponseSubReasonId,Code,Name,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ResponseReasonId,MotorActive")] ResponseSubReason responseSubReason)
        {
            if (ModelState.IsValid)
            {
				responseSubReason.UpdatedBy = User.Identity.GetUserId();
			    responseSubReason.UpdatedDate = DateTime.Now;
                db.Entry(responseSubReason).State = EntityState.Modified;
                db.SaveChanges();
                LoadComponent();
                return RedirectToAction("Index");
            }
            
            return View(responseSubReason);
        }

        // GET: /ResponseSubReason/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResponseSubReason responseSubReason = db.ResponseSubReasonSet.Find(id);
            if (responseSubReason == null)
            {
                return HttpNotFound();
            }
            return View(responseSubReason);
        }

        // POST: /ResponseSubReason/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ResponseSubReason responseSubReason = db.ResponseSubReasonSet.Find(id);
            db.ResponseSubReasonSet.Remove(responseSubReason);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public void LoadComponent()
        {
            var data = from row in db.ResponseReasonSet
                       select new {
                           ResponseReasonId = row.ResponseReasonId,
                           Name = row.ResponseCategory.Name + " => " + row.Name
                       };
            ViewBag.ResponseReasons = new SelectList(data, "ResponseReasonId", "Name");
        }
    }
}
