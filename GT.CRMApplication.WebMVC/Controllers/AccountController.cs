﻿using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model;
using GT.InboundApplication.Model.Common;
using GT.InboundApplication.Model.CRM;
using GT.CRMApplication.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System;
using System.Data.Entity.Migrations;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }


        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null && !user.IsVerified)
                {
                    ModelState.AddModelError("", "User Account is waiting for verify. Please contact Administrator to verify it.");
                }
                else if (user != null && user.IsBlocked)
                {
                    ModelState.AddModelError("", "User Account is blocked. Please contact Administrator to unblock it.");
                }
                else
                {
                    if (user != null)
                    {
                        await SignInAsync(user, model.RememberMe);
                        this.UpdateAccessTimeStamp(user.Id);
                        using(var db = new IGTCRMContext())
                        {
                            var userLogged = db.UserLoggedSet.Find(DateTime.Today, user.Id);
                            if (userLogged == null || userLogged.LoggedIn == null)
                            {
                                if(userLogged == null)userLogged = new UserLogged() { UserId = user.Id, Date = DateTime.Today };
                                userLogged.LoggedIn = DateTime.Now;
                                db.UserLoggedSet.AddOrUpdate(userLogged);
                                db.SaveChanges();
                            }                            
                        }
                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid username or password.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> LoginApi(string user, string pass)
        {
            if (ModelState.IsValid)
            {
                var u = await UserManager.FindAsync(user, pass);
                if (u != null && !u.IsVerified)
                {
                    ModelState.AddModelError("", "User Account is waiting for verify. Please contact Administrator to verify it.");
                }
                else if (u != null && u.IsBlocked)
                {
                    ModelState.AddModelError("", "User Account is blocked. Please contact Administrator to unblock it.");
                }
                else
                {
                    if (u != null)
                    {
                        await SignInAsync(u, true);
                        return Json(new { result = "1" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid username or password.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return Json(new { result = "0" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> UseLogin(string userid, string returnUrl)
        {
            if (User.IsSuperAdmin())
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByIdAsync(userid);
                    if (user != null && !user.IsVerified)
                    {
                        ModelState.AddModelError("", "User Account is waiting for verify. Please contact Administrator to verify it.");
                    }
                    else if (user != null && user.IsBlocked)
                    {
                        ModelState.AddModelError("", "User Account is blocked. Please contact Administrator to unblock it.");
                    }
                    else
                    {
                        if (user != null)
                        {
                            await SignInAsync(user, false);
                            return RedirectToLocal(returnUrl);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Invalid username or password.");
                        }
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return RedirectToLocal(returnUrl);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var vm = new RegisterViewModel();
            return View(vm);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RegisterSuccess(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "";
            }
            else
            {
                ViewBag.Message = "Register Successful. Please contact Administrator to verify your account.";
            }
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    DataPassword = model.Password,
                    DataUserName = model.UserName,
                    Department = "Telemarketing",
                    Email = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    NickName = string.IsNullOrEmpty(model.NickName) ? model.FirstName : model.NickName,
                    IsBlocked = false,
                    LicenseNumber = model.LicenseNumber,
                    TSRCode = model.TSRCode,
                    IsAgency = false,
                    EmailConfirmed = true,
                    IsVerified = false,
                    CreatedDate = DateTime.Now
                };
                if (ProjectSetupModel.GetProjectType() == "NL")
                {
                    user.EnablePilotFeature = true;
                }
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    this.SetDefaultUserRole(user.DataUserName, "");
                    this.SetExtensionCode(user.Id);
                    // Skip immediately log-in.
                    //await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("RegisterSuccess", "Account", new { @id = 1 });
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void SetDefaultUserRole(string username, string rolename)
        {
            if (!username.IsNullOrEmpty())
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var role = context.Roles.SingleOrDefault(row => row.Name.Equals(rolename));
                    var user = context.Users.SingleOrDefault(row => row.DataUserName.Equals(username));

                    if (!role.IsNullOrEmpty())
                    {
                        IdentityUserRole userrole = new IdentityUserRole() { RoleId = role.Id, UserId = user.Id };
                        context.Entry<IdentityUserRole>(userrole);
                        context.SaveChanges();
                    }
                }
            }
        }

        private void SetExtensionCode(string id)
        {
            if (!id.IsNullOrEmpty())
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var user = context.Users.SingleOrDefault(row => row.Id.Equals(id));
                    if (user != null)
                    {
                        // generate extension code with no duplicate with others.
                        string extensionCode = string.Empty;
                        bool isExist = true;
                        int x = 4;
                        while (isExist)
                        {
                            extensionCode = id.Substring((id.Length - x), 4);
                            isExist = context.Users.Any(row => row.ExtensionCode == extensionCode);
                            x++;
                        }
                        user.ExtensionCode = extensionCode;
                        context.Entry(user).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
            }
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }


        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            var user = db.Users.Find(User.Identity.GetUserId());
                            user.DataPassword = model.NewPassword;
                            db.SaveChanges();
                        }
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            using (var db = new IGTCRMContext())
            {
                var userLogged = db.UserLoggedSet.Find(DateTime.Today, User.Identity.GetUserId());
                if(userLogged==null)userLogged = new UserLogged() { UserId = User.Identity.GetUserId(), Date = DateTime.Today};
                userLogged.LoggedOut = DateTime.Now;
                db.UserLoggedSet.AddOrUpdate(userLogged);
                db.SaveChanges();
            }
            AuthenticationManager.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }



        #endregion

        // Verify Member
        [HttpGet]
        public ActionResult VerificationMember()
        {
            if (User.IsAdmin())
            {
                ViewBag.Title = "Verification Member";
                ApplicationDbContext db = new ApplicationDbContext();
                if (User.IsSuperAdmin())
                {
                    ViewBag.Roles = new SelectList(db.Roles.AsEnumerable(), "Id", "Name");
                }
                else
                {
                    var excluderoles = new string[] { "SYSTEMADMIN", "DM", "CO-ADMIN", "OWNER" };
                    ViewBag.Roles = new SelectList(db.Roles.Where(c => !excluderoles.Contains(c.Name)), "Id", "Name");
                }

                ViewBag.Supervisors = new SelectList(db.GetAdminSelectList(), "Id", "Name");
                var result = from row in db.Users.AsEnumerable()
                             where !row.IsVerified
                             select new RegisterVerificationModel()
                             {
                                 UserName = row.DataUserName,
                                 Department = row.Department,
                                 EmailAddress = row.Email,
                                 FirstName = row.FirstName,
                                 LastName = row.LastName,
                                 LicenseNumber = row.LicenseNumber,
                                 TSRCode = row.TSRCode,
                                 TSRCode_OL = row.TSRCode_OL,
                                 IsBlocked = row.IsBlocked,
                                 IsVerified = row.IsVerified,
                                 RoleId = (row.Roles.FirstOrDefault() == null ? "" : row.Roles.First().RoleId),
                                 SupervisorId = ""
                             };
                return View(result);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
        }

        // Accessing by Json
        [HttpGet]
        public ActionResult VerifyMember(string username, string tsrcode, string tsrcode_ol, string license, string license_ol, string role, string supervisorId)
        {
            if (User.IsAdmin())
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var selectedrole = db.Roles.SingleOrDefault(row => row.Id == role);
                    ApplicationUser user = db.Users.SingleOrDefault(item => item.UserName == username);
                    if (user != null)
                    {
                        if (selectedrole != null)
                        {
                            var userStore = new UserStore<ApplicationUser>(db);
                            var userManager = new UserManager<ApplicationUser>(userStore);
                            userManager.AddToRole(user.Id, selectedrole.Name);
                        }
                        user.LicenseNumber = license;
                        user.LicenseNumber_OL = license_ol;
                        user.TSRCode = tsrcode;
                        user.TSRCode_OL = tsrcode_ol;
                        user.IsVerified = !user.IsVerified;
                        user.SupervisorId = supervisorId;
                        if (!string.IsNullOrEmpty(supervisorId))
                        {
                            user.SupervisorName = db.ActiveUsers().Where(c => c.Id == supervisorId).Select(s => s.FirstName + " " + s.LastName).FirstOrDefault().Trim();
                        }
                        db.Entry<ApplicationUser>(user).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return RedirectToAction("VerificationMember");
                    }
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
        }

        // Access by Json
        [HttpGet]
        public ActionResult RemoveMember(string username)
        {
            if (User.IsAdmin())
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    ApplicationUser user = db.Users.SingleOrDefault(item => item.UserName == username);
                    if (user != null)
                    {
                        db.Entry<ApplicationUser>(user).State = EntityState.Deleted;
                        db.SaveChanges();
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return RedirectToAction("VerificationMember");
                    }
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
        }

    }
}