﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using GT.InboundApplication.Model.CRM;
using System.Data.Entity.Validation;

namespace GT.CRMApplication.WebMVC5.Controllers
{
    [Authorize]
    public class ValuePreferencesController : Controller
    {
        private IGTCRMContext db = new IGTCRMContext();

        // GET: /ValuePreferences/
        public ActionResult Index()
        {
            return View(db.ValuePreferenceSet.ToList());
        }

        // GET: /ValuePreferences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValuePreference valuePreference = db.ValuePreferenceSet.Find(id);
            if (valuePreference == null)
            {
                return HttpNotFound();
            }
            return View(valuePreference);
        }

        // GET: /ValuePreferences/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ValuePreferences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ValuePreferenceId,Code,Name,Value,Description,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate")] ValuePreference valuePreference)
        {
            
            if (ModelState.IsValid)
            {
                //this._FillInModelWithDefaultValue<ValuePreference>(valuePreference);
				valuePreference.CreatedBy =
				valuePreference.UpdatedBy = User.Identity.GetUserId();
				valuePreference.CreatedDate =
                valuePreference.UpdatedDate = DateTime.Now;
                db.ValuePreferenceSet.Add(valuePreference);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(valuePreference);
        }

        // GET: /ValuePreferences/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValuePreference valuePreference = db.ValuePreferenceSet.Find(id);
            if (valuePreference == null)
            {
                return HttpNotFound();
            }
            return View(valuePreference);
        }

        // POST: /ValuePreferences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ValuePreferenceId,Code,Name,Value,Description,Note,Active,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate")] ValuePreference valuePreference)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //_FillInModelWithDefaultValue<ValuePreference>(valuePreference);
                    valuePreference.UpdatedBy = User.Identity.GetUserId();
                    valuePreference.UpdatedDate = DateTime.Now;
                    db.Entry(valuePreference).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    ModelState.AddModelError("", string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ModelState.AddModelError("", string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                        ve.PropertyName, ve.ErrorMessage));
                    }
                }
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.InnerException.Message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(valuePreference);
        }

        // GET: /ValuePreferences/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ValuePreference valuePreference = db.ValuePreferenceSet.Find(id);
            if (valuePreference == null)
            {
                return HttpNotFound();
            }
            return View(valuePreference);
        }

        // POST: /ValuePreferences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ValuePreference valuePreference = db.ValuePreferenceSet.Find(id);
            db.ValuePreferenceSet.Remove(valuePreference);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
