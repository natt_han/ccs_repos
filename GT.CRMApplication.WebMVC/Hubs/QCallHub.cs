﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace GT.CRMApplication.WebMVC5.Hubs
{
    public class QCallHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void CallResponse(string UserId)
        {
            Clients.User(UserId).updateResponse("test");
        }

        public static void SendMessage(string username, string callid, short responsecode, int talktime)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<QCallHub>();
            hubContext.Clients.User(username).updateResponse(callid, responsecode, talktime);
        }
    }

    public class QCall
    {
        private readonly static Lazy<QCall> _instance = new Lazy<QCall>(() => new QCall(GlobalHost.ConnectionManager.GetHubContext<ClickToCallHub>().Clients));

        private QCall(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public static QCall Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public void UpdateResponse(string username, string callid, short responsecode, int talktime)
        {
            Instance.Clients.User(username).updateResponse(callid, responsecode, talktime);
        }

        public void Hello()
        {
            Instance.Clients.All.hello();
        }
    }
}