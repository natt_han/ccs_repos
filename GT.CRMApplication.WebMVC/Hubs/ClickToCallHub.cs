﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace GT.CRMApplication.WebMVC5.Hubs
{
    public class ClickToCallHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
            
        }

        public void CallResponse(string UserId)
        {
            Clients.User(UserId).updateResponse("test");
        }

        public static void SendMessage(string username, string callid, short responsecode, int talktime)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ClickToCallHub>();
            hubContext.Clients.User(username).updateResponse(callid, responsecode, talktime);
        }
    }

    public class ClickToCall
    {
        private readonly static Lazy<ClickToCall> _instance = new Lazy<ClickToCall>(() => new ClickToCall(GlobalHost.ConnectionManager.GetHubContext<ClickToCallHub>().Clients));

        private ClickToCall(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public static ClickToCall Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public void UpdateResponse(string username, string callId, string responseDesc, int talkTime, int ringTime)
        {
            Instance.Clients.User(username).updateResponse(callId, responseDesc, talkTime, ringTime);
        }

        public void Hello()
        {
            Instance.Clients.All.hello();
        }
    }
}