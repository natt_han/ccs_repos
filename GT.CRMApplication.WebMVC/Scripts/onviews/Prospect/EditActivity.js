﻿/* Prospect_EditActivity.js */
function _activitytypechange()
{
    // activity dropdown change
    var activity = $(this).val();
    var activity_text = $(this).find('option:selected').html();
    $('#subheadings-title').text("[" + activity_text + "]");
    switch (activity)
    {
        case "Service":
            $('#subheadings').hide();
            $('#service-div').show();
            $('#sale-div, #complaint-div,  #wrongcall-div, #uncontact-div').hide();
            $('#sale-div :input, #complaint-div :input,  #wrongcall-div :input, #uncontact-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "Sales":
            $('#subheadings').hide();
            $('#sale-div').show();
            $('#service-div, #complaint-div, #uncontact-div, #wrongcall-div').hide();
            $('#service-div :input, #complaint-div :input, #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
           
            break;
        case "Complaint":
            $('#subheadings').hide();
            $('#complaint-div').show();
            $('#sale-div, #service-div,  #uncontact-div, #wrongcall-div').hide();
            $('#sale-div :input, #service-div :input,  #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "NoContact":
            $('#subheadings').hide();
            $('#uncontact-div').show();
            $('#sale-div, #service-div,  #complaint-div, #wrongcall-div').hide();
            $('#sale-div :input, #service-div :input,  #complaint-div input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "WrongCall":
            $('#subheadings').hide();
            $('#wrongcall-div').show();
            $('#sale-div, #service-div,  #complaint-div, #uncontact-div').hide();
            $('#sale-div :input, #service-div :input,  #complaint-div :input, #uncontact-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        default:
            $('#sale-div, #complaint-div, #service-div,  #uncontact-div, #wrongcall-div').hide();
            $('#sale-div :input, #complaint-div :input, #service-div :input,  #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideUp(150);
            break;
    }
    if ($('#CallTypeCode_IB').is(':checked')) {
        $('#interesting-div').hide();
        $('#interesting-div').slideToggle(150);
    }
    else {
        $('#interesting-div').hide();
        $('#interesting-div :input').val('');
    }
}

// Edit Script 
function EditScript() {
    var calltypecode = $("#CallTypeCode_IB").is(':checked') ? "IB" : "OB";
    var activitytype = $('#ActivityTypeCode').val();
    var hasappointment = $('#Prospect_Appointment_AppointmentDate').val().length > 0 ? true : false;

    $('#subheadings-title').text("[" + activitytype + "]");
    switch (activitytype) {
        case "Service":
            $('#subheadings').hide();
            $('#service-div').show();
            $('#sale-div, #complaint-div, #wrongcall-div, #uncontact-div').hide();
            $('#sale-div :input, #complaint-div :input, #wrongcall-div :input, #uncontact-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "Sales":
            $('#subheadings').hide();
            $('#sale-div').show();
            $('#service-div, #complaint-div, #uncontact-div, #wrongcall-div').hide();
            $('#service-div :input, #complaint-div :input, #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "Complaint":
            $('#subheadings').hide();
            $('#complaint-div').show();
            $('#sale-div, #service-div, #uncontact-div, #wrongcall-div').hide();
            $('#sale-div :input, #service-div :input, #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "UnContact":
            $('#subheadings').hide();
            $('#uncontact-div').show();
            $('#sale-div, #service-div, #complaint-div, #wrongcall-div').hide();
            $('#sale-div :input, #service-div :input, #complaint-div input, #wrongcall-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        case "WrongCall":
            $('#subheadings').hide();
            $('#wrongcall-div').show();
            $('#sale-div, #service-div, #complaint-div, #uncontact-div').hide();
            $('#sale-div :input, #service-div :input, #complaint-div :input, #uncontact-div :input').val('');
            $('#subheadings').slideToggle(150);
            break;
        default:
            $('#sale-div, #complaint-div, #service-div, #uncontact-div, #wrongcall-div').hide();
            $('#sale-div :input, #complaint-div :input, #service-div :input, #uncontact-div :input, #wrongcall-div :input').val('');
            $('#subheadings').slideUp(150);
            break;
    }
    if ($('#CallTypeCode_IB').is(':checked')) {
        $('#interesting-div').hide();
        $('#interesting-div').slideToggle(150);
    }
    else {
        $('#interesting-div').hide();
        $('#interesting-div :input').val('');
    }

    if (hasappointment) {
        $('#newappointment').removeClass('btn-primary').addClass('btn-default');
        $('#appointment-div').slideDown(150);
    }
}
