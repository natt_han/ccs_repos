﻿// =========================
// Main Profile.js
// =========================

$(document).ready(function () {
    bindeventcontrol();
});
function bindeventcontrol()
{
    // View Historical data.
    $("button[name=btn-history-view]").click(function () {
        _ajax_showmodalpopup_history($(this).attr("id"));
    });
}
function _ajax_showmodalpopup_history(activityid) {
    var serviceURL = window.crmBaseUrl + 'Prospect/GetActivityInfo';
    $.ajax({
        type: "GET",
        url: serviceURL,
        data: { id: activityid },
        contentType: "application/json;",
        dataType: "json", 
        success: function (data) {
            var tag_head =      ''
                                + '<div id="call-div">'
                                + '<font color="#C21B17"><h2>ประวัติการสนทนา</h2></font>'
                                + '<dl class="dl-horizontal">'                                                         
                                + '<dt>เวลาที่สนทนา</dt>'
                                + '<dd>' + data.calldate + '</dd>'
                                + '</dl></div>';

            var tag_heading = ''
                            + '<font color="#C21B17"><h2> ' + data.activitytypename + ' </h2></font>';
            

            var tag_sales = ''
                         + '<div id="sale-div">'
                         + '<dl class="dl-horizontal">'
                         
                         + '              <dt>ผลตอบรับของลูกค้า</dt>'
                         + '              <dd>' + data.response_category
                         + '              </dd>'
                         + '              <dt>ผลตอบรับ (Level 1)</dt>'
                         + '              <dd>' + data.response_reason
                         + '              </dd>'                       
                         + '              <dt>รายละเอียดเพิ่มเติม</dt>'
                         + '              <dd>' + data.response_detail
                         + '              </dd>'
                         
                         + '          </dl>'
                         + '      </div>';            


            var tag_memo = ''
               + ' <div class="hr-line-dashed"></div>'
                + '<dl class="dl-horizontal">'
               + '     <dt>บันทึก</dt>'
               + '     <dd>' + data.note
               + '</dl>';

            var tag_appointment = ''
                       + ' <div id="appointment-div" class="gray-bg border-all">'
                       + '     <div class="form-group" style="padding-top:10px;">'
                       + '         <div class="col-sm-9 col-sm-offset-3">'
                       + '             <font color="#C21B17"><h2><i class="fa fa-calendar"></i>&nbsp; การนัดหมาย</h2></font>'
                       + '         </div>'
                       + '     </div>'
                       + '<dl class="dl-horizontal">'
                       + '         <dt>วันที่นัดหมาย</dt>'
                       + '         <dd>' + data.appointment_date + '</dd>'
                       + '         <dt>เวลา</dt>'
                       + '         <dd>' + data.appointment_time
                       + '         </dd>'
                       + '         <dt>หัวข้อ</dt>'
                       + '         <dd>' + data.appointment_topic
                       + '         </dd>'
                       + '         <dt>รายละเอียด</dt>'
                       + '         <dd>' + data.appointment_detail
                       + '         </dd>'
                       + '     </dl>'
                       + ' </div>';

            pop_message('ประวัติการสนทนากับ คุณ' + data.prospectname, '<div class="col-lg-11 col-lg-offset-1">' + tag_head + tag_heading + tag_sales + tag_appointment + '</div>');
        },
        error: function (err) { pop_message('Oops..', '_ajax_showmodalpopup_history load failed!', 'error'); console.log(err.message); }
    });
}

function ShowDisabledProfileScript(indexurl)
{
    pop_message('ข้อความแจ้งเตือน', 'ผู้ติดต่อนี้ไม่สามารถใช้งานได้ เนื่องจากถูก Block จากโปรแกรม [โปรแกรมจะนำทางกลับไปยังหน้าค้นหาอัตโนมัติ ใน 5 วินาที]');
    setTimeout(function () {
        window.location = indexurl;    
    }, 5000);
}

function ShowSuccessScript(message) {
    pop_message('ข้อความ', message);
}