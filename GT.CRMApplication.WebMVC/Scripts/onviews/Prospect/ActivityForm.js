﻿var startcall;
function _activitytypechange() {
    // activity dropdown change
    var activity = $(this).val();
    var activity_text = $(this).find('option:selected').html();
    $('#subheadings-title').text("[" + activity_text + "]");
    
    
}

// Edit Script 
function EditScript() {
    var calltypecode = $("#CallTypeCode_IB").is(':checked') ? "IB" : "OB";
    var activitytype = $('#ActivityTypeCode').val();
    var hasappointment = $('#Prospect_Appointment_AppointmentDate').val().length > 0 ? true : false;   
    //_ajax_binddropdown_responsesubreason($('#Prospect_SaleActivity_ResponseReasonCode').val());
    //_ajax_bindtoggle_oldinsurediv($('#Prospect_SaleActivity_ResponseCategoryCode').val());

    $('#subheadings-title').text("[" + activitytype + "]");
    

    if (hasappointment) {
        $('#newappointment').removeClass('btn-primary').addClass('btn-default');
        $('#appointment-div').slideDown(150);
    }
}

function validateform() {
    try {
        var message = "";
        if ($("#CallTime").val().length <= 0) { message += "<li> เวลาที่เริ่มสนทนา </li>"; }

        if (message != "") {
            pop_message('กรุณาระบุข้อมูลต่อไปนี้', '<ul>' + message + '</ul>', 'warning');
            return false;
        }

        message = "";
                
        if ($("#ResponseCategoryCode").val().length <= 0) { message += "<li> ผลการตอบรับของลูกค้า </li>"; }
        if ($("#ResponseReasonCode").val().length <= 0) { message += "<li> เหตุผล </li>"; }    
        if (message != "") {
            pop_message('กรุณาระบุข้อมูลต่อไปนี้', '<ul>' + message + '</ul>', 'warning');
            return false;
        }
        else {
            if (startcall) {
                var now = moment();
                $("#TalkTime").val(now.diff(startcall, 'seconds'));
            }
            $("#btn_submit").attr("disabled", true);
            $('#main-div form').submit();
        }
    }
    catch (e) {
        alert(e.message);
        console.log(e.stack);
        return false;
    }
}


function _ajax_binddropdown_responsecategory(calltypecode) {
    var serviceURL = window.crmBaseUrl + 'Prospect/GetResponseCategory/' + calltypecode + '';
    var targetcontrol = $("#ResponseCategoryCode");
    targetcontrol.find("option").remove();
    targetcontrol.append($('<option></option>').html("เลือก").val(""));
    $.ajax({
        type: "POST",
        url: serviceURL,
        contentType: "application/json;",
        dataType: "json", beforesend: startloading, complete: stoploading,
        success: function (data) {
            data.forEach(function (entry) { targetcontrol.append($('<option></option>').val(entry.id).html(entry.name)); })
        },
        error: function () { pop_message('Oops..', '_ajax_binddropdown_responsecategory load failed!', 'error'); }
    });
}


function _ajax_binddropdown_responsereason(responsecategorycode) {
    var serviceURL = window.crmBaseUrl + 'Prospect/GetResponseReason?id=' + responsecategorycode + '&prospectid=' + $('#Prospect_Profile_ProspectId').val();
    var targetcontrol = $("#ResponseReasonCode");
    targetcontrol.find("option").remove();
    targetcontrol.append($('<option></option>').html("เลือก").val(""));
    if (!responsecategorycode) return;
    $.ajax({
        url: serviceURL,
        contentType: "application/json;",
        dataType: "json", beforesend: startloading, complete: stoploading,
        success: function (data) {
            data.forEach(function (entry) { targetcontrol.append($('<option></option>').val(entry.id).html(entry.name)); })
        },
        error: function () { pop_message('Oops..', '_ajax_binddropdown_responsereason load failed!', 'error'); }
    });
}

function _ajax_binddropdown_responsesubreason(reasoncode) {
    var serviceURL = window.crmBaseUrl + 'Prospect/GetResponseSubReason?productid=' + $("#ddl-offerproduct").val() + "&id=" + reasoncode + '';
    var targetcontrol = $("#Prospect_SaleActivity_ResponseSubReasonCode");
    targetcontrol.find("option").remove();
    targetcontrol.append($('<option></option>').html("เลือก").val(""));
    if (!reasoncode) return;
    $.ajax({
        url: serviceURL,
        contentType: "application/json;",
        dataType: "json", beforesend: startloading, complete: stoploading,
        success: function (data) {
            data.forEach(function (entry) { targetcontrol.append($('<option></option>').val(entry.id).html(entry.name)); })
        },
        error: function () { pop_message('Oops..', '_ajax_binddropdown_responsesubreason load failed!', 'error'); }
    });
}


function AfterScript() {
    //var calltypecode = $("#CallTypeCode_IB").is(':checked') ? "IB" : "OB";
}

function QuickResponse(Cate, Reason, SubReason) {
    $.ajaxSetup({ async: false });
    $('#ResponseCategoryCode').val(Cate).trigger('change');
    $('#ResponseReasonCode').val(Reason).trigger('change');
    $('#ResponseSubReasonCode').val(SubReason).trigger('change');
    $.ajaxSetup({ async: true });
}