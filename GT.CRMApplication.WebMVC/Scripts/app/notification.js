﻿Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

var noti = localStorage.getObject("notifications");
var lastnotifetch = Number(localStorage.getItem("lastnotifetch"));
var noty = []

var interval = 120000
$().ready(function () {
    $("#noty_expand,#noty_collapse").click(function () {
        $(".noty-custom-body,#noty_expand,#noty_collapse").toggleClass("hidden");
    });
    $("#noty_clear").click(function () {
        for (item in noty) {            
            noty[item].close();
        }
        noti = {}
        localStorage.setObject("notifications", noti);
        $("#noty-custom-header").hide()
    })
    if (!noti) noti = {};
    //clear localstorage each day
    var lastfetchdate = new Date();
    lastfetchdate.setTime(lastnotifetch);
    if (lastfetchdate.getDate() != (new Date()).getDate()) {
        localStorage.clear();
        noti = {};
    }
    function Fetch() {
        var d = new Date();
        var now = d.getTime();
        
        if ((now - lastnotifetch) > interval) {
            $.ajax({
                async: true,
                url: window.crmBaseUrl + "Notification/Fetch"
                , success: function (data) {
                    data.forEach(function (item) {
                        noti[item.Tag] = item;
                    });
                    localStorage.setObject("notifications", noti);
                    ShowNotification(noti);
                }
            });
            localStorage.setItem("lastnotifetch", now);
        } else {
            ShowNotification(noti);
        }
    }
    Fetch();
    setInterval(function () {
        Fetch();
    }, interval);


    function ShowNotification(noti) {        
        for (var p =1; p>=0; p--) { //show by priority
            for (item in noti) {
                if (noty[item]) continue;
                if (noti[item].Priority != p) continue;
                $("#noty-custom-header").show();
                var n = new Noty({
                    id: 'noty-' + noti[item].Tag,
                    container: '.noty-custom-body',
                    queue: 'global',
                    type: noti[item].Type,
                    layout: 'bottomLeft',
                    //sounds:{sources:[window.applicationBaseUrl+"/Content/sounds/plucky.mp3"]},//not working
                    text: noti[item].Title,
                    timeout: noti[item].Timeout,
                    closeWith: ['click', 'button'],
                    callbacks: {
                        onClick: function () {
                            var tag = this.options.id.replace("noty-", "");
                            var local = noti[tag];
                            window.location = local.Url;
                            console.log(local.Url);
                        },
                        onClose: function () {
                            console.log(this.options.id);
                            var tag = this.options.id.replace("noty-", "");
                            delete noti[tag];
                            localStorage.setObject("notifications", noti);
                            delete noty[tag];
                            var noty_count = Object.keys(noty).length
                            $("#noty_counter").html(noty_count)
                            if (noty_count == 0) $("#noty-custom-header").hide();
                        }
                    }
                }).show();
                noty[item] = n;
            }
        }
        $("#noty_counter").html(Object.keys(noty).length)        
    }
});