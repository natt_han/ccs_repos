﻿/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.2
 *
 */
var bcm;
$(document).ready(function () {

    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').click(function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Close menu in canvas mode
    $('.close-canvas-menu').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    // Open close right sidebar
    $('.right-sidebar-toggle').click(function () {
        $('#right-sidebar').toggleClass('sidebar-open');
    });

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    });

    // Open close small chat
    $('.open-small-chat').click(function () {
        $(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
        $('.small-chat-box').toggleClass('active');
    });

    // Initialize slimscroll for small chat
    $('.small-chat-box .content').slimScroll({
        height: '234px',
        railOpacity: 0.4
    });

    // Small todo handler
    $('.check-link').click(function () {
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });

    // Minimalize menu
    $('.navbar-minimalize').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    // Tooltips
    $('.tooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body");

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
        }

    }
    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    })

    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    $("[data-toggle=popover]")
        .popover();

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    });

    // bind formating control 
    bindcontrol_accounting($('#main-div'));
    bindcontrol_numeric($('#main-div'));
    bindcontrol_appno($('#main-div'));
    bindcontrol_datepicker($('body'));
    bindcontrol_timepicker($('#main-div'));
    preventuser_typeenter($('#main-div'));
    bindcontrol_accounting_single_scale($('#main-div'));

    // form
    $('form').attr('autocomplete', 'off');

    // Note : setting up searchable dropdown.
    searchabledropdown_setup();  //*** hold on : taking more time to revamp it in all of page.

    bindcontrol_pidth($('#main-div'));
    $(".select2").select2();
    //bindcontrol_creditcard($("#main-div"));
    $(".checkbox.uncheckable").click(function (evt) {        
        var input = $(this).find("input");
        if (input.prop("checked")) { input.prop("checked", false); }
        else { input.prop("checked", true); }
        //evt.stopPropagation();
        evt.preventDefault();
    })
});
// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});

function BroadcastMessage() {
    bcm = new Vue({
        el: "#broadcast-message",
        data: {
            ibh: {},
            pm: { Message: "" }, sup: {}
        }
    });
    $("#broadcast-message").show();
    $.ajax({
        url: window.applicationBaseUrl + "BroadcastMessage/GetBroadcastMessage",
        ContentType: 'json',
        success: function (resp) {
            bcm.ibh = resp.ibh;
            bcm.pm = resp.pm;
            bcm.sup = resp.sup;
            Vue.nextTick(function () {
                $('.marquee').marquee({ speed: 90, pauseOnHover: true });
            })

        }
    });
}

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

/* all-page script */
function preventuser_typeenter(element) {
    $(document).keypress(function (event) {
        var key = event.keyCode || event.which;
        if (key == 10 || key == 13) {
            if (event.target.className.indexOf("note-editable") < 0) //&& !$(event.target).is("textarea")) 
                event.preventDefault();
        }
    });
}
function bindcontrol_numeric(element) {
    element.on("keypress",".input-numeric",function (event) {
        var key = event.keyCode || event.which;
        if (event.key.length == 1) {
            if (key < 48 || key > 57)
                event.preventDefault();
        }
    })
}
function bindcontrol_appno(element) {
    element.find(".input-appno").on('input', function (event) {
        this.value = this.value.replace(/[^A-z0-9-]/g, '');
    });
}
function bindcontrol_alpha(element) {
    element.find(".input-alpha").keypress(function (event) {
        var key = event.keyCode || event.which;
        if (event.key.length == 1) {
            if (key >= 48 && key <= 57)
                event.preventDefault();
        }
    })
}
function bindcontrol_accounting(element) {
    element.find(".input-accounting").priceFormat({
        centsLimit: 2,
        limit: 10,
        clearPrefix: true,
        prefix: '',
        thousandsSeparator: ',',
        suffix: ''
    });
}
function bindcontrol_accounting_single_scale(element) {
    element.find(".input-accounting-single-scale").priceFormat({
        centsLimit: 1,
        limit: 10,
        clearPrefix: true,
        prefix: '',
        thousandsSeparator: ',',
        suffix: ''
    });
}


function bindcontrol_datepicker(element) {
    element.delegate(".input-date","focusin", function(){
        $(this).datepicker({
            startView: 3,
            clearBtn: true,
            todayBtn: "linked",
            keyboardNavigation: true,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy",
            disableTouchKeyboard: true
        });
    });

    element.find(".input-date-past").datepicker({
        clearBtn: true,
        startView: 3,
        todayBtn: "linked",
        keyboardNavigation: true,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        disableTouchKeyboard: true,
        endDate: new Date()
    });

    element.find(".input-date-future").datepicker({
        clearBtn: true,
        startView: 3,
        todayBtn: "linked",
        keyboardNavigation: true,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        disableTouchKeyboard: true,
        startDate: new Date()
    });
}
function bindcontrol_timepicker(element) {
    element.find(".input-time-bottom").clockpicker({
        placement: 'bottom',
        autoclose: true,
        donetext: 'Done'
    });

    element.find(".input-time-top").clockpicker({
        placement: 'top',
        autoclose: true,
        donetext: 'Done'
    });

    element.find(".input-time").clockpicker({
        placement: 'bottom',
        donetext: 'Done'
    });
    element.find(".input-time").on('blur', function () {
        //console.log($(this).val());
        if (!moment($(this).val(), "HH:mm").isValid()) {
            $(this).val('');
        }
    });
}

function bindcontrol_pidth(element) {
    $(element).find(".pidth").tooltip({ trigger: "manual" })
    $(element).find(".pidth").on('blur', function () {
        var el = $(this);
        if (el.val() && el.val().length==13 && !isValidPid(el.val())) {
            el.tooltip('show');
        } else {
            el.tooltip("hide");
        }
    });
}
function bindcontrol_creditcard(element) {
    $(element).find(".creditcard").tooltip({ trigger: "manual" })
    $(element).find(".creditcard").on('blur', function () {
        var el = $(this);
        if (el.val() && !valid_credit_card(el.val())) {
            el.tooltip('show');
        } else {
            el.tooltip("hide");
        }
    });
}
// Error stack
// console.log(e.stack);
function stackTrace() {
    var err = new Error();
    return err.stack;
}
function stacktrace() {
    function st2(f) {
        return !f ? [] :
            st2(f.caller).concat([f.toString().split('(')[0].substring(9) + '(' + f.arguments.join(',') + ')']);
    }
    return st2(arguments.callee.caller);
}
/* chosen js (searchable dropdownlist) */
function searchabledropdown_setup()
{
    // Note : config sequently.
    var config = {
        'searchable': { width: "160px" }
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
        $(selector).bind('change', function () {
            $(this).trigger('chosen:updated');
        })
    }
}
// Note : take this function when your dropdownlist 
function searchabledropdown_refresh()
{
    var config = {
        'searchable': { width: "160px" }
    }
    for (var selector in config) {
        $(selector).trigger('chosen:updated');
    }
}
// Note : take this extention function when you rebinding the new set of option value in dropdownlist.

// Extention
jQuery.fn.extend({
    searchable_rebind: function () {
        this.trigger('chosen:updated');
    }
});

//datatable view template
function salecatrender (data, type, row, meta){
    switch (data)
    {
        case "1": return '<span class="label label-info">Follow up</span>'; break;
        case "2": return '<span class="label label-success">Success</span>'; break;
        case "3": return '<span class="label label-default">Not Target</span>'; break;
        case "4": return '<span class="label label-default">Reject</span>'; break;
        case "5": return '<span class="label label-white">No Contact</span>'; break;
        case "6": return '<span class="label label-default">Not Update</span>'; break;
        case "7": return '<span class="label label-success">Success-Online</span>'; break;
        case "8": return '<span class="label label-default">Duplicate</span>'; break;
        case "9": return '<span class="label label-default">Refer</span>'; break;
        case "10": return '<span class="label label-default">Unsuccess</span>'; break;
        case "11": return '<span class="label label-default">Letter</span>'; break;
        default: return data;
    }
}

function leadflagrender(data, type, row, meta) {
    switch (data) {
        case "N": return '<span class="label label-danger">ลีดใหม่</span>'; break;
        case "O": return '<span class="label label-default">ลีดเก่า</span>'; break;
        case "D": return '<span class="label label-default">ลีดซ้ำ</span>'; break;
        case "U": return '<span class="label label-warning">ลีดใหม่คละ</span>'; break;//Used
    }
}


function parseToDate(dateString) {

    //var dateString = "23/10/2015"; // Oct 23
    var dateParts = dateString.split("/");

    // month is 0-based, that's why we need dataParts[1] - 1
    return dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

    //document.body.innerHTML = dateObject.toString();
}

// update progress 
function showprogress() {
}
function hideprogress(message) {
}
// end update progress

function swalConfirm(title, message, callback) {
    swal({
        title: title,
        text: message,
        type: 'warning',
        buttons: true
    }).then((confirmed) => {
        if (confirmed) {
            callback();
        }
        //callback(confirmed && confirmed.value == true);
    });
}