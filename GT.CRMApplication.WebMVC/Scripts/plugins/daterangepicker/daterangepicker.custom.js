﻿$().ready(function () {
    var emptyinput = $(".input-daterange.input-daterange-with-clear").filter(function () {return !this.value;});
    $(".input-daterange").daterangepicker({
        //"autoApply": false,
        //autoUpdateInput: false,
        //locale: {
        //    cancelLabel: 'Clear'
        //},
        "alwaysShowCalendars": true,
        "showCustomRangeLabel": false,
        ranges: {            
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'This Week': [moment().startOf('week'), moment()],
            'Last 3 Days': [moment().subtract(2, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
    emptyinput.val('');

    $('.input-group>.input-daterange-with-clear').after("<a class='input-group-addon btn-clear-prev-input'><i class='fa fa-close' ></i></a>");
    $(".btn-clear-prev-input").click(function () { $(this).prev().val('');})
});