﻿using GT.InboundApplication.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GT.CRMApplication.WebMVC5.ViewModels
{
    public class TimeAttendanceViewModel
    {
        public DateTime Date { get; set; }
        public string UserId { get; set; }
        public string TSRName { get; set; }
        public DateTime? LoggedIn { get; set; }
        public DateTime? LoggedOut { get; set; }
        public DateTime? FirstCall { get; set; }
        public DateTime? LastCall { get; set; }
        public int? TotalCall { get; set; }
        public int? TalkTime { get; set; }
        public string DateRemark { get; set; }
        public int? HolidayCount { get; set; }
        public string LeaveRemark { get; set; }

        public TimeSpan? Late(TimeSpan CheckTime)
        {
            if (LoggedIn != null && LoggedIn.Value.TimeOfDay > CheckTime)
            {
                return LoggedIn.Value.TimeOfDay.Subtract(CheckTime);
            }
            return null;
        }

        public TimeSpan? EarlyLeave(TimeSpan CheckTime)
        {
            if (LoggedOut != null && LoggedOut.Value.TimeOfDay < CheckTime)
            {
                return CheckTime.Subtract(LoggedOut.Value.TimeOfDay);
            }
            return null;
        }

        public bool IsAbsent()
        {
            return this.LoggedIn == null && this.FirstCall == null && this.HolidayCount != 1 && !this.Date.IsWeekend() && this.Date < DateTime.Today;
        }
    }
}