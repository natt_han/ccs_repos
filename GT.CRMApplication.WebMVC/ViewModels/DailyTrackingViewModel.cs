﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GT.CRMApplication.WebMVC5.ViewModels
{
    public class DailyTrackingViewModel
    {
        public string CallDate { get; set; }
        public string TSR { get; set; }
        public string ImportDate { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
        public string Product { get; set; }
        public string TM { get; set; }
        public string PolicyName { get; set; }
        public string PolicyNo { get; set; }
        
        public string Telphone { get; set; }
        public string ResponseCategory { get; set; }
        public string ResponseReason { get; set; }
        public string Detail { get; set; }
        public string Appointment { get; set; }
        public int TodayCall { get; set; }
        public int TotalCall { get; set; }
    }
}