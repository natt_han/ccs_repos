﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GT.CRMApplication.WebMVC5.ViewModels
{
    public class LeadViewModel
    {

    }

    public class CountByGroup
    {
        public string GroupName { get; set; }
        public int Count { get; set; }
    }

    public class NewLeadAmountViewModel
    {
        public int NewLead { get; set; }
        public int ExpireLead { get; set; }
    }

    public class LifeInputLead
    {
        public string TM { get; set; }
        public string Item { get; set; }
        public string Status { get; set; }
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string Planname { get; set; }
        public string NetPremium { get; set; }
        public string SumInsured { get; set; }
        public string InsuredTelNo { get; set; }
        public string InsuredName { get; set; }
        public string AgentName { get; set; }
        public string IssueDate { get; set; }
        public string Payplan { get; set; }

        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Province { get; set; }
        public string Zipcode { get; set; }
        public string PostPolicy { get; set; }
        public string PostDate { get; set; }
        public string CallDate { get; set; }
        public string ExpireDate { get; set; }
    }


}