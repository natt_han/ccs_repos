﻿using System.Web;
using System.Web.Optimization;

namespace GT.CRMApplication.WebMVC5
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.1.1.min.js"));

            // jQueryUI CSS
            bundles.Add(new ScriptBundle("~/Scripts/plugins/jquery-ui/jqueryuiStyles").Include(
                        "~/Scripts/plugins/jquery-ui/jquery-ui.css"));

            // jQueryUI 
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/plugins/jquery-ui/jquery-ui.min.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/plugins/validate/jquery.form.min.js"));

            // boot strap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));

            // vue.js
            bundles.Add(new ScriptBundle("~/bundles/vuejs").Include(
                      "~/Scripts/plugins/vuejs/vue.js",
                      "~/Scripts/plugins/vuejs/vue2-filters.min.js"));

            // Main script
            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                      "~/Scripts/plugins/metisMenu/metisMenu.min.js",
                      "~/Scripts/plugins/pace/pace.min.js",
                      "~/Scripts/plugins/toastr/toastr.min.js",
                      "~/Scripts/plugins/noty/noty.js",
                      "~/Scripts/app/notification.js",
                      "~/Scripts/plugins/select2/select2.full.min.js",
                      "~/Scripts/plugins/jquery.marquee/jquery.marquee.min.js",
                      "~/Scripts/plugins/fullcalendar/moment.min.js",
                      "~/Scripts/app/main.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(
                      "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            // jQuery plugins
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include(
                      "~/Scripts/plugins/metisMenu/metisMenu.min.js"));

            bundles.Add(new ScriptBundle("~/plugins/pace").Include(
                      "~/Scripts/plugins/pace/pace.min.js"));

            // CSS style (bootstrap/inspinia)
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/plugins/toastr/toastr.min.css",
                      "~/Content/plugins/noty/noty.css",
                      "~/Content/plugins/select2/select2.min.css",
                      "~/Content/style.css"));

            // Font Awesome icons
            bundles.Add(new StyleBundle("~/font-awesome/css").Include(
                      "~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransformWrapper()));

            // jasnyBootstrap styles
            bundles.Add(new StyleBundle("~/plugins/jasnyBootstrapStyles").Include(
                      "~/Content/plugins/jasny/jasny-bootstrap.min.css"));

            // jasnyBootstrap 
            bundles.Add(new ScriptBundle("~/plugins/jasnyBootstrap").Include(
                      "~/Scripts/plugins/jasny/jasny-bootstrap.min.js"));

            // iCheck css styles
            bundles.Add(new StyleBundle("~/Content/plugins/iCheck/iCheckStyles").Include(
                      "~/Content/plugins/iCheck/custom.css"));

            // iCheck
            bundles.Add(new ScriptBundle("~/plugins/iCheck").Include(
                      "~/Scripts/plugins/iCheck/icheck.min.js"));

            // Awesome bootstrap checkbox
            bundles.Add(new StyleBundle("~/plugins/awesomeCheckboxStyles").Include(
                      "~/Content/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"));

            // dataPicker styles
            bundles.Add(new StyleBundle("~/plugins/dataPickerStyles").Include(
                      "~/Content/plugins/datapicker/datepicker3.css"));

            // dataPicker 
            bundles.Add(new ScriptBundle("~/plugins/dataPicker").Include(
                      "~/Scripts/plugins/datapicker/bootstrap-datepicker.js"));

            // Clockpicker styles
            bundles.Add(new StyleBundle("~/plugins/clockpickerStyles").Include(
                      "~/Content/plugins/clockpicker/clockpicker.css"));

            // Clockpicker
            bundles.Add(new ScriptBundle("~/plugins/clockpicker").Include(
                      "~/Scripts/plugins/clockpicker/clockpicker.js"));

            // Price Format (external package)
            bundles.Add(new ScriptBundle("~/plugins/priceformat").Include(
                      "~/Scripts/plugins/priceformat/jquery.price_format.2.0.js"));

            // jeditable 
            bundles.Add(new ScriptBundle("~/plugins/jeditable").Include(
                      "~/Scripts/plugins/jeditable/jquery.jeditable.js"));

            // jqGrid styles
            bundles.Add(new StyleBundle("~/Content/plugins/jqGrid/jqGridStyles").Include(
                      "~/Content/plugins/jqGrid/ui.jqgrid.css"));

            // jqGrid 
            bundles.Add(new ScriptBundle("~/plugins/jqGrid").Include(
                      "~/Scripts/plugins/jqGrid/i18n/grid.locale-th.js",
                      "~/Scripts/plugins/jqGrid/jquery.jqGrid.min.js"));

            // dataTables css styles
            bundles.Add(new StyleBundle("~/Content/plugins/dataTables/dataTablesStyles").Include(
                      "~/Content/plugins/dataTables/datatables.min.css"));

            // dataTables 
            bundles.Add(new ScriptBundle("~/plugins/dataTables").Include(
                      "~/Scripts/plugins/dataTables/datatables.min.js",
                      "~/Scripts/plugins/dataTables/dataTables.checkboxes.min.js",
                      "~/Scripts/plugins/dataTables/sorting/dateeuro.js"));

            // summernote styles
            bundles.Add(new StyleBundle("~/plugins/summernoteStyles").Include(
                      "~/Content/plugins/summernote/summernote.css",
                      "~/Content/plugins/summernote/summernote-bs3.css"));

            // summernote 
            bundles.Add(new ScriptBundle("~/plugins/summernote").Include(
                      "~/Scripts/plugins/summernote/summernote.min.js"));

            // Sweet alert Styless
            bundles.Add(new StyleBundle("~/plugins/sweetAlertStyles").Include(
                      "~/Content/plugins/sweetalert/sweetalert.css"));

            // Sweet alert
            bundles.Add(new ScriptBundle("~/plugins/sweetAlert").Include(
                      "~/Scripts/plugins/sweetalert/sweetalert.min.js"));

            // Footable Styless
            bundles.Add(new StyleBundle("~/plugins/footableStyles").Include(
                      "~/Content/plugins/footable/footable.core.css", new CssRewriteUrlTransformWrapper()));

            // Footable alert
            bundles.Add(new ScriptBundle("~/plugins/footable").Include(
                      "~/Scripts/plugins/footable/footable.all.min.js"));

            // fullCalendar styles
            bundles.Add(new StyleBundle("~/plugins/fullCalendarStyles").Include(
                      "~/Content/plugins/fullcalendar/fullcalendar.css"));

            // fullCalendar 
            bundles.Add(new ScriptBundle("~/plugins/fullCalendar").Include(
                      "~/Scripts/plugins/fullcalendar/fullcalendar.min.js"));

            // Chartist
            bundles.Add(new StyleBundle("~/plugins/chartistStyles").Include(
                      "~/Content/plugins/chartist/chartist.min.css", 
                      "~/Content/plugins/chartist/chartist-plugin-tooltip.css"));

            // Chartist
            bundles.Add(new ScriptBundle("~/plugins/chartist").Include(
                      "~/Scripts/plugins/chartist/chartist.min.js",
                      "~/Scripts/plugins/chartist/chartist-plugin-tooltip.min.js"));

            bundles.Add(new ScriptBundle("~/plugins/tinycon").Include(
                       "~/Scripts/plugins/tinycon/tinycon.min.js"));

            //blueimp file-upload
            bundles.Add(new StyleBundle("~/plugins/blueimp/css").Include(
                      "~/Content/plugins/blueimp/css/jquery.fileupload.css"));

            bundles.Add(new ScriptBundle("~/plugins/blueimp").Include(
                       "~/Scripts/plugins/blueimp/jquery.fileupload.js",
                       "~/Scripts/plugins/blueimp/jquery.iframe-transport.js"));

            //chosen
            bundles.Add(new StyleBundle("~/Content/plugins/chosen/chosenStyles").Include(
                    "~/Content/plugins/chosen/chosen.css"));
            bundles.Add(new ScriptBundle("~/plugins/chosen").Include(
                    "~/Scripts/plugins/chosen/chosen.jquery.js"));

            //select2
            bundles.Add(new StyleBundle("~/plugins/select2/css").Include(
                    "~/Content/plugins/select2/select2.min.css"));
            bundles.Add(new ScriptBundle("~/plugins/select2").Include(
                    "~/Scripts/plugins/select2/select2.full.min.js"));

            //multiple-select
            bundles.Add(new StyleBundle("~/plugins/multiple-select/css").Include(
                    "~/Content/plugins/multiple-select/multiple-select.css", new CssRewriteUrlTransformWrapper()));
            bundles.Add(new ScriptBundle("~/plugins/multiple-select").Include(
                    "~/Scripts/plugins/multiple-select/multiple-select.js"));

            //daterangepicker
            bundles.Add(new StyleBundle("~/plugins/daterangepicker/css").Include(
                    "~/Content/plugins/daterangepicker/daterangepicker.2.1.css"));
            bundles.Add(new ScriptBundle("~/plugins/daterangepicker").Include(
                    "~/Scripts/plugins/daterangepicker/daterangepicker.2.1.js",
                    "~/Scripts/plugins/daterangepicker/daterangepicker.custom.js"));

            bundles.Add(new StyleBundle("~/plugins/duallistbox/css").Include("~/Scripts/plugins/duallistbox/bootstrap-duallistbox.min.css"));
            bundles.Add(new ScriptBundle("~/plugins/duallistbox").Include("~/Scripts/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js"));

            // ********************************************************************* //

            bundles.Add(new ScriptBundle("~/onviews/Prospect/MainProfile").Include(
                      "~/Scripts/onviews/Prospect/MainProfile.js"));
            bundles.Add(new ScriptBundle("~/onviews/Prospect/ActivityForm").Include(
                      "~/Scripts/onviews/Prospect/ActivityForm.js"));

            // On page Scripts (Create)
            bundles.Add(new ScriptBundle("~/onviews/Prospect/CreateActivity").Include(
                      "~/Scripts/onviews/Prospect/CreateActivity.js"));
            bundles.Add(new ScriptBundle("~/onviews/Prospect/CreateFollowupActivity").Include(
                     "~/Scripts/onviews/Prospect/CreateFollowupActivity.js"));
            bundles.Add(new ScriptBundle("~/onviews/Prospect/CreateProfile").Include(
                     "~/Scripts/onviews/Prospect/CreateProfile.js"));
            bundles.Add(new ScriptBundle("~/onviews/UnknownCall/CreateActivity").Include(
                      "~/Scripts/onviews/UnknownCall/CreateActivity.js"));
            
            
            // On page Scripts (Edit)
            bundles.Add(new ScriptBundle("~/onviews/Prospect/EditActivity").Include(
                      "~/Scripts/onviews/Prospect/EditActivity.js"));
            bundles.Add(new ScriptBundle("~/onviews/Prospect/EditProfile").Include(
                    "~/Scripts/onviews/Prospect/EditProfile.js"));
            
        }
    }
}
