﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GT.CRMApplication.WebMVC5.Startup))]
namespace GT.CRMApplication.WebMVC5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }
    }
}
