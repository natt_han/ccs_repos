﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.GTApplicationDB;

namespace GT.CRMApplication.WebMVC5.Helpers
{
    public class AppConfigHelper
    {
        public static string RecipientRoleId()
        {
            return System.Configuration.ConfigurationManager.AppSettings["RecipientRoleId"].ToString();
        }
        public static string ImportFilePath()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ImportFilePath"].ToString();
        }
        public static string ServerName()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerName"].ToString();
        }
        public static string DisplayImportFilePath()
        {
            return System.Configuration.ConfigurationManager.AppSettings["DisplayImportFilePath"].ToString();
        }
        public static string CRMLink()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Link_GTCRM"].ToString();
        }
        public static string SalesLink()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Link_GTSALES"].ToString();
        }
        public static string ProjectName()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetProjectName();
        }

        public static string ProjectType()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetProjectType();
        }
        public static string QCallPrefix()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetQCallPrefix();
        }
        public static string QCallRandomPrefix()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetQCallRandomPrefix();
        }
        public static string LeadIntegrationAPIEndPoint()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetLeadIntegrationAPIEndPoint();
        }
        public static string PortNo()
        {
            return GT.CRMApplication.Helpers.ProjectSetupModel.GetPortNo();
        }
    }
}