﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.CRM;
using GT.InboundApplication.Model.Common;
using System.Web.Optimization;
using GT.CRMApplication.WebMVC5.Helpers;

namespace GT.CRMApplication.WebMVC5
{
    public static class MvcHelperExtension
    {
        public static string CRMLink(this HttpRequestBase req)
        {
            return req.Url.GetLeftPart(UriPartial.Authority) + AppConfigHelper.CRMLink();
        }

        public static string SalesLink(this HttpRequestBase req)
        {
            return req.Url.GetLeftPart(UriPartial.Authority) + AppConfigHelper.SalesLink();
        }

        public static RedirectToRouteResult WithRouteValue(
    this RedirectToRouteResult result,
    string key,
    object value)
        {
            if (value == null)
                throw new ArgumentException("value cannot be null");

            result.RouteValues.Add(key, value);

            return result;
        }

        public static RedirectToRouteResult WithRouteValue<T>(
            this RedirectToRouteResult result,
            string key,
            IEnumerable<T> values)
        {
            if (result.RouteValues.Keys.Any(k => k.StartsWith(key + "[")))
                throw new ArgumentException("Key already exists in collection");

            if (values == null)
                throw new ArgumentNullException("values cannot be null");

            var valuesList = values.ToList();

            for (int i = 0; i < valuesList.Count; i++)
            {
                result.RouteValues.Add(String.Format("{0}[{1}]", key, i), valuesList[i]);
            }

            return result;
        }

        public static void UpdateAccessTimeStamp(this Controller ctrl,string userid){            
            using (var db = new GT.InboundApplication.Model.Common.ApplicationDbContext())
            using (var crm = new IGTCRMContext()) {
                if (ctrl.Session["AccessTimeStamp"] == null)
                {
                    ctrl.Session["AccessTimeStamp"] = db.UpdateAccessTime(userid);
                    //crm.UpdateWorkingStatus(userid);
                }
                else
                {
                    var ats = (DateTime)ctrl.Session["AccessTimeStamp"];
                    if (ats.AddMinutes(30) < DateTime.Now)
                    {
                        ctrl.Session["AccessTimeStamp"] = db.UpdateAccessTime(userid);
                    }
                }

            }
        }
    }
    public static class HTMLHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html, string action = null, string controller = null)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            string[] actions = null;
            string[] controllers = null;

            if (action.Contains(',')) actions = action.Split(',');
            else actions = new string[] { action };

            if (controller.Contains(',')) controllers = controller.Split(',');
            else controllers = new string[] { controller };

            return controllers.Contains(currentController) && action.Contains(currentAction) ?
                cssClass : String.Empty;
        }

        public static string IsSelected(this HtmlHelper html, string action = null)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["controller"] + "/" + (string)html.ViewContext.RouteData.Values["action"];
            string[] actions = null;

            if (action.Contains(',')) actions = action.Split(',');
            else actions = new string[] { action };

            return action.Contains(currentAction) ?
                cssClass : String.Empty;
        }
        public static string ParseDateToStringFormat(DateTime? date)
        {
            if (date != null)
            {
                return date.ToString().ParseToDate().ParseToDateString("d/M/yyyy");
            }
            return string.Empty; 
        }
        public static void EvacateValueModel<T>(this T holder, T pusher)
        {
            if (holder == null || pusher == null) return;
            var properties = holder.GetType().GetProperties();
            var ex_properties = pusher.GetType().GetProperties();
            if (properties.Count() == ex_properties.Count())
            {
                foreach (var property in properties.OrderBy(item => item.Name))
                {
                    var ex_property = ex_properties.SingleOrDefault(item => item.Name == property.Name);
                    if (ex_property != null)
                    {
                        try
                        {
                            var new_value = property.GetValue(pusher);
                            ex_property.SetValue(holder, new_value);
                        }
                        catch { }
                    }
                }
            }
        }
        public static void FillInModelWithDefaultValue<T>(this T model)
        {
            if (model == null) return;
            var properties = model.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanWrite) continue;
                if (property.GetValue(model) == null)
                {
                    if (property.PropertyType == typeof(Int32) || property.PropertyType == typeof(System.Nullable<Int32>))
                    {
                        property.SetValue(model, 0);
                    }
                    else if (property.PropertyType == typeof(string))
                    {
                        property.SetValue(model, string.Empty);
                    }
                    else if (property.PropertyType == typeof(decimal) || property.PropertyType == typeof(System.Nullable<decimal>))
                    {
                        property.SetValue(model, Decimal.Zero);
                    }
                    else if (property.PropertyType == typeof(Int16) || property.PropertyType == typeof(System.Nullable<Int16>))
                    {
                        Int16 temp = 0;
                        property.SetValue(model, temp);
                    }
                    else if (property.PropertyType == typeof(Int32) || property.PropertyType == typeof(System.Nullable<Int32>))
                    {
                        property.SetValue(model, 0);
                    }
                    else if (property.PropertyType == typeof(Int64) || property.PropertyType == typeof(System.Nullable<Int64>))
                    {
                        Int64 temp = 0;
                        property.SetValue(model, temp);
                    }
                    else if (property.PropertyType == typeof(Boolean) || property.PropertyType == typeof(System.Nullable<Boolean>))
                    {
                        if (property.Name == "HideFlag") return;
                        property.SetValue(model, false);
                    }
                    else if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(System.Nullable<DateTime>))
                    {
                        if (property.Name == "OptOut_Date") return;                        
                        property.SetValue(model, DateTime.Now);
                    }
                }
            }
        }
        public static string GetAgentFullName(this HtmlHelper html, string agentid)
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                var user = context.Users.SingleOrDefault(x => x.Id == agentid);
                if (user != null)
                {
                    return string.Format("{0} {1}", user.FirstName, user.LastName);
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public static string GetServerName(this HtmlHelper html)
        {
            IGTCRMContext db = new IGTCRMContext();
            string prefixword = "data source=";
            string postfixword = ";";
            int startindex = db.Database.Connection.ConnectionString.IndexOf(prefixword);
            int length = prefixword.Length;
            int endindex = db.Database.Connection.ConnectionString.IndexOf(postfixword, startindex + length);
            return db.Database.Connection.ConnectionString.Substring(startindex + length, endindex - (startindex + length));
        }
        public static string GetDatabaseName(this HtmlHelper html)
        {
            IGTCRMContext db = new IGTCRMContext();
            string prefixword = "initial catalog=";
            string postfixword = ";";
            int startindex = db.Database.Connection.ConnectionString.IndexOf(prefixword);
            int length = prefixword.Length;
            int endindex = db.Database.Connection.ConnectionString.IndexOf(postfixword, startindex + length);
            return db.Database.Connection.ConnectionString.Substring(startindex + length, endindex - (startindex + length));
        }
	}

    public static class GTScripts
    {
        public static IHtmlString Render(params string[] paths)
        {
            string appversion = System.Configuration.ConfigurationManager.AppSettings["application_version"].ToString();
            return System.Web.Optimization.Scripts.RenderFormat("<script type=\"text/javascript\" src=\"{0}?v=" + appversion + " \"></script>", paths);
        }
    }
    public static class GTStyles
    {
        public static IHtmlString Render(params string[] paths)
        {
            string appversion = System.Configuration.ConfigurationManager.AppSettings["application_version"].ToString();
            return System.Web.Optimization.Styles.RenderFormat("<link href=\"{0}?v=" + appversion + "\" rel=\"stylesheet\">", paths);
        }
    }
    public class CssRewriteUrlTransformWrapper : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }
}
