﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace GT.CRMApplication.WebMVC5.Helpers
{
    public class EmailHelper
    {

        public static void DoSendEmail(
              List<string> to
            , List<string> cc
            , string subject
            , string body
            , string form = "ccsnoreply@generali.co.th"
            , string formName = "Confirmation Call System"
            , MemoryStream memoryStream = null
            , string attachmentFileName = null
            , Attachment attachment = null
            )
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(form, formName);

            string mailTo = string.Join(",", to);
            string mailCC = "";
            if(cc != null) mailCC = string.Join(",", cc);

            if (mailTo == "") throw new Exception("Mail-To: is empty.");
            mail.To.Add(mailTo);

            if (mailCC != "")
                mail.CC.Add(mailCC);

            mail.Subject = subject;
            mail.Body = body;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            if (memoryStream != null)
            {
                mail.Attachments.Add(new Attachment(memoryStream, attachmentFileName));
            }
            else if (attachment != null)
            {
                mail.Attachments.Add(attachment);
            }
            
            SmtpClient smtpClient = new SmtpClient("webmail.generali.co.th");
            ServicePointManager.ServerCertificateValidationCallback = CertificateValidationCallBack;
            smtpClient.Send(mail);
        }

        private static bool CertificateValidationCallBack(
              object sender
             , System.Security.Cryptography.X509Certificates.X509Certificate certificate
             , System.Security.Cryptography.X509Certificates.X509Chain chain
             , System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            // If the certificate is a valid, signed certificate, return true.
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }

            // If there are errors in the certificate chain, look at each error to determine the cause.
            if ((sslPolicyErrors & System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus status in chain.ChainStatus)
                    {
                        if ((certificate.Subject == certificate.Issuer) &&
                           (status.Status == System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot))
                        {
                            // Self-signed certificates with an untrusted root are valid. 
                            continue;
                        }
                        else
                        {
                            if (status.Status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
                            {
                                // If there are any other errors in the certificate chain, the certificate is invalid,
                                // so the method returns false.
                                return false;
                            }
                        }
                    }
                }

                // When processing reaches this line, the only errors in the certificate chain are 
                // untrusted root errors for self-signed certificates. These certificates are valid
                // for default Exchange server installations, so return true.
                return true;
            }
            else
            {
                // In all other cases, return false.
                return false;
            }
        }

    }
}