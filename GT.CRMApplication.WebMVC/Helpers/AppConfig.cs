﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;

namespace GT.CRMApplication.WebMVC5.Helpers
{
    public class AppConfig
    {
        public const int LeadExpireDays = 45;
        static private int _supexpiredays = -1;
        public static int SupExpireDays
        {
            get {
                if (_supexpiredays > 0)
                {
                    return _supexpiredays; 
                }
                else
                {
                    _supexpiredays = ConfigurationManager.AppSettings["supexpiredays"].ParseToPositiveInt();                    
                }
                return _supexpiredays; 
            }
            //set { } 
        }
    }
    
}