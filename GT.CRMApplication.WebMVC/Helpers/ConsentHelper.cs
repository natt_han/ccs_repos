﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;

namespace GT.CRMApplication.WebMVC5.Helpers
{
    public class ConsentHelper
    {
        #region Consent

        public static void SetConsent(dynamic profile, FormCollection form)
        {
            try
            {
                var consentFlag = form.GetValue("consent_flag");
                var consentDataSource = form.GetValue("consent_data_source");
                var consentType = form.GetValue("consent_type");
                var consentVersion = form.GetValue("consent_version");
                var consentId = form.GetValue("consent_id");

                if (consentFlag!=null && profile.ConsentFlag != consentFlag.AttemptedValue)
                {
                    profile.ConsentDate = DateTime.Now.Date;
                    profile.ConsentTime = DateTime.Now.ToString("HH:mm");
                    profile.ConsentBy = profile.UpdatedBy;

                    profile.ConsentFlag = consentFlag.AttemptedValue;
                    profile.ConsentDataSource = consentDataSource.AttemptedValue;
                    profile.ConsentType = consentType.AttemptedValue;
                    profile.ConsentVersion = consentVersion.AttemptedValue;
                    profile.ConsentId = consentId.AttemptedValue.ParseToPositiveInt();
                }
            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        public static void GetConsent(dynamic ViewBag, dynamic profile)
        {
            ViewBag.ConsentId = profile.ConsentId;
            ViewBag.ConsentDate = profile.ConsentDate;
            ViewBag.ConsentTime = profile.ConsentTime;

            if (profile.ConsentFlag == null || profile.ConsentFlag == "")
            {
                ViewBag.ConsentFlag = "NoSelect";
            }
            else
            {
                ViewBag.ConsentFlag = profile.ConsentFlag;
            }
            
            if(ViewBag.ConsentFlag != "NoSelect")
            {
                ViewBag.ConsentHistoryText = String.Format("เคยสอบถามไปแล้วเมื่อ {0} เมื่อเวลา {1} โดยลูกค้าตอบว่า {2}", profile.ConsentDate.ToString("dd/MM/yyyy"), profile.ConsentTime, (profile.ConsentFlag == "Yes" ? "ยินยอม" : "ไม่ยินยอม"));
            }
        }
        public static void SetInformFlag(GT.InboundApplication.Model.CRM.LeadProfile profile, FormCollection form)
        {
            try
            {
                var consentFlag = form.GetValue("leadsource_inform_flag");
                if (consentFlag != null && profile.LeadsourceInformFlag != (consentFlag.AttemptedValue=="True"?true:false))
                {
                    profile.LeadsourceInformFlag = (consentFlag.AttemptedValue == "True" ? true : false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }


        #endregion  

    }
}