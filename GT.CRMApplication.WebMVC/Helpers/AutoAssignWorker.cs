﻿using GT.InboundApplication.Model.CRM;
using System;
using System.Linq;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.GTApplicationDB;

namespace GT.CRMApplication.WebMVC5.Helpers
{
    public class AutoAssignWorker
    {
        private IGTCRMContext db = new IGTCRMContext();
        private const string ChallengeMode = "Challenge";
        private const string SetZeroMode = "Set Zero";
        private const string ConfigCode = "AUTOASSIGN";
        private const string ConfigWorkingMode = "WORKINGMODE";
        private const string ConfigSetZeroDay = "SETZERODAY";
        public AutoAssignWorker()
        {
            db.Database.Log = sql => System.Diagnostics.Debug.Write(sql);
        }

        /// <summary>
        /// Check is auto assign working
        /// - working day , within working time (Mon-Fri:9.00-19.30|Sat-Sun:9:30-18:00)
        /// - config to on
        /// </summary>
        /// <returns></returns>
        public bool IsAuto()
        {
            var now = DateTime.Now;
            if (now.Hour < 9 || now.TimeOfDay >= System.TimeSpan.Parse("18:00")) return false;
            if (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday)
            {
                if ((now.TimeOfDay < System.TimeSpan.Parse("9:30")) || (now.Hour >= 18)) return false;
            } 
            
                var config = db.ValuePreferenceSet.SingleOrDefault(x => x.Active && x.Code == ConfigCode && x.Name== ConfigWorkingMode);
                if (config == null) return false;
                if (config.Value != "1") return false;
            
            if (!db.IsWorkingDate(DateTime.Today)) return false;
            return true;
        }

        public string WorkingMode()
        {
            
                var config = db.ValuePreferenceSet.SingleOrDefault(x => x.Active && x.Code == ConfigCode && x.Name == ConfigWorkingMode);
                if (config == null) return null;
                return config.Value;
        }

        public int SetZeroDay()
        {
            
                var config = db.ValuePreferenceSet.SingleOrDefault(x => x.Active && x.Code == ConfigCode && x.Name == ConfigSetZeroDay);
                if (config == null) return 5;
                return config.Value.ParseToPositiveInt();
        }

        public void TurnOn()
        {
            
                var config = db.ValuePreferenceSet.SingleOrDefault(x => x.Active && x.Code == ConfigCode && x.Name == ConfigWorkingMode);
                config.Value = "1";
                db.SaveChanges();
        }

        public void TurnOff()
        {
            
                var config = db.ValuePreferenceSet.SingleOrDefault(x => x.Active && x.Code == ConfigCode && x.Name == ConfigWorkingMode);
                config.Value = "0";
                db.SaveChanges();
        }

        public string AssignMode()
        {
            if (db.GetWorkingDay(DateTime.Today) > this.SetZeroDay())
            {
                return ChallengeMode;
            }
            return SetZeroMode;
        }

        public string AutoAssign(long leadid)
        {            
            if (AssignMode() == ChallengeMode)
            {
                return AutoAssignByTier(leadid);
            }
            return AutoAssignEquallyIgnoreTier(leadid);
        }

        public string AutoAssignEqually(long leadid)
        {
            var lead = db.LeadProfileSet.Find(leadid);
            var srcid = lead.CallSourceCode.ParseToPositiveLong();
            //var rank = db.LeadRankSet.Where(c => c.CallSourceId == srcid).FirstOrDefault();
            var today = DateTime.Today;
            var t = db.AutoAssignSet.Where(c => c.AsOfDate == today && c.AutoAssignMode != 0 && c.IsWorking);
            AutoAssign selectedtsr;
            var leadrank =  "E";
            switch (leadrank)
            {
                case "A": t = t.OrderBy(o => o.LeadA).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
                case "B": t = t.OrderBy(o => o.LeadB).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
                case "C": t = t.OrderBy(o => o.LeadC).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
                case "D": t = t.OrderBy(o => o.LeadD).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
                default: t = t.OrderBy(o => o.LeadE).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            }
            selectedtsr = t.FirstOrDefault();
            if (selectedtsr != null)
            {
                var leadAssignment = lead.AssignLead("AUTO", selectedtsr.UserId, selectedtsr.FullName);
                selectedtsr.IncreaseCounter(leadrank);
                db.SaveChanges();
                lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = leadAssignment.LeadAssignmentId;
                db.SaveChanges();
                return selectedtsr.FullName;
            }
            else
            {
                return "0";
            }
        }

        public string AutoAssignEquallyIgnoreTier(long leadid)
        {
            var lead = db.LeadProfileSet.Find(leadid);
            var srcid = lead.CallSourceCode.ParseToPositiveLong();
            // rank = db.LeadRankSet.Where(c => c.CallSourceId == srcid).FirstOrDefault();
            var today = DateTime.Today;
            var t = db.AutoAssignSet.Where(c => c.AsOfDate == today && c.AutoAssignMode != 0 && c.IsWorking).OrderBy(o=>o.LeadTotal).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); ;
            AutoAssign selectedtsr;
            var leadrank = "E";
            //switch (leadrank)
            //{
            //    case "A": t = t.OrderBy(o => o.LeadA).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            //    case "B": t = t.OrderBy(o => o.LeadB).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            //    case "C": t = t.OrderBy(o => o.LeadC).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            //    case "D": t = t.OrderBy(o => o.LeadD).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            //    default: t = t.OrderBy(o => o.LeadE).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv); break;
            //}
            selectedtsr = t.FirstOrDefault();
            if (selectedtsr != null)
            {
                var leadAssignment = lead.AssignLead("AUTO", selectedtsr.UserId, selectedtsr.FullName);
                selectedtsr.IncreaseCounter(leadrank);
                db.SaveChanges();
                lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = leadAssignment.LeadAssignmentId;
                db.SaveChanges();
                return selectedtsr.FullName;
            }
            else
            {
                return "0";
            }
        }

        public string AutoAssignByTier(long leadid)
        {
            var lead = db.LeadProfileSet.Find(leadid);
            var srcid = lead.CallSourceCode.ParseToPositiveLong();
            //var rank = db.LeadRankSet.Where(c => c.CallSourceId == srcid).FirstOrDefault();
            var today = DateTime.Today;
            AutoAssign selectedtsr=null;
            var leadrank = "E";

            selectedtsr = db.GetNextForAutoAssignByPerformance(leadrank);

            if (selectedtsr != null)
            {
                var leadAssignment = lead.AssignLead("AUTO", selectedtsr.UserId, selectedtsr.FullName);
                selectedtsr.IncreaseCounter(leadrank);
                db.SaveChanges();
                lead.RecentLeadAssignment.LeadAssignment_LeadAssignmentId = leadAssignment.LeadAssignmentId;
                db.SaveChanges();
                return selectedtsr.FullName;
            }
            else
            {
                return "0";
            }
        }

    }
}