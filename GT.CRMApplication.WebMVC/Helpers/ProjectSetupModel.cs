﻿using GT.InboundApplication.Model.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.CRMApplication.Helpers
{
    public static class ProjectSetupModel
    {
        public static string GetProjectName()
        {
            using (IGTCRMContext appDB = new IGTCRMContext())
            {
                var projectItem = appDB.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.ToUpper().Equals("PROJECTNAME"));
                if (projectItem != null)
                {
                    return projectItem.Value.ToUpper();
                }
            }
            return string.Empty;
        }

        public static string GetProjectType()
        {
            var proj = GetProjectName();
            if (proj == "IGL" || proj.StartsWith("HB"))
            {
                return "LIFE";
            }
            else
            {
                return "NL";
            }
        }

        public static string GetPortNo()
        {
            using (IGTCRMContext appDB = new IGTCRMContext())
            {
                var projectItem = appDB.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.ToUpper().Equals("PORTNO"));
                if (projectItem != null)
                {
                    return projectItem.Value.ToUpper();
                }
            }
            return string.Empty;
        }


        public static string GetLeadIntegrationAPIEndPoint()
        {
            using (IGTCRMContext appDB = new IGTCRMContext())
            {
                var item = appDB.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.ToUpper().Equals("LEADINTEGRATIONAPIENDPOINT"));
                if (item != null)
                {
                    return item.Value;
                }
            }
            return string.Empty;
        }

        public static string GetQCallPrefix()
        {
            using (IGTCRMContext appDB = new IGTCRMContext())
            {
                var projectItem = appDB.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.ToUpper().Equals("QCALLPREFIX"));
                if (projectItem != null)
                {
                    return projectItem.Value.ToUpper();
                }
            }
            return string.Empty;
        }
        public static string GetQCallRandomPrefix()
        {
            using (IGTCRMContext appDB = new IGTCRMContext())
            {
                var projectItem = appDB.ValuePreferenceSet.SingleOrDefault(row => row.Active && row.Code.ToUpper().Equals("QCALLPREFIX_RANDOM"));
                if (projectItem != null)
                {
                    return projectItem.Value.ToUpper();
                }
            }
            return string.Empty;
        }
    } 
}
