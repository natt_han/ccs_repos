function getFlexApp(appName) {
  if (navigator.appName.indexOf ("Microsoft") !=-1) {
	return window[appName];
  } else {
	return document[appName];
	}
}

function spLogin(uname,pwd)
{
  getFlexApp('QAgentLite').spLogin(uname,pwd);
}

function spLogout()
{
	getFlexApp('QAgentLite').spLogout();
}

function spReady()
{
	getFlexApp('QAgentLite').spReady();
}

function spNotReady(reason)
{
	getFlexApp('QAgentLite').spNotReady();
}

function spMute()
{
	getFlexApp('QAgentLite').spMute();
}

function spDial(phoneno,custName)
{
	getFlexApp('QAgentLite').spDial(phoneno,custName);
}

function spAnswer()
{
	getFlexApp('QAgentLite').spAnswer();
}

function spHangup()
{
	getFlexApp('QAgentLite').spHangup();
}

function spHold()
{
	getFlexApp('QAgentLite').spHold();
}

function spXfer(phoneno)
{
	getFlexApp('QAgentLite').spXfer(phoneno);
}

function spConf()
{
	getFlexApp('QAgentLite').spConf();
}

function spNumpad()
{
	getFlexApp('QAgentLite').spNumpad();
}

function spDTMF(digit)
{
	getFlexApp('QAgentLite').spDTMF(digit);
}

function spDisp(disp)
{
	getFlexApp('QAgentLite').spDisp(disp);
}
	