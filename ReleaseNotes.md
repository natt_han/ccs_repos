#CCS Release Notes
## V1.2.0 (2021-10-11)
* เพิ่มการบันทึกข้อมูลเวลา Log-in/Log-out , วันหยุด, วันลา
* เพิ่มรายงาน TimeAttendance

## V1.1.0 (2021-08-30)
* Masking เบอร์โทรสำหรับ TSR [E4S-J6U-3MXR](http://192.168.0.98/hesk/admin/admin_ticket.php?track=E4S-J6U-3MXR)
