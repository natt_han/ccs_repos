//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GT.InboundApplication.Model.CRM
{
    using System;
    
    public partial class PROC_DASHBRD_CALL_BYCALLTYPE_PIE_Result
    {
        public Nullable<int> IB_CALL { get; set; }
        public Nullable<int> OB_CALL { get; set; }
        public Nullable<int> FLWUP_CALL { get; set; }
    }
}
