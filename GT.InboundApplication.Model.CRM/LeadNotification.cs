//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GT.InboundApplication.Model.CRM
{
    using System;
    using System.Collections.Generic;
    
    public partial class LeadNotification
    {
        public LeadNotification()
        {
            this.IsRead = false;
            this.IsActive = true;
            this.NotiType = "NewLead";
        }
    
        public int ItemId { get; set; }
        public long LeadProfileId { get; set; }
        public string FullName { get; set; }
        public long CurrentLeadProfileId { get; set; }
        public string CallSource { get; set; }
        public Nullable<bool> IsAssign { get; set; }
        public bool IsRead { get; set; }
        public string ReceiverUserId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public string NotiType { get; set; }
        public string JsonData { get; set; }
    }
}
