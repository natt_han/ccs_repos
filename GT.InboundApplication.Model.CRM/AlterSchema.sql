﻿-- ==============================================
-- alter table schema for enhance lead prioritize
-- witsaruth 27/08/2020
-- ==============================================
ALTER TABLE [dbo].[RecentLeadAssignmentSet] ADD HideFlag bit null;
CREATE TABLE [dbo].[LeadSourceConversionByTSRSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportMonth] [varchar](10) NOT NULL,
	[CallSourceId] [bigint] NOT NULL,
	[ReceiverUserId] [nvarchar](50) NULL,
	[NewLead] [int] NULL,
	[Success] [int] NULL,
 CONSTRAINT [PK_LeadSourceConversionByTSRSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
);
CREATE TABLE [dbo].[LeadSourceConversionTopTSRSet](
	[CallSourceId] [bigint] NOT NULL,
	[newlead] [int] NOT NULL,
	[success] [int] NOT NULL,
	[conv] [numeric](24, 12) NOT NULL,
	[top_tsr] [nvarchar](max) NOT NULL,
	[tsr_newlead] [int] NOT NULL,
	[tsr_success] [int] NOT NULL,
	[tsr_conv] [numeric](24, 12) NULL
);
--create procedure PROC_LeadSourceConversion_by_TSR
--create procedure [dbo].[PROC_UpdateLeadSourceConbyTSR]
--create procedure PROC_UpdateLeadSourceConTopTSR
--create view VW_ActiveLead

-- ==============================================
-- alter table schema for lead partner report
-- witsaruth 04/08/2020
-- ==============================================
-- CREATE TYPE LeadPerformanceReport
-- create table LeadPerformanceSet
-- CREATE [PROC_RPT_KPI_BY_DB_PRODUCT]
-- CREATE [PROC_RPT_KPI_BY_DB_PRODUCT]
-- CREATE [PROC_UpdateLeadPerformance]


ALTER TABLE [GTCRM_DB].[dbo].[LeadConversionBySourceSet] ADD NEW_DMC int null;
-- ==============================================
-- alter table schema for rework consent
-- witsaruth 20/12/2019
-- ==============================================
alter table dbo.LeadProfileSet add LeadsourceInformFlag bit NULL, ConsentFlag nvarchar(1) null ,ConsentVersion nvarchar(max) null;
alter table [GTLeadIntegration_DB].[dbo].[ConsentMappings] add [LeadSourceWording] nvarchar(100) NULL;

CREATE SYNONYM [dbo].[ConsentMappings] FOR [GTLeadIntegration_DB].[dbo].[ConsentMappings]
GO
-- add consent
Go;
ALTER PROCEDURE [dbo].[PROC_LEADINTEGRATION_GetLeadTransaction]
	@PR_LEADTRANSACTIONID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 

	   0 AS [LeadProfileId]
      ,'' AS [SalutationCode]
      ,'' AS [Salutation]
      ,LT.[Fname] AS [FirstName]
      ,LT.[Lname] AS [LastName]
      ,'' AS [GenderCode]
      ,'' AS [GenderName]
      ,'' AS [IsCustomer]
      ,'' AS [Occupation]
      ,'' AS [AnnualIncome]
      ,'' AS [CardId]
      ,'' AS [CardType]
      ,LT.[Tel] AS [TelPhone]
      ,'' AS [TelPhone2]
      ,CreatedBy AS [CreatedBy]
      ,'' AS [UpdatedBy]
      ,CreatedDate AS [CreatedDate]
      ,CreatedDate AS [UpdatedDate]
      --, DBO.RemoveHTMLTag(LT.[MailBody]) AS [Remark]
	  ,case when LT.[MailBody]='' and LT.[CallbackTime] <> '' then  'Callback Time : '+lt.CallbackTime else DBO.RemoveHTMLTag(LT.[MailBody]) end AS [Remark]
      ,'' AS [Note]
      ,'' AS [BirthDate]
      ,LT.[Email] AS [Email]
      ,'' AS [TelPhone_Extension]
      ,'' AS [TelPhone2_Extension]
      ,'1' AS [Status]
	  ,LT.CallSourceId AS [CallSourceCode]
	  ,LT.CallSource AS [CallSource]
      ,(SELECT TOP 1 [name] from dbo.OfferProductSet where [name] = LT.[ProductName]) AS [Product]
      ,(SELECT TOP 1 [OfferProductId] from dbo.OfferProductSet where [name] = LT.[ProductName]) AS [ProductCode]
      ,LT.[src] AS [LeadSource]
      ,'N' AS [LeadFlag]
      ,'' AS [ProspectId]
      ,'' AS [SystemRemark]
	  ,LT.LeadTransactionId AS [LeadTransactionId]
	  ,LT.ApplicationNo
	  ,LT.ReferrerFullName
	  ,LT.ReferrerRelationShip
	  ,LT.ReferrerTel 
	  ,cast(lt.ConsentFlag as int) ConsentFlag
	  ,lt.ConsentVersion
	FROM DBO.LeadTransactions LT
	WHERE LT.LeadTransactionId = @PR_LEADTRANSACTIONID
END;

update [CallSourceSet] set AppName='CCC',				src=''		where CallSourceId=10;
update [CallSourceSet] set AppName='LN-Chatbot',		src='GEN'	where CallSourceId=32;
update [CallSourceSet] set AppName='Fb-Comment',		src='MNL'	where CallSourceId=36;
update [CallSourceSet] set AppName='CCC',				src=''		where CallSourceId=38;
update [CallSourceSet] set AppName='Call-in',			src='WEB'	where CallSourceId=39;
update [CallSourceSet] set AppName='Call-in',			src='SMS'	where CallSourceId=108;
update [CallSourceSet] set AppName='Call-in',			src='FBK'	where CallSourceId=115;
update [CallSourceSet] set AppName='LN-Chatbot',		src='LAC'	where CallSourceId=117;
update [CallSourceSet] set AppName='CCC',				src='CCC'	where CallSourceId=127;
update [CallSourceSet] set AppName='GT Landing Page',	src='PRT'	where CallSourceId=141;
update [CallSourceSet] set AppName='Call-in',			src='SEM'	where CallSourceId=20041;
update [CallSourceSet] set AppName='Call-in',			src='LNE'	where CallSourceId=20072;
update [CallSourceSet] set AppName='FB-Admin',			src='AMK'	where CallSourceId=20077;
update [CallSourceSet] set AppName='FB-Admin',			src='AMV'	where CallSourceId=20078;
update [CallSourceSet] set AppName='FB-Admin',			src='AMD'	where CallSourceId=20079;
update [CallSourceSet] set AppName='IG-Admin',			src='IAK'	where CallSourceId=20094;
update [CallSourceSet] set AppName='IG-Admin',			src='IAV'	where CallSourceId=20095;
update [CallSourceSet] set AppName='IG-Admin',			src='IAD'	where CallSourceId=20096;
update [CallSourceSet] set AppName='FB-Admin',			src='AMU'	where CallSourceId=20124;
update [CallSourceSet] set AppName='IG-Admin',			src='IAU'	where CallSourceId=20126;

-- alter table schema for opt-out log
-- witsaruth 02/01/2019
-- ==============================================
alter table dbo.Prospect_ActivitySet add [OptOut] varchar(max);
-- ==============================================
-- alter table schema for lead prioritize
-- witsaruth 02/12/2019
-- ==============================================
alter table dbo.Prospect_ActivitySet add [CallFrom] varchar(20), [Priority] int NULL,[SubPriority] int NULL,[PriorityVersion] int NULL,[PriorityTime] Datetime;
alter table dbo.RecentLeadAssignmentSet add [Priority] int NULL,[SubPriority] int NULL,[PriorityVersion] int NULL,[PriorityTime] Datetime;
alter table dbo.LeadAssignmentSet add [Priority] int NULL,[SubPriority] int NULL,[PriorityVersion] int NULL,[PriorityTime] Datetime;
CREATE TABLE [dbo].[LeadPrioritizeHist](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LeadProfileId] [bigint] NOT NULL,
	[LeadAssignmentId] [bigint] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[Priority] [int] NOT NULL,
	[SubPriority] [int] NOT NULL,
	[Version] [int] NOT NULL,
	 CONSTRAINT [PK_LeadPrioritizeHist] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	)
);
GO;
CREATE TRIGGER TRG_AI_LEAD_CAL_PRIORITY
   ON  dbo.LeadAssignmentSet
   AFTER INSERT
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @LeadProfileId as bigint,	@LeadAssignmentId as bigint;
	select @LeadProfileId=LeadProfileLeadProfileId,@LeadAssignmentId=LeadAssignmentId from inserted;
	EXEC PROC_LEAD_PRIORITY @LeadProfileId ,@LeadAssignmentId;

END
GO;

ALTER TABLE dbo.AspNetUsers add [EnablePilotFeature] bit null;

-- ==============================================
-- alter table schema
-- witsaruth 25/11/2019
-- ==============================================
alter table dbo.LeadAssignmentSet add [UnAssignUserId] nvarchar(128) NULL;
alter table dbo.RecentLeadAssignmentSet add [CallBackOf] nvarchar(128) NULL;

-- ==============================================
-- alter table schema
-- sukasem siwaporn 14/06/2018
-- ==============================================

alter table dbo.Prospect_ProfileSet add   	[ConsentFlag] nvarchar(max)  NULL,     [ConsentType] nvarchar(max)  NULL,     [ConsentDataSource] nvarchar(max)  NULL,     [ConsentDate] datetime  NULL,     [ConsentTime] nvarchar(max)  NULL,     [ConsentVersion] nvarchar(max)  NULL,     [ConsentId] int  NULL,     [ConsentRevenueId] int  NULL,     [ConsentRevenueFlag] nvarchar(max)  NULL,     [ConsentRevenueVersion] nvarchar(max)  NULL;
