﻿using System.Collections.Generic;

namespace GT.InboundApplication.Model.CRM
{
    public class LeadDistributionTarget
    {
        public decimal TargetAPE_IGT { get; set; }
        public decimal TargetAPE_IG2 { get; set; }
        public decimal TargetAPE_IGL { get; set; }
        public decimal TargetAPE_HBT { get; set; }
        public decimal TargetAPE_HB2 { get; set; }
        public List<LeadDistributionItem> LeadDistributionItems { get; set; }
    }
    
    public class LeadDistributionItem
    {
        public int No { get; set; }
        public string CallSource { get; set; }
        public int CallSourceId { get; set; }

        public double? IGTDistributePercentage { get; set; }
        public double? IG2DistributePercentage { get; set; }
        public double? IG3DistributePercentage { get; set; }

        public double? IGLDistributePercentage { get; set; }
        public double? HBTDistributePercentage { get; set; }
        public double? HB2DistributePercentage { get; set; }

        public int IGTActualLead { get; set; }
        public int IG2ActualLead { get; set; }
        public int IG3ActualLead { get; set; }

        public int IGLActualLead { get; set; }
        public int HBTActualLead { get; set; }
        public int HB2ActualLead { get; set; }

        public double IGTMTDLeadConversion { get; set; }
        public double IG2MTDLeadConversion { get; set; }
        public double IG3MTDLeadConversion { get; set; }

        public double IGLMTDLeadConversion { get; set; }
        public double HBTMTDLeadConversion { get; set; }
        public double HB2MTDLeadConversion { get; set; }

        public double LTMLeadConversion { get; set; }
        public double MTDLeadConversion { get; set; }

    }
}
