﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GT.InboundApplication.Model.CRM
{
    [Table("UnsuccessLife")]
    public class UnsuccessLife
    {
        public string Product { get; set; }
        public DateTime IssueDate { get; set; }
        
        public string TM { get; set; }
        [Key]
        public string PolicyNo { get; set; }
        public string ApplicationNo { get; set; }
        public string FirstName { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public DateTime CallDate { get; set; }
        public string StatCode { get; set; }
        public string BusinessDate { get; set; }
    }
}
