﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("UserLoggedSet")]
    public class UserLogged
    {        
        [Key, Column(Order = 0,TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date {get; set;}
        [Key, Column(Order = 1)]
        public string UserId { get; set; }
        public DateTime? LoggedIn { get; set; }
        public DateTime? LoggedOut { get; set; }
        public string Remark { get; set; }
    }
}
