﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("LeadSourceConversionTopTSRSet")]
    public class TopTSRConversion
    {
        [Key]
        public long CallSourceId { get; set; }

        public string top_tsr { get; set; }

        public decimal tsr_conv { get; set; }
    }
}
