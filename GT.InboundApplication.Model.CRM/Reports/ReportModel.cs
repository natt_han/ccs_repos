﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.Generics;
using GT.InboundApplication.Model.Common;
using DM.Utilities.NetFramework.Models;
using System.Security.Policy;
using GT.InboundApplication.Model.GTApplicationDB;
using System.Linq;

namespace GT.InboundApplication.Model.CRM
{
    public class ReportModel
    {
        public int _ExecutionTimeOut { get; set; }
        public ReportModel()
        {
            this._ExecutionTimeOut = ConfigurationManager.AppSettings["ExecutionTimeOut"].ToString().ParseToPositiveInt();
        }

        public DataSet loadRPT_KPI_TSR_ALL(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_TSR_ALL", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_KPI_TSR(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_TSR", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_TSR_KPIbyProductNL(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_KPI_BY_PRODUCT_NL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_TSR_KPIbyProductOL(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_KPI_BY_PRODUCT_OL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadRPT_TSR_KPIbyProductNLAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_KPI_BY_PRODUCT_NL_ALL", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataSet loadRPT_TSR_KPIbyProductOLAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_KPI_BY_PRODUCT_OL_ALL", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_KPI_TSR_IGM(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_TSR_IGM", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_KPI_DB(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            if (!string.IsNullOrEmpty(tsr)) supervisor = null;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_DB_V2", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_KPI_DB_ALL(string connectionString, DateTime start, DateTime end,string project)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            if(project.IsNullOrEmpty() == false)
            {
                param.Add("PR_PROJECT", project);
            }
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_DB_ALL_V2", param, this._ExecutionTimeOut);
            DataTable temp = ds.Tables[0].Clone();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                temp.Merge(ds.Tables[i]);
            }
            var dt = temp.AsEnumerable()
                    .GroupBy(r => new { sourcecode= r["sourcecode"] ,sourcename=r["sourcename"]})
                    .Select(g =>
                    {
                        var row = temp.NewRow();
                        var total_lead = g.Sum(r => r.Field<int?>("total_lead"));
                        var new_lead = g.Sum(r => r.Field<int?>("new_lead"));
                        var total_used = g.Sum(r => r.Field<int?>("total_used"));
                        var new_used = g.Sum(r => r.Field<int?>("new_used"));
                        var total_call = g.Sum(r => r.Field<int?>("total_call"));
                        var success = g.Sum(r => r.Field<int?>("success"));
                        var new_success = g.Sum(r => r.Field<int?>("new_success"));
                        var dmc = g.Sum(r => r.Field<int?>("dmc"));
                        var new_dmc = g.Sum(r => r.Field<int?>("new_dmc"));
                        var approved = g.Sum(r => r.Field<int?>("approved"));
                        var sum_approved = g.Sum(r => r.Field<decimal?>("sum_approved"));
                        var new_approved = g.Sum(r => r.Field<int?>("new_approved"));
                        row["sourcecode"] = g.Key.sourcecode;
                        row["sourcename"] = g.Key.sourcename;
                        row["total_lead"] = g.Sum(r => r.Field<int>("total_lead"));
                        row["new_lead"] = g.Sum(r => r.Field<int>("new_lead"));
                        row["old_lead"] = g.Sum(r => r.Field<int>("old_lead"));
                        row["total_used"] = g.Sum(r => r.Field<int>("total_used"));
                        row["total_call"] = g.Sum(r => r.Field<int>("total_call"));
                        row["call_attempt"] = SolutionHelper.divide(total_call , total_used);
                        row["nocontact"] = g.Sum(r => r.Field<int>("nocontact"));
                        row["followup"] = g.Sum(r => r.Field<int>("followup"));
                        row["wait"] = g.Sum(r => r.Field<int>("wait"));
                        row["inprogress"] = g.Sum(r => r.Field<int>("inprogress"));
                        row["reject"] = g.Sum(r => r.Field<int>("reject"));
                        row["reject_45"] = g.Sum(r => r.Field<int?>("reject_45"));
                        row["reject_im"] = g.Sum(r => r.Field<int?>("reject_im"));
                        row["not_update"] = g.Sum(r => r.Field<int>("not_update"));
                        row["not_update_45"] = g.Sum(r => r.Field<int?>("not_update_45"));
                        row["not_update_im"] = g.Sum(r => r.Field<int?>("not_update_im"));
                        row["not_target"] = g.Sum(r => r.Field<int>("not_target"));
                        row["not_target_45"] = g.Sum(r => r.Field<int?>("not_target_45"));
                        row["not_target_im"] = g.Sum(r => r.Field<int?>("not_target_im"));
                        row["success"] = g.Sum(r => r.Field<int>("success"));
                        row["dmc"] = dmc;
                        row["new_dmc"] = new_dmc;
                        row["dmc_rate"] = SolutionHelper.divide(dmc * 100.0 , total_used);
                        row["new_dmc_rate"] = SolutionHelper.divide(new_dmc * 100.0, new_used);
                        row["list_conv"] = SolutionHelper.divide(approved * 100.0 , total_lead);
                        row["respond_rate"] = SolutionHelper.divide(success * 100.0 , dmc);
                        row["new_respond_rate"] = SolutionHelper.divide(new_success * 100.0, new_dmc);
                        row["submit"] = g.Sum(r => r.Field<int?>("submit"));
                        row["sum_submit"] = g.Sum(r => r.Field<decimal?>("sum_submit"));
                        row["approved"] = approved;
                        row["sum_approved"] = g.Sum(r => r.Field<decimal?>("sum_approved"));
                        row["case_size"] = SolutionHelper.divide(sum_approved , approved);
                        row["new_approved"] = new_approved;
                        row["sumnew_approved"] = g.Sum(r => r.Field<decimal?>("sumnew_approved"));
                        row["lead_conv"] = SolutionHelper.divide(new_approved * 100.0, new_lead);

                        return row;
                    }).CopyToDataTable();
            return dt;
        }

        public DataTable loadRPT_KPI_DB_PRODUCT_ALL(string connectionString, DateTime start,string product)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PRODUCT", string.IsNullOrEmpty(product) ? null : product);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "[PROC_RPT_KPI_BY_DB_PRODUCT_ALL]", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_KPI_MediaChanel_ALL(string connectionString, DateTime start, DateTime end, string project)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            if (project.IsNullOrEmpty() == false)
            {
                param.Add("PR_PROJECT", project);
            }
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_MEDIACHANNEL_ALL", param, this._ExecutionTimeOut);
            DataTable temp = ds.Tables[0].Clone();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                temp.Merge(ds.Tables[i]);
            }
            var dt = temp.AsEnumerable()
                    .GroupBy(r => r["CreditFor"])
                    .Select(g =>
                    {
                        var row = temp.NewRow();
                        var total_lead = g.Sum(r => r.Field<int?>("total_lead"));
                        var new_lead = g.Sum(r => r.Field<int?>("new_lead"));
                        var total_used = g.Sum(r => r.Field<int?>("total_used"));
                        var new_used = g.Sum(r => r.Field<int?>("new_used"));
                        var total_call = g.Sum(r => r.Field<int?>("total_call"));
                        var success = g.Sum(r => r.Field<int?>("success"));
                        var new_success = g.Sum(r => r.Field<int?>("new_success"));
                        var dmc = g.Sum(r => r.Field<int?>("dmc"));
                        var new_dmc = g.Sum(r => r.Field<int?>("new_dmc"));
                        var approved = g.Sum(r => r.Field<int?>("approved"));
                        var sum_approved = g.Sum(r => r.Field<decimal?>("sum_approved"));
                        var new_approved = g.Sum(r => r.Field<int?>("new_approved"));
                        row["CreditFor"] = g.Key;
                        row["total_lead"] = g.Sum(r => r.Field<int>("total_lead"));
                        row["new_lead"] = g.Sum(r => r.Field<int>("new_lead"));
                        row["old_lead"] = g.Sum(r => r.Field<int>("old_lead"));
                        row["total_used"] = g.Sum(r => r.Field<int>("total_used"));
                        row["total_call"] = g.Sum(r => r.Field<int>("total_call"));
                        row["call_attempt"] = SolutionHelper.divide(total_call, total_used);
                        row["nocontact"] = g.Sum(r => r.Field<int>("nocontact"));
                        row["followup"] = g.Sum(r => r.Field<int>("followup"));
                        row["wait"] = g.Sum(r => r.Field<int>("wait"));
                        row["inprogress"] = g.Sum(r => r.Field<int>("inprogress"));
                        row["reject"] = g.Sum(r => r.Field<int>("reject"));
                        row["reject_45"] = g.Sum(r => r.Field<int?>("reject_45"));
                        row["reject_im"] = g.Sum(r => r.Field<int?>("reject_im"));
                        row["not_update"] = g.Sum(r => r.Field<int>("not_update"));
                        row["not_update_45"] = g.Sum(r => r.Field<int?>("not_update_45"));
                        row["not_update_im"] = g.Sum(r => r.Field<int?>("not_update_im"));
                        row["not_target"] = g.Sum(r => r.Field<int>("not_target"));
                        row["not_target_45"] = g.Sum(r => r.Field<int?>("not_target_45"));
                        row["not_target_im"] = g.Sum(r => r.Field<int?>("not_target_im"));
                        row["success"] = g.Sum(r => r.Field<int>("success"));
                        row["dmc"] = dmc;
                        row["new_dmc"] = new_dmc;
                        row["dmc_rate"] = SolutionHelper.divide(dmc * 100.0, total_used);
                        row["new_dmc_rate"] = SolutionHelper.divide(new_dmc * 100.0, new_used);
                        row["list_conv"] = SolutionHelper.divide(approved * 100.0, total_lead);
                        row["respond_rate"] = SolutionHelper.divide(success * 100.0, dmc);
                        row["new_respond_rate"] = SolutionHelper.divide(new_success * 100.0, new_dmc);
                        row["submit"] = g.Sum(r => r.Field<int?>("submit"));
                        row["sum_submit"] = g.Sum(r => r.Field<decimal?>("sum_submit"));
                        row["approved"] = approved;
                        row["sum_approved"] = g.Sum(r => r.Field<decimal?>("sum_approved"));
                        row["case_size"] = SolutionHelper.divide(sum_approved, approved);
                        row["new_approved"] = new_approved;
                        row["sumnew_approved"] = g.Sum(r => r.Field<decimal?>("sumnew_approved"));
                        row["lead_conv"] = SolutionHelper.divide(new_approved * 100.0, new_lead);

                        return row;
                    }).CopyToDataTable();
            return dt;
        }
        public DataTable loadRPT_KPI_Product(string connectionString, DateTime start, DateTime end, string tsr, string supervisor, bool allProject = false)
        {
            // Note : Connect and Retrieve Report by ADO
            if (!string.IsNullOrEmpty(tsr)) supervisor = null;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, (allProject ? "PROC_RPT_KPI_BY_LEADPRODUCT_ALL": "PROC_RPT_KPI_BY_LEADPRODUCT"), param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadConversion(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LeadConversion", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadRPT_LeadConversionAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            //param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            //param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LeadConversion_All", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataSet loadRPT_LeadPartner(string connectionString, DateTime current,string type,string partner)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            //param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_FROMDATE", new DateTime(current.Year, current.Month, 1).ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", current.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TYPE", type);
            param.Add("PR_PARTNER", partner);
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_PARTNER_LEAD", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_LeadAmount(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAmountAll(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_NewLeadAmount(string connectionString, DateTime current)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_NEW_LEAD_AMOUNT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_NewLeadAmountAll(string connectionString, DateTime current)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_NEW_LEAD_AMOUNT_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAmountPTD(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT_PTD_BYPRODUCT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAmountPTDAll(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT_PTD_BYPRODUCT_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadConversionPTD(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_CONV_PTD", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadConversionPTDAll(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_CONV_PTD_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAmountByProduct(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT_BYPRODUCT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAmountByProductAll(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_AMOUNT_BYPRODUCT_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_CallPerformance(string connectionString, DateTime current)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_CALL_PERFORMANCE_MTD", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_CallPerformanceAll(string connectionString, DateTime current)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_CALL_PERFORMANCE_MTD_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadAssignment(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_USAGE_ASSIGNMENT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadUsage(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_USAGE_CALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadUsageRaw(string connectionString, DateTime current)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", current.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_USAGE_RAW", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }


        // main menu
        public DataTable loadRPT_TSRSummaryReport(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_SUMMARY", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        // marketing report
        public DataTable loadRPT_LeadTracking(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_TRACKING", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadRPT_LeadTrackingAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_TRACKING_ALL", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_LeadTrackingMotor(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_TRACKING_MOTOR", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadPerformance(string connectionString, DateTime start, DateTime end, string tsr, string supervisor)
        {
            // Note : Connect and Retrieve Report by ADO
            if (!string.IsNullOrEmpty(tsr)) supervisor = null;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_BY_LEADSOURCE", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadPerformanceByProduct(string connectionString, DateTime start, DateTime end, string tsr, string supervisor, bool allProject = false,bool sum = true)
        {
            // Note : Connect and Retrieve Report by ADO
            if (!string.IsNullOrEmpty(tsr)) supervisor = null;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("TSR", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("SUP", string.IsNullOrEmpty(supervisor) ? null : supervisor);

            string procedureName = allProject ? "PROC_RPT_KPI_BY_PRODUCT_ALL" : "PROC_RPT_KPI_BY_PRODUCT";
            if (allProject)
            {
                param.Add("SUM", sum?"1":"0");
            }

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadPerformanceByMediaChannel(string connectionString, DateTime start, DateTime end, string tsr, string supervisor, bool allProject = false, bool sum = true)
        {
            // Note : Connect and Retrieve Report by ADO
            if (!string.IsNullOrEmpty(tsr)) supervisor = null;
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            string procedureName = allProject ? "[PROC_RPT_LeadPerformanceByMediaChannel_All]" : "[PROC_RPT_LeadPerformanceByMediaChannel]";
            if (allProject)
            {
                param.Add("SUM", sum ? "1" : "0");
            }

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }


        public DataTable loadRPT_LeadIntegrationReport(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_INTEGRATION", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_LeadIntegrationReportAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_INTEGRATION_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        
        public DataTable loadRPT_IncompleteDocumentReport(string connectionString)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_INCOMPLETE_DOCUMENT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        // sales report
        public DataTable loadRPT_CallTracking(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_CALL_TRACKING", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadRPT_CallTrackingAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_CALL_TRACKING_ALL", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_SuccessCallWithOutApp(string connectionString, DateTime start, DateTime end, string userid)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_USERID", userid);
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_CALL_SUCCESS_WITHOUT_APP", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_DailySalesSummary(string connectionString, DateTime start, DateTime end, string productcat, bool allproject = false)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_TODATE", end.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_PRODUCTCAT", productcat);

            string procedureName = allproject ? "PROC_RPT_DAILY_SALES_SUMMARY_ALL":  "PROC_RPT_DAILY_SALES_SUMMARY";
            
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_DailySalesSummaryMotor(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            //param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_DAILY_SALES_SUMMARY_MOTOR", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        
        public DataTable loadRPT_DailySales(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_DAILY_SALES", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_DailySalesAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_DAILY_SALES_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_FaxReport(string connectionString, DateTime start, DateTime end, bool allProject = false)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, allProject ? "PROC_RPT_FAX_ALL": "PROC_RPT_FAX", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_InboundCallReport(string connectionString, DateTime date)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_DATE", date.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_INBOUND_CALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_TeamReport(string connectionString, DateTime selecteddate, bool allProject = false)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", selecteddate.Date.ToString("yyyy-MM-dd"));

            string procedureName = allProject ? "PROC_RPT_TEAM_REPORT_ALL" : "PROC_RPT_TEAM_REPORT";
            
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        public DataTable loadRPT_TeamSubmitReport(string connectionString, DateTime selecteddate, bool allProject = false)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", selecteddate.Date.ToString("yyyy-MM-dd"));

            string procedureName = allProject ? "PROC_RPT_TEAM_REPORT_SUBMIT_ALL" : "PROC_RPT_TEAM_REPORT_SUBMIT";

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        public DataTable loadRPT_TeamTsrReport(string connectionString, DateTime selecteddate, bool allProject = false)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", selecteddate.Date.ToString("yyyy-MM-dd"));

            string procedureName = allProject ? "PROC_RPT_TEAM_REPORT_TSR_ALL" : "PROC_RPT_TEAM_REPORT_TSR";

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, procedureName, param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadRPT_SalePerLead(string connectionString, DateTime selecteddate)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", selecteddate.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_SALE_PER_LEAD_MTD", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataSet loadRPT_SalePerLeadAll(string connectionString, DateTime selecteddate)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", selecteddate.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_SALE_PER_LEAD_MTD_ALL", param, this._ExecutionTimeOut);
            return ds;
        }
        
        // not used
        public DataTable loadRPT_TsrSalesByProduct(string connectionString, DateTime start, DateTime end)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_SALES_BY_PRODUCT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }



        public int GetWorkingTsrs(string connectionString, DateTime pR_DATE, bool allProject = false)
        {
            string functionName = (allProject ? "dbo.GetWorkingTsrsAll" : "dbo.GetWorkingTsrs");
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("WorkingDate", pR_DATE.Date.ToString("yyyy-MM-dd"));
            return ADONETHelper.ExecuteScalar(connectionString, "select " + functionName + "(@WorkingDate)", param).ToString().ParseToPositiveInt();
        }
        
        public int GetSaleTsrs(string connectionString, DateTime pR_DATE, bool allProject = false)
        {
            string functionName = (allProject ? "dbo.GetSaleTsrsAll" : "dbo.GetSaleTsrs");
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("WorkingDate", pR_DATE.Date.ToString("yyyy-MM-dd"));
            return ADONETHelper.ExecuteScalar(connectionString, "select " + functionName + "(@WorkingDate)", param).ToString().ParseToPositiveInt();
        }

        public DataTable LoadDailyAFMReport(string connectionString, DateTime pR_FROMDATE, DateTime pr_TODATE)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", pR_FROMDATE.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", pr_TODATE.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_REPORT_NO", "1");
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_AFM_TFC_REPORT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        public DataTable LoadMTDAFMReport(string connectionString, DateTime pR_FROMDATE, DateTime pr_TODATE)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", pR_FROMDATE.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", pr_TODATE.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_REPORT_NO", "2");
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_AFM_TFC_REPORT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        public void FreeProcCache(string connectionString)
        {
            ADONETHelper.ExecuteNonQuery(connectionString, "PROC_FREECACHE");
        }

        public void FreeProcCacheAll(string connectionString)
        {
            ADONETHelper.ExecuteNonQuery(connectionString, "PROC_FREECACHE_ALL");
        }

        public DataTable loadSupTracking(string connectionString, DateTime start, DateTime end, bool allProject = false)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("P_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("P_TODATE", end.Date.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, allProject ? "PROC_RPT_SUPERVISOR_TRACKING_ALL" : "PROC_RPT_SUPERVISOR_TRACKING", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadSupTrackingData(string connectionString,string type, DateTime pdate, bool allProject = false)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("P_TYPE", type);
            param.Add("P_TODATE", pdate.Date.ToString("yyyy-MM-dd") + " 23:59:59");

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, allProject ? "PROC_RPT_SUPERVISOR_TRACKING_DATA_ALL": "PROC_RPT_SUPERVISOR_TRACKING_DATA", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadSupervisorTargetAll(string connectionString)
        {
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_SUPERVISOR_TARGET_ALL", new Dictionary<string, string>(), this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataSet loadLeadDuplicationReport(string connectionString, DateTime start, DateTime end, string type)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TODATE", end.Date.ToString("yyyy-MM-dd"));
            param.Add("PR_TYPE", type);
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_DUPLICATION", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataSet loadTSRTrackingReport(string connectionString, string year, string tsr,string sup)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("year", year);
            param.Add("tsr", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("sup", string.IsNullOrEmpty(sup) ? null : sup);
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_TRACKING", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataSet loadTSRTrackingReport2(string connectionString, string month, string sup)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("month", month);
            param.Add("sup", string.IsNullOrEmpty(sup) ? null : sup);
            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TSR_TRACKING2", param, this._ExecutionTimeOut);
            return ds;            
        }

        public DataTable loadTSRTargetTracking(string connectionstring, DateTime start, DateTime end)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("P_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("P_TODATE", end.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionstring, "PROC_RPT_TSR_TARGET_TRACKING", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadTSRTargetTrackingAll(string connectionstring, DateTime start, DateTime end)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("P_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("P_TODATE", end.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionstring, "PROC_RPT_TSR_TARGET_TRACKING_All", param, this._ExecutionTimeOut);
            DataTable temp = ds.Tables[0].Clone();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                temp.Merge(ds.Tables[i]);
            }
            return temp;
        }

        public DataTable loadKPINLTracking(string connectionString, DateTime start, DateTime end,string tsr, string sup)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_TODATE", end.ToString("yyyy-MM-dd HH:mm"));
            param.Add("tsr", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("sup", string.IsNullOrEmpty(sup) ? null : sup);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_NL_TRACKING", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadKPINL(string connectionString, DateTime start, DateTime end, string tsr, string sup)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_TODATE", end.ToString("yyyy-MM-dd HH:mm"));
            param.Add("tsr", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("sup", string.IsNullOrEmpty(sup) ? null : sup);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_NL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadKPINLAll(string connectionString, DateTime start, DateTime end, string tsr, string sup)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_TODATE", end.ToString("yyyy-MM-dd HH:mm"));
            param.Add("tsr", string.IsNullOrEmpty(tsr) ? null : tsr);
            param.Add("sup", string.IsNullOrEmpty(sup) ? null : sup);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_NL_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadKPILifeAll(string connectionString, DateTime start, DateTime end)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("PR_FROMDATE", start.ToString("yyyy-MM-dd HH:mm"));
            param.Add("PR_TODATE", end.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_KPI_LIFE_ALL", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadDailyKPIbyProduct(string connectionString, DateTime cdate)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDATE", cdate.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_DAILY_KPI_BY_PRODUCT", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadTargetAudienceExclusionReportData(string connectionString, string productType = "non_life")
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_product", productType);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_TARGET_AUDIENCE_EXCLUSION", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }
        
        public DataSet loadLeadSummaryPartnerReport(string connectionString, DateTime ReportDate, string partner,string product)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("CDate", ReportDate.ToString("yyyy-MM-dd"));
            param.Add("Partner", partner);
            param.Add("Product", string.IsNullOrEmpty(product) ? null : product);

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_RPT_LEAD_SUMMARY_PARTNER", param, this._ExecutionTimeOut);
            return ds;
        }

        public DataTable loadRPT_SubStatusTracking(string connectionString, DateTime startdate,DateTime enddate)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("p_fromdate", startdate.ToString("yyyy-MM-dd"));
            param.Add("p_todate", enddate.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "[PROC_RPT_SUBSTAUS_TRACKING]", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public DataTable loadRPT_SubStatusTrackingAll(string connectionString, DateTime startdate, DateTime enddate)
        {
            // Note : Connect and Retrieve Report by ADO
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("p_fromdate", startdate.ToString("yyyy-MM-dd"));
            param.Add("p_todate", enddate.ToString("yyyy-MM-dd HH:mm"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "[PROC_RPT_SUBSTAUS_TRACKING_ALL]", param, this._ExecutionTimeOut);
            return ds.Tables[0];
        }

        public List<ApplicationUser> loadActiveTSRAll(string connectionString)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_GET_ACTIVE_TSR_ALL", param, this._ExecutionTimeOut);
            
            List<ApplicationUser> tsrs = new List<ApplicationUser>();
            foreach (DataRow dr in ds.Tables[0].AsEnumerable())
            {
                ApplicationUser tsr = new ApplicationUser();
                ModelHelper.FillByDataRow(tsr, dr);
                tsrs.Add(tsr);
            }
            return tsrs;
        }

        public List<ApplicationUser> loadActiveUserAll(string connectionString)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_GET_ACTIVE_USER_ALL", param, this._ExecutionTimeOut);

            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach (DataRow dr in ds.Tables[0].AsEnumerable())
            {
                ApplicationUser user = new ApplicationUser();
                ModelHelper.FillByDataRow(user, dr);
                users.Add(user);
            }
            return users;
        }

        public List<MasterPolicy> loadPolicyAll(string connectionString, DateTime from , DateTime to)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("pr_from", from.ToString("yyyy-MM-dd"));
            param.Add("pr_to", to.ToString("yyyy-MM-dd"));

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_LIST_POLICY_ALL", param, this._ExecutionTimeOut);
            
            List<MasterPolicy> policies = new List<MasterPolicy>();
            foreach (DataRow dr in ds.Tables[0].AsEnumerable())
            {
                MasterPolicy policy = new MasterPolicy();
                ModelHelper.FillByDataRow(policy, dr);
                policy.API = dr["API"].ToString().ParseToPositiveDecimal();
                policies.Add(policy);
            }
            return policies;
        }

        public List<Product> loadProductAll(string connectionString)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();

            DataSet ds = ADONETHelper.ExecuteProcedure(connectionString, "PROC_LIST_PRODUCT_ALL", param, this._ExecutionTimeOut);

            List<Product> products = new List<Product>();
            foreach (DataRow dr in ds.Tables[0].AsEnumerable())
            {
                Product product = new Product();
                ModelHelper.FillByDataRow(product, dr);
                products.Add(product);
            }
            return products;
        }
    }
}