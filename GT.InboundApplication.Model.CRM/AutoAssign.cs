﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("AutoAssignSet")]
    public partial class AutoAssign
    {
        [Key]
        public int Id { get; set; }
        public DateTime AsOfDate { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int new_lead { get; set; }
        public int success { get; set; }
        public double lead_conv { get; set; }
        public string AutoRank { get; set; }
        public string TSRRank { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public decimal ApprovedAPE { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public decimal AvgCaseSize { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public decimal PureAnnualModeAPE { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}%")]
        public double PureAnnualModePercent { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}%")]
        public double PercentCancer { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}%")]
        public double PercentCredit { get; set; }

        public int? PolicyCase { get; set; }
        public decimal? CreditAPE { get; set; }
        public decimal? CancerAPE { get; set; }
        public decimal? HLSAPE { get; set; }
        public decimal? IPDAPE { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}%")]
        public double IORatio { get; set; }
        public int LeadA { get; set; }
        public int LeadB { get; set; }
        public int LeadC { get; set; }
        public int LeadD { get; set; }
        public int LeadE { get; set; }
        public int LeadTotal { get; set; }
        public bool IsWorking { get; set; }
        /// <summary>
        /// 0=Stop,1=Auto Rank,2=Manual Rank
        /// </summary>
        public int AutoAssignMode { get; set; }

        public string Remark { get; set; }
        public virtual AutoAssignConfig AutoAssignConfig { get; set; }

        [NotMapped]
        public int OldLead { get; set; }
        [NotMapped]
        public int NewLead { get; set; }
        [NotMapped]
        public int TotalLead { get; set; }
        [NotMapped]
        public int AutoLead { get; set; }

        public int CalLeadTotal()
        {
            return LeadA + LeadB + LeadC + LeadD + LeadE;
        }

        public void IncreaseCounter(string leadrank)
        {
            switch (leadrank)
            {
                case "A": this.LeadA++; break;
                case "B": this.LeadB++; break;
                case "C": this.LeadC++; break;
                case "D": this.LeadD++; break;
                default: this.LeadE++; break;
            }
            this.LeadTotal = this.CalLeadTotal();
        }

        public void Reset()
        {
            this.LeadA = 0;
            this.LeadB = 0;
            this.LeadC = 0;
            this.LeadD = 0;
            this.LeadE = 0;
            this.LeadTotal = 0;
            this.IsWorking = true;            
        }
    }

    [Table("AutoAssignConfigSet")]
    public partial class AutoAssignConfig
    {
        [Key]
        public string TSRRank { get; set; }
        public int RankSeq { get; set; }
        public int LeadA { get; set; }
        public int LeadB { get; set; }
        public int LeadC { get; set; }
        public int LeadD { get; set; }
        public int LeadE { get; set; }
        public int MaxLead { get; set; }
        public virtual ICollection<AutoAssign> AutoAssigns{ get; set; }

        public int CalLeadTotal()
        {
            return LeadA + LeadB + LeadC + LeadD + LeadE;
        }
    }

    public class AutoAssignConfiguration : EntityTypeConfiguration<AutoAssign>
    {
        public AutoAssignConfiguration()
        {
            this.HasRequired(t => t.AutoAssignConfig).WithMany(t => t.AutoAssigns).HasForeignKey(k => k.TSRRank);
        }
    }

    [Table("AutoAssignExcludeSourceSet")]
    public partial class AutoAssignExcludeSource
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CallSourceId { get; set; }
    }
}
