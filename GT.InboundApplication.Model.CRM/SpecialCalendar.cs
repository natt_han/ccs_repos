﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GT.InboundApplication.Model.CRM
{
    public class SpecialCalendar
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CalendarDate { get; set; }
        public string Description { get; set; }
        public int HolidayCount { get; set; }
    }
}
