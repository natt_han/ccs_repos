﻿using DM.Utilities.NetFramework.Generics;
using DM.Utilities.NetFramework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("VW_ActiveLead")]
    public class ActiveLead : LeadProfile
    {
        
        public long RecentLeadAssignmentId { get; set; }
        public string ReceiverUserId { get; set; }
        public string AssignUserId { get; set; }
        public string TSRCode { get; set; }
        public string TSRName { get; set; }
        public string TSRCode_OL { get; set; }
        public string Activity { get; set; }
        public string ActivityCode { get; set; }
        public Nullable<System.DateTime> CallDate { get; set; }
        public string ResponseCategoryCode { get; set; }
        public string ResponseCategoryName { get; set; }
        public string ResponseReasonCode { get; set; }
        public string ResponseReason { get; set; }
        public string ResponseSubReasonCode { get; set; }
        public string ReaponseSubReason { get; set; }
        public string OfferProductCode { get; set; }
        public string OfferProduct { get; set; }
        public string Detail { get; set; }
        public string AppointmentId { get; set; }
        public string AppointmentTopic { get; set; }
        public Nullable<System.DateTime> AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string AppointmentContent { get; set; }
        public string BuyForWhom { get; set; }
        public string BuyForWhomCode { get; set; }
        public string ReceiverUserName { get; set; }
        public Nullable<long> LeadAssignment_LeadAssignmentId { get; set; }
        public Nullable<int> CallCount { get; set; }
        public Nullable<System.DateTime> AssignDate { get; set; }
        public string CallBackOf { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<int> SubPriority { get; set; }
        public Nullable<int> PriorityVersion { get; set; }
        public Nullable<System.DateTime> PriorityTime { get; set; }

        public int TodayCall { get; set; }
        public Nullable<bool> HideFlag { get; set; }
       
        [NotMapped]
        public int LeadCountdown
        {
            get
            {
                return 55 - LeadAge;
            }
        }
        
        [NotMapped]
        public bool IsDMC
        {
            get
            {
                if (ResponseCategoryCode.EqualsAny("2")) return true;
                if (ResponseReasonCode.EqualsAny("74", "82")) return true;
                if (ResponseSubReasonCode.EqualsAny("96", "98", "99", "100", "101", "102")) return true;
                return false;
            }
        }        
        
    }
}
