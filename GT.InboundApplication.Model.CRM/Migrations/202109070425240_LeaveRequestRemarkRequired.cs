﻿namespace GT.InboundApplication.Model.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveRequestRemarkRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LeaveRequestSet", "Remark", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LeaveRequestSet", "Remark", c => c.String());
        }
    }
}
