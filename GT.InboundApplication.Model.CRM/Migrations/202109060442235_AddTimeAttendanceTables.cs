﻿namespace GT.InboundApplication.Model.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeAttendanceTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeaveRequestSet",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        UserId = c.String(maxLength: 128),
                        UserName = c.String(),
                        Remark = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Date, t.UserId }, unique: true, name: "IX_LeaveRequest");
            
            CreateTable(
                "dbo.SpecialCalendars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CalendarDate = c.DateTime(nullable: false, storeType: "date"),
                        Description = c.String(),
                        HolidayCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserLoggedSet",
                c => new
                    {
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoggedIn = c.DateTime(),
                        LoggedOut = c.DateTime(),
                        Remark = c.String(),
                    })
                .PrimaryKey(t => new { t.Date, t.UserId });
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.LeaveRequestSet", "IX_LeaveRequest");
            DropTable("dbo.UserLoggedSet");
            DropTable("dbo.SpecialCalendars");
            DropTable("dbo.LeaveRequestSet");
        }
    }
}
