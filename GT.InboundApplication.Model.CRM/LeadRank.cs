﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("VW_LeadConversion3M")]
    public partial class LeadConversion
    {
        [Key]
        public long CallSourceId { get; set; }
        public string LeadRank { get; set; }
    }
}
