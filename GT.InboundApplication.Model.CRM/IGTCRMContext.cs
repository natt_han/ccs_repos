﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace GT.InboundApplication.Model.CRM
{
    public partial class IGTCRMContext : DbContext
    {
        public IGTCRMContext() : base("name=IGTCRMContext") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //throw new UnintentionalCodeFirstException();
            Database.SetInitializer<IGTCRMContext>(null);
            modelBuilder.Entity<LeadProfile>()
                .ToTable("LeadProfileSet")
            .HasKey(t => t.LeadProfileId);

            //modelBuilder.Entity<LeadProfile>()
            //    .HasRequired(t => t.RecentLeadAssignment)
            //    .WithRequiredDependent(t => t.LeadProfile).Map(m=>m.MapKey("LeadProfile_LeadProfileId"));
            modelBuilder.Entity<RecentLeadAssignment>()
                .HasRequired(t => t.LeadProfile)
                .WithOptional(t => t.RecentLeadAssignment).Map(m => m.MapKey("LeadProfile_LeadProfileId"));

            modelBuilder.Entity<LeadProfile>().HasMany(t => t.LeadAssignmentSet);

            modelBuilder.Entity<RecentLeadAssignment>().ToTable("RecentLeadAssignmentSet").HasKey(k => k.RecentLeadAssignmentId);

            modelBuilder.Entity<LeadAssignment>().ToTable("LeadAssignmentSet");
            modelBuilder.Entity<LeadNotification>().ToTable("LeadNotificationSet").HasKey(k => k.ItemId);

            modelBuilder.Entity<Prospect_Profile>().ToTable("Prospect_ProfileSet").HasKey(k => k.ProspectId).HasMany(t => t.Prospect_Activity);
            modelBuilder.Entity<Prospect_Activity>().ToTable("Prospect_ActivitySet").HasKey(k => k.ItemId);
            modelBuilder.Entity<Prospect_Activity>().HasOptional(e => e.Prospect_Appointment).WithRequired(e => e.Prospect_Activity).Map(m=>m.MapKey("Prospect_Activity_ItemId"));


            modelBuilder.Entity<Prospect_PhoneNumber>().ToTable("Prospect_PhoneNumberSet").HasKey(k => k.ItemId).HasRequired(t => t.Prospect_Profile);
            //modelBuilder.Entity<Prospect_Appointment>().ToTable("Prospect_AppointmentSet").HasKey(k => k.ItemId).HasRequired(t => t.Prospect_Activity);
            modelBuilder.Entity<TelephonyResponse>().ToTable("TelephonyResponseSet").HasKey(k => k.Id);

            modelBuilder.Entity<OfferProduct>().ToTable("OfferProductSet").HasKey(k => k.OfferProductId);

            modelBuilder.Entity<ResponseCategory>().ToTable("ResponseCategorySet").HasKey(k => k.ResponseCategoryId);
            modelBuilder.Entity<ResponseReason>().ToTable("ResponseReasonSet").HasKey(k => k.ResponseReasonId);
            modelBuilder.Entity<ResponseSubReason>().ToTable("ResponseSubReasonSet").HasKey(k => k.ResponseSubReasonId);
            modelBuilder.Entity<Activity>().ToTable("ActivitySet").HasKey(k => k.ActivityId);

            modelBuilder.Configurations.Add(new AutoAssignConfiguration());
        }

        public virtual DbSet<AutoAssign> AutoAssignSet { get; set; }
        public virtual DbSet<AutoAssignConfig> AutoAssignConfigSet { get; set; }
        public virtual DbSet<AutoAssignExcludeSource> AutoAssignExcludeSourceSet { get; set; }
        
        public virtual DbSet<LeadProfile> LeadProfileSet { get; set; }
        public virtual DbSet<RecentLeadAssignment> RecentLeadAssignmentSet { get; set; }
        public virtual DbSet<LeadAssignment> LeadAssignmentSet { get; set; }
        public virtual DbSet<LeadNotification> LeadNotificationSet { get; set; }
        public virtual DbSet<Prospect_Profile> Prospect_ProfileSet { get; set; }
        //public virtual DbSet<Prospect_PhoneNumber> Prospect_PhoneNumberSet { get; set; }
        public virtual DbSet<Prospect_Activity> Prospect_ActivitySet { get; set; }
        public virtual DbSet<Prospect_Appointment> Prospect_AppointmentSet { get; set; }

        public virtual DbSet<TelephonyRequest> TelephonyRequestSet { get; set; }
        public virtual DbSet<TelephonyResponse> TelephonyResponseSet { get; set; }
        public virtual DbSet<ResponseCategory> ResponseCategorySet { get; set; }
        public virtual DbSet<ResponseReason> ResponseReasonSet { get; set; }
        public virtual DbSet<ResponseSubReason> ResponseSubReasonSet { get; set; }
        public virtual DbSet<Activity> ActivitySet { get; set; }
        //public virtual DbSet<Emotion> EmotionSet { get; set; }
        public virtual DbSet<OfferProduct> OfferProductSet { get; set; }

        public virtual DbSet<QuickResponse> QuickResponses { get; set; }
        public virtual DbSet<ActiveLead> ActiveLeadSet { get; set; }

        public virtual DbSet<ValuePreference> ValuePreferenceSet { get; set; }

        public virtual DbSet<Coverage> CoverageSet { get; set; }
        public virtual DbSet<MasterPlan> MasterPlanSet { get; set; }
        public virtual DbSet<UnsuccessLife> UnsuccessLifeSet { get; set; }
        public virtual DbSet<UnsuccessNL> UnsuccessNLSet { get; set; }

        public virtual DbSet<SpecialCalendar> SpecialCalendarSet { get; set; }
        public virtual DbSet<UserLogged> UserLoggedSet { get; set; }
        public virtual DbSet<LeaveRequest> LeaveRequestSet { get; set; }

        public bool IsWorkingDate(DateTime querydate)
        {
            DateTime pdate = querydate;
            if (pdate.DayOfWeek == DayOfWeek.Saturday || pdate.DayOfWeek == DayOfWeek.Sunday)
            {
                return (this.SpecialCalendarSet.Any(c => c.CalendarDate == pdate && c.HolidayCount == -1));
            }
            else
            {
                return !(this.SpecialCalendarSet.Any(c => c.CalendarDate == pdate));
            }
        }

        //[DbFunction("dbo", "FN_GET_WORKINGDAY")]
        public int GetWorkingDay(DateTime querydate)
        {
            //    var lObjectContext = ((IObjectContextAdapter)this).ObjectContext;
            //    var output = lObjectContext.
            //    CreateQuery<int?>("NaturalGroundingVideosModel.Store.fn_GetRatingValue(@height, @depth, @ratio)", parameters.ToArray())
            //.Execute(System.Data.Entity.Core.Objects.MergeOption.NoTracking)
            //.FirstOrDefault();
            //    return output;
            return this.Database.SqlQuery<int>("SELECT dbo.FN_GET_WORKINGDAY(@p0)", querydate).FirstOrDefault();
        }

        /// <summary>
        /// Update IsWorking=true;
        /// </summary>
        /// <param name="UserId"></param>
        public void UpdateWorkingStatus(string UserId)
        {
            var today = DateTime.Today;
            var item = this.AutoAssignSet.SingleOrDefault(c => c.AsOfDate == today && c.UserId == UserId);
            if (item != null && item.IsWorking == false)
            {
                item.IsWorking = true;
                this.SaveChanges();
            }
        }

        public IQueryable<RecentLeadAssignment> UnusedAutoAssigned(string tsrid)
        {
            var today = DateTime.Today;
            return this.RecentLeadAssignmentSet.Where(c => c.AssignDate >= today && c.ReceiverUserId == tsrid && c.AssignUserId == "AUTO" && c.CallDate == null);
        }
        public AutoAssign GetNextForAutoAssignByPerformance(string leadrank)
        {
            var today = DateTime.Today;
            var t = this.AutoAssignSet.Where(c => c.AsOfDate == today && c.AutoAssignMode != 0 && c.IsWorking && c.LeadTotal < c.AutoAssignConfig.MaxLead).ToList();//&& c.LeadTotal < c.AutoAssignConfig.MaxLead);
            AutoAssign selectedtsr = null;

            //min first
            var min = t.Where(c => c.LeadA < c.AutoAssignConfig.LeadA);
            switch (leadrank)
            {
                case "A": min = t.Where(c => c.LeadA < c.AutoAssignConfig.LeadA); break;
                case "B": min = t.Where(c => c.LeadB < c.AutoAssignConfig.LeadB); break;
                case "C": min = t.Where(c => c.LeadC < c.AutoAssignConfig.LeadC); break;
                case "D": min = t.Where(c => c.LeadD < c.AutoAssignConfig.LeadD); break;
                default: min = t.Where(c => c.LeadE < c.AutoAssignConfig.LeadE); break;
            }
            if (min.Any())
            {
                t = min.ToList();
                switch (leadrank)
                {
                    case "A": t = t.Where(c => c.AutoAssignConfig.LeadA > 0).OrderBy(o => o.LeadA).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "B": t = t.Where(c => c.AutoAssignConfig.LeadB > 0).OrderBy(o => o.LeadB).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "C": t = t.Where(c => c.AutoAssignConfig.LeadC > 0).OrderBy(o => o.LeadC).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "D": t = t.Where(c => c.AutoAssignConfig.LeadD > 0).OrderBy(o => o.LeadD).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    default: t = t.Where(c => c.AutoAssignConfig.LeadE > 0).OrderBy(o => o.LeadE).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    ////order by %ส่วนต่างจาก minlead
                    //case "A": t = t.Where(c => c.AutoAssignConfig.LeadA > 0).OrderBy(o => (o.LeadA * 1.0 / o.AutoAssignConfig.LeadA)).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    //case "B": t = t.Where(c => c.AutoAssignConfig.LeadB > 0).OrderBy(o => (o.LeadB * 1.0 / o.AutoAssignConfig.LeadB)).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    //case "C": t = t.Where(c => c.AutoAssignConfig.LeadC > 0).OrderBy(o => (o.LeadC * 1.0 / o.AutoAssignConfig.LeadC)).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    //case "D": t = t.Where(c => c.AutoAssignConfig.LeadD > 0).OrderBy(o => (o.LeadD * 1.0 / o.AutoAssignConfig.LeadD)).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    //default: t = t.Where(c => c.AutoAssignConfig.LeadE > 0).OrderBy(o => (o.LeadE * 1.0 / o.AutoAssignConfig.LeadE)).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                }
            }
            else
            {
                var unused = this.RecentLeadAssignmentSet.Where(c => c.AssignDate >= today && c.AssignUserId == "AUTO" && c.CallDate == null).GroupBy(g => g.ReceiverUserId).Select(s => new {TsrId = s.Key,count=s.Count() }).ToList();
                switch (leadrank)
                {                                                            //fill min                                                       //order by unused lead                                     //order by ส่วนต่างจาก minlead
                    case "A": t = t.Where(c => c.AutoAssignConfig.LeadA > 0).OrderBy(o => o.LeadTotal - o.AutoAssignConfig.CalLeadTotal()).ThenBy(o => (unused.SingleOrDefault(c => c.TsrId == o.UserId)?.count) ?? 0).ThenBy(o => o.LeadA - o.AutoAssignConfig.LeadA).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "B": t = t.Where(c => c.AutoAssignConfig.LeadB > 0).OrderBy(o => o.LeadTotal - o.AutoAssignConfig.CalLeadTotal()).ThenBy(o => (unused.SingleOrDefault(c => c.TsrId == o.UserId)?.count) ?? 0).ThenBy(o => o.LeadB - o.AutoAssignConfig.LeadB).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "C": t = t.Where(c => c.AutoAssignConfig.LeadC > 0).OrderBy(o => o.LeadTotal - o.AutoAssignConfig.CalLeadTotal()).ThenBy(o => (unused.SingleOrDefault(c => c.TsrId == o.UserId)?.count) ?? 0).ThenBy(o => o.LeadC - o.AutoAssignConfig.LeadC).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    case "D": t = t.Where(c => c.AutoAssignConfig.LeadD > 0).OrderBy(o => o.LeadTotal - o.AutoAssignConfig.CalLeadTotal()).ThenBy(o => (unused.SingleOrDefault(c => c.TsrId == o.UserId)?.count) ?? 0).ThenBy(o => o.LeadD - o.AutoAssignConfig.LeadD).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                    default: t = t.Where(c => c.AutoAssignConfig.LeadE > 0).OrderBy(o => o.LeadTotal - o.AutoAssignConfig.CalLeadTotal()).ThenBy(o => (unused.SingleOrDefault(c => c.TsrId == o.UserId)?.count) ?? 0).ThenBy(o => o.LeadE - o.AutoAssignConfig.LeadE).ThenBy(o => o.AutoAssignConfig.RankSeq).ThenByDescending(o => o.lead_conv).ToList(); break;
                }
            }
            selectedtsr = t.FirstOrDefault();
            return selectedtsr;
        }
    }
}
