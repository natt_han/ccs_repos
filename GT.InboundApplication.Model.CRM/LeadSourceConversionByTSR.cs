﻿using DM.Utilities.NetFramework.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("LeadSourceConversionByTSRSet")]
    public class LeadSourceConversionByTSR
    {
        [Key]
        public int Id { get; set; }
        public string ReportMonth { get; set; }
        public long CallSourceId { get; set; } 
        public string ReceiverUserId { get; set; }
	    public int? NewLead { get; set; }
	    public int? Success { get; set; }

        [NotMapped]
        public decimal Conversion { 
            get { 
                return this.NewLead>0? this.Success.Value * 1.0m / this.NewLead.Value : 0;
            } 
        }
    }
}
