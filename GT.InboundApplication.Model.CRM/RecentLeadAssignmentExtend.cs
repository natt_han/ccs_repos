﻿using System;
using System.ComponentModel.DataAnnotations;
using DM.Utilities.NetFramework.Databases.MsSQLs;
using DM.Utilities.NetFramework.DataTables;
using DM.Utilities.NetFramework.Models;
using DM.Utilities.NetFramework.Generics;


namespace GT.InboundApplication.Model.CRM
{

    public class RecentLeadAssignmentMetadata
    {
        [UIHint("SalesResponse")]
        public string ResponseCategoryCode;
    }

    [MetadataType(typeof(RecentLeadAssignmentMetadata))]
    public partial class RecentLeadAssignment
    {
        public int LeadAge
        {
            get
            {
                //return AssignDate.HasValue ? (DateTime.Today - this.AssignDate.Value.Date).Days : 0;
                return (LeadProfile!=null)?(LeadProfile.CreatedDate.HasValue ? (DateTime.Today - this.LeadProfile.CreatedDate.Value.Date).Days : 0) :0;
            }
        }

        public int Countdown
        {
            get
            {
                return (45 - LeadAge)<0?this.LeadProfile.Countdown:45-LeadAge;
            }
        }

        public bool IsDMC
        {
            get
            {
                if (ResponseCategoryCode.EqualsAny("2")) return true;
                if (ResponseReasonCode.EqualsAny("74","82")) return true;
                if (ResponseSubReasonCode.EqualsAny("96", "98","99","100","101","102")) return true;
                return false;
            }
        }
    }
}
