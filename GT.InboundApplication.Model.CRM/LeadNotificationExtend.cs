﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GT.InboundApplication.Model.Common;

namespace GT.InboundApplication.Model.CRM
{
    public partial class LeadNotification
    {
        public string Message()
        {
            if (this.NotiType == "Rebooking")
            {
                JObject detail = JObject.Parse(this.JsonData);
                var Message = string.Format("คุณ {0} ระบุเวลานัดหมายเข้ามาใหม่เป็นวันที่ {1} ช่วงเวลา {2}", this.FullName, (detail != null && detail["propose_date"] != null ? detail["propose_date"].ToString() : ""), (detail != null && detail["callbacktime"] != null ? detail["callbacktime"].ToString() : ""));
                if (detail["phonenumber"] != null)
                {
                    var phonenum = detail["phonenumber"].ToString();
                    Message += " และแจ้งเบอร์โทรศัพท์ใหม่เป็น " + phonenum.FormatPhonenumber() ;
                }
                return Message;
            }
            else if (this.NotiType == "GEBCard")
            {
                JObject detail = JObject.Parse(this.JsonData);
                var Message = string.Format("ได้รับภาพบัตรประกันกลุ่มใบคำขอ {0} แล้ว", (detail != null && detail["app_no"] != null ? detail["app_no"].ToString() : ""));
                return Message;
            }
            else
            {
                return string.Format("คุณ {0} มีลีดเข้ามาใหม่ จากช่องทาง {1}", this.FullName, this.CallSource);
            }
        }
    }
}
