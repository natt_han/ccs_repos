﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    public static class CRMExtension
    {
        public static class CallFrom
        {
            public const string MyLead = "MyLead";
            public const string HotLead = "HotLead";
            public const string Appointment = "Appointment";
            public const string NewLeadNoti = "NewLeadNoti";
            public const string Application = "Application";
        } 

        public static Prospect_Profile Search(this DbSet<Prospect_Profile> profileset, string telphone, string email, string telephone2, string telephone2_ex)
        {
            if (!String.IsNullOrEmpty(telphone))
            {
                var profile = profileset.FirstOrDefault(c => c.TelPhone == telphone.Replace(" ", ""));
                if (profile != null) return profile;
            }
            else if (!String.IsNullOrEmpty(email))
            {
                var profile = profileset.FirstOrDefault(c => c.Email == email);
                if (profile != null) return profile;
            }
            return null;
        }

        public static IQueryable<LeadProfile> Search(this IQueryable<LeadProfile> leadset, string telphone, string telephone2, string telephone2_ex)
        {
            if (!String.IsNullOrEmpty(telphone))
            {
                return leadset.Where(c => c.TelPhone == telphone);
            }
            else if (!String.IsNullOrEmpty(telephone2))
            {
                return leadset.Where(c => c.TelPhone2 == telephone2 && c.TelPhone2_Extension == telephone2_ex);                
            }
            return leadset;
        }

        public static System.Data.Entity.Infrastructure.DbSqlQuery<LeadProfile> SearchLead(this IGTCRMContext db, string telphone, string telephone2, string telephone2_ex,string productcate)
        {
            if (!String.IsNullOrEmpty(telphone))
            {
                return db.LeadProfileSet.SqlQuery("select l.* from LeadProfileSet l left join OfferProductSet p on l.ProductCode=p.OfferProductId where l.status=1 and l.LeadFlag in ('N','O') and TelPhone=@p0 and ISNULL(p.Category,'Health')=@p1", telphone, productcate);
            }
            else if (!String.IsNullOrEmpty(telephone2))
            {
                return db.LeadProfileSet.SqlQuery("select l.* from LeadProfileSet l left join OfferProductSet p on l.ProductCode=p.OfferProductId where l.status=1 and l.LeadFlag in ('N','O') and TelPhone2=@p0 and TelPhone2_Extension=@p1 and ISNULL(p.Category,'Health')=@p2", telephone2, telephone2_ex, productcate);
            }
            return null;
        }

        public static IQueryable<LeadProfile> ActiveSet(this DbSet<LeadProfile> leadset)
        {
            string[] activeFlag= {"N","O"};
            return leadset.Where(c => c.Status == true && activeFlag.Contains(c.LeadFlag));
        }

        public static IQueryable<LeadProfile> WaitingForAssign(this DbSet<LeadProfile> leadset)
        {
            return leadset.ActiveSet().Where(c => !c.LeadAssignmentSet.Any() && c.CreatedDate >= DateTime.Today);
        }

        public static IQueryable<LeadProfile> WaitingForAssign(this DbSet<LeadProfile> leadset,int days)
        {
            var datestart = DateTime.Today.AddDays(-days);
            return leadset.ActiveSet().Where(c => !c.LeadAssignmentSet.Any() && c.CreatedDate >= datestart);
        }

        public static IQueryable<LeadProfile> WaitingForContact(this DbSet<LeadProfile> leadset, string userid,int? days = null)
        {
            var datestart = days.HasValue?DateTime.Today.AddDays(-days.Value):DateTime.Today;
            return leadset.ActiveSet().Where(c => c.RecentLeadAssignment.AssignDate >= datestart && c.RecentLeadAssignment.ReceiverUserId == userid && c.RecentLeadAssignment.CallDate == null && (c.LeadFlag == "N" || c.LeadAssignmentSet.Count == 1));
        }

        public static IQueryable<ActiveLead> WaitingForContact(this DbSet<ActiveLead> leadset, string userid, int? days = null)
        {
            var datestart = days.HasValue ? DateTime.Today.AddDays(-days.Value) : DateTime.Today;
            return leadset.Where(c => c.AssignDate >= datestart && c.ReceiverUserId == userid && c.CallDate == null && c.LeadFlag == "N");
        }

        public static IQueryable<LeadProfile> LastAssignTo(this IQueryable<LeadProfile> leadset, string userid)
        {
            return leadset.Where(c => c.RecentLeadAssignment.LeadAssignment_LeadAssignmentId == 0 && c.LeadAssignmentSet.OrderByDescending(nc => nc.AssignDate).FirstOrDefault().ReceiverUserId == userid);
        }

        public static IQueryable<LeadProfile> LastAssignTo(this IQueryable<LeadProfile> leadset, string[] userids)
        {
            return leadset.Where(c => c.RecentLeadAssignment.LeadAssignment_LeadAssignmentId == 0 && userids.Contains( c.LeadAssignmentSet.OrderByDescending(nc => nc.AssignDate).FirstOrDefault().ReceiverUserId ));
        }

        public static IQueryable<LeadProfile> FilterByResponse(this IQueryable<LeadProfile> leadset, string response)
        {
            if (String.IsNullOrEmpty(response)) return leadset;
            switch (response)
            {
                case "I": return leadset.Where(c => new[] { "", "1", "5" }.Contains(c.RecentLeadAssignment.ResponseCategoryCode) );
                case "C": return leadset.Where(c => new[] { "2", "3", "4", "6" }.Contains(c.RecentLeadAssignment.ResponseCategoryCode));
                //case "1": case "2": case "3": case "4": case "5": case "6": return leadset.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == response);
                default: return leadset.Where(c => c.RecentLeadAssignment.ResponseCategoryCode == response); ;
            }
        }

        public static IQueryable<T> CreateDateInRange<T>(this IQueryable<T> leadset, string daterange) where T : LeadProfile
        {
            var dates = daterange.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
            var endDate = DateTime.Parse(dates[1] , System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
            return leadset.Where(c=>c.CreatedDate>= startDate && c.CreatedDate < endDate);
        }

        public static IQueryable<T> ExpireDateInRange<T>(this IQueryable<T> leadset, string daterange) where T : LeadProfile
        {
            var dates = daterange.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
            var endDate = DateTime.Parse(dates[1], System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
            return leadset.Where(c => c.ExpireDate >= startDate && c.ExpireDate < endDate);
        }

        public static IQueryable<T> IssueDateInRange<T>(this IQueryable<T> leadset, string daterange) where T : LeadProfile
        {
            var dates = daterange.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
            var endDate = DateTime.Parse(dates[1], System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
            return leadset.Where(c => c.IssueDate >= startDate && c.IssueDate < endDate);
        }

        public static IQueryable<LeadProfile> UpdateDateInRange(this IQueryable<LeadProfile> leadset, string daterange)
        {
            var dates = daterange.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            var startDate = DateTime.Parse(dates[0], System.Globalization.CultureInfo.CurrentCulture);
            var endDate = DateTime.Parse(dates[1], System.Globalization.CultureInfo.CurrentCulture).AddDays(1);
            return leadset.Where(c => c.UpdatedDate >= startDate && c.UpdatedDate < endDate);
        }

    }        
}
