﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.InboundApplication.Model.CRM
{
    [Table("LeaveRequestSet")]
    public class LeaveRequest
    {
        [Key]
        public int Id { get; set; }
        [Index("IX_LeaveRequest",1,IsUnique =true)]
        [Column( TypeName = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Index("IX_LeaveRequest", 2, IsUnique = true)]
        [MaxLength(128)]
        public string UserId { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Remark { get; set; }
        public int Status { get; set; }
    }
}
